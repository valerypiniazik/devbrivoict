package com.brivo.intercom.web.endpoint;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.AbstractEndpoint;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@PropertySource("classpath:application.version")
@ConfigurationProperties("classpath:application.version")
public class ApplicationVersionEndpoint extends AbstractEndpoint<Map<String, String>>
{
    private static final String ID = "version";

    @Value("${buildVersion}")
    private String version;

    @Value("${buildNumber}")
    private String buildNumber;

    @Value("${buildDate}")
    private String date;

    public ApplicationVersionEndpoint() {
        super(ID);
    }

    @Override
    public Map<String, String> invoke() {
        Map<String, String> applicationVersion = new HashMap<>();
        applicationVersion.put("version", version);
        applicationVersion.put("buildNumber", buildNumber);
        applicationVersion.put("date", date);
        return applicationVersion;
    }
}

