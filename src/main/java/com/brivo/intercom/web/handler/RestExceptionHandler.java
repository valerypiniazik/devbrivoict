package com.brivo.intercom.web.handler;

import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class RestExceptionHandler
{
    private static final String NULL = "NULL";
    private static final String CONFLICT = "Conflict";
    private static final String BAD_REQUEST = "Bad Request";

    private static final String API_GROUP_CONFIGS = "/api/group-configs/";
    private static final String API_GROUP_CONFIGS_ERROR_MESSAGE = "Group config for specified Brivo group and intercom already exists";
    private static final String API_INTERCOMS = "/api/intercoms/";
    private static final String API_INTERCOMS_ERROR_MESSAGE = "Intercom with such mac address already exists.";

    @ExceptionHandler(DataIntegrityViolationException.class)
    @ResponseBody
    public ResponseEntity<RestApiError> handle(DataIntegrityViolationException exception, HttpServletRequest request) {
        String errorMessage = ((ConstraintViolationException) exception.getCause()).getSQLException().getMessage();

        if (errorMessage.contains(NULL)) {
            String message = errorMessage.substring(0, errorMessage.indexOf(";"));

            return buildResponse(HttpStatus.BAD_REQUEST, BAD_REQUEST, message, request.getServletPath());
        }

        int detailStartIndex = errorMessage.indexOf("Detail: ");

        String message = null;
        if (API_GROUP_CONFIGS.contains(request.getServletPath())){
            message = API_GROUP_CONFIGS_ERROR_MESSAGE;
        } 
        if (API_INTERCOMS.contains(request.getServletPath())) {
            message = API_INTERCOMS_ERROR_MESSAGE;
        } 
        if (message == null){
            message = (detailStartIndex != -1)
                ? errorMessage.substring(detailStartIndex).replace("Detail: ", "")
                : errorMessage;
        }

        return buildResponse(HttpStatus.CONFLICT, CONFLICT, message, request.getServletPath());
    }

    private ResponseEntity<RestApiError> buildResponse(HttpStatus status, String error, String message, String path) {
        return ResponseEntity
                .status(status)
                .body(new RestApiError()
                        .setStatus(status.value())
                        .setError(error)
                        .setException(DataIntegrityViolationException.class.getName())
                        .setMessage(message)
                        .setPath(path));
    }

    public static class RestApiError
    {
        private Long timestamp = System.currentTimeMillis();
        private Integer status;
        private String error;
        private String exception;
        private String message;
        private String path;

        public Long getTimestamp() {
            return timestamp;
        }

        public Integer getStatus() {
            return status;
        }

        public RestApiError setStatus(Integer status) {
            this.status = status;
            return this;
        }

        public String getError() {
            return error;
        }

        public RestApiError setError(String error) {
            this.error = error;
            return this;
        }

        public String getException() {
            return exception;
        }

        public RestApiError setException(String exception) {
            this.exception = exception;
            return this;
        }

        public String getMessage() {
            return message;
        }

        public RestApiError setMessage(String message) {
            this.message = message;
            return this;
        }

        public String getPath() {
            return path;
        }

        public RestApiError setPath(String path) {
            this.path = path;
            return this;
        }
    }
}