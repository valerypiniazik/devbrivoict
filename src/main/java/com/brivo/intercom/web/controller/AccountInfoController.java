package com.brivo.intercom.web.controller;

import com.brivo.api.rest.model.account.Account;
import com.brivo.api.rest.model.identity.Administrator;
import com.brivo.intercom.aspect.annotation.ValidatedBrivoAccountId;
import com.brivo.intercom.domain.model.AdminInfo;
import com.brivo.intercom.service.AccountService;
import javax.servlet.http.Cookie;

import com.brivo.intercom.service.rest.BrivoAccountRestService;
import com.brivo.intercom.service.rest.BrivoAdministratorRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/account-info")
public class AccountInfoController
{
    private static Logger LOGGER = LoggerFactory.getLogger(AccountInfoController.class);

    private final AccountService accountService;
    private final BrivoAccountRestService brivoAccountRestService;
    private final BrivoAdministratorRestService brivoAdministratorRestService;

    public AccountInfoController(AccountService accountService, BrivoAccountRestService brivoAccountRestService, BrivoAdministratorRestService brivoAdministratorRestService) {
        this.accountService = accountService;
        this.brivoAccountRestService = brivoAccountRestService;
        this.brivoAdministratorRestService = brivoAdministratorRestService;
    }

    @PostMapping
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('INSTALLER') AND !hasRole('CSR')")
    public void updateAccountRefreshToken(@CookieValue("brivoAccountId") Cookie brivoAccountId){
        LOGGER.debug("Going to update refresh token for account {}", brivoAccountId.getValue());
        accountService.reconnect(Long.valueOf(brivoAccountId.getValue()));
    }

    @GetMapping
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('INSTALLER')")
    public ResponseEntity<Object> getRefreshTokenStatus(@CookieValue("brivoAccountId") Cookie brivoAccountId){
        LOGGER.debug("Get token status for account {}", brivoAccountId.getValue());
        return ResponseEntity.ok().body(accountService.getRefreshTokenStatus(Long.valueOf(brivoAccountId.getValue())));
    }

    @GetMapping("/status")
    @ValidatedBrivoAccountId
    public ResponseEntity<AdminInfo> getAccountStatus(@CookieValue("brivoAccountId") Cookie brivoAccountId){
        Long accountId = Long.valueOf(brivoAccountId.getValue());
        Account account = brivoAccountRestService.getAccountById(accountId);
        Administrator admin = brivoAdministratorRestService.getAdminByUsername(account.getMasterAdmin().getUsername());

        admin.setAcsAccountId(accountId);

        try {
            brivoAdministratorRestService.runAsToken(admin.getId());
            return ResponseEntity.ok(new AdminInfo(admin, true));
        } catch (Exception e ) {
            return ResponseEntity.ok(new AdminInfo(admin, false));
        }
    }

}
