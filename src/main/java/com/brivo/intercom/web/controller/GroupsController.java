package com.brivo.intercom.web.controller;

import com.brivo.api.rest.model.group.Group;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/groups")
public class GroupsController {
    private final BrivoGroupRestService brivoGroupRestService;

    public GroupsController(BrivoGroupRestService brivoGroupRestService) {
        this.brivoGroupRestService = brivoGroupRestService;
    }

    @GetMapping
    public List<Group> getGroups(@CookieValue("brivoAccountId") Cookie brivoAccountId) {
        return brivoGroupRestService.getGroups(Long.valueOf(brivoAccountId.getValue())).stream()
                .sorted(Comparator.comparing(Group::getName))
                .collect(Collectors.toList());
    }
}