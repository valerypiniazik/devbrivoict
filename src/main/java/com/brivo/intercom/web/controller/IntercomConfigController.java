package com.brivo.intercom.web.controller;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.component.generator.ConfigGeneratorHelper;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/deviceapi/config/2n")
public class IntercomConfigController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(IntercomConfigController.class);
    private static final Boolean IS_TEST = false;

    private final ConfigGeneratorHelper generatorHelper;
    private final IntercomRepository intercomRepository;

    public IntercomConfigController(ConfigGeneratorHelper generatorHelper, IntercomRepository intercomRepository) {
        this.generatorHelper = generatorHelper;
        this.intercomRepository = intercomRepository;
    }

    @RequestMapping(path = "/{fileName:.+}", method = RequestMethod.GET)
    public ResponseEntity<String> downloadConfigFile(@PathVariable String fileName, Principal principal) {
        LOGGER.info("Going to generate config file {} for intercom with mac address {}", fileName, principal.getName());
        try {
            Intercom intercom = intercomRepository.findOneByMacAddress(principal.getName()).orElseGet(() -> {
                LOGGER.debug("Wrong intercom MAC address or password", principal);
                throw new ResourceNotFoundException("Intercom with mac address " + principal.getName() + " not found");
            });

            return generatorHelper.generateConfigFileResponse(fileName, intercom, IS_TEST);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
