package com.brivo.intercom.web.controller;

import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.service.security.SecurityContextService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users")
public class UserController
{
    private final SecurityContextService securityContextService;

    public UserController(SecurityContextService securityContextService) {
        this.securityContextService = securityContextService;
    }

    @GetMapping("/current")
    public UserInfo getUserInfo() {
        return securityContextService.getCurrentlyLoggedUserInfo();
    }

}
