package com.brivo.intercom.web.controller;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.account.Account;
import com.brivo.intercom.service.rest.BrivoAccountRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/accounts")
public class AccountsController
{
    private static Logger LOGGER = LoggerFactory.getLogger(AccountsController.class);

    private final BrivoAccountRestService brivoAccountRestService;

    public AccountsController(BrivoAccountRestService brivoAccountRestService) {
        this.brivoAccountRestService = brivoAccountRestService;
    }

    @GetMapping
    public ApiPageResult<Account> getAccounts(@RequestParam(value = "accountNumber", required = false) String accountNumber,
                                              @RequestParam(value = "name", required = false) String name) {
        LOGGER.debug("Performing attempt to get accounts corresponding to filter (account number = {}, name = {})", accountNumber, name);
        return brivoAccountRestService.getAccounts(accountNumber, name);
    }
}
