package com.brivo.intercom.web.controller;

import com.brivo.intercom.aspect.annotation.ValidatedBrivoAccountId;
import com.brivo.intercom.component.generator.ConfigGeneratorHelper;
import com.brivo.intercom.component.template.ConfigFileTemplateKeys;
import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.dto.ExecutionDTO;
import com.brivo.intercom.domain.dto.IntercomDTO;
import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.domain.mapper.ExecutionMapper;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.mapper.IntercomMapper;
import com.brivo.intercom.domain.mapper.RequestLogMapper;
import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.exception.generator.ConfigFileFatalFailureException;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.service.HistoryService;
import com.brivo.intercom.service.IntercomGroupConfigService;
import com.brivo.intercom.service.IntercomService;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.HashMap;
import java.util.Map;
import javax.validation.Valid;

import com.brivo.intercom.service.rest.BrivoGroupRestService;

import java.util.Comparator;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/intercoms")
public class IntercomController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IntercomController.class);
    private static final Boolean IS_TEST = true;

    private final IntercomService intercomService;
    private final IntercomGroupConfigService intercomGroupConfigService;
    private final HistoryService historyService;
    private final BrivoGroupRestService brivoGroupRestService;
    private final IntercomMapper intercomMapper;
    private final IntercomGroupConfigMapper intercomGroupConfigMapper;
    private final RequestLogMapper requestLogMapper;
    private final ConfigGeneratorHelper generatorHelper;
    private final IntercomRepository intercomRepository;
    private final TemplateEngine templateEngine;
    private final ExecutionMapper executionMapper;

    public IntercomController(IntercomService intercomService, IntercomGroupConfigService intercomGroupConfigService, HistoryService historyService, BrivoGroupRestService brivoGroupRestService, IntercomMapper intercomMapper, IntercomGroupConfigMapper intercomGroupConfigMapper, RequestLogMapper requestLogMapper, ConfigGeneratorHelper generatorHelper, IntercomRepository intercomRepository, TemplateEngine templateEngine, ExecutionMapper executionMapper) {
        this.intercomService = intercomService;
        this.intercomGroupConfigService = intercomGroupConfigService;
        this.historyService = historyService;
        this.brivoGroupRestService = brivoGroupRestService;
        this.intercomMapper = intercomMapper;
        this.intercomGroupConfigMapper = intercomGroupConfigMapper;
        this.requestLogMapper = requestLogMapper;
        this.generatorHelper = generatorHelper;
        this.intercomRepository = intercomRepository;
        this.templateEngine = templateEngine;
        this.executionMapper = executionMapper;
    }

    @PostMapping
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity<IntercomDTO> save(@RequestBody @Valid CreateIntercomRequest request,
                                            @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Performing attempt to create intercom with mac address {}", request.getMacAddress());
        Intercom intercom = intercomService.save(request, Long.valueOf(brivoAccountId.getValue()));
        return new ResponseEntity<>(intercomMapper.intercomToIntercomDto(intercom), HttpStatus.CREATED);
    }

    @GetMapping
    @ValidatedBrivoAccountId
    public ResponseEntity<List<IntercomDTO>> getIntercoms(@CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Get all intercoms for account {}", brivoAccountId.getValue());
        return ResponseEntity.ok(intercomMapper.intercomsToIntercomDtos(intercomService.findAll(Long.valueOf(brivoAccountId.getValue()))));
    }

    @GetMapping("/{id}")
    @ValidatedBrivoAccountId
    public ResponseEntity<IntercomDTO> getIntercom(@PathVariable("id") Long id,
                                                   @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Get intercom with id {}", id);
        Optional<Intercom> intercom = intercomService.findById(id, Long.valueOf(brivoAccountId.getValue()));
        return intercom.map(currentIntercom ->
            ResponseEntity.ok(intercomMapper.intercomToIntercomDto(currentIntercom))
        ).orElseThrow(() -> new ResourceNotFoundException("Intercom with id " + id + " not found"));

    }

    @PutMapping("/{id}")
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity<IntercomDTO> update(@PathVariable("id") Long id,
                                              @RequestBody @Valid UpdateIntercomRequest intercomDto,
                                              @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Performing attempt to update intercom with id {}", id);
        Optional<Intercom> intercom = intercomService.findById(id, Long.valueOf(brivoAccountId.getValue()));

        return intercom.map(currentIntercom ->
            new ResponseEntity<>(intercomMapper.intercomToIntercomDto(intercomService.update(intercomDto, currentIntercom)), HttpStatus.OK)
        ).orElseThrow(() -> new ResourceNotFoundException("Intercom with id " + id + " not found"));
    }

    @DeleteMapping("/{id}")
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity delete(@PathVariable("id") Long id,
                                 @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Goint to delete intercom with id {}", id);
        Optional<Intercom> intercom = intercomService.findById(id, Long.valueOf(brivoAccountId.getValue()));

        return intercom.map(currentIntercom -> {
            intercomService.delete(currentIntercom);
            return ResponseEntity.noContent().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Intercom with id " + id + " not found"));
    }

    @GetMapping("/{id}/group-configs")
    @ValidatedBrivoAccountId
    public ResponseEntity<List<IntercomGroupConfigDTO>> getIntercomGroupConfigs(@PathVariable("id") Long id,
                                                                                @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Get all group configs for intercom {}", id);
        Long accountId = Long.valueOf(brivoAccountId.getValue());
        List<IntercomGroupConfig> intercomGroupConfigs = intercomGroupConfigService.findAll(id, accountId);

        return ResponseEntity.ok(intercomGroupConfigs.stream()
            .map(config -> intercomGroupConfigMapper.intercomGroupConfigToIntercomGroupConfigDto(config, accountId, brivoGroupRestService))
            .sorted(Comparator.comparing(IntercomGroupConfigDTO::getBrivoGroupName, Comparator.nullsFirst(Comparator.naturalOrder())))
            .collect(Collectors.toList()));
    }

    @GetMapping("/{id}/history")
    @ValidatedBrivoAccountId
    public ResponseEntity<Page<ExecutionDTO>> getRequestLogs(Pageable pageable,
                                                             @PathVariable("id") Long id,
                                                             @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Get history for intercom with id {}", id);
        intercomService.findById(id, Long.valueOf(brivoAccountId.getValue()))
            .orElseThrow(() -> new ResourceNotFoundException("Intercom with id " + id + " not found"));

        return ResponseEntity.ok(historyService.getHistory(pageable, id, Long.valueOf(brivoAccountId.getValue()))
            .map(execution -> executionMapper.executionToExecutionDto(execution, requestLogMapper)));
    }

    @GetMapping("/{id}/config")
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity downloadConfigFile(@PathVariable("id") Long id,
                                             @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Going to generate config file for intercom with id {}", id);
        Intercom intercom = intercomRepository.findByIdAndBrivoAccountInfoId(id, Long.valueOf(brivoAccountId.getValue())).orElseGet(() -> {
            LOGGER.debug("No intercom was found with id " + id);
            throw new ResourceNotFoundException("Intercom with id " + id + " not found");
        });

        Map<String, Object> templateKeys = new HashMap<>();
        templateKeys.put(ConfigFileTemplateKeys.MAC_ADDRESS, intercom.getMacAddress()); //TODO extend the list with more fields
        String fileName = templateEngine.compileTemplate("fileName", intercom.getType().getPattern(), templateKeys);
        try {
            return generatorHelper.generateConfigFileResponse(fileName, intercom, IS_TEST);
        } catch (ConfigFileFatalFailureException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getRequestLog());
        }
    }
}
