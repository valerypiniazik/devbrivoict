package com.brivo.intercom.web.controller;

import com.brivo.intercom.aspect.annotation.ValidatedBrivoAccountId;
import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.service.IntercomGroupConfigService;
import com.brivo.intercom.service.IntercomService;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/group-configs")
public class IntercomGroupConfigController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(IntercomGroupConfigController.class);

    private final IntercomService intercomService;
    private final IntercomGroupConfigService intercomGroupConfigService;
    private final BrivoGroupRestService brivoGroupRestService;
    private final IntercomGroupConfigMapper mapper;

    public IntercomGroupConfigController(IntercomService intercomService, IntercomGroupConfigService intercomGroupConfigService, BrivoGroupRestService brivoGroupRestService, IntercomGroupConfigMapper mapper) {
        this.intercomService = intercomService;
        this.intercomGroupConfigService = intercomGroupConfigService;
        this.brivoGroupRestService = brivoGroupRestService;
        this.mapper = mapper;
    }

    @PostMapping
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity<IntercomGroupConfigDTO> save(@RequestBody @Valid IntercomGroupConfigDTO intercomGroupConfigDto,
                                                       @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Performing attempt to create group config for intercom with id {}", intercomGroupConfigDto.getIntercomId());
        Optional<Intercom> intercom = intercomService.findById(intercomGroupConfigDto.getIntercomId(), Long.valueOf(brivoAccountId.getValue()));

        return intercom.map(currentIntercom ->
            new ResponseEntity<>(mapper.intercomGroupConfigToIntercomGroupConfigDto(intercomGroupConfigService.save(currentIntercom, intercomGroupConfigDto), brivoGroupRestService), HttpStatus.CREATED)
        ).orElse(ResponseEntity.badRequest().build());
    }

    @GetMapping("/{id}")
    @ValidatedBrivoAccountId
    public ResponseEntity<IntercomGroupConfigDTO> getIntercomGroupConfig(@PathVariable("id") Long id,
                                                                         @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Get group config with id {}", id);
        Long accountId = Long.valueOf(brivoAccountId.getValue());
        IntercomGroupConfig intercomGroupConfig = intercomGroupConfigService.findById(id, accountId);

        return intercomGroupConfig != null
            ? ResponseEntity.ok(mapper.intercomGroupConfigToIntercomGroupConfigDto(intercomGroupConfig, accountId, brivoGroupRestService))
            : ResponseEntity.notFound().build();
    }

    @PutMapping("/{id}")
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity<IntercomGroupConfigDTO> update(@PathVariable("id") Long id,
                                                         @RequestBody @Valid IntercomGroupConfigDTO intercomGroupConfigDto,
                                                         @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Performing attempt to update group config {}", id);
        IntercomGroupConfig intercomGroupConfig = intercomGroupConfigService.findById(id, Long.valueOf(brivoAccountId.getValue()));

        if (intercomGroupConfig == null) {
            LOGGER.debug("Group config with id {} not found", id);
            return ResponseEntity.notFound().build();
        }

        IntercomGroupConfig updatedIntercom = intercomGroupConfigService.update(intercomGroupConfig, intercomGroupConfigDto);

        return new ResponseEntity<>(mapper.intercomGroupConfigToIntercomGroupConfigDto(updatedIntercom, brivoGroupRestService), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ValidatedBrivoAccountId
    @PreAuthorize("!hasRole('CSR')")
    public ResponseEntity delete(@PathVariable("id") Long id,
                                 @CookieValue("brivoAccountId") Cookie brivoAccountId) {
        LOGGER.debug("Performing attempt to delete group config with id {}", id);
        IntercomGroupConfig intercomGroupConfig = intercomGroupConfigService.findById(id, Long.valueOf(brivoAccountId.getValue()));

        if (intercomGroupConfig == null) {
            LOGGER.debug("Group config with id {} not found", id);
            return ResponseEntity.notFound().build();
        }

        intercomGroupConfigService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
