package com.brivo.intercom.service.security;

import com.brivo.intercom.domain.model.UserInfo;

public interface SecurityContextService
{
    UserInfo getCurrentlyLoggedUserInfo();
}
