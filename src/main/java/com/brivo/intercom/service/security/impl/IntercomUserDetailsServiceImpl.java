package com.brivo.intercom.service.security.impl;

import com.brivo.intercom.repository.IntercomRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.Set;

@Service
public class IntercomUserDetailsServiceImpl implements UserDetailsService
{
    private static final Logger logger = LoggerFactory.getLogger(IntercomUserDetailsServiceImpl.class);
    private static final String INTERCOM_ROLE = "INTERCOM";

    private final IntercomRepository intercomRepository;
    private final OAuth2RestTemplate oAuth2RestTemplate;

    public IntercomUserDetailsServiceImpl(IntercomRepository intercomRepository, OAuth2RestTemplate oAuth2RestTemplate) {
        this.intercomRepository = intercomRepository;
        this.oAuth2RestTemplate = oAuth2RestTemplate;
    }

    @Override
    public UserDetails loadUserByUsername(String macAddress) {
        logger.debug("Intercom {} performs attempt to login", macAddress);
        logger.debug("Is cool password? {}", new BCryptPasswordEncoder().encode("12345678"));
        return intercomRepository.findOneByMacAddress(macAddress).map(intercom -> {
            logger.debug("Intercom {} from database", intercom);
            Set<GrantedAuthority> grantedAuthorities = Collections.singleton(new SimpleGrantedAuthority(INTERCOM_ROLE));
            User result = new User(intercom.getMacAddress(), intercom.getHashedPassword(), grantedAuthorities);
            logger.debug("Intercom successfully found");

            updateOAuth2ClientContext(intercom.getBrivoAccountInfo().getRefreshToken());

            return result;
        }).orElseThrow(() -> {
            logger.debug("Intercom {} not found in the database", macAddress);
            return new UsernameNotFoundException("Intercom " + macAddress + " not found in the database");
        });
    }

    private void updateOAuth2ClientContext(String refreshToken) {
        DefaultOAuth2AccessToken accessToken = new DefaultOAuth2AccessToken("invalid access token");
        accessToken.setRefreshToken(new DefaultOAuth2RefreshToken(refreshToken));
        accessToken.setExpiration(new Date());

        oAuth2RestTemplate.getOAuth2ClientContext().setAccessToken(accessToken);
    }
}
