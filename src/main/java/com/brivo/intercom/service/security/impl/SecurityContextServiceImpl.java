package com.brivo.intercom.service.security.impl;

import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.service.security.SecurityContextService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class SecurityContextServiceImpl implements SecurityContextService
{
    private final OAuth2RestTemplate oAuth2RestTemplate;

    public SecurityContextServiceImpl(OAuth2RestTemplate oAuth2RestTemplate) {
        this.oAuth2RestTemplate = oAuth2RestTemplate;
    }

    @Override
    public UserInfo getCurrentlyLoggedUserInfo() {
        OAuth2Authentication authentication = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        OAuth2AccessToken accessToken = oAuth2RestTemplate.getAccessToken();

        List<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        return new UserInfo()
                .setUsername(authentication.getName())
                .setRefreshToken(Optional.ofNullable(accessToken.getRefreshToken()).map(OAuth2RefreshToken::getValue).orElse(""))
                .setRoles(roles);
    }
}