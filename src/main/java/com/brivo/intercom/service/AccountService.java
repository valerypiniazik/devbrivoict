package com.brivo.intercom.service;

import com.brivo.intercom.domain.model.RefreshTokenStatus;

public interface AccountService
{
    void reconnect(Long brivoAccountId);

    RefreshTokenStatus getRefreshTokenStatus(Long brivoAccountId);
}
