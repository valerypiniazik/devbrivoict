package com.brivo.intercom.service;

import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.Intercom;

import java.util.List;
import java.util.Optional;

public interface IntercomService
{
    Intercom save(CreateIntercomRequest request, Long brivoAccountId);

    List<Intercom> findAll(Long brivoAccountId);

    Optional<Intercom> findById(Long id, Long brivoAccountId);

    Intercom update(UpdateIntercomRequest request, Intercom intercom);

    void delete(Intercom intercom);
}
