package com.brivo.intercom.service;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;

import java.util.List;

public interface IntercomGroupConfigService
{
    IntercomGroupConfig save(Intercom intercom, IntercomGroupConfigDTO intercomGroupConfigDto);

    List<IntercomGroupConfig> findAll(Long intercomId, Long brivoAccountId);

    IntercomGroupConfig findById(Long id, Long brivoAccountId);

    IntercomGroupConfig update(IntercomGroupConfig intercomGroupConfig, IntercomGroupConfigDTO intercomGroupConfigDto);

    void delete(Long id);
}
