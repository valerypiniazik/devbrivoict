package com.brivo.intercom.service.impl;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.model.RefreshTokenStatus;
import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.repository.BrivoAccountInfoRepository;
import com.brivo.intercom.service.security.SecurityContextService;
import com.brivo.intercom.service.AccountService;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService
{
    private static final int MILLISECONDS_IN_SECOND = 1000;

    private final SecurityContextService securityContextService;
    private final BrivoAccountInfoRepository brivoAccountInfoRepository;
    private final ObjectMapper objectMapper;

    public AccountServiceImpl(SecurityContextService securityContextService, BrivoAccountInfoRepository brivoAccountInfoRepository, ObjectMapper objectMapper) {
        this.securityContextService = securityContextService;
        this.brivoAccountInfoRepository = brivoAccountInfoRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public void reconnect(Long brivoAccountId) {
        BrivoAccountInfo brivoAccountInfo = Optional.ofNullable(brivoAccountInfoRepository.findOne(brivoAccountId))
                .orElse(new BrivoAccountInfo(brivoAccountId));

        UserInfo userInfo = securityContextService.getCurrentlyLoggedUserInfo();

        brivoAccountInfo.setUsername(userInfo.getUsername());
        brivoAccountInfo.setRefreshToken(userInfo.getRefreshToken());
        brivoAccountInfo.setRefreshTokenUpdateTime(LocalDateTime.now());

        brivoAccountInfoRepository.save(brivoAccountInfo);
    }

    @Override
    public RefreshTokenStatus getRefreshTokenStatus(Long brivoAccountId) {
        Optional<BrivoAccountInfo> brivoAccountInfo = Optional.ofNullable(brivoAccountInfoRepository.findOne(brivoAccountId));
        return brivoAccountInfo.map(accountInfo -> {
            Boolean isValid;
            try {
                Map<String, Object> claims = objectMapper.readValue(
                        JwtHelper.decode(accountInfo.getRefreshToken()).getClaims(), new TypeReference<Map<String, Object>>() {});
                Date expiration = new Date(Long.parseLong(claims.get("exp").toString()) * MILLISECONDS_IN_SECOND);
                isValid = expiration.after(new Date());
            } catch (Exception e) {
                isValid = false;
            }
            return new RefreshTokenStatus(isValid, true, accountInfo.getUsername(), accountInfo.getRefreshTokenUpdateTime());
        }).orElse(new RefreshTokenStatus(false, false, null, null));
    }
}
