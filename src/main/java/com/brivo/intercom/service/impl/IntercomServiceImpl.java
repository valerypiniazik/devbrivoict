package com.brivo.intercom.service.impl;

import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.repository.BrivoAccountInfoRepository;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.repository.rest.TypeRepository;
import com.brivo.intercom.service.security.SecurityContextService;
import com.brivo.intercom.service.IntercomService;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class IntercomServiceImpl implements IntercomService
{
    private final IntercomRepository intercomRepository;
    private final TypeRepository typeRepository;
    private final BrivoAccountInfoRepository brivoAccountInfoRepository;
    private final SecurityContextService securityContextService;

    public IntercomServiceImpl(IntercomRepository intercomRepository,
                               TypeRepository typeRepository,
                               BrivoAccountInfoRepository brivoAccountInfoRepository,
                               SecurityContextService securityContextService) {
        this.intercomRepository = intercomRepository;
        this.typeRepository = typeRepository;
        this.brivoAccountInfoRepository = brivoAccountInfoRepository;
        this.securityContextService = securityContextService;
    }

    @Override
    public List<Intercom> findAll(Long brivoAccountId) {
        return intercomRepository.findAllByBrivoAccountInfoIdOrderByName(brivoAccountId);
    }

    @Override
    public Optional<Intercom> findById(Long id, Long brivoAccountId) {
        return intercomRepository.findByIdAndBrivoAccountInfoId(id, brivoAccountId);
    }

    @Override
    public Intercom save(CreateIntercomRequest request, Long brivoAccountId) {
        Intercom intercom = new Intercom();

        intercom.setMacAddress(request.getMacAddress());
        intercom.setName(request.getName());
        intercom.setHashedPassword(request.getPassword());

        Type type = typeRepository.findOne(request.getTypeId());

        intercom.setType(type);

        UserInfo userInfo = securityContextService.getCurrentlyLoggedUserInfo();

        BrivoAccountInfo brivoAccountInfo = Optional.ofNullable(brivoAccountInfoRepository.findOne(brivoAccountId))
                .orElse(new BrivoAccountInfo(brivoAccountId));

        brivoAccountInfo.setUsername(userInfo.getUsername());
        brivoAccountInfo.setRefreshToken(userInfo.getRefreshToken());
        brivoAccountInfo.setRefreshTokenUpdateTime(LocalDateTime.now());
        intercom.setBrivoAccountInfo(brivoAccountInfoRepository.save(brivoAccountInfo));

        List<SwitchCode> newSwitchCodes = request.getSwitchCodes().stream().filter(code -> code.getCode() != null)
                .peek(code -> code.setIntercom(intercom))
                .collect(Collectors.toList());
        intercom.setSwitchCodes(newSwitchCodes);

        return intercomRepository.save(intercom);
    }

    @Override
    public Intercom update(UpdateIntercomRequest intercomDto, Intercom intercom) {
        intercom.setMacAddress(intercomDto.getMacAddress());
        intercom.setName(intercomDto.getName());
        intercom.setHashedPassword(intercomDto.getPassword());

        Type type = typeRepository.findOne(intercomDto.getTypeId());

        intercom.setType(type);

        UserInfo userInfo = securityContextService.getCurrentlyLoggedUserInfo();

        BrivoAccountInfo brivoAccountInfo = intercom.getBrivoAccountInfo();

        brivoAccountInfo.setUsername(userInfo.getUsername());
        brivoAccountInfo.setRefreshToken(userInfo.getRefreshToken());

        List<SwitchCode> updatedSwitchCodes = intercomDto.getSwitchCodes().stream()
                .filter(code -> code.getCode() != null)
                .peek(code -> code.setIntercom(intercom))
                .collect(Collectors.toList());
        intercom.getSwitchCodes().clear();
        intercom.getSwitchCodes().addAll(updatedSwitchCodes);

        return intercomRepository.save(intercom);
    }

    @Override
    public void delete(Intercom intercom) {
        intercomRepository.delete(intercom);
    }
}
