package com.brivo.intercom.service.impl;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.repository.IntercomGroupConfigRepository;
import com.brivo.intercom.service.IntercomGroupConfigService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class IntercomGroupConfigServiceImpl implements IntercomGroupConfigService {
    private final IntercomGroupConfigRepository intercomGroupConfigRepository;
    private final IntercomGroupConfigMapper mapper;

    public IntercomGroupConfigServiceImpl(IntercomGroupConfigRepository intercomGroupConfigRepository, IntercomGroupConfigMapper mapper) {
        this.intercomGroupConfigRepository = intercomGroupConfigRepository;
        this.mapper = mapper;
    }

    @Override
    @Transactional
    public IntercomGroupConfig save(Intercom intercom, IntercomGroupConfigDTO intercomGroupConfigDto) {
        IntercomGroupConfig intercomGroupConfig = mapper.intercomGroupConfigDtoToIntercomGroupConfig(intercomGroupConfigDto);

        intercomGroupConfig.setIntercom(intercom);

        return intercomGroupConfigRepository.save(intercomGroupConfig);
    }

    @Override
    public List<IntercomGroupConfig> findAll(Long intercomId, Long brivoAccountId) {
        return intercomGroupConfigRepository.findAllByIntercomIdAndIntercomBrivoAccountInfoId(intercomId, brivoAccountId);
    }

    @Override
    public IntercomGroupConfig findById(Long id, Long brivoAccountId) {
        return intercomGroupConfigRepository.findAllByIdAndIntercomBrivoAccountInfoId(id, brivoAccountId);
    }

    @Override
    public IntercomGroupConfig update(IntercomGroupConfig intercomGroupConfig, IntercomGroupConfigDTO intercomGroupConfigDto) {
        return intercomGroupConfigRepository.save(mapper.updateIntercomGroupConfig(intercomGroupConfig, intercomGroupConfigDto));
    }

    @Override
    public void delete(Long id) {
        intercomGroupConfigRepository.delete(id);
    }
}