package com.brivo.intercom.service.impl;

import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.repository.ExecutionRepository;
import com.brivo.intercom.service.HistoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class HistoryServiceImpl implements HistoryService
{
    private final ExecutionRepository executionRepository;

    public HistoryServiceImpl(ExecutionRepository executionRepository) {
        this.executionRepository = executionRepository;
    }

    @Override
    public Page<Execution> getHistory(Pageable pageable, Long intercomId, Long brivoAccountId) {
        return executionRepository.findAllByIntercomIdAndIntercomBrivoAccountInfoId(intercomId, brivoAccountId, pageable);
    }
}
