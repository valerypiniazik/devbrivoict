package com.brivo.intercom.service;

import com.brivo.intercom.domain.entity.Execution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface HistoryService
{
    Page<Execution> getHistory(Pageable pageable, Long id, Long brivoAccountId);
}
