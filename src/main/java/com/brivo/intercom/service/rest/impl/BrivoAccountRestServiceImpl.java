package com.brivo.intercom.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.account.Account;
import com.brivo.intercom.service.rest.BrivoAccountRestService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.springframework.http.HttpMethod.GET;

@Service
public class BrivoAccountRestServiceImpl extends AbstractBrivoRestService implements BrivoAccountRestService
{
    private static final int MAX_PAGE_SIZE = 100;
    private static final int ACCOUNTS_PAGE_SIZE = 50;
    private static final int CACHE_INVALIDATE_HOURS = 1;

    private final AccountCache accountCache = new AccountCache();

    public BrivoAccountRestServiceImpl(OAuth2RestTemplate oAuth2RestTemplate) {
        super(oAuth2RestTemplate);
    }

    @Override
    public ApiPageResult<Account> getAccounts(String accountNumber, String name) {
        if (isCsr() && isSearch(accountNumber, name)) {
            accountCache.LOCK.lock();
            clearCacheIfNeeded();
            if (accountCache.accounts.isEmpty()) {
                try {
                    loadAccounts(accountCache);
                } finally {
                    accountCache.LOCK.unlock();
                }
            } else {
                accountCache.LOCK.unlock();
            }

            List<Account> data = filterAccounts(accountCache.accounts, accountNumber, name);

            return new ApiPageResult<>(data.size(), data.size() > ACCOUNTS_PAGE_SIZE ? data.subList(0, ACCOUNTS_PAGE_SIZE) : data);
        } else {
            return call(GET, apiBase + "/accounts?pageSize=" + ACCOUNTS_PAGE_SIZE, new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }).getBody();
        }
    }

    @Override
    public Account getAccountById(Long brivoAccountId) {
        try {
            return call(GET, apiBase + "/accounts/" + brivoAccountId, Account.class).getBody();
        } catch (Exception ignored) {
            return null;
        }
    }

    private void loadAccounts(AccountCache accountCache) {
        ApiPageResult<Account> response = loadAccountsPage(0);
        accountCache.accounts = new ArrayList<>(response.getData());

        IntStream.range(1, (int) Math.ceil((float) response.getCount() / MAX_PAGE_SIZE))
            .forEach(pageNumber -> accountCache.accounts.addAll(loadAccountsPage(pageNumber).getData()));

        accountCache.invalidateAt = LocalDateTime.now().plusHours(CACHE_INVALIDATE_HOURS);
    }

    private boolean isSearch(String accountNumber, String name) {
        return StringUtils.isNotEmpty(accountNumber) || StringUtils.isNotEmpty(name);
    }

    private List<Account> filterAccounts(List<Account> accounts, String accountNumber, String name) {
        Stream<Account> accountStream = accounts.stream();

        if (StringUtils.isNotEmpty(accountNumber)) {
            accountStream = accountStream
                .filter(a -> StringUtils.isNotEmpty(a.getAccountNumber()))
                .filter(a -> a.getAccountNumber().contains(accountNumber));
        }

        if (StringUtils.isNotEmpty(name)) {
            accountStream = accountStream
                .filter(a -> StringUtils.isNotEmpty(a.getName()))
                .filter(a -> a.getName().contains(name));
        }

        return accountStream.collect(Collectors.toList());
    }

    private ApiPageResult<Account> loadAccountsPage(int pageNumber) {
        return call(GET, apiBase + "/accounts?offset=" + (pageNumber * MAX_PAGE_SIZE) + "&pageSize=" + MAX_PAGE_SIZE,
            new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }).getBody();
    }

    private boolean isCsr() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_CSR"));
    }

    private void clearCacheIfNeeded() {
        if (accountCache.invalidateAt != null && accountCache.invalidateAt.isBefore(LocalDateTime.now())) {
            accountCache.accounts.clear();
        }
    }

    private static class AccountCache
    {
        private List<Account> accounts = new ArrayList<>();

        private LocalDateTime invalidateAt;

        private final ReentrantLock LOCK = new ReentrantLock();
    }
}
