package com.brivo.intercom.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.service.rest.BrivoUserRestService;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static org.springframework.http.HttpMethod.GET;

@Service
public class BrivoUserRestServiceImpl extends AbstractBrivoRestService implements BrivoUserRestService
{
    private static final Integer MAX_PAGE_SIZE = 100;

    public BrivoUserRestServiceImpl(OAuth2RestTemplate oAuth2RestTemplate) {
        super(oAuth2RestTemplate);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getGroupUsers(Long groupId) {
        ApiPageResult<User> response = loadGroupUsersPage(groupId, 0);
        List<User> users = new ArrayList<>(response.getData());
        IntStream.range(1, (int) Math.ceil((float) response.getCount() / MAX_PAGE_SIZE))
                .forEach(pageNumber -> users.addAll(loadGroupUsersPage(groupId, pageNumber).getData()));
        return users;
    }

    @Override
    @SuppressWarnings("unchecked")
    public User getUserById(Long userId) {
        return call(GET, apiBase + "/users/" + userId, User.class).getBody();
    }

    @SuppressWarnings("unchecked")
    private ApiPageResult<User> loadGroupUsersPage(Long groupId, int pageNumber) {
        return call(GET, apiBase + "/groups/" + groupId + "/users?offset=" + (pageNumber * MAX_PAGE_SIZE) + "&pageSize=" + MAX_PAGE_SIZE, new ParameterizedTypeReference<ApiPageResult<User>>() {}).getBody();
    }
}
