package com.brivo.intercom.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.account.Account;
import com.brivo.api.rest.model.group.Group;
import com.brivo.api.rest.model.identity.Administrator;
import com.brivo.intercom.service.rest.BrivoAccountRestService;
import com.brivo.intercom.service.rest.BrivoAdministratorRestService;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static org.springframework.http.HttpMethod.GET;

@Service
public class BrivoGroupRestServiceImpl extends AbstractBrivoRestService implements BrivoGroupRestService
{
    private static final Integer MAX_PAGE_SIZE = 100;

    private final OAuth2RestTemplate oAuth2RestTemplate;

    private final BrivoAccountRestService brivoAccountRestService;

    private final BrivoAdministratorRestService brivoAdministratorRestService;

    public BrivoGroupRestServiceImpl(OAuth2RestTemplate oAuth2RestTemplate, BrivoAccountRestService brivoAccountRestService, BrivoAdministratorRestService brivoAdministratorRestService) {
        super(oAuth2RestTemplate);
        this.oAuth2RestTemplate = oAuth2RestTemplate;
        this.brivoAccountRestService = brivoAccountRestService;
        this.brivoAdministratorRestService = brivoAdministratorRestService;
    }

    @Override
    public Group getGroupById(Long groupId, Long brivoAccountId) {
        try {
            if (isCsr()) {
                return impersonateAndExecute(brivoAccountId, () -> loadGroup(groupId));
            } else {
                return loadGroup(groupId);
            }
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public List<Group> getGroups(Long brivoAccountId) {
        if (isCsr()) {
            return impersonateAndExecute(brivoAccountId, this::loadGroups);
        } else {
            return loadGroups();
        }
    }

    private <R> R impersonateAndExecute(Long brivoAccountId, Supplier<R> operation) {
        OAuth2AccessToken csrToken = oAuth2RestTemplate.getAccessToken();

        Account account = brivoAccountRestService.getAccountById(brivoAccountId);
        Administrator admin = brivoAdministratorRestService.getAdminByUsername(account.getMasterAdmin().getUsername());

        try {
            String adminToken = brivoAdministratorRestService.runAsToken(admin.getId());
            oAuth2RestTemplate.getOAuth2ClientContext().setAccessToken(new DefaultOAuth2AccessToken(adminToken));

            return operation.get();
        } finally {
            oAuth2RestTemplate.getOAuth2ClientContext().setAccessToken(csrToken);
        }
    }

    private Group loadGroup(Long groupId) {
        return call(GET, apiBase + "/groups/" + groupId, Group.class).getBody();
    }

    private List<Group> loadGroups() {
        ApiPageResult<Group> response = loadGroupsPage(0);
        List<Group> groups = new ArrayList<>(response.getData());
        IntStream.range(1, (int) Math.ceil((float) response.getCount() / MAX_PAGE_SIZE))
                .forEach(pageNumber -> groups.addAll(loadGroupsPage(pageNumber).getData()));
        return groups;
    }


    private ApiPageResult<Group> loadGroupsPage(int pageNumber) {
        return call(GET, apiBase + "/groups?offset=" + (pageNumber * MAX_PAGE_SIZE) + "&pageSize=" + MAX_PAGE_SIZE, new ParameterizedTypeReference<ApiPageResult<Group>>() {}).getBody();
    }

    private boolean isCsr() {
        return SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream().anyMatch(a -> a.getAuthority().equals("ROLE_CSR"));
    }
}
