package com.brivo.intercom.service.rest;

import com.brivo.api.rest.model.identity.User;

import java.util.List;

public interface BrivoUserRestService
{
  List<User> getGroupUsers(Long groupId);

  User getUserById(Long userId);
}
