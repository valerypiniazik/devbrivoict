package com.brivo.intercom.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.identity.Administrator;
import com.brivo.intercom.service.rest.BrivoAdministratorRestService;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Service
public class BrivoAdministratorRestServiceImpl extends AbstractBrivoRestService implements BrivoAdministratorRestService
{
    @Value("${security.oauth2.client.clientId}")
    private String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    private String secret;

    private ConfigProperties configProperties;

    public BrivoAdministratorRestServiceImpl(OAuth2RestTemplate oAuth2RestTemplate) {
        super(oAuth2RestTemplate);
    }

    @PostConstruct
    public void initConfigProperties() {
        configProperties = new ConfigProperties(clientId, secret);
    }

    @Override
    public Administrator getAdminByUsername(String username) {
        try {
            return call(GET,
                apiBase + "/administrators?filter=username__eq:" + username.replace(" ", "%20"),
                new ParameterizedTypeReference<ApiPageResult<Administrator>>() {}).getBody().getData().iterator().next();
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public String runAsToken(Long adminId) {
        return call(POST, configProperties, apiBase + "/administrators/" + adminId + "/run-as-token", JsonNode.class).getBody().get("access_token").asText();
    }

    public static class ConfigProperties {

        private String clientId;

        private String secret;

        ConfigProperties(String clientId, String secret) {
            this.clientId = clientId;
            this.secret = secret;
        }

        public String getClientId() {
            return clientId;
        }

        public String getSecret() {
            return secret;
        }
    }
}
