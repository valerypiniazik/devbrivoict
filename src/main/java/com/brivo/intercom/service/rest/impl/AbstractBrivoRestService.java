package com.brivo.intercom.service.rest.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;

import java.util.Collections;

abstract class AbstractBrivoRestService extends AbstractRestService {
    private static final String API_KEY_HEADER = "api-key";

    @Value("${brivo.api.key}")
    String apiKey;

    @Value("${brivo.api.base}")
    String apiBase;

    public final HttpHeaders brivoHttpHeaders;

    AbstractBrivoRestService(OAuth2RestTemplate oAuth2RestTemplate) {
        super(oAuth2RestTemplate);
        brivoHttpHeaders = buildHttpHeaders();
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, ParameterizedTypeReference<RES> responseType) {
        return call(method, url, responseType, brivoHttpHeaders);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, Class<RES> responseType) {
        return call(method, url, responseType, brivoHttpHeaders);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, REQ request, String url, Class<RES> responseType) {
        return call(method, url, request, responseType, brivoHttpHeaders);
    }

    private HttpHeaders buildHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add(API_KEY_HEADER, apiKey);
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        return headers;
    }
}
