package com.brivo.intercom.service.rest.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

abstract class AbstractRestService {
    private static Logger logger = LoggerFactory.getLogger(AbstractRestService.class);

    private RestTemplate restTemplate;

    AbstractRestService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, Class<RES> responseType) {
        return call(method, url, null, responseType);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, ParameterizedTypeReference<RES> responseType) {
        return call(method, url, null, responseType);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, String uriVariableName, Object uriVariableValue,
                                             Class<RES> responseType) {
        return call(method, url, singletonMap(uriVariableName, uriVariableValue), responseType);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, Map<String, ?> uriVariables,
                                             ParameterizedTypeReference<RES> responseType) {
        return call(method, url, null, uriVariables, responseType);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request,
                                                  ParameterizedTypeReference<RES> responseType) {
        return call(method, url, request, null, responseType);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, Map<String, ?> uriVariables,
                                                  Class<RES> responseType) {
        return call(method, url, (REQ) null, uriVariables, responseType);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request, Class<RES> responseType) {
        return call(method, url, request, emptyMap(), responseType);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, ParameterizedTypeReference<RES> responseType, HttpHeaders httpHeaders) {
        return callInternal(method, url, null, null, responseType, httpHeaders);
    }

    protected <RES> ResponseEntity<RES> call(HttpMethod method, String url, Class<RES> responseType, HttpHeaders httpHeaders) {
        return callInternal(method, url, null, null, responseType, httpHeaders);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request, Class<RES> responseType,
                                                  HttpHeaders httpHeaders) {
        return callInternal(method, url, request, null, responseType, httpHeaders);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request, Map<String, ?> uriVariables,
                                                  Class<RES> responseType) {
        return callInternal(method, url, request, uriVariables, responseType, null);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request, Map<String, ?> uriVariables,
                                                  ParameterizedTypeReference<RES> responseType) {
        return callInternal(method, url, request, uriVariables, responseType, null);
    }

    protected <REQ, RES> ResponseEntity<RES> call(HttpMethod method, String url, REQ request, Map<String, ?> uriVariables,
                                                  ParameterizedTypeReference<RES> responseType, HttpHeaders httpHeaders) {
        return callInternal(method, url, request, uriVariables, responseType, httpHeaders);
    }

    @SuppressWarnings("unchecked")
    protected <REQ, RES> ResponseEntity<RES> callInternal(HttpMethod method, String url, REQ request, Map<String, ?> uriVariables,
                                                          Object responseType, HttpHeaders httpHeaders) {
        ResponseEntity<RES> restResponse;

        HttpEntity<REQ> restRequest = request != null ?
                httpHeaders != null ?
                        new HttpEntity<>(request, httpHeaders) :
                        new HttpEntity<>(request) :
                httpHeaders != null ? new HttpEntity<>(httpHeaders) : null;

        try {
            restResponse = responseType instanceof ParameterizedTypeReference ?
                    uriVariables != null ?
                            restTemplate.exchange(url, method, restRequest, (ParameterizedTypeReference<RES>) responseType, uriVariables) :
                            restTemplate.exchange(url, method, restRequest, (ParameterizedTypeReference<RES>) responseType) :
                    uriVariables != null ?
                            restTemplate.exchange(url, method, restRequest, (Class<RES>) responseType, uriVariables) :
                            restTemplate.exchange(url, method, restRequest, (Class<RES>) responseType);
            logger.debug("Received successfully response from {}", url);
        } catch (ResourceAccessException e) {
            logger.error("REST service connection error for url {}", url, e);
            throw new RuntimeException("REST service connection error", e);
        } catch (Exception e) {
            logger.error("Received failed response from {}", url, e);
            throw new RuntimeException("REST service error", e);
        }

        return restResponse;
    }
}