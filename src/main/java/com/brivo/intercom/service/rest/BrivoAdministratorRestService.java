package com.brivo.intercom.service.rest;

import com.brivo.api.rest.model.identity.Administrator;

public interface BrivoAdministratorRestService
{
    Administrator getAdminByUsername(String username);

    String runAsToken(Long adminId);
}
