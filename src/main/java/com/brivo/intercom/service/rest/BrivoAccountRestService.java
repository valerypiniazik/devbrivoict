package com.brivo.intercom.service.rest;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.account.Account;

public interface BrivoAccountRestService
{
    ApiPageResult<Account> getAccounts(String accountNumber, String name);

    Account getAccountById(Long brivoAccountId);
}
