package com.brivo.intercom.service.rest;

import com.brivo.api.rest.model.group.Group;

import java.util.List;

public interface BrivoGroupRestService
{
    Group getGroupById(Long groupId, Long brivoAccountId);

    List<Group> getGroups(Long brivoAccountId);
}