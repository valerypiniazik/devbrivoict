package com.brivo.intercom.aspect;

import com.brivo.intercom.service.rest.BrivoAccountRestService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import java.util.Optional;
import java.util.stream.Stream;

@Aspect
@Component
public class BrivoAccountIdValidationAspect
{
    private BrivoAccountRestService brivoRestService;

    public BrivoAccountIdValidationAspect(BrivoAccountRestService brivoRestService) {
        this.brivoRestService = brivoRestService;
    }

    @Around("@annotation(com.brivo.intercom.aspect.annotation.ValidatedBrivoAccountId)")
    public Object validateBrivoAccountId(ProceedingJoinPoint joinPoint) throws Throwable {
        Optional<Cookie> cookie = Stream.of(joinPoint.getArgs())
                .filter(arg -> arg instanceof Cookie)
                .map(arg -> (Cookie) arg)
                .filter(arg -> arg.getName().equals("brivoAccountId"))
                .findFirst();

        if (!cookie.isPresent()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        Long brivoAccountId;

        try {
            brivoAccountId = Long.parseLong(cookie.get().getValue());
        } catch (NumberFormatException e) {
            return ResponseEntity.badRequest();
        }

        boolean userHasAccountId = brivoRestService.getAccountById(brivoAccountId) != null;

        return userHasAccountId ? joinPoint.proceed() : ResponseEntity.status(HttpStatus.FORBIDDEN).build();
    }
}