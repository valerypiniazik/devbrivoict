package com.brivo.intercom.exception.generator;

import com.brivo.intercom.domain.dto.RequestLogDTO;

public class ConfigFileFatalFailureException extends RuntimeException
{
    private RequestLogDTO requestLog;

    public ConfigFileFatalFailureException(RequestLogDTO requestLog){
        this.requestLog = requestLog;
    }

    public RequestLogDTO getRequestLog() {
        return requestLog;
    }
}
