package com.brivo.intercom.exception.service;

import com.brivo.api.rest.model.identity.Administrator;

public class InvalidAdminException extends RuntimeException
{
    private Administrator admin;

    public InvalidAdminException(Administrator admin){
        this.admin = admin;
    }

    public Administrator getAdmin() {
        return admin;
    }
}
