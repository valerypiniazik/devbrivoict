package com.brivo.intercom.component.template;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import java.io.StringWriter;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TemplateEngine
{
    private static final Logger logger = LoggerFactory.getLogger(TemplateEngine.class);

    public String compileTemplate(String templateName, String templateBody, Map<String, Object> values) {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_28);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        cfg.setDefaultEncoding("UTF-8");

        StringTemplateLoader stringLoader = new StringTemplateLoader();
        stringLoader.putTemplate(templateName, templateBody);
        try {
            cfg.setTemplateLoader(stringLoader);
            Template template = cfg.getTemplate(templateName);
            StringWriter output = new StringWriter();
            template.process(values, output);
            return output.toString();
        } catch (Exception e) {
            logger.error("Unable to process template {} due to {} ", templateName, e);
            throw new RuntimeException(e);
        }
    }
}
