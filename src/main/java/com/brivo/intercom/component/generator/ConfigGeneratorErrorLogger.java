package com.brivo.intercom.component.generator;

import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.model.generator.ErrorSources;
import com.brivo.intercom.domain.mapper.RequestLogMapper;
import com.brivo.intercom.domain.entity.ErrorType;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.RequestLog;
import com.brivo.intercom.exception.generator.ConfigFileFatalFailureException;
import com.brivo.intercom.repository.ExecutionRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.brivo.intercom.service.rest.BrivoGroupRestService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static com.brivo.intercom.component.generator.ConfigGenerator.DEFAULT_MAX_NUMBER_OF_USERS_VALUE;
import static com.brivo.intercom.component.generator.ConfigGenerator.DEFAULT_MAX_SHORT_NAME_LENGTH_VALUE;
import static com.brivo.intercom.component.generator.ConfigGenerator.DEFAULT_WARNINGS_LIMIT_VALUE;
import static com.brivo.intercom.component.generator.ConfigGenerator.MAX_NUMBER_OF_USERS;
import static com.brivo.intercom.component.generator.ConfigGenerator.MAX_SHORT_NAME_LENGTH;
import static com.brivo.intercom.component.generator.ConfigGenerator.WARNINGS_LIMIT;

@Component
public class ConfigGeneratorErrorLogger
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigGeneratorErrorLogger.class);

    private static final String SUCCESS_MESSAGE = "Configuration file generation finished successfully for intercom with id ";

    public static final String RETRIEVE_USER_ERROR = "RETRIEVE_USER_ERROR";
    public static final String RETRIEVE_GROUP_USERS_ERROR = "RETRIEVE_GROUP_USERS_ERROR";
    public static final String INVALID_VARIABLE_NAMES_IN_FORMAT_STRING = "INVALID_VARIABLE_NAMES_IN_FORMAT_STRING";
    public static final String UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING = "UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING";
    public static final String APPLICATION_ERROR = "APPLICATION_ERROR";
    public static final String TOO_MANY_WARNINGS = "TOO_MANY_WARNINGS";

    public static final String MISSING_VALUE_FOR_FORMAT_STRING = "MISSING_VALUE_FOR_FORMAT_STRING";
    public static final String NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND = "NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND";
    public static final String USER_LIMIT_EXCEEDED = "USER_LIMIT_EXCEEDED";
    public static final String DISPLAY_NAME_EXCEEDED_LENGTH_LIMIT = "DISPLAY_NAME_EXCEEDED_MAX_LENGTH";

    private static final String GROUP_NAME = "group_name";
    private static final String GROUP_ID = "group_id";
    private static final String USER_ID = "user_id";
    private static final String USER_USERNAME = "username";
    private static final String USERS_LIMIT = "users_limit";
    private static final String DISPLAY_NAME_LIMIT = "display_name_limit";

    private static final String EXECUTION_STATUS_SUCCESS = "OK";
    private static final String EXECUTION_STATUS_WARNING = "Warning";
    private static final String EXECUTION_STATUS_ERROR = "Error";
    final static String EXECUTION_STATUS_PENDING = "Pending";

    private final RequestLogMapper requestLogMapper;
    private final ExecutionRepository executionRepository;
    private final BrivoGroupRestService brivoGroupRestService;
    private final IntercomGroupConfigMapper intercomGroupConfigMapper;

    public ConfigGeneratorErrorLogger(RequestLogMapper requestLogMapper,
                                      ExecutionRepository executionRepository,
                                      BrivoGroupRestService brivoGroupRestService,
                                      IntercomGroupConfigMapper intercomGroupConfigMapper) {
        this.requestLogMapper = requestLogMapper;
        this.executionRepository = executionRepository;
        this.brivoGroupRestService = brivoGroupRestService;
        this.intercomGroupConfigMapper = intercomGroupConfigMapper;
    }

    public void trackError(Map<String, ErrorType> types, String name, ErrorSources sources, Execution ex){
        ErrorType errorType = types.get(name);
        Map<String, String> descriptionSources = generateDescriptionSources(sources, ex);

        if (Optional.ofNullable(errorType.getFailureFatal()).orElse(false)) {
            LOGGER.error(name, sources.getIntercom(), errorType);
            RequestLog requestLog = buildRequest(errorType, descriptionSources, ex);
            saveExecution(ex, EXECUTION_STATUS_ERROR);
            throw new ConfigFileFatalFailureException(requestLogMapper.requestLogToRequestLogDto(requestLog));
        } else {
            LOGGER.warn(name, sources.getIntercom(), errorType);

            int warningsLimit = Integer.valueOf(
                ex.getIntercom().getType().getConfig().getOrDefault(WARNINGS_LIMIT, DEFAULT_WARNINGS_LIMIT_VALUE));

            if (ex.getRequestLogs().size() <= warningsLimit) {
                buildRequest(errorType, descriptionSources, ex);
            }

            if (ex.getRequestLogs().size() == warningsLimit) {
                trackError(types, TOO_MANY_WARNINGS, sources, ex);
            }
        }
    }

    public void logSuccess(Execution ex) {
        LOGGER.debug(SUCCESS_MESSAGE, ex);
        buildRequest(null, Collections.emptyMap(), ex);
        saveExecution(ex, ex.getRequestLogs().size() > 1 ? EXECUTION_STATUS_WARNING : EXECUTION_STATUS_SUCCESS);
    }

    private RequestLog buildRequest(ErrorType errorType, Map<String, String> description, Execution execution) {
        RequestLog requestLog = new RequestLog(execution, description, errorType);
        execution.getRequestLogs().add(requestLog);
        return requestLog;
    }

    private Map<String, String> generateDescriptionSources(ErrorSources sources, Execution ex){
        Map<String, String> templateKeys = initTemplateKeys(ex.getIntercom().getType().getConfig());

        User user = sources.getUser();
        IntercomGroupConfigDTO groupConfigDTO = intercomGroupConfigMapper
            .intercomGroupConfigToIntercomGroupConfigDto(sources.getIntercomGroupConfig(), brivoGroupRestService);

        if (groupConfigDTO != null) {
            templateKeys.put(GROUP_ID, groupConfigDTO.getBrivoGroupId().toString());
            templateKeys.put(GROUP_NAME, Optional.ofNullable(groupConfigDTO.getBrivoGroupName()).orElse(""));
        }

        if (sources.getUser() != null) {
            String username = Stream.of(user.getFirstName(), user.getMiddleName(), user.getLastName())
                .filter(Objects::nonNull).collect(Collectors.joining(" "));
            templateKeys.put(USER_ID, sources.getUser().getId().toString());
            templateKeys.put(USER_USERNAME, username);
        }

        return templateKeys;
    }

    private Map<String, String> initTemplateKeys(Map<String, String> config) {
        Map<String, String> templateKeys = Stream.of(GROUP_ID, GROUP_NAME, USER_USERNAME)
                .collect(Collectors.toMap(k -> k, k -> StringUtils.EMPTY));

        templateKeys.put(USERS_LIMIT, config.getOrDefault(MAX_NUMBER_OF_USERS, DEFAULT_MAX_NUMBER_OF_USERS_VALUE));
        templateKeys.put(DISPLAY_NAME_LIMIT, config.getOrDefault(MAX_SHORT_NAME_LENGTH, DEFAULT_MAX_SHORT_NAME_LENGTH_VALUE));

        return templateKeys;
    }

    private void saveExecution(Execution execution, String executionStatus) {
        execution.setStatus(executionStatus);
        execution.setFinishedAt(LocalDateTime.now());
        executionRepository.save(execution);
    }
}
