package com.brivo.intercom.component.generator;

import com.brivo.api.rest.model.identity.CustomFieldValue;
import com.brivo.api.rest.model.identity.Telephone;
import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.entity.ErrorType;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.domain.entity.IntercomGroupConfig.IntercomGroupContactConfig;
import com.brivo.intercom.domain.model.generator.Code;
import com.brivo.intercom.domain.model.generator.ConfigFile;
import com.brivo.intercom.domain.model.generator.ErrorSources;
import com.brivo.intercom.domain.model.generator.Number;
import com.brivo.intercom.domain.model.generator.Switch;
import com.brivo.intercom.domain.model.generator.UserModel;
import com.brivo.intercom.service.rest.BrivoUserRestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.DISPLAY_NAME_EXCEEDED_LENGTH_LIMIT;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.INVALID_VARIABLE_NAMES_IN_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.MISSING_VALUE_FOR_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.RETRIEVE_GROUP_USERS_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.RETRIEVE_USER_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.USER_LIMIT_EXCEEDED;

@Component
public class ConfigGenerator
{
    private static Logger LOGGER = LoggerFactory.getLogger(ConfigGenerator.class);

    private static final Integer SWITCH_ACCESS_MODE = 1;
    private static final Integer SWITCH_ENABLED = 1;
    private static final Integer NUMBERS_NUMBER = 3;
    static final String DEFAULT_MAX_NUMBER_OF_USERS_VALUE = "1999";
    static final String DEFAULT_MAX_SHORT_NAME_LENGTH_VALUE = "63";
    static final String DEFAULT_WARNINGS_LIMIT_VALUE = "25";
    private static final String LAST_NAME = "lastName";
    private static final String FIRST_NAME = "firstName";
    private static final String MIDDLE_NAME = "middleName";
    private static final String TEMPLATE_KEY = "shortName";
    private static final String CUSTOM_FIELD_PREFIX = "c_";
    private static final String DEFAULT_TREE_PATH = "/";

    static final String MAX_NUMBER_OF_USERS = "users_limit";
    static final String MAX_SHORT_NAME_LENGTH = "display_name_limit";
    static final String WARNINGS_LIMIT = "warnings_limit";

    private final TemplateEngine templateEngine;
    private final BrivoUserRestService brivoRestService;
    private final ConfigGeneratorErrorLogger configGeneratorErrorLogger;

    public ConfigGenerator(TemplateEngine templateEngine,
                           BrivoUserRestService brivoRestService,
                           ConfigGeneratorErrorLogger configGeneratorErrorLogger) {
        this.templateEngine = templateEngine;
        this.brivoRestService = brivoRestService;
        this.configGeneratorErrorLogger = configGeneratorErrorLogger;
    }

    public ConfigFile generateConfig(Intercom intercom, Map<String, ErrorType> errorTypes, ErrorSources errorMessage,
                                     String fileName, Execution execution) {
        List<UserModel> users = generateUsersInfo(intercom, errorTypes, errorMessage, execution);
        ConfigFile configFile = generateFileBody(fileName, users, intercom);
        configFile.setFileName(fileName);
        return configFile;
    }

    private List<User> fetchGroupUsers(IntercomGroupConfig intercomGroupConfig) {
        return brivoRestService.getGroupUsers(intercomGroupConfig.getBrivoGroupId());
    }

    private List<UserModel> generateUsersInfo(Intercom intercom, Map<String, ErrorType> errTypes, ErrorSources errSources, Execution ex) {
        List<UserModel> users = new ArrayList<>();
        List<User> response;
        for (IntercomGroupConfig intercomGroupConfig : intercom.getIntercomGroupConfigs()) {
            errSources.setIntercomGroupConfig(intercomGroupConfig);
            try {
                LOGGER.info("Going to fetch all users from group {}", intercomGroupConfig.getBrivoGroupId());
                response = fetchGroupUsers(intercomGroupConfig);
            } catch (RuntimeException e) {
                configGeneratorErrorLogger.trackError(errTypes, RETRIEVE_GROUP_USERS_ERROR, errSources, ex);
                response = Collections.emptyList();
            }

            int maxNumberOfUsers = Integer.valueOf(
                ex.getIntercom().getType().getConfig().getOrDefault(MAX_NUMBER_OF_USERS, DEFAULT_MAX_NUMBER_OF_USERS_VALUE));

            //Filter users after conversion to DTO since some users might be filtered out during the process
            if (maxNumberOfUsers - users.size() > response.size()) {
                users.addAll(buildUsersDTO(response, intercomGroupConfig, errSources, errTypes, ex));
            } else {
                configGeneratorErrorLogger.trackError(errTypes, USER_LIMIT_EXCEEDED, errSources, ex);
                users.addAll(buildUsersDTO(response.subList(0, maxNumberOfUsers - users.size()), intercomGroupConfig, errSources, errTypes, ex));
                break;
            }
        }
        users.sort(Comparator.comparing(UserModel::getShortName, String.CASE_INSENSITIVE_ORDER));

        IntStream.range(0, users.size()).forEach(index -> users.get(index).setUserNumber(index));

        return users;
    }

    private List<UserModel> buildUsersDTO(Collection<User> users,
                                          IntercomGroupConfig intercomGroupConfig,
                                          ErrorSources errorMessage,
                                          Map<String, ErrorType> errorTypes,
                                          Execution execution) {
        List<IntercomGroupConfig.IntercomGroupContactConfig> intercomGroupContactConfigs = intercomGroupConfig.getIntercomGroupContactConfigs();
        intercomGroupContactConfigs.sort(Comparator.comparing(IntercomGroupContactConfig::getIndex));

        LOGGER.info("Going to fetch users for config file with username based on template {}", intercomGroupConfig.getFormatString());
        return users.stream()
            .filter(user -> !user.getSuspended())
            .map(user -> {
                User userData;

                LOGGER.info("Going to fetch user {}", user.getId());
                try {
                    userData = brivoRestService.getUserById(user.getId());
                } catch (RuntimeException e) {
                    configGeneratorErrorLogger.trackError(errorTypes, RETRIEVE_USER_ERROR, errorMessage, execution);
                    userData = user;
                }

                    errorMessage.setUser(user);
                    List<Number> phoneNumbers = buildPhoneNumbers(userData, intercomGroupConfig.getIntercomGroupContactConfigs(), errorTypes, errorMessage, execution);
                    String shortName = generateShortName(userData, intercomGroupConfig.getFormatString(), errorTypes, errorMessage, execution);

                    return new UserModel(shortName, DEFAULT_TREE_PATH, phoneNumbers);
                })
                .filter(user -> !user.getNumbers().isEmpty())
                .collect(Collectors.toList());
    }

    private String generateShortName(User user, String template, Map<String, ErrorType> errorTypes,
                                     ErrorSources errorMessage,
                                     Execution execution) {
        Map<String, Object> templateKeys = new HashMap<>();
        templateKeys.put(FIRST_NAME, user.getFirstName());
        templateKeys.put(MIDDLE_NAME, user.getMiddleName());
        templateKeys.put(LAST_NAME, user.getLastName());

        for (CustomFieldValue customField : user.getCustomFields()) {
            templateKeys.put(CUSTOM_FIELD_PREFIX + customField.getFieldName(), customField.getValue());
        }

        boolean hasMissingValues = templateKeys.entrySet().stream().anyMatch(e -> template.contains(e.getKey()) && e.getValue() == null);

        if (hasMissingValues) {
            configGeneratorErrorLogger.trackError(errorTypes, MISSING_VALUE_FOR_FORMAT_STRING, errorMessage, execution);
        }

        templateKeys.entrySet().stream().filter(e -> e.getValue() == null).forEach(e -> e.setValue(""));

        String userShortName;
        try {
            template.getBytes("UTF-8");
            LOGGER.info("Going to generate username");
            userShortName = templateEngine.compileTemplate(TEMPLATE_KEY, template, templateKeys);
        } catch (UnsupportedEncodingException e) {
            configGeneratorErrorLogger.trackError(errorTypes, UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING, errorMessage, execution);
            userShortName = template;
        } catch (RuntimeException e) {
            configGeneratorErrorLogger.trackError(errorTypes, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, errorMessage, execution);
            userShortName = template;
        }

        int maxShortNameLength = Integer.valueOf(
            execution.getIntercom().getType().getConfig().getOrDefault(MAX_SHORT_NAME_LENGTH, DEFAULT_MAX_SHORT_NAME_LENGTH_VALUE));

        if (userShortName.length() >= maxShortNameLength) {
            configGeneratorErrorLogger.trackError(errorTypes, DISPLAY_NAME_EXCEEDED_LENGTH_LIMIT, errorMessage, execution);
        }
        return userShortName;
    }

    private ConfigFile generateFileBody(String fileName, List<UserModel> users, Intercom intercom) {
        return new ConfigFile(fileName, generateSwitches(intercom), users);
    }

    private List<Number> buildPhoneNumbers(User user,
                                               Collection<IntercomGroupContactConfig> sortedContactTypes,
                                               Map<String, ErrorType> errorTypes,
                                               ErrorSources errorMessage,
                                               Execution execution) {
        List<Telephone> displayedPhoneNumbers = getPhoneNumbersSortedByType(user.getPhoneNumbers(), sortedContactTypes);

        if (displayedPhoneNumbers.isEmpty()) {
            configGeneratorErrorLogger.trackError(errorTypes, NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND, errorMessage, execution);
        }

        return  IntStream.range(0, Math.min(displayedPhoneNumbers.size(), NUMBERS_NUMBER))
                .mapToObj(index -> new Number(index, removeNonNumericSymbols(displayedPhoneNumbers.get(index).getNumber())))
                .collect(Collectors.toList());
    }

    private String removeNonNumericSymbols(String str){
        return str.replaceAll("[^\\d]", "");
    }

    private List<Telephone> getPhoneNumbersSortedByType(List<Telephone> allNumbers,
                                                        Collection<IntercomGroupContactConfig> sortedContactTypes) {
        List<Telephone> matchingNumbers = new ArrayList<>();
        sortedContactTypes
            .forEach(type -> allNumbers.stream().filter(number -> type.getBrivoContactType().equals(number.getType())).findFirst()
                .ifPresent(matchingNumbers::add));
        return matchingNumbers;
    }

    private List<Switch> generateSwitches(Intercom intercom) {
        return intercom.getSwitchCodes().stream()
            .map(switchCode ->
                new Switch(switchCode.getIndex(), SWITCH_ENABLED, Collections.singletonList(new Code(0, switchCode.getCode(), SWITCH_ACCESS_MODE))))
            .collect(Collectors.toList());
    }
}
