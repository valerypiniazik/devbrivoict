package com.brivo.intercom.component.generator;

import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.entity.ConfigurationFileVersion;
import com.brivo.intercom.domain.entity.ErrorType;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.model.generator.ConfigFile;
import com.brivo.intercom.domain.model.generator.ErrorSources;
import com.brivo.intercom.repository.ErrorTypeRepository;
import com.brivo.intercom.repository.ExecutionRepository;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.APPLICATION_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.EXECUTION_STATUS_PENDING;

@Component
public class ConfigGeneratorHelper {
    private static Logger LOGGER = LoggerFactory.getLogger(ConfigGeneratorHelper.class);

    private final static String FILE_GENERATION_REQUEST_DURATION = "fileGenerationRequestDuration";
    private final static String FILE_GENERATION_REQUESTS_COUNT = "fileGenerationRequestsCount";
    private final static String FILE_GENERATION_USERS_NUMBER = "fileGenerationUsersNumber";

    private final ConfigGenerator generator;
    private final ConfigGeneratorErrorLogger configGeneratorErrorLogger;
    private final ErrorTypeRepository errorTypeRepository;
    private final ExecutionRepository executionRepository;

    private final UserCountGauge userCountGauge;
    private final Histogram requestDurationHistogram;
    private final Meter requestCountMeter;
    private final TemplateEngine templateEngine;

    public ConfigGeneratorHelper(ConfigGenerator generator,
                                 ConfigGeneratorErrorLogger configGeneratorErrorLogger,
                                 ErrorTypeRepository errorTypeRepository,
                                 ExecutionRepository executionRepository,
                                 TemplateEngine templateEngine,
                                 MetricRegistry metricRegistry) {
        this.generator = generator;
        this.configGeneratorErrorLogger = configGeneratorErrorLogger;
        this.errorTypeRepository = errorTypeRepository;
        this.executionRepository = executionRepository;

        this.userCountGauge = metricRegistry.register(FILE_GENERATION_USERS_NUMBER, new UserCountGauge());
        this.requestDurationHistogram = metricRegistry.histogram(FILE_GENERATION_REQUEST_DURATION);
        this.requestCountMeter = metricRegistry.meter(FILE_GENERATION_REQUESTS_COUNT);
        this.templateEngine = templateEngine;
    }

    public ResponseEntity<String> generateConfigFileResponse(String fileName, Intercom intercom, Boolean isTest) {
        long start = System.currentTimeMillis();

        ErrorSources errorMessage = new ErrorSources(intercom);
        Execution execution = saveExecution(intercom, isTest);

        Map<String, ErrorType> errorTypes = fetchErrorTypes();
        ConfigFile sources = generator.generateConfig(intercom, errorTypes, errorMessage, fileName, execution);

        requestDurationHistogram.update(System.currentTimeMillis() - start);
        requestCountMeter.mark();
        userCountGauge.setValue(sources.getUsers().size());

        String templatedXmlConfig = StringUtils.EMPTY;

        try {
            String template = intercom.getType().getDefaultConfigurationVersion().stream().filter(ConfigurationFileVersion::getDefault).findFirst().get().getTemplate();
            Map<String, Object> templateKeys = new HashMap<>();
            templateKeys.put("sources", sources);
            LOGGER.info("Going to generate xml config based on template {}", template);
            templatedXmlConfig = templateEngine.compileTemplate("template", template, templateKeys);

            configGeneratorErrorLogger.logSuccess(execution);
        } catch (RuntimeException e) {
            configGeneratorErrorLogger.trackError(errorTypes, APPLICATION_ERROR, errorMessage, execution);
        }

        return ResponseEntity.ok().headers(generateConfigFileResponseHeaders(templatedXmlConfig, fileName)).body(templatedXmlConfig);
    }

    private Execution saveExecution(Intercom intercom, Boolean isTest) {
        return executionRepository.save(new Execution(intercom, LocalDateTime.now(), EXECUTION_STATUS_PENDING, isTest));
    }

    private Map<String, ErrorType> fetchErrorTypes() {
        return StreamSupport.stream(errorTypeRepository.findAll().spliterator(), false)
            .collect(Collectors.toMap(ErrorType::getName, error -> error));
    }

    private HttpHeaders generateConfigFileResponseHeaders(String body, String fileName) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
        headers.setContentLength(body.getBytes(StandardCharsets.UTF_8).length);
        return headers;
    }

    public static class UserCountGauge implements Gauge<Integer> {

        private volatile Integer value;

        @Override
        public Integer getValue() {
            return this.value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }
    }
}
