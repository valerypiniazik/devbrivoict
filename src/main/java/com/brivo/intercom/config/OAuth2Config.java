package com.brivo.intercom.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

import java.util.Optional;

@Configuration
@EnableResourceServer
public class OAuth2Config extends ResourceServerConfigurerAdapter
{

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http
                .requestMatcher(request -> Optional.ofNullable(request.getHeader("Authorization"))
                        .map(value -> value.startsWith("Bearer"))
                        .orElse(false))
                .authorizeRequests()
                .anyRequest().authenticated();
    }
}
