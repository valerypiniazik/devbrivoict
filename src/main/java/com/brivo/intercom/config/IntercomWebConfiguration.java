package com.brivo.intercom.config;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.catalina.valves.AccessLogValve;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.GenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.fasterxml.jackson.annotation.JsonInclude;

import static org.springframework.http.MediaType.*;

@Configuration
public class IntercomWebConfiguration extends WebMvcConfigurerAdapter implements EmbeddedServletContainerCustomizer
{
    @Value("${logging.path}")
    private String accessLogLocation;

    @Value("${server.tomcat.access-log-pattern}")
    private String accessLogPattern;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
        registry.addViewController("/intercom/**").setViewName("forward:/index.html");
        registry.addViewController("/error403").setViewName("forward:/index.html");
    }

    @Bean
    public ServletRegistrationBean dispatcherRegistration(DispatcherServlet dispatcherServlet) {
        ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet);
        registration.addUrlMappings("/");
        return registration;
    }

    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        if (container instanceof TomcatEmbeddedServletContainerFactory) {
            configureAccessLog((TomcatEmbeddedServletContainerFactory) container);
        } else {
            throw new RuntimeException("Container not supported. Tomcat container expected.");
        }
    }

    private void configureAccessLog(TomcatEmbeddedServletContainerFactory tomcatConfFactory) {
        AccessLogValve customAccessLogValve = new AccessLogValve();
        customAccessLogValve.setDirectory(accessLogLocation);
        customAccessLogValve.setPattern(accessLogPattern);
        tomcatConfFactory.addContextValves(customAccessLogValve);
    }

    @Bean
    public GenericHttpMessageConverter<Object> mappingJackson2HttpMessageConverter() {
        ObjectMapper mapper = new Jackson2ObjectMapperBuilder().failOnUnknownProperties(false).failOnEmptyBeans(false)
                .serializationInclusion(JsonInclude.Include.NON_NULL).build();
        return new MappingJackson2HttpMessageConverter(mapper);
    }

    @Bean
    public StringHttpMessageConverter stringConverter() {
        final StringHttpMessageConverter stringConverter = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        stringConverter.setSupportedMediaTypes(Arrays.asList(TEXT_PLAIN, TEXT_HTML, APPLICATION_JSON));
        return stringConverter;
    }

    @Bean
    public ByteArrayHttpMessageConverter byteArrayHttpMessageConverter() {
        final ByteArrayHttpMessageConverter byteArrayConverter = new ByteArrayHttpMessageConverter();

        List<MediaType> supportedTypes = new ArrayList<>(byteArrayConverter.getSupportedMediaTypes());
        supportedTypes.add(new MediaType("image", "*"));
        byteArrayConverter.setSupportedMediaTypes(supportedTypes);

        return byteArrayConverter;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(stringConverter());
        converters.add(mappingJackson2HttpMessageConverter());
        converters.add(byteArrayHttpMessageConverter());
        super.configureMessageConverters(converters);
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
        configurer.setUseRegisteredSuffixPatternMatch(true);
    }

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }
}
