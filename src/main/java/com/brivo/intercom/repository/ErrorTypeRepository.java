package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.ErrorType;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ErrorTypeRepository extends PagingAndSortingRepository<ErrorType, Long>
{
}