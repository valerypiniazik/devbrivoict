package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.Intercom;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;


public interface IntercomRepository extends PagingAndSortingRepository<Intercom, Long>
{
    List<Intercom> findAllByBrivoAccountInfoIdOrderByName(Long brivoAccountInfoId);

    Optional<Intercom> findByIdAndBrivoAccountInfoId(Long id, Long brivoAccountInfoId);

    Optional<Intercom> findOneByMacAddress(String macAddress);
}
