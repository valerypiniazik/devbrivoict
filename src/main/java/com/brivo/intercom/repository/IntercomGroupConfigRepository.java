package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface IntercomGroupConfigRepository extends PagingAndSortingRepository<IntercomGroupConfig, Long>
{
    List<IntercomGroupConfig> findAllByIntercomIdAndIntercomBrivoAccountInfoId(Long intercomId, Long brivoAccountInfoId);

    IntercomGroupConfig findAllByIdAndIntercomBrivoAccountInfoId(Long id, Long brivoAccountInfoId);
}
