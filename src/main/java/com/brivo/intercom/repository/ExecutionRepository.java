package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.Execution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ExecutionRepository extends PagingAndSortingRepository<Execution, Long>
{
    Page<Execution> findAllByIntercomIdAndIntercomBrivoAccountInfoId(Long intercomId, Long brivoAccountId, Pageable pageable);
}
