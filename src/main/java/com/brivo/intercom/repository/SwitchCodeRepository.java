package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.SwitchCode;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SwitchCodeRepository extends PagingAndSortingRepository<SwitchCode, Long>
{
}