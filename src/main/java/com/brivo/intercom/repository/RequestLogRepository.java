package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.RequestLog;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RequestLogRepository extends PagingAndSortingRepository<RequestLog, Long>, JpaSpecificationExecutor<RequestLog>
{
}