package com.brivo.intercom.repository;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BrivoAccountInfoRepository extends PagingAndSortingRepository<BrivoAccountInfo, Long>
{
}
