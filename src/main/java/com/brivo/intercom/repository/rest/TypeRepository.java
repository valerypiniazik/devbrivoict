package com.brivo.intercom.repository.rest;

import com.brivo.intercom.domain.entity.Type;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TypeRepository extends PagingAndSortingRepository<Type, Long>
{
}
