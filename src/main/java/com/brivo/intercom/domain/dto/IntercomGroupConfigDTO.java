package com.brivo.intercom.domain.dto;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class IntercomGroupConfigDTO
{
    private Long id;

    @NotNull
    private Long brivoGroupId;

    private String brivoGroupName;

    @NotNull
    private String formatString;

    private List<IntercomGroupContactConfigDTO> intercomGroupContactConfigs = new ArrayList<>();

    @NotNull
    private Long intercomId;

    public IntercomGroupConfigDTO() {
    }

    public IntercomGroupConfigDTO(Long brivoGroupId, String brivoGroupName, String formatString, List<IntercomGroupContactConfigDTO> intercomGroupContactConfigs) {
        this.brivoGroupId = brivoGroupId;
        this.brivoGroupName = brivoGroupName;
        this.formatString = formatString;
        this.intercomGroupContactConfigs = intercomGroupContactConfigs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getBrivoGroupId() {
        return brivoGroupId;
    }

    public void setBrivoGroupId(Long brivoGroupId) {
        this.brivoGroupId = brivoGroupId;
    }

    public String getBrivoGroupName() {
        return brivoGroupName;
    }

    public void setBrivoGroupName(String brivoGroupName) {
        this.brivoGroupName = brivoGroupName;
    }

    public String getFormatString() {
        return formatString;
    }

    public void setFormatString(String formatString) {
        this.formatString = formatString;
    }

    public List<IntercomGroupContactConfigDTO> getIntercomGroupContactConfigs() {
        return intercomGroupContactConfigs;
    }

    public void setIntercomGroupContactConfigs(List<IntercomGroupContactConfigDTO> intercomGroupContactConfigs) {
        this.intercomGroupContactConfigs = intercomGroupContactConfigs;
    }

    public Long getIntercomId() {
        return intercomId;
    }

    public void setIntercomId(Long intercomId) {
        this.intercomId = intercomId;
    }

    public static class IntercomGroupContactConfigDTO {
        @NotNull
        private String brivoContactType;

        @NotNull
        private Integer index;

        public IntercomGroupContactConfigDTO() {
        }

        public IntercomGroupContactConfigDTO(String brivoContactType) {
            this.brivoContactType = brivoContactType;
        }

        public String getBrivoContactType() {
            return brivoContactType;
        }

        public void setBrivoContactType(String brivoContactType) {
            this.brivoContactType = brivoContactType;
        }

        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }
    }
}