package com.brivo.intercom.domain.dto;

import java.time.LocalDateTime;
import java.util.List;

public class ExecutionDTO
{
    private LocalDateTime initiatedAt;

    private LocalDateTime finishedAt;

    private String status;

    private Boolean isTest;

    private Integer warningsNumber;

    private List<RequestLogDTO> requestLogs;

    public LocalDateTime getInitiatedAt() {
        return initiatedAt;
    }

    public void setInitiatedAt(LocalDateTime initiatedAt) {
        this.initiatedAt = initiatedAt;
    }

    public LocalDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsTest() {
        return isTest;
    }

    public void setIsTest(Boolean isTest) {
        this.isTest = isTest;
    }

    public Integer getWarningsNumber() {
        return warningsNumber;
    }

    public void setWarningsNumber(Integer warningsNumber) {
        this.warningsNumber = warningsNumber;
    }

    public List<RequestLogDTO> getRequestLogs() {
        return requestLogs;
    }

    public void setRequestLogs(List<RequestLogDTO> requestLogs) {
        this.requestLogs = requestLogs;
    }
}
