package com.brivo.intercom.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.util.Map;

public class RequestLogDTO
{
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", shape = JsonFormat.Shape.STRING)
    private LocalDateTime time;

    private Map<String, String> descriptionSources;

    private ErrorTypeDto errorType = new ErrorTypeDto();

    public LocalDateTime getTime() {
        return time;
    }

    public RequestLogDTO setTime(LocalDateTime time) {
        this.time = time;
        return this;
    }

    public Map<String, String> getDescriptionSources() {
        return descriptionSources;
    }

    public RequestLogDTO setDescriptionSources(Map<String, String> descriptionSources) {
        this.descriptionSources = descriptionSources;
        return this;
    }

    public ErrorTypeDto getErrorType() {
        return errorType;
    }

    public RequestLogDTO setErrorType(ErrorTypeDto errorType) {
        this.errorType = errorType;
        return this;
    }

    public static class ErrorTypeDto {
        private String name;

        private Boolean failureFatal;

        public String getName() {
            return name;
        }

        public ErrorTypeDto setName(String name) {
            this.name = name;
            return this;
        }

        public Boolean getFailureFatal() {
            return failureFatal;
        }

        public ErrorTypeDto setFailureFatal(Boolean failureFatal) {
            this.failureFatal = failureFatal;
            return this;
        }
    }
}