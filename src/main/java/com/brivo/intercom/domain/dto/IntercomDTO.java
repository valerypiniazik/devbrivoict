package com.brivo.intercom.domain.dto;

import com.brivo.intercom.domain.entity.SwitchCode;

import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class IntercomDTO
{
    private Long id;

    @Pattern(regexp = "([0-9A-F]{2}[-]){5}([0-9A-F]{2})", message = "Invalid mac address format")
    @NotNull
    private String macAddress;

    @NotNull
    private String name;

    @NotNull
    private Long typeId;

    @NotNull
    private List<SwitchCode> switchCodes;

    public IntercomDTO() {
    }

    public IntercomDTO(Long id, String macAddress, String password, String name, Long typeId, List<SwitchCode> switchCodes) {
        this.id = id;
        this.macAddress = macAddress;
        this.name = name;
        this.typeId = typeId;
        this.switchCodes = switchCodes;
    }

    public Long getId() {
        return id;
    }

    public IntercomDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public IntercomDTO setMacAddress(String macAddress) {
        this.macAddress = macAddress;
        return this;
    }

    public String getName() {
        return name;
    }

    public IntercomDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Long getTypeId() {
        return typeId;
    }

    public IntercomDTO setTypeId(Long typeId) {
        this.typeId = typeId;
        return this;
    }

    public List<SwitchCode> getSwitchCodes() {
        return switchCodes;
    }

    public void setSwitchCodes(List<SwitchCode> switchCodes) {
        this.switchCodes = switchCodes;
    }
}
