package com.brivo.intercom.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "intercom")
public class Intercom implements Serializable
{
    private static final BCryptPasswordEncoder PASSWORD_ENCODER = new BCryptPasswordEncoder();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "mac_address", unique = true, nullable = false)
    private String macAddress;

    @Column(name = "hashed_password", nullable = false)
    private String hashedPassword;

    @Column(name = "name", nullable = false)
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", nullable = false)
    private Type type;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brivo_account_id", nullable = false)
    @JsonIgnore
    private BrivoAccountInfo brivoAccountInfo;

    @OneToMany(mappedBy = "intercom", cascade = CascadeType.ALL)
    private List<Execution> executions = new ArrayList<>();

    @OneToMany(mappedBy = "intercom", cascade = CascadeType.ALL)
    private List<IntercomGroupConfig> intercomGroupConfigs = new ArrayList<>();

    @OneToMany(mappedBy = "intercom", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SwitchCode> switchCodes = new ArrayList<>();

    public Intercom() {
    }

    public Intercom(String macAddress, String hashedPassword, String name, Type type) {
        this.macAddress = macAddress;
        this.hashedPassword = hashedPassword;
        this.name = name;
        this.type = type;
    }

    public Intercom(Type type, BrivoAccountInfo brivoAccountInfo, List<IntercomGroupConfig> intercomGroupConfigs, List<SwitchCode> switchCodes) {
        this.type = type;
        this.brivoAccountInfo = brivoAccountInfo;
        this.intercomGroupConfigs = intercomGroupConfigs;
        this.switchCodes = switchCodes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @JsonIgnore
    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String password) {
        if (StringUtils.isNotEmpty(password)) {
            this.hashedPassword = PASSWORD_ENCODER.encode(password);
        }
    }

    public String getName() {
        return name;
    }

    public Intercom setName(String name) {
        this.name = name;
        return this;
    }

    public Type getType() {
        return type;
    }

    public Intercom setType(Type type) {
        this.type = type;
        return this;
    }

    public BrivoAccountInfo getBrivoAccountInfo() {
        return brivoAccountInfo;
    }

    public void setBrivoAccountInfo(BrivoAccountInfo brivoAccountInfo) {
        this.brivoAccountInfo = brivoAccountInfo;
    }

    public List<Execution> getExecutions() {
        return executions;
    }

    public void setExecutions(List<Execution> executions) {
        this.executions = executions;
    }

    public List<IntercomGroupConfig> getIntercomGroupConfigs() {
        return intercomGroupConfigs;
    }

    public void setIntercomGroupConfigs(List<IntercomGroupConfig> intercomGroupConfigs) {
        this.intercomGroupConfigs = intercomGroupConfigs;
    }

    public List<SwitchCode> getSwitchCodes() {
        return switchCodes;
    }

    public void setSwitchCodes(List<SwitchCode> switchCodes) {
        this.switchCodes = switchCodes;
    }
}
