package com.brivo.intercom.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "switch_code")
public class SwitchCode
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "code")
    @Min(value = 1000L, message = "Code must have at least 4 digits")
    @Max(value = 999999999999999L, message = "Code must not exceed 15 digits")
    private Long code;

    @Column(name = "index")
    private Integer index;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "intercom_id")
    private Intercom intercom;

    @Column(name = "name")
    private String name;

    public SwitchCode() {
    }

    public SwitchCode(Long code, Integer index) {
        this.code = code;
        this.index = index;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Intercom getIntercom() {
        return intercom;
    }

    public void setIntercom(Intercom intercom) {
        this.intercom = intercom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
