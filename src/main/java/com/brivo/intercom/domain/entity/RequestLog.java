package com.brivo.intercom.domain.entity;

import com.brivo.intercom.domain.entity.converter.MapOfStringsToStringConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Map;

@Entity
@Table(name = "request_log")
public class RequestLog implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "execution_id")
    private Execution execution;

    @Column(name = "time")
    private LocalDateTime time;


    @Column(name = "description_sources")
    @Convert(converter = MapOfStringsToStringConverter.class)
    private Map<String, String> descriptionSources;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "error_type_id")
    private ErrorType errorType;

    public RequestLog() {
    }

    public RequestLog(Execution execution, Map<String, String> descriptionSources, ErrorType errorType) {
        this.execution = execution;
        this.descriptionSources = descriptionSources;
        this.errorType = errorType;
        this.time = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Execution getExecution() {
        return execution;
    }

    public void setExecution(Execution execution) {
        this.execution = execution;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public Map<String, String> getDescriptionSources() {
        return descriptionSources;
    }

    public void setDescriptionSources(Map<String, String> descriptionSources) {
        this.descriptionSources = descriptionSources != null ? descriptionSources : Collections.emptyMap();
    }

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }
}
