package com.brivo.intercom.domain.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "error_type")
public class ErrorType implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "failure_fatal")
    private Boolean failureFatal;

    public ErrorType() {
    }

    public ErrorType(String name) {
        this.name = name;
    }

    public ErrorType(String name, Boolean failureFatal) {
        this.name = name;
        this.failureFatal = failureFatal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getFailureFatal() {
        return failureFatal;
    }

    public void setFailureFatal(Boolean failureFatal) {
        this.failureFatal = failureFatal;
    }
}
