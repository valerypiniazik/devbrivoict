package com.brivo.intercom.domain.entity;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "intercom_group_config")
public class IntercomGroupConfig implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "intercom_id", nullable = false)
    private Intercom intercom;

    @Column(name = "brivo_group_id", nullable = false)
    private Long brivoGroupId;

    @Column(name = "format_string", nullable = false)
    private String formatString;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "intercom_group_contact_config", joinColumns = {@JoinColumn(name = "intercom_group_config_id")})
    private List<IntercomGroupContactConfig> intercomGroupContactConfigs = new ArrayList<>();

    public IntercomGroupConfig() {
    }

    public IntercomGroupConfig(Intercom intercom, Long brivoGroupId, String formatString, List<IntercomGroupContactConfig> intercomGroupContactConfigs) {
        this.intercom = intercom;
        this.brivoGroupId = brivoGroupId;
        this.formatString = formatString;
        this.intercomGroupContactConfigs = intercomGroupContactConfigs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Intercom getIntercom() {
        return intercom;
    }

    public void setIntercom(Intercom intercom) {
        this.intercom = intercom;
    }

    public Long getBrivoGroupId() {
        return brivoGroupId;
    }

    public void setBrivoGroupId(Long brivoGroupId) {
        this.brivoGroupId = brivoGroupId;
    }

    public String getFormatString() {
        return formatString;
    }

    public void setFormatString(String formatString) {
        this.formatString = formatString;
    }

    public List<IntercomGroupContactConfig> getIntercomGroupContactConfigs() {
        return intercomGroupContactConfigs;
    }

    public void setIntercomGroupContactConfigs(List<IntercomGroupContactConfig> intercomGroupContactConfigs) {
        this.intercomGroupContactConfigs = intercomGroupContactConfigs;
    }

    @Embeddable
    public static class IntercomGroupContactConfig implements Serializable
    {
        @Column(name = "brivo_contact_type", nullable = false)
        private String brivoContactType;

        @Column(name = "index", nullable = false)
        private Integer index;

        public IntercomGroupContactConfig() {
        }

        public IntercomGroupContactConfig(String brivoContactType) {
            this.brivoContactType = brivoContactType;
        }

        public IntercomGroupContactConfig(String brivoContactType, Integer index) {
            this.brivoContactType = brivoContactType;
            this.index = index;
        }

        public String getBrivoContactType() {
            return brivoContactType;
        }

        public void setBrivoContactType(String brivoContactType) {
            this.brivoContactType = brivoContactType;
        }

        public Integer getIndex() {
            return index;
        }

        public void setIndex(Integer index) {
            this.index = index;
        }
    }
}