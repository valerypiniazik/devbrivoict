package com.brivo.intercom.domain.entity.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@Converter
public class MapOfStringsToStringConverter implements AttributeConverter<Map<String, String>, String>
{
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private static final TypeReference<Map<String, String>> MAP_OF_STRING_TYPE_REF = new TypeReference<Map<String, String>>() {};

    @Override
    public String convertToDatabaseColumn(Map<String, String> attribute) {
        try {
            return OBJECT_MAPPER.writeValueAsString(Optional.ofNullable(attribute).orElse(Collections.emptyMap()));
        } catch (JsonProcessingException e) {
            return StringUtils.EMPTY;
        }
    }

    @Override
    public Map<String, String> convertToEntityAttribute(String dbData) {
        try {
            return OBJECT_MAPPER.readValue(Optional.ofNullable(dbData).orElse(StringUtils.EMPTY), MAP_OF_STRING_TYPE_REF);
        } catch (IOException e) {
            return Collections.emptyMap();
        }
    }
}
