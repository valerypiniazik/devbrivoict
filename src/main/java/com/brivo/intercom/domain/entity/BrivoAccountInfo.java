package com.brivo.intercom.domain.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "brivo_account_info")
public class BrivoAccountInfo
{
    @Id
    @Column(name = "brivo_account_id")
    private Long id;

    @Column(name = "refresh_token", length = 1024, nullable = false)
    private String refreshToken;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "refresh_token_update_time")
    private LocalDateTime refreshTokenUpdateTime;

    @OneToMany(mappedBy = "brivoAccountInfo")
    private List<Intercom> intercoms= new ArrayList<>();

    public BrivoAccountInfo() {
    }

    public BrivoAccountInfo(Long id) {
        this.id = id;
    }

    public BrivoAccountInfo(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public BrivoAccountInfo(Long id, String refreshToken, String username, LocalDateTime refreshTokenUpdateTime, List<Intercom> intercoms) {
        this.id = id;
        this.refreshToken = refreshToken;
        this.username = username;
        this.refreshTokenUpdateTime = refreshTokenUpdateTime;
        this.intercoms = intercoms;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getRefreshTokenUpdateTime() {
        return refreshTokenUpdateTime;
    }

    public void setRefreshTokenUpdateTime(LocalDateTime refreshTokenUpdateTime) {
        this.refreshTokenUpdateTime = refreshTokenUpdateTime;
    }

    public List<Intercom> getIntercoms() {
        return intercoms;
    }

    public void setIntercoms(List<Intercom> intercoms) {
        this.intercoms = intercoms;
    }
}