package com.brivo.intercom.domain.entity;

import com.brivo.intercom.domain.entity.converter.MapOfStringsToStringConverter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "type")
public class Type implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "pattern")
    private String pattern;

    @Column(name = "config")
    @Convert(converter = MapOfStringsToStringConverter.class)
    private Map<String, String> config = new HashMap<>();

    @OneToMany(mappedBy = "type", cascade = CascadeType.ALL)
    private List<ConfigurationFileVersion> defaultConfigurationVersion;

    public Type() {
    }

    public Type(String pattern, Map<String, String> config) {
        this.pattern = pattern;
        this.config = config;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public Map<String, String> getConfig() {
        return config;
    }

    public void setConfig(Map<String, String> config) {
        this.config = config;
    }

    public List<ConfigurationFileVersion> getDefaultConfigurationVersion() {
        return defaultConfigurationVersion;
    }

    public void setDefaultConfigurationVersion(List<ConfigurationFileVersion> defaultConfigurationVersion) {
        this.defaultConfigurationVersion = defaultConfigurationVersion;
    }
}
