package com.brivo.intercom.domain.entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "execution")
public class Execution
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "intercom_id")
    private Intercom intercom;

    @Column(name = "initiated_at")
    private LocalDateTime initiatedAt;

    @Column(name = "finished_at")
    private LocalDateTime finishedAt;

    @Column(name = "status")
    private String status;

    @Column(name = "is_test")
    private Boolean isTest;

    @OneToMany(mappedBy = "execution", cascade = CascadeType.ALL)
    private List<RequestLog> requestLogs = new ArrayList<>();

    public Execution() {
    }

    public Execution(Intercom intercom, LocalDateTime initiatedAt, String status, Boolean isTest) {
        this.intercom = intercom;
        this.initiatedAt = initiatedAt;
        this.status = status;
        this.isTest = isTest;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Intercom getIntercom() {
        return intercom;
    }

    public void setIntercom(Intercom intercom) {
        this.intercom = intercom;
    }

    public LocalDateTime getInitiatedAt() {
        return initiatedAt;
    }

    public void setInitiatedAt(LocalDateTime initiatedAt) {
        this.initiatedAt = initiatedAt;
    }

    public LocalDateTime getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(LocalDateTime finishedAt) {
        this.finishedAt = finishedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getIsTest() {
        return isTest;
    }

    public void setIsTest(Boolean isTest) {
        this.isTest = isTest;
    }

    public List<RequestLog> getRequestLogs() {
        return requestLogs;
    }

    public void setRequestLogs(List<RequestLog> requestLogs) {
        this.requestLogs = requestLogs;
    }
}
