package com.brivo.intercom.domain.mapper;

import com.brivo.intercom.domain.dto.RequestLogDTO;
import com.brivo.intercom.domain.entity.RequestLog;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RequestLogMapper
{
    RequestLogDTO requestLogToRequestLogDto(RequestLog requestLog);

    List<RequestLogDTO> requestLogsToRequestLogDtos(List<RequestLog> requestLogs);
}

