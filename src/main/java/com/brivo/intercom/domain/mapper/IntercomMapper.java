package com.brivo.intercom.domain.mapper;

import com.brivo.intercom.domain.dto.IntercomDTO;
import com.brivo.intercom.domain.entity.Intercom;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IntercomMapper
{
    @Mappings({
            @Mapping(target = "id", source = "id"),
            @Mapping(target = "macAddress", source = "macAddress"),
            @Mapping(target = "name", source = "name"),
            @Mapping(target = "typeId", source = "type.id"),
            @Mapping(target = "switchCodes", source = "switchCodes")

    })
    IntercomDTO intercomToIntercomDto(Intercom intercom);

    List<IntercomDTO> intercomsToIntercomDtos(List<Intercom> intercoms);
}
