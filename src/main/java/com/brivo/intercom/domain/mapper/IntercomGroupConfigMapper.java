package com.brivo.intercom.domain.mapper;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", uses = BrivoGroupRestService.class, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface IntercomGroupConfigMapper
{
    @Mappings({
            @Mapping(target = "id", ignore = true)
    })
    IntercomGroupConfig updateIntercomGroupConfig(@MappingTarget IntercomGroupConfig intercomGroupConfig, IntercomGroupConfigDTO intercomGroupConfigDto);

    IntercomGroupConfig intercomGroupConfigDtoToIntercomGroupConfig(IntercomGroupConfigDTO intercomGroupConfigDto);

    @Mappings({
        @Mapping(target = "brivoGroupName",
            expression = "java(java.util.Optional.ofNullable(brivoGroupRestService.getGroupById(intercomGroupConfig.getBrivoGroupId(), 0L)).map(group -> group.getName()).orElse(null))")
    })
    IntercomGroupConfigDTO intercomGroupConfigToIntercomGroupConfigDto(IntercomGroupConfig  intercomGroupConfig,
                                                                       @Context BrivoGroupRestService brivoGroupRestService);

    @Mappings({
            @Mapping(target = "brivoGroupName",
                    expression = "java(java.util.Optional.ofNullable(brivoGroupRestService.getGroupById(intercomGroupConfig.getBrivoGroupId(), brivoAccountId)).map(group -> group.getName()).orElse(null))")
    })
    IntercomGroupConfigDTO intercomGroupConfigToIntercomGroupConfigDto(IntercomGroupConfig  intercomGroupConfig,
                                                                       @Context Long brivoAccountId,
                                                                       @Context BrivoGroupRestService brivoGroupRestService);
}
