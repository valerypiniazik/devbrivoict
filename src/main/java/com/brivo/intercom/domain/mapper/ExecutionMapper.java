package com.brivo.intercom.domain.mapper;

import com.brivo.intercom.domain.dto.ExecutionDTO;

import com.brivo.intercom.domain.entity.Execution;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface ExecutionMapper
{

    @Mappings({
            @Mapping(target = "warningsNumber", expression = "java(execution.getRequestLogs().size() - 1)"),
            @Mapping(target = "requestLogs", expression = "java(requestLogMapper.requestLogsToRequestLogDtos(execution.getRequestLogs()))"),
    })
    ExecutionDTO executionToExecutionDto(Execution execution, @Context RequestLogMapper requestLogMapper);
}
