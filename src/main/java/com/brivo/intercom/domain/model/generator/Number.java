package com.brivo.intercom.domain.model.generator;

public class Number {

    private Integer numberNumber;

    private String phoneNumber;

    public Number() {
    }

    public Number(Integer numberNumber, String phoneNumber) {
        this.numberNumber = numberNumber;
        this.phoneNumber = phoneNumber;
    }

    public Integer getNumberNumber() {
        return numberNumber;
    }

    public void setNumberNumber(Integer numberNumber) {
        this.numberNumber = numberNumber;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
