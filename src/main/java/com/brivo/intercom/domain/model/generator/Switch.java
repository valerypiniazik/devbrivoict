package com.brivo.intercom.domain.model.generator;

import java.util.List;

public class Switch
{
    private Integer switchNumber;

    private Integer enabled;

    private List<Code> codes;

    private Integer index;

    public Switch() {
    }

    public Switch(Integer switchNumber, Integer enabled, List<Code> codes) {
        this.switchNumber = switchNumber;
        this.enabled = enabled;
        this.codes = codes;
    }

    public Switch(List<Code> codes, Integer index) {
        this.codes = codes;
        this.index = index;
    }

    public Integer getSwitchNumber() {
        return switchNumber;
    }

    public Switch setSwitchNumber(Integer switchNumber) {
        this.switchNumber = switchNumber;
        return this;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public Switch setEnabled(Integer enabled) {
        this.enabled = enabled;
        return this;
    }

    public List<Code> getCodes() {
        return codes;
    }

    public Switch setCodes(List<Code> codes) {
        this.codes = codes;
        return this;
    }

    public Integer getIndex() {
        return index;
    }

    public Switch setIndex(Integer index) {
        this.index = index;
        return this;
    }
}
