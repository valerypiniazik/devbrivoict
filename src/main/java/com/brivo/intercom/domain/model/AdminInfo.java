package com.brivo.intercom.domain.model;

import com.brivo.api.rest.model.identity.Administrator;

public class AdminInfo {
    private Administrator admin;

    private boolean isActive;

    public AdminInfo(Administrator admin, boolean isActive) {
        this.admin = admin;
        this.isActive = isActive;
    }

    public Administrator getAdmin() {
        return admin;
    }

    public AdminInfo setAdmin(Administrator admin) {
        this.admin = admin;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public AdminInfo setActive(boolean active) {
        isActive = active;
        return this;
    }
}
