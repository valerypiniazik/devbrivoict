package com.brivo.intercom.domain.model.generator;

public class Code
{
    private Integer codeNumber;

    private Long code;

    private Integer accessMode;

    public Code() {
    }

    public Code(Integer codeNumber,Integer accessMode) {
        this.codeNumber = codeNumber;
        this.accessMode = accessMode;
    }

    public Code(Integer codeNumber, Long code, Integer accessMode) {
        this.codeNumber = codeNumber;
        this.code = code;
        this.accessMode = accessMode;
    }

    public Integer getCodeNumber() {
        return codeNumber;
    }

    public void setCodeNumber(Integer codeNumber) {
        this.codeNumber = codeNumber;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public Integer getAccessMode() {
        return accessMode;
    }

    public void setAccessMode(Integer accessMode) {
        this.accessMode = accessMode;
    }
}
