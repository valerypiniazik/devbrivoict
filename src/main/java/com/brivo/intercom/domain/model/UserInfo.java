package com.brivo.intercom.domain.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;

public class UserInfo
{
    private String username;

    @JsonIgnore
    private String refreshToken;

    private List<String> roles;

    public UserInfo() {
    }

    public UserInfo(String username, String refreshToken, List<String> roles) {
        this.username = username;
        this.refreshToken = refreshToken;
        this.roles = roles;
    }

    public String getUsername() {
        return username;
    }

    public UserInfo setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public UserInfo setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
        return this;
    }

    public List<String> getRoles() {
        return roles;
    }

    public UserInfo setRoles(List<String> roles) {
        this.roles = roles;
        return this;
    }
}
