package com.brivo.intercom.domain.model.generator;

import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;

public class ErrorSources
{
    private Intercom intercom;
    private User user;
    private IntercomGroupConfig intercomGroupConfig;

    public ErrorSources() {
    }

    public ErrorSources(Intercom intercom) {
        this.intercom = intercom;
    }

    public Intercom getIntercom() {
        return intercom;
    }

    public void setIntercom(Intercom intercom) {
        this.intercom = intercom;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public IntercomGroupConfig getIntercomGroupConfig() {
        return intercomGroupConfig;
    }

    public ErrorSources setIntercomGroupConfig(IntercomGroupConfig intercomGroupConfig) {
        this.intercomGroupConfig = intercomGroupConfig;
        return this;
    }
}
