package com.brivo.intercom.domain.model.generator;

import java.util.List;

public class UserModel
{
    private Integer userNumber;

    private Integer enabled;

    private String uuid;

    private String shortName;

    private String treePath;

    private List<Number> numbers;

    private List<SwitchCode> switchCodes;

    public UserModel() {
    }

    public UserModel(String shortName, String treePath, List<Number> numbers) {
        this.shortName = shortName;
        this.treePath = treePath;
        this.numbers = numbers;
    }

    public Integer getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(Integer userNumber) {
        this.userNumber = userNumber;
    }

    public Integer getEnabled() {
        return enabled;
    }

    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String treePath) {
        this.treePath = treePath;
    }

    public List<Number> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Number> numbers) {
        this.numbers = numbers;
    }

    public List<SwitchCode> getSwitchCodes() {
        return switchCodes;
    }

    public void setSwitchCodes(List<SwitchCode> switchCodes) {
        this.switchCodes = switchCodes;
    }
}
