package com.brivo.intercom.domain.model.generator;

public class SwitchCode
{
    private Integer switchCodeNumber;

    private Long code;

    public Integer getSwitchCodeNumber() {
        return switchCodeNumber;
    }

    public void setSwitchCodeNumber(Integer switchCodeNumber) {
        this.switchCodeNumber = switchCodeNumber;
    }

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }
}
