package com.brivo.intercom.domain.model;

import java.time.LocalDateTime;

public class RefreshTokenStatus
{
    private Boolean isValid;

    private String username;

    private LocalDateTime updatedAt;

    private Boolean isExist;

    public RefreshTokenStatus() {
    }

    public RefreshTokenStatus(Boolean isValid, Boolean isExist, String username, LocalDateTime updatedAt) {
        this.isValid = isValid;
        this.isExist = isExist;
        this.username = username;
        this.updatedAt = updatedAt;
    }

    public Boolean getValid() {
        return isValid;
    }

    public void setValid(Boolean valid) {
        isValid = valid;
    }

    public Boolean getExist() {
        return isExist;
    }

    public void setExist(Boolean isExist) {
        this.isExist = isExist;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}
