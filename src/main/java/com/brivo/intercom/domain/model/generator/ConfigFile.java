package com.brivo.intercom.domain.model.generator;

import java.util.List;

public class ConfigFile
{
    private String fileVersion;

    private String fileName;

    private List<Switch> switches;

    private List<UserModel> users;

    public ConfigFile(String fileName, List<Switch> switches, List<UserModel> users) {
        this.fileName = fileName;
        this.switches = switches;
        this.users = users;
    }

    public String getFileVersion() {
        return fileVersion;
    }

    public void setFileVersion(String fileVersion) {
        this.fileVersion = fileVersion;
    }

    public String getFileName() {
        return fileName;
    }

    public ConfigFile setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public List<Switch> getSwitches() {
        return switches;
    }

    public void setSwitches(List<Switch> switches) {
        this.switches = switches;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
}
