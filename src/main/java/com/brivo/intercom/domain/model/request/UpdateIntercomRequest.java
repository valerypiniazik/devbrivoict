package com.brivo.intercom.domain.model.request;

import com.brivo.intercom.domain.entity.SwitchCode;
import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UpdateIntercomRequest
{
    private Long id;

    @Pattern(regexp = "([0-9A-F]{2}[-]){5}([0-9A-F]{2})", message = "Invalid mac address format")
    @NotNull
    private String macAddress;

    @Size(min = 8)
    private String password;

    @NotNull
    @Size(max = 255)
    private String name;

    @NotNull
    private Long typeId;

    @NotNull
    @Valid
    private List<SwitchCode> switchCodes;

    public UpdateIntercomRequest() {
    }

    public UpdateIntercomRequest(Long id, String macAddress, String password, String name, Long typeId, List<SwitchCode> switchCodes) {
        this.id = id;
        this.macAddress = macAddress;
        this.password = password;
        this.name = name;
        this.typeId = typeId;
        this.switchCodes = switchCodes;
    }

    public Long getId() {
        return id;
    }

    public UpdateIntercomRequest setId(Long id) {
        this.id = id;
        return this;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public UpdateIntercomRequest setMacAddress(String macAddress) {
        this.macAddress = macAddress;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UpdateIntercomRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getName() {
        return name;
    }

    public UpdateIntercomRequest setName(String name) {
        this.name = name;
        return this;
    }

    public Long getTypeId() {
        return typeId;
    }

    public UpdateIntercomRequest setTypeId(Long typeId) {
        this.typeId = typeId;
        return this;
    }

    public List<SwitchCode> getSwitchCodes() {
        return switchCodes;
    }

    public UpdateIntercomRequest setSwitchCodes(List<SwitchCode> switchCodes) {
        this.switchCodes = switchCodes;
        return this;
    }
}
