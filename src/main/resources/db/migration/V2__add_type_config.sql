ALTER TABLE type ADD config TEXT;

UPDATE type SET config = '{"users_limit":"1999","display_name_limit":"63","switch_codes_number":"4","warnings_limit":"25"}' WHERE make='2N' AND model='Verso';

ALTER TABLE type ALTER COLUMN config SET NOT NULL;
