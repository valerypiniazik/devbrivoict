
CREATE TABLE brivo_account_info (
    brivo_account_id bigint,
    refresh_token text,
    username character varying(255),
    refresh_token_update_time timestamp without time zone
);

CREATE TABLE error_type (
    id bigserial,
    name character varying(255) NOT NULL,
    failure_fatal boolean NOT NULL DEFAULT FALSE
);

CREATE TABLE execution (
    id bigserial,
    intercom_id bigint NOT NULL,
    initiated_at timestamp without time zone NOT NULL,
    finished_at timestamp without time zone,
    status character varying(255) NOT NULL,
    is_test boolean NOT NULL
);

CREATE TABLE intercom (
    id bigserial,
    mac_address character varying(17) NOT NULL,
    hashed_password character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    type_id bigint NOT NULL,
    brivo_account_id bigint NOT NULL
);

CREATE TABLE intercom_group_config (
    id bigserial,
    intercom_id bigint NOT NULL,
    brivo_group_id bigint NOT NULL,
    format_string character varying(255) NOT NULL
);

CREATE TABLE intercom_group_contact_config (
    intercom_group_config_id bigint NOT NULL,
    brivo_contact_type character varying(255) NOT NULL,
    index integer NOT NULL
);

CREATE TABLE request_log (
    id bigserial,
    "time" timestamp without time zone NOT NULL,
    error_type_id bigint,
    description_sources text,
    execution_id bigint NOT NULL
);

CREATE TABLE switch_code (
    id bigserial,
    code bigint NOT NULL,
    index integer NOT NULL,
    name character varying(255),
    intercom_id bigint NOT NULL
);

CREATE TABLE type (
    id bigserial,
    make character varying(255),
    model character varying(255),
    pattern character varying(255) NOT NULL
);

ALTER TABLE ONLY brivo_account_info
    ADD CONSTRAINT brivo_account_info_pkey PRIMARY KEY (brivo_account_id);

ALTER TABLE ONLY error_type
    ADD CONSTRAINT error_type_pkey PRIMARY KEY (id);

ALTER TABLE ONLY execution
    ADD CONSTRAINT execution_pkey PRIMARY KEY (id);

ALTER TABLE ONLY intercom_group_config
    ADD CONSTRAINT intercom_group_config_intercom_id_brivo_group_id_key UNIQUE (intercom_id, brivo_group_id);

ALTER TABLE ONLY intercom_group_config
    ADD CONSTRAINT intercom_group_config_pkey PRIMARY KEY (id);

ALTER TABLE ONLY intercom_group_contact_config
    ADD CONSTRAINT intercom_group_contact_config_pkey PRIMARY KEY (intercom_group_config_id, brivo_contact_type);

ALTER TABLE ONLY intercom
    ADD CONSTRAINT intercom_mac_address_key UNIQUE (mac_address);

ALTER TABLE ONLY intercom
    ADD CONSTRAINT intercom_pkey PRIMARY KEY (id);

ALTER TABLE ONLY request_log
    ADD CONSTRAINT request_log_pkey PRIMARY KEY (id);

ALTER TABLE ONLY switch_code
    ADD CONSTRAINT switch_code_pkey PRIMARY KEY (id);

ALTER TABLE ONLY type
    ADD CONSTRAINT type_pkey PRIMARY KEY (id);

ALTER TABLE ONLY execution
    ADD CONSTRAINT execution_intercom_id_fkey FOREIGN KEY (intercom_id) REFERENCES intercom(id);

ALTER TABLE ONLY intercom_group_config
    ADD CONSTRAINT fk30jl2w4i0fkqhi9y3qmvhvemi FOREIGN KEY (intercom_id) REFERENCES intercom(id);

ALTER TABLE ONLY intercom_group_contact_config
    ADD CONSTRAINT fk7rguau65w8flsn0aq23swhuhn FOREIGN KEY (intercom_group_config_id) REFERENCES intercom_group_config(id);

ALTER TABLE ONLY intercom
    ADD CONSTRAINT fkacqfgo7e5bh570vyypmdvmn69 FOREIGN KEY (type_id) REFERENCES type(id);

ALTER TABLE ONLY intercom
    ADD CONSTRAINT fkesb8e6kc3cet9rcroep80vlw7 FOREIGN KEY (brivo_account_id) REFERENCES brivo_account_info(brivo_account_id);

ALTER TABLE ONLY switch_code
    ADD CONSTRAINT fkghx57poj7ys2td4pn1apv94ge FOREIGN KEY (intercom_id) REFERENCES intercom(id);

ALTER TABLE ONLY request_log
    ADD CONSTRAINT fkp1e9737875t7jesjwr5pr0he3 FOREIGN KEY (error_type_id) REFERENCES error_type(id);

ALTER TABLE ONLY request_log
    ADD CONSTRAINT request_log_execution_id_fkey FOREIGN KEY (execution_id) REFERENCES execution(id);

INSERT INTO type(make, model, pattern) VALUES ('2N', 'Verso', 'hipve-$'||'{mac_address}.xml');

INSERT INTO error_type(
	name, failure_fatal)
	VALUES ('RETRIEVE_USER_ERROR', true),
  ('RETRIEVE_GROUP_USERS_ERROR', true),
	('INVALID_VARIABLE_NAMES_IN_FORMAT_STRING', true),
	('UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING', true),
	('APPLICATION_ERROR', true),
	('TOO_MANY_WARNINGS', true),
	('MISSING_VALUE_FOR_FORMAT_STRING', false),
	('NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND', false),
	('USER_LIMIT_EXCEEDED', false),
	('DISPLAY_NAME_EXCEEDED_MAX_LENGTH', false);