CREATE TABLE configuration_file_version(
    id bigserial PRIMARY KEY,
    version character varying(255) NOT NULL,
    content_type character varying(255) NOT NULL,
    template text NOT NULL,
    intercom_type_id bigint NOT NULL REFERENCES type(id),
    is_default boolean NOT NULL
);

INSERT INTO configuration_file_version(
    version, content_type, template, intercom_type_id, is_default)
    SELECT '17', 'application/xml', '<?xml version="1.0" encoding="ISO-8859-1"?><DeviceDatabase Version="17"><Display><PhoneBook><Enabled>1</Enabled><Tree/><ManModeEnabled>1</ManModeEnabled></PhoneBook></Display><Switches><#list sources.switches as switch><Switch At="$'||'{switch.switchNumber}"><Enabled>$'||'{switch.enabled}</Enabled><Code At="0"><#list switch.codes as code><#if code.code??><Code>$'||'{code.code?c}</Code><#else><Code/></#if><AccessMode>$'||'{code.accessMode}</AccessMode></#list></Code></Switch></#list></Switches><Directory><#list sources.users as user><User At="$'||'{user.userNumber}"><Uuid/><Name>$'||'{user.shortName}</Name><TreePath>$'||'{user.treePath}</TreePath><Calling><#list user.numbers as number><Position At="$'||'{number.numberNumber}"><Peer>$'||'{number.phoneNumber}</Peer></Position></#list></Calling></User></#list></Directory></DeviceDatabase>', id, true FROM type WHERE make='2N' AND model='Verso';