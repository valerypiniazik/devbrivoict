import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class ReconnectButton extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        e.preventDefault();
        this.props.onClick();
    }

    render() {
        return (
            <button
                id={this.props.id}
                className="btn btn-primary blue"
                onClick={this.onClick}
            >
                <span className="text">
                    <FormattedMessage id="intercoms.reconnect" />
                </span>
            </button>
        );
    }
}

ReconnectButton.propTypes = {
    id: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};

export default ReconnectButton;
