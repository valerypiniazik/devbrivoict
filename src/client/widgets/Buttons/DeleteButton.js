import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class DeleteButton extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        e.preventDefault();
        this.props.onClick();
    }

    render() {
        const disabledClassNamePart = this.props.disabled ? 'disabled' : '';
        return (
            <a className={disabledClassNamePart} onClick={this.onClick}>
                <div className={`d-flex  ${this.props.classNames}`}>
                    <span class="d-inline-block deleteIcon on" />
                    <span
                        class="d-inline-block d-block d-sm-block d-md-none"
                        style={{ paddingLeft: '5px' }}
                    >
                        {this.props.label}
                    </span>
                </div>
            </a>
        );
    }
}

DeleteButton.propTypes = {
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    classNames: PropTypes.string,
};

DeleteButton.defaultProps = {
    classNames: '',
};

export default DeleteButton;
