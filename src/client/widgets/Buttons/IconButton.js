import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

class IconButton extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.renderInnerContent = this.renderInnerContent.bind(this);
        this.renderLink = this.renderLink.bind(this);
        this.renderNonLink = this.renderNonLink.bind(this);
        this.getDisabledClass = this.getDisabledClass.bind(this);
    }

    onClick(e) {
        if (!!this.props.onClick) {
            e.preventDefault();
            this.props.onClick();
        }
    }

    renderInnerContent() {
        return (
            <div className={`d-flex custom-button ${this.props.classNames}`}>
                <FontAwesomeIcon icon={this.props.icon} />
                <span
                    class="d-inline-block d-block d-sm-block d-md-none text"
                    style={{ paddingLeft: '5px' }}
                >
                    {this.props.label}
                </span>
            </div>
        );
    }

    getDisabledClass() {
        return this.props.disabled ? 'disabled' : '';
    }

    renderLink() {
        return (
            <Link
                className="text-link"
                id={this.props.id}
                className={this.getDisabledClass()}
                onClick={this.onClick}
                to={this.props.href}
            >
                {this.renderInnerContent()}
            </Link>
        );
    }

    renderNonLink() {
        return (
            <a
                className="text-link"
                id={this.props.id}
                className={this.getDisabledClass()}
                onClick={this.onClick}
            >
                {this.renderInnerContent()}
            </a>
        );
    }

    render() {
        return !!this.props.href ? this.renderLink() : this.renderNonLink();
    }
}

IconButton.propTypes = {
    href: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    classNames: PropTypes.string,
    icon: PropTypes.string.isRequired,
};

IconButton.defaultProps = {
    classNames: '',
};

export default IconButton;
