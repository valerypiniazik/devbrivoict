import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class SimpleButton extends React.PureComponent {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick(e) {
        e.preventDefault();
        this.props.onClick();
    }

    render() {
        let disabledClassNamePart = this.props.disabled ? 'disabled' : '';
        return (
            <button
                id={this.props.id}
                className={`${
                    this.props.className
                } brivo-button text-center ${disabledClassNamePart}`}
                onClick={this.onClick}
            >
                <span className="text">{this.props.label}</span>
            </button>
        );
    }
}

SimpleButton.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    className: PropTypes.string,
    disabled: PropTypes.bool,
};

SimpleButton.defaultProps = {
    className: '',
};

export default SimpleButton;
