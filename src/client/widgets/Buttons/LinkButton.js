import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

class LinkButton extends React.PureComponent {
    render() {
        if (this.props.disabled) {
            return (
                <a
                    id={this.props.id}
                    className={`${
                        this.props.className
                    } brivo-button text-center disabled`}
                >
                    <span className="text">{this.props.label}</span>
                </a>
            );
        } else {
            return (
                <Link
                    id={this.props.id}
                    className={`${
                        this.props.className
                    } brivo-button text-center`}
                    to={this.props.href}
                    onClick={this.props.onClick}
                >
                    <span className="text">{this.props.label}</span>
                </Link>
            );
        }
    }
}

LinkButton.propTypes = {
    id: PropTypes.string.isRequired,
    href: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    className: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
};

LinkButton.defaultProps = {
    className: '',
};

export default LinkButton;
