import React from 'react';
import PropTypes from 'prop-types';
import IconButton from './IconButton';

class EditButton extends React.Component {
    render() {
        return <IconButton icon="pencil-alt" {...this.props} />;
    }
}

EditButton.propTypes = {
    href: PropTypes.string,
    onClick: PropTypes.func,
    disabled: PropTypes.bool,
    classNames: PropTypes.string,
    icon: PropTypes.string.isRequired,
};

EditButton.defaultProps = {
    classNames: '',
};

export default EditButton;
