import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button, Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import { faQuestionCircle } from '@fortawesome/free-regular-svg-icons';

class TextInput extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.renderTooltip = this.renderTooltip.bind(this);
        this.toggle = this.toggle.bind(this);
        this.state = {
            popoverOpen: false,
        };
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    }

    toggle() {
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        });
    }
    renderTooltip() {
        if (!!this.props.tooltip) {
            const popoverId = `${this.props.id}_popover`;
            return (
                <span
                    id={popoverId}
                    style={{ cursor: 'pointer', paddingRight: '5px' }}
                    onClick={this.toggle}
                >
                    <Popover
                        placement="right"
                        isOpen={this.state.popoverOpen}
                        target={popoverId}
                        toggle={this.toggle}
                    >
                        <PopoverBody>{this.props.tooltip}</PopoverBody>
                    </Popover>
                    <FontAwesomeIcon
                        style={{ fontSize: '14px' }}
                        icon={faQuestionCircle}
                    />
                </span>
            );
        } else {
            return null;
        }
    }

    render() {
        const tooltipClass = this.props.tooltip ? 'custom-input-tooltip' : '';

        return (
            <div className={`form-group ${this.props.containerClassName}`}>
                <label
                    for={this.props.id}
                    className={`${this.props.labelClassName}`}
                >
                    {this.props.label}
                </label>
                <div
                    className={`d-flex flex-row  align-items-center ${tooltipClass}`}
                >
                    <input
                        type={this.props.inputType}
                        className="form-control custom-input"
                        id={this.props.id}
                        value={this.props.value}
                        min={this.props.min}
                        max={this.props.max}
                        maxlength={this.props.max}
                        minlength={this.props.min}
                        onChange={this.onChange}
                    />
                    {this.renderTooltip()}
                </div>
            </div>
        );
    }
}

TextInput.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    inputType: PropTypes.string,
    value: PropTypes.string,
    tooltip: PropTypes.string,
    min: PropTypes.number,
    max: PropTypes.number,
    onChange: PropTypes.func.isRequired,
    containerClassName: PropTypes.string,
    labelClassName: PropTypes.string,
    inputClassName: PropTypes.string,
};

TextInput.defaultProps = {
    containerClassName: '',
    labelClassName: '',
    inputClassName: '',
    value: '',
    inputType: 'text',
};

export default TextInput;
