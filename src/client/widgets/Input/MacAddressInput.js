import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-maskedinput';

class MacAddressInput extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
    }

    onChange(event) {
        this.props.onChange(event.target.value);
    }

    render() {
        const formatChar = {
            w: {
                validate: function(char) {
                    return /[1234567890ABCDEFabcdef]/.test(char);
                },
                transform: function(char) {
                    return char.toUpperCase();
                },
            },
        };
        return (
            <div>
                <div className="form-row">
                    <div className="col-12">
                        <label className={`${this.props.labelClassName}`}>
                            {this.props.label}
                        </label>
                    </div>
                </div>
                <div className="form-row">
                    <div className="form-group col-12 d-flex flex-row">
                        <MaskedInput
                            type="text"
                            className="custom-input"
                            mask="ww-ww-ww-ww-ww-ww"
                            formatCharacters={formatChar}
                            placeholder=""
                            name="card"
                            size="12"
                            value={this.props.value}
                            onChange={this.onChange}
                            style={{ width: '140px' }}
                        />
                    </div>
                </div>
            </div>
        );
    }
}

MacAddressInput.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    containerClassName: PropTypes.string,
    labelClassName: PropTypes.string,
};

MacAddressInput.defaultProps = {
    containerClassName: '',
    labelClassName: '',
    value: '',
};

export default MacAddressInput;
