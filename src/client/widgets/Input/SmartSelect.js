import React from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import { FormattedMessage } from 'react-intl';

import 'react-select/dist/react-select.css';
import './SmartSelect.css';

class SmartSelect extends React.Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.renderReadOnlyInput = this.renderReadOnlyInput.bind(this);
        this.renderDropdown = this.renderDropdown.bind(this);
    }

    onChange(event) {
        if (event) {
            this.props.onChange(event.value);
        } else {
            this.props.onChange(undefined);
        }
    }

    shouldComponentUpdate(nextProps) {
        return (
            this.props.options.length != nextProps.options.length ||
            this.props.value != nextProps.value
        );
    }

    renderReadOnlyInput() {
        let valueLabel = '';
        if (this.props.readOnly) {
            let selectedValue = this.props.options.filter(
                row => row.value == this.props.value
            )[0];
            valueLabel = selectedValue ? selectedValue.label : '';
        } else {
            valueLabel =
                this.props.options.length == 1
                    ? this.props.options[0].label
                    : '';
        }
        return (
            <div className={`form-group ${this.props.readOnlyClassName}`}>
                <div className="d-flex flex-column">
                    <div>
                        <label className={`${this.props.labelClassName}`}>
                            {this.props.label}
                        </label>
                    </div>
                    <div
                        className="form-control form-control-plain-text"
                        id={this.props.id}
                    >
                        {valueLabel}
                    </div>
                </div>
            </div>
        );
    }

    renderDropdown() {
        const options = this.props.options.slice();

        let selectValue = options.filter(
            row => this.props.value == row.value
        )[0];
        if (!selectValue) {
            selectValue = {
                value: null,
                label: <FormattedMessage id="select.empty" />,
            };
        }

        return (
            <div className={`form-group ${this.props.selectClassName}`}>
                <label
                    for={this.props.id}
                    className={`${this.props.labelClassName}`}
                >
                    {this.props.label}
                </label>
                <Select
                    searchable={false}
                    name={this.props.id}
                    value={selectValue}
                    options={options}
                    onChange={this.onChange}
                />
            </div>
        );
    }

    render() {
        if (!this.props.readOnly && this.props.options.length > 1) {
            return this.renderDropdown();
        } else {
            return this.renderReadOnlyInput();
        }
    }
}

SmartSelect.propTypes = {
    id: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    inputType: PropTypes.string,
    options: PropTypes.array.isRequired,
    value: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    selectClassName: PropTypes.string,
    readOnlyClassName: PropTypes.string,
    labelClassName: PropTypes.string,
    inputClassName: PropTypes.string,
    readOnly: PropTypes.boolean,
};

SmartSelect.defaultProps = {
    readOnlyClassName: '',
    selectClassName: '',
    labelClassName: '',
    inputClassName: '',
    value: '',
    inputType: 'text',
};

export default SmartSelect;
