import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { UserService } from '../services/';
import Selectors from '../selectors/';

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

class LimitedAccess extends Component {
    constructor(props) {
        super(props);
        this.hasPermissions = this.hasPermissions.bind(this);
    }

    hasPermissions() {
        if (this.props.requiredRoles) {
            return UserService.hasPermissions(
                this.props.currentUser,
                this.props.requiredRoles
            );
        }
        if (this.props.nonAllowedRoles) {
            return !UserService.hasPermissions(
                this.props.currentUser,
                this.props.nonAllowedRoles
            );
        }
        return false;
    }

    render() {
        let content = null;
        if (this.hasPermissions()) {
            content = <Fragment>{this.props.children}</Fragment>;
        }
        return content;
    }
}

LimitedAccess.propTypes = {
    requiredRoles: PropTypes.array,
    nonAllowedRoles: PropTypes.array,
};

export default connect(
    mapStateToProps,
    null
)(LimitedAccess);
