import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import Selectors from '../../selectors/';

const mapStateToProps = state => ({
    errors: Selectors.getAllErrors(state),
});

class ErrorPanel extends React.Component {
    render() {
        const errorsToRender = this.props.errors
            .filter(row => this.props.operationTypes.includes(row.operation))
            .map(error => (
                <div className="error">
                    <FormattedMessage
                        id={error.message ? error.message : error.operation}
                        values={error.error && error.error.descriptionSources}
                    />
                </div>
            ));
        return errorsToRender.length > 0 ? errorsToRender : null;
    }
}

ErrorPanel.propTypes = {
    operationTypes: PropTypes.array.isRequired,
};

ErrorPanel.defaultProps = [];

export default connect(
    mapStateToProps,
    null
)(ErrorPanel);
