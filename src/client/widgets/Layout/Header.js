import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { NavigationService } from '../../services/';
import { UserActions } from '../../actions';
import OnAirLogo from '../../styles/images/brivo_on_air_logo.png';
import Selectors from '../../selectors/';
import {
    ROLE_MASTER_ADMIN,
    ROLE_SUPER_ADMIN,
    ROLE_SENIOR_ADMIN,
    ROLE_INSTALLER,
} from '../../constants/roles';

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

class Header extends React.Component {
    constructor(props) {
        super(props);
        this.renderUserInfo = this.renderUserInfo.bind(this);
        this.getLogoClass = this.getLogoClass.bind(this);
    }

    getLogoClass() {
        const roles = !!this.props.currentUser.roles
            ? this.props.currentUser.roles
            : [];
        if (
            roles.includes(ROLE_MASTER_ADMIN) ||
            roles.includes(ROLE_INSTALLER)
        ) {
            return 'master';
        }
        if (roles.includes(ROLE_SENIOR_ADMIN)) {
            return 'senior';
        }
        return '';
    }

    renderUserInfo() {
        return (
            <div className="d-flex align-items-center">
                <div
                    className="user-name user-name-dropdown"
                    id="user-info-control"
                >
                    <i className={`user-avatar ${this.getLogoClass()}`} />
                    {this.props.currentUser.username}
                </div>
            </div>
        );
    }

    render() {
        return (
            <header
                className="header chrome-background-color d-flex justify-content-between"
                id="main-header"
            >
                <div className="logoContainer">
                    <a
                        id="hyperlink_logo"
                        href={NavigationService.buildLinkToLandingPage()}
                    >
                        <div className="logo">
                            <img
                                id="logo"
                                src={OnAirLogo}
                                alt="Brivo On Air logo"
                            />
                        </div>
                    </a>
                </div>
                <div className="d-flex justify-content-center align-items-center">
                    <div className="text-center chrome-text-color d-none d-sm-block d-md-block">
                        <b>
                            <FormattedMessage id="header.applicationName" />
                        </b>
                    </div>
                </div>
                {this.props.currentUser ? this.renderUserInfo() : <div />}
            </header>
        );
    }
}

Header.propTypes = {};
Header.defaultProps = {};

export default connect(
    mapStateToProps,
    null
)(Header);
