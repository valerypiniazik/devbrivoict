import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { SimpleButton } from '../Buttons/';

class DeleteConfirmationModal extends Component {
    render() {
        return (
            <Modal
                isOpen={this.props.visible}
                toggle={this.props.onCancel}
                className="modal-dialog-centered"
            >
                <ModalHeader
                    className="modal-title"
                    toggle={this.props.onCancel}
                >
                    <FormattedMessage id="removal.title" />
                </ModalHeader>
                <ModalBody className="modal-body wrapped-text-break-word">
                    {this.props.visible ? this.props.renderLabel() : null}
                    {this.props.warningMessage}
                </ModalBody>
                <ModalFooter>
                    <div
                        className="d-flex justify-content-center"
                        style={{ width: '100%' }}
                    >
                        <SimpleButton
                            id={`confirmModal_${this.props.id}`}
                            label={<FormattedMessage id="removal.ok" />}
                            onClick={this.props.onConfirm}
                        />
                        <SimpleButton
                            id={`rejectModal_${this.props.id}`}
                            label={<FormattedMessage id="removal.cancel" />}
                            onClick={this.props.onCancel}
                        />
                    </div>
                </ModalFooter>
            </Modal>
        );
    }
}

DeleteConfirmationModal.propTypes = {
    id: PropTypes.string.isRequired,
    onCancel: PropTypes.func.isRequired,
    onConfirm: PropTypes.func.isRequired,
    renderLabel: PropTypes.func.isRequired,
    visible: PropTypes.bool.isRequired,
    warningMessage: PropTypes.string,
};

export default DeleteConfirmationModal;
