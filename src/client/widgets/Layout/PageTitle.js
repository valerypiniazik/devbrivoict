import React from 'react';
import PropTypes from 'prop-types';

export const PageTitle = props => (
    <div className="topbar">
        <h1>
            <div className="d-flex flex-row justify-content-between">
                <div> {props.title}</div>
                {!!props.children ? (
                    <div class="refresh-token-status-wrapper">
                        {props.children}
                    </div>
                ) : null}
            </div>
        </h1>
    </div>
);

PageTitle.propTypes = {
    title: PropTypes.any.isRequired,
    children: PropTypes.node,
};
PageTitle.defaultProps = {};

export default PageTitle;
