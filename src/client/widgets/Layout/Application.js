import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Header from './Header';

export const Application = ({ children }) => (
    <Fragment>
        <Header />
        {children}
    </Fragment>
);

Application.propTypes = {
    children: PropTypes.node,
};

export default Application;
