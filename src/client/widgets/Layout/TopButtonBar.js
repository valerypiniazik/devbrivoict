import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TopButtonBar extends Component {
    render() {
        return (
            <div className="col sectionLinks button-group">
                <div className="button-container">{this.props.children}</div>
            </div>
        );
    }
}

TopButtonBar.propTypes = {};
TopButtonBar.defaultProps = {};

export default TopButtonBar;
