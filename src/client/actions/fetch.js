export function buildGetParams() {
    return buildParams('GET');
}

export function buildDeleteParams() {
    return buildParams('DELETE');
}

export function buildPostParams(body) {
    let params = buildParams('POST');
    params.body = JSON.stringify(body);
    return params;
}

export function buildPutParams(body) {
    let params = buildParams('PUT');
    params.body = JSON.stringify(body);
    return params;
}

export function handleErrors(response) {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response);
    } else {
        if (response.status == 404 || response.status == 401) {
            return Promise.reject({ status: response.status });
        }
        return response.json().then(result => {
            return Promise.reject(result);
        });
    }
}

function buildParams(method) {
    return {
        credentials: 'include',
        method: method,
        mode: 'cors',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        cache: 'default',
    };
}
