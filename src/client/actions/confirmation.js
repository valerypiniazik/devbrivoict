import { createAction } from 'redux-action';
import {
    SHOW_DELETE_CONFIRMATION,
    HIDE_DELETE_CONFIRMATION,
} from '../constants/confirmation';

const showDeleteConfirmation = createAction(SHOW_DELETE_CONFIRMATION);
const hideDeleteConfirmation = createAction(HIDE_DELETE_CONFIRMATION);

export default {
    showDeleteConfirmation(intercom) {
        return showDeleteConfirmation(intercom);
    },

    hideDeleteConfirmation() {
        return hideDeleteConfirmation();
    },
};
