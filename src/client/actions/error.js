import { createAction } from 'redux-action';
import { RAISE_ERROR, SUPPRESS_ERROR, AUTH_ERROR } from '../constants/error';

const raiseError = createAction(RAISE_ERROR);
const suppressError = createAction(SUPPRESS_ERROR);
const raiseAuthError = createAction(AUTH_ERROR);

export default {
    clearError(dispatch, operation) {
        dispatch(suppressError(operation));
    },

    clearErrorsByTypes(operations) {
        return dispatch => {
            if (operations) {
                operations.map(operation => {
                    const payload = { operation: operation };
                    dispatch(suppressError(payload));
                });
            }
        };
    },

    raiseError(dispatch, error) {
        if (
            !!error &&
            !!error.error &&
            !!error.error.status &&
            error.error.status == 401
        ) {
            dispatch(raiseAuthError(error));
        } else {
            dispatch(raiseError(error));
        }
    },
};
