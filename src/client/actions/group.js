import { createAction } from 'redux-action';

import { buildGetParams, handleErrors } from './fetch';

import { INIT_GROUPS } from '../constants/group';

const GROUPS_URL = '/api/groups';

const initGroups = createAction(INIT_GROUPS);

export default {
    getAllGroups() {
        return dispatch => {
            fetch(GROUPS_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(groups => {
                    dispatch(initGroups(groups));
                })
                .catch(error => {
                    console.log('Unable to fetch groups');
                });
        };
    },
};
