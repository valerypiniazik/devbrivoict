import {
    INIT_OPERATION,
    CLEAR_OPERATION,
    UPDATE_OPERATION,
} from '../constants/operation';

export default {
    initOperation(name, entity) {
        return {
            type: INIT_OPERATION,
            payload: {
                name: name,
                entity: entity,
            },
        };
    },

    clearOperation(name) {
        return { type: CLEAR_OPERATION, payload: { name: name } };
    },

    updateOperation(name, entity) {
        return {
            type: UPDATE_OPERATION,
            payload: {
                name: name,
                entity: entity,
            },
        };
    },
};
