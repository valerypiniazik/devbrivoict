import { createAction } from 'redux-action';
import { push } from 'react-router-redux';

import {
    buildGetParams,
    buildPostParams,
    buildDeleteParams,
    buildPutParams,
    handleErrors,
} from './fetch';

import {
    INTERCOM_LOADING,
    ALL_INTERCOMS_LOADING,
    INTERCOM_SAVING,
    INTERCOM_REMOVAL,
    INTERCOM_UPDATE,
    ALL_INTERCOM_TYPES_LOADING,
    INTERCOM_VALIDATION,
    INTERCOM_HISTORY_LOADING,
    INTERCOM_CONFIG_GENERATION,
} from '../constants/error';

import {
    INIT_INTERCOMS,
    INIT_TOKEN_STATUS,
    INIT_INTERCOM_TYPES,
    INIT_CURRENT_INTERCOM,
    INIT_INTERCOM_HISTORY,
} from '../constants/intercom';
import { INTERCOM, INTERCOM_TYPE } from '../constants/entity';
import ErrorActions from './error';
import OperationActions from './operation';
import { NavigationService } from '../services/';

const initIntercoms = createAction(INIT_INTERCOMS);
const initTokenStatus = createAction(INIT_TOKEN_STATUS);
const initIntercomTypes = createAction(INIT_INTERCOM_TYPES);
const initCurrentIntercom = createAction(INIT_CURRENT_INTERCOM);
const initIntercomHistory = createAction(INIT_INTERCOM_HISTORY);

const INTERCOMS_URL = '/api/intercoms';
const INTERCOM_TYPES_URL = '/api/types';
const TOKEN_STATUS_URL = '/api/account-info';

class IntercomAction {
    constructor() {
        this.deleteIntercom = this.deleteIntercom.bind(this);
        this.getAllIntercoms = this.getAllIntercoms.bind(this);
        this.getIntercomTypes = this.getIntercomTypes.bind(this);
        this.saveIntercom = this.saveIntercom.bind(this);
        this.updateIntercom = this.updateIntercom.bind(this);
        this.initCurrentIntercom = this.initCurrentIntercom.bind(this);
        this.cancelCurrentIntercom = this.cancelCurrentIntercom.bind(this);
        this.initIntercomHistory = this.initIntercomHistory.bind(this);
        this.downloadIntercomConfig = this.downloadIntercomConfig.bind(this);
        this.getCurrentTokenStatus = this.getCurrentTokenStatus.bind(this);
    }

    downloadIntercomConfig(intercom, history) {
        return dispatch => {
            history.push(
                NavigationService.buildLinkToIntercomHistoryPage(intercom.id)
            );
            fetch(`${INTERCOMS_URL}/${intercom.id}/config`, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    const header = response.headers.get('content-disposition');
                    let fileName;
                    if (!!header) {
                        fileName = header.replace('attachment; filename=', '');
                    }
                    if (!fileName) {
                        fileName = 'config_file';
                    }
                    return response.blob().then(data => {
                        return {
                            data: data,
                            fileName: fileName,
                        };
                    });
                })
                .then(responseDto => {
                    const a = document.createElement('a');
                    document.body.appendChild(a);
                    a.style = 'display: none';
                    const url = window.URL.createObjectURL(responseDto.data);
                    a.href = url;
                    a.download = responseDto.fileName;
                    a.click();
                    if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(
                            responseDto.data,
                            responseDto.fileName
                        );
                    } else {
                        window.URL.revokeObjectURL(url);
                    }
                    ErrorActions.clearError(dispatch, {
                        operation: INTERCOM_CONFIG_GENERATION,
                    });
                    this.initIntercomHistory(intercom.id, history, true)(
                        dispatch
                    );
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        error: error,
                        entity: INTERCOM,
                        operation: INTERCOM_CONFIG_GENERATION,
                        message: `history.error.message.${
                            error.errorType.name
                        }`,
                    });
                    this.initIntercomHistory(intercom.id, history, true)(
                        dispatch
                    );
                });
        };
    }

    showDeleteConfirmation(intercom) {
        return showDeleteConfirmation(intercom);
    }

    hideDeleteConfirmation() {
        return hideDeleteConfirmation;
    }

    reconnect() {
        return hideDeleteConfirmation;
    }

    initCurrentIntercom(intercom) {
        return dispatch => {
            dispatch(initCurrentIntercom(intercom));
        };
    }

    cancelCurrentIntercom() {
        return dispatch => {
            dispatch(initCurrentIntercom(null));
        };
    }

    cancelCurrentIntercomHistory() {
        return dispatch => {
            dispatch(initIntercomHistory(null));
        };
    }

    validate(dispatch, intercom) {
        if (!intercom.name) {
            ErrorActions.raiseError(dispatch, {
                entity: INTERCOM,
                operation: INTERCOM_VALIDATION,
                message: 'intercoms.validation.no_name',
            });
            return false;
        }

        if (!intercom.typeId) {
            ErrorActions.raiseError(dispatch, {
                entity: INTERCOM,
                operation: INTERCOM_VALIDATION,
                message: 'intercoms.validation.no_type',
            });
            return false;
        }
        if (!intercom.macAddress) {
            ErrorActions.raiseError(dispatch, {
                entity: INTERCOM,
                operation: INTERCOM_VALIDATION,
                message: 'intercoms.validation.no_mac',
            });
            return false;
        } else {
            if (
                !/^[0-9a-f]{1,2}([\.:-])(?:[0-9a-f]{1,2}\1){4}[0-9a-f]{1,2}$/i.test(
                    intercom.macAddress
                )
            ) {
                ErrorActions.raiseError(dispatch, {
                    entity: INTERCOM,
                    operation: INTERCOM_VALIDATION,
                    message: 'intercoms.validation.invalid_mac',
                });
                return false;
            }
        }

        const switchCodeValueValidation =
            !!intercom.switchCodes &&
            intercom.switchCodes.map(
                row =>
                    Number(row.code) < 0 || !Number.isInteger(Number(row.code))
            );
        if (switchCodeValueValidation.reduce((a, b) => a || b, false)) {
            ErrorActions.raiseError(dispatch, {
                entity: INTERCOM,
                operation: INTERCOM_VALIDATION,
                message: 'intercoms.validation.invalid_switch_code',
            });
            return false;
        }
        const switchCodeSizeValidation =
            !!intercom.switchCodes &&
            intercom.switchCodes.map(row => row.code.length < 4);

        if (switchCodeSizeValidation.reduce((a, b) => a || b, false)) {
            ErrorActions.raiseError(dispatch, {
                entity: INTERCOM,
                operation: INTERCOM_VALIDATION,
                message: 'intercoms.validation.too_small_switch_code',
            });
            return false;
        }
        return true;
    }

    saveIntercom(intercom, history) {
        return dispatch => {
            if (intercom && this.validate(dispatch, intercom)) {
                fetch(`${INTERCOMS_URL}`, buildPostParams(intercom))
                    .then(handleErrors)
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        this.getAllIntercoms()(dispatch);
                        history.push(
                            NavigationService.buildLinkToIntercomsPage()
                        );
                        dispatch(OperationActions.clearOperation(INTERCOM));
                        ErrorActions.clearErrorsByTypes([
                            INTERCOM_SAVING,
                            INTERCOM_VALIDATION,
                        ])(dispatch);
                    })
                    .catch(error => {
                        if (error.status == 409) {
                            ErrorActions.raiseError(dispatch, {
                                entity: INTERCOM,
                                operation: INTERCOM_VALIDATION,
                                message: 'intercoms.error.duplicated_mac',
                            });
                        } else {
                            ErrorActions.raiseError(dispatch, {
                                entity: INTERCOM,
                                operation: INTERCOM_SAVING,
                            });
                        }
                    });
            }
        };
    }

    updateIntercom(intercom, history) {
        return dispatch => {
            if (intercom && this.validate(dispatch, intercom)) {
                fetch(
                    `${INTERCOMS_URL}/${intercom.id}`,
                    buildPutParams(intercom)
                )
                    .then(handleErrors)
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        this.getAllIntercoms()(dispatch);
                        history.push(
                            NavigationService.buildLinkToIntercomsPage()
                        );
                        dispatch(OperationActions.clearOperation(INTERCOM));
                        ErrorActions.clearErrorsByTypes([
                            INTERCOM_UPDATE,
                            INTERCOM_VALIDATION,
                        ])(dispatch);
                    })
                    .catch(error => {
                        console.log('Unable to update intercom', error);
                        if (error.status == 409) {
                            ErrorActions.raiseError(dispatch, {
                                entity: INTERCOM,
                                operation: INTERCOM_VALIDATION,
                                message: 'intercoms.error.duplicated_mac',
                            });
                        } else {
                            ErrorActions.raiseError(dispatch, {
                                entity: INTERCOM,
                                operation: INTERCOM_UPDATE,
                            });
                        }
                    });
            }
        };
    }

    deleteIntercom(intercomId) {
        if (intercomId) {
            return dispatch => {
                fetch(`${INTERCOMS_URL}/${intercomId}`, buildDeleteParams())
                    .then(handleErrors)
                    .then(data => {
                        this.getAllIntercoms()(dispatch);
                        ErrorActions.clearError(dispatch, {
                            entity: INTERCOM,
                        });
                    })
                    .catch(error => {
                        ErrorActions.raiseError(dispatch, {
                            entity: INTERCOM,
                            operation: INTERCOM_REMOVAL,
                            error: error,
                        });
                    });
            };
        }
    }

    getAllIntercoms() {
        return dispatch => {
            fetch(INTERCOMS_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initIntercoms(data));
                    ErrorActions.clearError(dispatch, {
                        operation: ALL_INTERCOMS_LOADING,
                    });
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        entity: INTERCOM,
                        operation: ALL_INTERCOMS_LOADING,
                        error: error,
                    });
                });
        };
    }

    loadIntercom(intercomId, history) {
        return dispatch => {
            fetch(`${INTERCOMS_URL}/${intercomId}`, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    initCurrentIntercom(data)(dispatch);
                    dispatch(OperationActions.initOperation(INTERCOM, data));
                    ErrorActions.clearError(dispatch, {
                        operation: INTERCOM_LOADING,
                    });
                })
                .catch(error => {
                    history.push(NavigationService.buildLinkToIntercomsPage());
                    ErrorActions.raiseError(dispatch, {
                        id: intercomId,
                        entity: INTERCOM,
                        operation: INTERCOM_LOADING,
                        error: error,
                    });
                });
        };
    }

    initIntercomHistory(intercomId, history, keepError) {
        return dispatch => {
            fetch(
                `${INTERCOMS_URL}/${intercomId}/history?size=50&sort=initiatedAt,desc`,
                buildGetParams()
            )
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initIntercomHistory(data));
                    if (!keepError) {
                        ErrorActions.clearError(dispatch, {
                            operation: INTERCOM_HISTORY_LOADING,
                        });
                        ErrorActions.clearError(dispatch, {
                            operation: INTERCOM_CONFIG_GENERATION,
                        });
                    }
                })
                .catch(error => {
                    console.log('Error loading intercom history', error);
                    history.push(NavigationService.buildLinkToIntercomsPage());
                    ErrorActions.raiseError(dispatch, {
                        id: intercomId,
                        operation: INTERCOM_HISTORY_LOADING,
                        error: error,
                    });
                });
        };
    }

    getIntercomTypes() {
        return dispatch => {
            fetch(INTERCOM_TYPES_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initIntercomTypes(data._embedded.types));
                    ErrorActions.clearError(dispatch, {
                        entity: INTERCOM_TYPE,
                    });
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        entity: INTERCOM_TYPE,
                        operation: ALL_INTERCOM_TYPES_LOADING,
                        error: error,
                    });
                });
        };
    }

    getCurrentTokenStatus() {
        return dispatch => {
            fetch(TOKEN_STATUS_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    if (data.exist) {
                        dispatch(initTokenStatus(data));
                    } else {
                        dispatch(initTokenStatus(null));
                    }
                })
                .catch(error => {
                    dispatch(initTokenStatus(null));
                });
        };
    }
}

const instance = new IntercomAction();
export default instance;
