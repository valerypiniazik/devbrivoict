import { createAction } from 'redux-action';

import {
    buildGetParams,
    buildDeleteParams,
    buildPostParams,
    buildPutParams,
    handleErrors,
} from './fetch';
import ErrorActions from './error';
import OperationActions from './operation';
import { NavigationService } from '../services/';

import {
    ALL_DIRECTORY_RULES_LOADING,
    DIRECTORY_RULE_LOADING,
    DIRECTORY_RULE_REMOVAL,
    DIRECTORY_RULE_SAVING,
    DIRECTORY_RULE_UPDATE,
    DIRECTORY_RULE_VALIDATION,
} from '../constants/error';
import { INIT_DIRECTORY_RULES } from '../constants/directoryRules';
import { DIRECTORY_RULE } from '../constants/entity';

const initAll = createAction(INIT_DIRECTORY_RULES);
const GROUP_CONTACT_URL = '/api/group-configs';

class DirectoryRulesAction {
    constructor() {
        this.remove = this.remove.bind(this);
        this.save = this.save.bind(this);
        this.update = this.update.bind(this);
        this.loadAllByIntercom = this.loadAllByIntercom.bind(this);
        this.loadById = this.loadById.bind(this);
        this.validate = this.validate.bind(this);
    }

    validate(dispatch, rule) {
        if (
            !rule.intercomGroupContactConfigs ||
            rule.intercomGroupContactConfigs.length == 0
        ) {
            ErrorActions.raiseError(dispatch, {
                entity: DIRECTORY_RULE,
                operation: DIRECTORY_RULE_VALIDATION,
                message: 'rules.validation.no_contact_types',
            });
            return false;
        } else {
            const allContactTypes = rule.intercomGroupContactConfigs.map(
                row => row.brivoContactType
            );
            const distinctContactTypes = allContactTypes.filter(
                (value, index, self) => self.indexOf(value) === index
            );
            if (allContactTypes.length != distinctContactTypes.length) {
                ErrorActions.raiseError(dispatch, {
                    entity: DIRECTORY_RULE,
                    operation: DIRECTORY_RULE_VALIDATION,
                    message: 'rules.validation.duplicated_contact_type',
                });
                return false;
            }
        }
        if (!rule.brivoGroupId) {
            ErrorActions.raiseError(dispatch, {
                entity: DIRECTORY_RULE,
                operation: DIRECTORY_RULE_VALIDATION,
                message: 'rules.validation.no_group',
            });
            return false;
        }
        if (!rule.formatString) {
            ErrorActions.raiseError(dispatch, {
                entity: DIRECTORY_RULE,
                operation: DIRECTORY_RULE_VALIDATION,
                message: 'rules.validation.no_format_string',
            });
            return false;
        }
        return true;
    }

    remove(directoryRuleId, intercomId) {
        if (directoryRuleId) {
            return dispatch => {
                fetch(
                    `${GROUP_CONTACT_URL}/${directoryRuleId}`,
                    buildDeleteParams()
                )
                    .then(handleErrors)
                    .then(data => {
                        this.loadAllByIntercom(intercomId)(dispatch);
                        ErrorActions.clearError(dispatch, {
                            entity: DIRECTORY_RULE,
                        });
                    })
                    .catch(error => {
                        ErrorActions.raiseError(dispatch, {
                            entity: DIRECTORY_RULE,
                            error: error,
                        });
                    });
            };
        }
    }

    loadAllByIntercom(intercomId) {
        return dispatch => {
            fetch(
                `/api/intercoms/${intercomId}/group-configs`,
                buildGetParams()
            )
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initAll(data));
                    ErrorActions.clearError(dispatch, {
                        operation: ALL_DIRECTORY_RULES_LOADING,
                    });
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        operation: ALL_DIRECTORY_RULES_LOADING,
                        error: error,
                    });
                });
        };
    }

    loadById(intercomId, groupConfigId, history) {
        return dispatch => {
            fetch(`${GROUP_CONTACT_URL}/${groupConfigId}`, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(
                        OperationActions.initOperation(DIRECTORY_RULE, data)
                    );
                    ErrorActions.clearError(dispatch, {
                        operation: DIRECTORY_RULE_LOADING,
                    });
                })
                .catch(error => {
                    console.log('Error loading directory rule', error);
                    history.push(
                        NavigationService.buildLinkToIntercomDirectoryPage(
                            intercomId
                        )
                    );
                    ErrorActions.raiseError(dispatch, {
                        id: groupConfigId,
                        operation: DIRECTORY_RULE_LOADING,
                        error: error,
                    });
                });
        };
    }

    save(intercomId, rule, history) {
        return dispatch => {
            if (rule && this.validate(dispatch, rule)) {
                rule.intercomId = intercomId;

                fetch(`${GROUP_CONTACT_URL}`, buildPostParams(rule))
                    .then(handleErrors)
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        this.loadAllByIntercom(intercomId)(dispatch);
                        history.push(
                            NavigationService.buildLinkToIntercomDirectoryPage(
                                intercomId
                            )
                        );
                        dispatch(
                            OperationActions.clearOperation(DIRECTORY_RULE)
                        );
                        ErrorActions.clearError(dispatch, {
                            operation: DIRECTORY_RULE_SAVING,
                        });
                    })
                    .catch(error => {
                        console.log('Unable to save directory rule', error);
                        ErrorActions.raiseError(dispatch, {
                            error: error,
                            entity: DIRECTORY_RULE,
                            operation: DIRECTORY_RULE_SAVING,
                        });
                    });
            }
        };
    }

    update(intercomId, rule, history) {
        return dispatch => {
            if (rule && this.validate(dispatch, rule)) {
                rule.intercomId = intercomId;

                fetch(`${GROUP_CONTACT_URL}/${rule.id}`, buildPutParams(rule))
                    .then(handleErrors)
                    .then(response => {
                        return response.json();
                    })
                    .then(data => {
                        this.loadAllByIntercom(intercomId)(dispatch);
                        history.push(
                            NavigationService.buildLinkToIntercomDirectoryPage(
                                intercomId
                            )
                        );
                        dispatch(
                            OperationActions.clearOperation(DIRECTORY_RULE)
                        );
                        ErrorActions.clearError(dispatch, {
                            operation: DIRECTORY_RULE_UPDATE,
                        });
                    })
                    .catch(error => {
                        console.log('Unable to update directory rule', error);
                        ErrorActions.raiseError(dispatch, {
                            entity: DIRECTORY_RULE,
                            operation: DIRECTORY_RULE_UPDATE,
                            error: error,
                        });
                    });
            }
        };
    }
}

const instance = new DirectoryRulesAction();
export default instance;
