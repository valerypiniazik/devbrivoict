import ErrorActions from './error';
import IntercomActions from './intercom';
import { createAction } from 'redux-action';
import { INIT_CURRENT_USER } from '../constants/user';
import { buildGetParams, handleErrors, buildPostParams } from './fetch';
const REQUEST_CONFIG = {
    headers: new Headers({
        'Content-Type': 'application/json',
    }),
};

const RECONNECT_ACCOUNT_INFO_URL = '/api/account-info';
const CURRENT_USER_URL = '/api/users/current';
const LOGOUT_URL = '/logout';

const initCurrentUser = createAction(INIT_CURRENT_USER);

export default {
    reconnectUser() {
        return dispatch => {
            fetch(RECONNECT_ACCOUNT_INFO_URL, buildPostParams())
                .then(handleErrors)
                .then(onceDone => {
                    dispatch(IntercomActions.getCurrentTokenStatus());
                })
                .catch(error => {
                    console.log(
                        'Unable to refresh intercom token using current user',
                        error
                    );
                    ErrorActions.raiseError(dispatch, error);
                });
        };
    },

    logout() {
        return dispatch => {
            dispatch(initCurrentUser(null));
            /* fetch(LOGOUT_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    dispatch(initCurrentUser(null));
                })
                .catch(error => {
                    console.log('Unable to perform logout', error);
                    ErrorActions.raiseError(dispatch, error);
                });*/
        };
    },

    getCurrentUser() {
        return dispatch => {
            fetch(CURRENT_USER_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initCurrentUser(data));
                })
                .catch(error => {
                    console.log(
                        'Unable to get details of current user intercom token using current user',
                        error
                    );
                    ErrorActions.raiseError(dispatch, error);
                });
        };
    },
};
