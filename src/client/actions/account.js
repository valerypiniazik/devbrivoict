import { createAction } from 'redux-action';
import { ALL_ACCOUNTS_LOADING } from '../constants/error';

import { buildGetParams, handleErrors } from './fetch';

import { ACCOUNT } from '../constants/entity';
import {
    INIT_ACCOUNTS,
    SELECT_ACCOUNT,
    CHANGE_ACCOUNTS_FILTER,
    FINISH_ACCOUNT_FILTERING,
    START_ACCOUNT_FILTERING,
    INIT_CURRENT_ACCOUNT_STATUS,
} from '../constants/account';
import ErrorActions from './error';

const ACCOUNTS_URL = '/api/accounts';
const ACCOUNT_STATUS_URL = '/api/account-info/status';

const initAccounts = createAction(INIT_ACCOUNTS);
const selectAccount = createAction(SELECT_ACCOUNT);
const changeAccountsFilter = createAction(CHANGE_ACCOUNTS_FILTER);
const finishAccountFiltering = createAction(FINISH_ACCOUNT_FILTERING);
const initAccountStatus = createAction(INIT_CURRENT_ACCOUNT_STATUS);
const startAccountFiltering = createAction(START_ACCOUNT_FILTERING);

function buildAccountSearchUrl(accountNumber, accountName) {
    const accountNumberPart = !!accountNumber
        ? 'accountNumber=' + accountNumber + '&'
        : '';
    const accountNamePart = !!accountName ? 'name=' + accountName : '';
    return ACCOUNTS_URL + '?' + accountNumberPart + accountNamePart;
}

export default {
    getAllAccounts() {
        return dispatch => {
            fetch(ACCOUNTS_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initAccounts(data));
                    ErrorActions.clearError(dispatch, {
                        operation: ALL_ACCOUNTS_LOADING,
                    });
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        entity: ACCOUNT,
                        operation: ALL_ACCOUNTS_LOADING,
                        error: error,
                    });
                });
        };
    },

    filterAccounts(accountNumber, accountName) {
        return dispatch => {
            dispatch(startAccountFiltering());
            fetch(
                buildAccountSearchUrl(accountNumber, accountName),
                buildGetParams()
            )
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(finishAccountFiltering(data));
                    ErrorActions.clearError(dispatch, {
                        operation: ALL_ACCOUNTS_LOADING,
                    });
                })
                .catch(error => {
                    ErrorActions.raiseError(dispatch, {
                        entity: ACCOUNT,
                        operation: ALL_ACCOUNTS_LOADING,
                        error: error,
                    });
                });
        };
    },

    getAccountStatus() {
        return dispatch => {
            fetch(ACCOUNT_STATUS_URL, buildGetParams())
                .then(handleErrors)
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    dispatch(initAccountStatus(data));
                })
                .catch(error => {});
        };
    },

    changeAccountFilter(filterValue) {
        return changeAccountsFilter(filterValue);
    },

    selectAccount(accountId) {
        return selectAccount(accountId);
    },
};
