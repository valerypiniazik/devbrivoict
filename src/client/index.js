/** APPLICATION ENTRY POINT */
import 'babel-polyfill';
import 'whatwg-fetch';
import 'intl';
import React from 'react';
import ReactDOM from 'react-dom';
import { CookiesProvider } from 'react-cookie';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';

import locale from './i18n/';
import './setup/fontAwesome';
import './styles/';
import { store } from './setup/store';

// components
import ApplicationEntry from './ApplicationEntry';

ReactDOM.render(
    <Provider store={store} key="provider">
        <IntlProvider
            locale={locale.currentLocale}
            messages={locale.localeWording}
        >
            <CookiesProvider>
                <BrowserRouter>
                    <ApplicationEntry />
                </BrowserRouter>
            </CookiesProvider>
        </IntlProvider>
    </Provider>,
    document.getElementById('mount-root')
);

// enable debugging
if (process.env.NODE_ENV !== 'production') {
    window.React = React;
}
