import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faMinusCircle,
    faPencilAlt,
    faSearch,
    faChevronDown,
    faChevronUp,
} from '@fortawesome/free-solid-svg-icons';

library.add(faMinusCircle, faPencilAlt, faSearch, faChevronDown, faChevronUp);
