import { createReducer } from 'redux-action';
import { INIT_GROUPS } from '../constants/group';

const initialState = {
    all: [],
};

export default createReducer(initialState, {
    [INIT_GROUPS]: allGroups => ({
        all: allGroups,
    }),
});
