import { createReducer } from 'redux-action';

import {
    SHOW_DELETE_CONFIRMATION,
    HIDE_DELETE_CONFIRMATION,
} from '../constants/confirmation';

const initialState = {
    isVisible: false,
    confirmationEntity: null,
};

export default createReducer(initialState, {
    [SHOW_DELETE_CONFIRMATION]: entity => {
        return {
            confirmationEntity: entity,
            isVisible: true,
        };
    },
    [HIDE_DELETE_CONFIRMATION]: () => {
        return {
            confirmationEntity: null,
            isVisible: false,
        };
    },
});
