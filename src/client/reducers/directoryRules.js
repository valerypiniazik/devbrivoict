import { createReducer } from 'redux-action';
import { INIT_DIRECTORY_RULES } from '../constants/directoryRules';

const initialState = { all: [] };

export default createReducer(initialState, {
    [INIT_DIRECTORY_RULES]: allRules => ({
        all: allRules,
    }),
});
