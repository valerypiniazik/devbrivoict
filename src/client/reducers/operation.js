import {
    INIT_OPERATION,
    CLEAR_OPERATION,
    UPDATE_OPERATION,
} from '../constants/operation';

const initialState = { operations: {} };

function reducer(state = initialState, action) {
    switch (action.type) {
        case INIT_OPERATION: {
            const updatedOperations = Object.assign({}, state.operations);
            updatedOperations[action.payload.name] = {
                original: Object.assign({}, action.payload.entity),
                current: Object.assign({}, action.payload.entity),
            };
            return Object.assign({}, state, {
                operations: updatedOperations,
            });
        }
        case CLEAR_OPERATION: {
            const updatedOperations = Object.assign({}, state.operations);
            updatedOperations[action.payload.name] = undefined;
            return Object.assign({}, state, {
                operations: updatedOperations,
            });
        }

        case UPDATE_OPERATION: {
            const updatedOperations = Object.assign({}, state.operations);
            updatedOperations[action.payload.name].current = Object.assign(
                {},
                action.payload.entity
            );
            return Object.assign({}, state, {
                operations: updatedOperations,
            });
        }
        default: {
            return state;
        }
    }
}

module.exports = reducer;
