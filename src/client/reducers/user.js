import { createReducer } from 'redux-action';

import { INIT_CURRENT_USER } from '../constants/user';

const initialState = {
    current: null,
};

export default createReducer(initialState, {
    [INIT_CURRENT_USER]: currentUser => {
        return {
            current: currentUser,
        };
    },
});
