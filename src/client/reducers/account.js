import {
    INIT_ACCOUNTS,
    SELECT_ACCOUNT,
    CHANGE_ACCOUNTS_FILTER,
    START_ACCOUNT_FILTERING,
    FINISH_ACCOUNT_FILTERING,
    INIT_CURRENT_ACCOUNT_STATUS,
} from '../constants/account';

const initialState = {
    accounts: null,
    totalCount: null,
    current: null,
    status: null,
    rawAccounts: null,
    filterValue: {},
    isSearchInitiated: false,
    isSearchInProgress: false,
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case INIT_ACCOUNTS: {
            const isSingleAccount =
                action.payload &&
                action.payload.data &&
                action.payload.data.length == 1;

            const currentAccount = isSingleAccount
                ? action.payload.data[0].id
                : null;
            if (isSingleAccount) {
                return Object.assign({}, state, {
                    accounts: action.payload.data,
                    current: currentAccount,
                });
            }

            if (
                action.payload &&
                action.payload.data &&
                action.payload.data.length != 1
            ) {
                return Object.assign({}, state, {
                    accounts: null,
                    current: null,
                    totalCount: action.payload.count,
                    rawAccounts: action.payload.data,
                });
            }
            return state;
        }
        case CHANGE_ACCOUNTS_FILTER: {
            return Object.assign({}, state, {
                filterValue: action.payload,
            });
        }
        case START_ACCOUNT_FILTERING: {
            return Object.assign({}, state, {
                isSearchInProgress: true,
            });
        }
        case INIT_CURRENT_ACCOUNT_STATUS: {
            return Object.assign({}, state, {
                status: action.payload,
            });
        }
        case FINISH_ACCOUNT_FILTERING: {
            return Object.assign({}, state, {
                accounts: action.payload.data,
                totalCount: action.payload.count,
                isSearchInitiated: true,
                isSearchInProgress: false,
            });
        }
        case SELECT_ACCOUNT: {
            return Object.assign({}, state, {
                current: action.payload,
            });
        }
        default: {
            return state;
        }
    }
}

module.exports = reducer;
