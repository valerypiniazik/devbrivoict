import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import intercom from './intercom';
import operation from './operation';
import account from './account';
import confirmation from './confirmation';
import user from './user';
import error from './error';
import directoryRules from './directoryRules';
import group from './group';

export default combineReducers({
    confirmation,
    intercom,
    operation,
    account,
    user,
    error,
    group,
    directoryRules,
    routing: routerReducer,
});

export function getCurrentUser(state) {
    return state.user.current;
}
