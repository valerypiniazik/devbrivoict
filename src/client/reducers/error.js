import { RAISE_ERROR, SUPPRESS_ERROR, AUTH_ERROR } from '../constants/error';

const initialState = {
    errors: [],
};

function reducer(state = initialState, action) {
    switch (action.type) {
        case AUTH_ERROR: {
            window.location.assign('/');
            return state;
        }
        case RAISE_ERROR: {
            const payload = Object.assign({}, action.payload);
            const updatedErrors = state.errors.slice();
            const filteredByOperationType = updatedErrors.filter(
                row => row.operation != action.payload.operation
            );
            filteredByOperationType.unshift(payload);
            return Object.assign({}, state, {
                errors: filteredByOperationType,
            });
        }
        case SUPPRESS_ERROR: {
            const filteredByOperationType = action.payload.operation
                ? state.errors.filter(
                      row => row.operation != action.payload.operation
                  )
                : state.errors;
            const filteredByEntity = action.payload.entity
                ? filteredByOperationType.filter(
                      row => row.entity != action.payload.entity
                  )
                : filteredByOperationType;
            return Object.assign({}, state, {
                errors: filteredByEntity,
            });
        }
        default: {
            return state;
        }
    }
}

module.exports = reducer;
