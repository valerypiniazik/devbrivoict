import { createReducer } from 'redux-action';

import {
    INIT_INTERCOMS,
    INIT_INTERCOM_TYPES,
    INIT_CURRENT_INTERCOM,
    INIT_INTERCOM_HISTORY,
    INIT_TOKEN_STATUS,
} from '../constants/intercom';

const initialState = {
    intercoms: null,
    current: null,
    types: [],
    currentIntercomHistory: null,
    tokenStatus: null,
};

export default createReducer(initialState, {
    [INIT_INTERCOMS]: intercoms => ({
        intercoms: intercoms,
    }),
    [INIT_INTERCOM_TYPES]: intercomTypes => ({
        types: intercomTypes,
    }),
    [INIT_CURRENT_INTERCOM]: intercom => ({
        current: intercom,
    }),
    [INIT_TOKEN_STATUS]: tokenStatus => ({
        tokenStatus: tokenStatus,
    }),
    [INIT_INTERCOM_HISTORY]: intercomHistory => ({
        currentIntercomHistory: !!intercomHistory
            ? intercomHistory.content
            : null,
    }),
});
