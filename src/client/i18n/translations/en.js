const translations = {
    'intercoms.all.title': 'Audio Device List',
    'intercoms.all.header_message_valid':
        'Connections to the listed devices were last established by {username} on {date}.',
    'intercoms.all.header_message_invalid':
        'Connections to the listed devices are no longer valid. The previous connections were established by {username} on {date}. To restore service, reconnect using the button to the right.',
    'intercoms.all.header_message_invalid_csr':
        'Connections to the listed devices were last established by {username} on {date} and are no longer valid. An account administrator must reconnect the connections to restore service.',
    'intercoms.all.create': 'Add Device',
    'intercoms.all.history': 'View History >>',
    'intercoms.all.edit': 'Edit Device',
    'intercoms.all.view': 'View Device',
    'intercoms.all.define': 'Define',
    'intercoms.all.viewDefinition': 'View Definition',
    'intercoms.all.delete': 'Delete',
    'intercoms.all.empty': 'No audio devices configured.',
    'intercoms.all.nameColumn': 'Name',
    'intercoms.all.modelColumn': 'Model',
    'intercoms.all.historyColumn': 'History',
    'intercoms.all.deviceColumn': 'Device',
    'intercoms.all.directoryColumn': 'Directory',
    'intercoms.all.deleteColumn': 'Delete',
    'intercoms.reconnect': 'Reconnect',
    'intercoms.form.name': 'Device Name',
    'intercoms.form.model': 'Model',
    'intercoms.form.password': 'Password',
    'intercoms.form.regenerate': 'Regenerate',
    'intercoms.form.mac': 'MAC Address',
    'intercoms.edit.title': 'Edit Audio Device',
    'intercoms.edit.csrTitle': 'View Audio Device',
    'intercoms.form.cancel': 'Cancel',
    'intercoms.form.test': 'Generate Config File',
    'intercoms.form.viewHistory': 'View History',
    'intercoms.form.save': 'Save Device',
    'intercoms.new.title': 'Add Audio Device',
    'intercoms.all.confirmRemoval': 'Delete Audio Device {name} ?',

    'intercoms.form.switch1Value': 'Code for Switch 0',
    'intercoms.form.switch2Value': 'Code for Switch 1',
    'intercoms.form.switch3Value': 'Code for Switch 2',
    'intercoms.form.switch4Value': 'Code for Switch 3',
    'intercoms.form.switch1Description': 'Description',
    'intercoms.form.switch2Description': 'Description',
    'intercoms.form.switch3Description': 'Description',
    'intercoms.form.switch4Description': 'Description',

    'intercoms.validation.no_name':
        'Invalid device name. Names must be less than 255 characters and cannot contain special characters.',
    'intercoms.validation.no_type': 'Invalid device type.',
    'intercoms.error.duplicated_mac':
        'Mac address is already used by another device',
    'intercoms.validation.no_mac': 'Invalid MAC address.',
    'intercoms.validation.invalid_mac': 'Invalid MAC address.',
    'intercoms.validation.invalid_switch_code': 'Switch codes must be numbers.',
    'intercoms.validation.no_switch_codes':
        'Device must have at least 1 switch code',
    'intercoms.validation.too_small_switch_code':
        'Switch codes must have at least 4 digits.',
    'intercoms.error.all_loading':
        'Device list unavailable. Contact Brivo Customer Care for assistance.',
    'intercoms.error.single_loading':
        'One or more devices could not be retrieved. Contact Brivo Customer Care for assistance.',
    'intercoms.error.saving':
        'The device could not be saved. Please try again. If problem persists, please contact Brivo Customer Care.',
    'intercoms.error.update':
        'The device could not be saved. Please try again. If problem persists, please contact Brivo Customer Care.',
    'intercoms.error.removal':
        'The device could not be removed. Please try again. If problem persists, please contact Brivo Customer Care.',
    'intercoms.error.config_generation':
        'Unable to generate Configuration file',
    'intercoms.error.no_mac': 'Mac Address is required',
    'models.error.all_loading': 'Unable to retrieve device model information.',

    'accounts.all.title': 'Account Search',
    'accounts.filter.id': 'Account ID',
    'accounts.filter.name': 'Account Name',
    'accounts.filter.search': 'Search',
    'accounts.filter.searchResult': 'Search Results',
    'accounts.all.idColumn': 'Account ID',
    'accounts.all.nameColumn': 'Account Name',
    'accounts.all.view': 'View',
    'accounts.all.resultsTotal': 'Showing {amount} results of {total_amount}',

    'accounts.error.all_loading': 'Unable to retrieve account information.',
    'history.error.all_loading':
        'Device history unavailable. Contact Brivo Customer Care for assistance.',

    'rules.all.title': 'Edit Directory Rule',
    'rules.all.csrTitle': 'View Directory Rule',
    'rules.all.add': 'Add Rule',
    'rules.all.cancel': 'Go To Device List',
    'rules.all.confirmRemoval': 'Delete Directory Rule {groupName}?',

    'rules.form.group': 'Group',
    'rules.form.pattern': 'Listing Pattern',
    'rules.form.patternTooltip':
        'Specify the pattern to be used for entries in the directory list on the device. To include the value of a variable, surround the variable name with ${ and }. Possible variables are: firstName, middleName, lastName, and custom fields using the field name prefixed with "c_". For example: ${c_companyName} : ${lastName}, ${firstName} could result in a listing like: "Brivo : Smith, John"',
    'rules.form.firstPhoneType': 'First Phone Type',
    'rules.form.secondPhoneType': 'Second Phone Type',
    'rules.form.thirdPhoneType': 'Third Phone Type',
    'rules.form.intercomTitle': 'Applied to : {name}',
    'rules.form.cancel': 'Cancel',
    'rules.form.save': 'Save Rule',
    'rules.edit.title': 'Edit Directory Rules',
    'rules.edit.csrTitle': 'View Directory Rules',
    'rules.new.title': 'New Directory Rule',

    'rules.all.wrongGroup':
        'The group associated with this rule is no longer available. Please edit the rule.',
    'rules.all.edit': 'Edit Rule',
    'rules.all.delete': 'Delete',
    'rules.all.groupColumn': 'Based on Group',
    'rules.all.listingPatternColumn': 'Listing Definition',
    'rules.all.firstPhoneTypeColumn': 'First Phone',
    'rules.all.secondPhoneTypeColumn': 'Second Phone',
    'rules.all.thirdPhoneTypeColumn': 'Third Phone',
    'rules.all.empty': 'No directory rules defined.',
    'rules.all.editColumn': 'Edit',
    'rules.all.viewColumn': 'View',
    'rules.all.deleteColumn': 'Delete',
    'rules.all.intercomLabel': 'Rules will be applied to: {name}',
    'rules.validation.no_group': 'Select group to associate with this rule.',
    'rules.validation.no_contact_types': 'Unknown phone type.',
    'rules.validation.duplicated_contact_type':
        'A phone type may only be used once.',
    'rules.validation.no_format_string':
        'Define the directory listing pattern associated with this group.',
    'rules.error.all_loading':
        'Rules for device unavailable. Contact Brivo Customer Care for assistance.',
    'rules.error.single_loading':
        'One or more rules could not be retrieved. Contact Brivo Customer Care for assistance.',
    'rules.error.saving':
        'The rule could not be saved. Please try again. If problem persists, please contact Brivo Customer Care.',
    'rules.error.update':
        'The rule could not be saved. Please try again. If problem persists, please contact Brivo Customer Care.',
    'rules.error.removal':
        'The rule could not be removed. Please try again. If problem persists, contact Brivo Customer Care.',

    'history.all.title': 'Configuration File History',
    'history.all.cancel': '<< Back',
    'history.all.refresh': 'Refresh',

    'history.all.intercomTitle': 'File Generation History for: {name}',
    'history.all.empty':
        'Configuration file has not been generated for this device.',
    'history.all.timeColumn': 'Execution Time',
    'history.all.statusColumn': 'Status',
    'history.all.detailsColumn': 'Details',
    'history.all.isTestColumn': 'Type',
    'history.all.statusPending': 'IN PROGRESS',
    'history.all.statusOk': 'OK',
    'history.all.statusAmountOfWarnings': '{amount} WARNING(S)',
    'history.all.statusWarning': 'WARNING',
    'history.all.viewLogs': 'View all warnings',
    'history.all.statusError': 'ERROR',
    'history.all.testGeneration': 'User Requested',
    'history.all.nonTestGeneration': 'Scheduled',
    'history.error.message.INVALID_VARIABLE_NAMES_IN_FORMAT_STRING':
        'Invalid variable in listing pattern.',
    'history.error.message.UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING':
        'Invalid characters in listing pattern.',
    'history.error.message.APPLICATION_ERROR':
        'Unable to generate configuration file. Return to the device list and use the Reconnect button to restore service. If problem persists, contact Brivo Customer Care.',
    'history.error.message.NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND':
        'No phone numbers of the specified types were found for {username} from group {group_name}.',
    'history.error.message.RETRIEVE_USER_ERROR':
        'Unable to retrieve information for {username} in group {group_name}.',
    'history.error.message.RETRIEVE_GROUP_USERS_ERROR':
        'Unable to retrieve display name information for group {group_name}.',
    'history.error.message.MISSING_VALUE_FOR_FORMAT_STRING':
        'Unable to create display name for {username} in group {group_name}.',
    'history.error.message.USER_LIMIT_EXCEEDED':
        'Device directory cannot include more than {users_limit} entries.',
    'history.error.message.DISPLAY_NAME_EXCEEDED_MAX_LENGTH':
        'Display name for {username} cannot exceed {display_name_limit} characters.',
    'history.error.message.TOO_MANY_WARNINGS':
        'Too many non-fatal errors encountered. Aborting file generation. Adjust configuration settings and retry.',

    'error.title': 'An Error Occurred',
    'error.error404.part1': 'There was an error.',
    'error.error404.part2': 'Page does not exist!',
    'error.error403':
        'The account associated with your user ID does not have an active subscription to Brivo audio services. Please contact Brivo Customer Care for assistance at +1-866-274-8648.',

    'error.csrNoAccess.rootCause':
        'We could not log in via run as token with the master administrator account.',
    'error.csrNoAccess.masterId': ' Master Admin Id: {master_admin_id}',
    'error.csrNoAccess.accountId': 'Account Id: {account_id}',
    'error.csrNoAccess.username': 'Username: {username}',
    'error.csrNoAccess.name': 'Name: {name}',

    'error.csrNoAccess.email': 'Email: {email}',
    'error.csrNoAccess.checklistMessage':
        'Please confirm the following before trying again:',
    'error.csrNoAccess.checklistItem1': 'The admin is not locked',
    'error.csrNoAccess.checklistItem2': 'The admin is active',
    'error.csrNoAccess.checklistItem3':
        'The admin does not have a temporary password',
    'error.csrNoAccess.checklistItem4': 'The admin is not deleted',
    'error.csrNoAccess.checklistItem5': 'The admin is not expired',

    'header.logout': ' Log Out',
    'header.logoAlt': 'Brivo On Air logo',
    'header.applicationName': 'Audio Configuration Tool',
    'removal.title': 'Confirm Removal',
    'removal.cancel': 'Cancel',
    'removal.ok': 'OK',
    'select.empty': 'Not selected',
};

export default translations;
