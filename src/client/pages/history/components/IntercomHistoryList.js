import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Collapsible from 'react-collapsible';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { NavigationService } from '../../../services';
import './Collapsible.css';

const gridClasses = {
    column1: 'col-lg-2 col-md-3 col-4',
    column2: 'col-lg-2 col-md-2 col-4 text-center',
    column3: 'col-lg-2 col-md-2 col-4 text-center',
    column4: 'col-lg-6 col-md-5 col-12',
};

class IntercomHistoryList extends Component {
    constructor(props) {
        super(props);
        this.generateRows = this.generateRows.bind(this);
        this.generateExecutionRow = this.generateExecutionRow.bind(this);
        this.determineTopLevelMessage = this.determineTopLevelMessage.bind(
            this
        );
    }

    generateLogRecord(record, index) {
        let statusClass = 'badge-success';
        let status = <FormattedMessage id="history.all.statusOk" />;
        if (!!record.errorType) {
            if (record.errorType.failureFatal) {
                statusClass = 'badge-danger';
                status = <FormattedMessage id="history.all.statusError" />;
            } else {
                statusClass = 'badge-warning';
                status = <FormattedMessage id="history.all.statusWarning" />;
            }
        }
        return (
            <div className="row data1 brivo-list-row log-summary">
                <div
                    className="col-6 col-lg-4 col-md-5 align-self-center"
                    style={{ paddingLeft: '45px' }}
                >
                    {record.time && <FormattedDate value={record.time} />}{' '}
                    {record.time && (
                        <FormattedTime
                            value={record.time}
                            hour="numeric"
                            minute="numeric"
                            second="numeric"
                        />
                    )}
                </div>

                <div className="col-md-2 col-lg-2 col-6 align-self-center text-center">
                    <span className={`badge ${statusClass}`}>{status}</span>
                </div>

                <div className="col-12 col-lg-6 col-md-5 align-self-center">
                    {record.errorType && (
                        <FormattedMessage
                            id={`history.error.message.${
                                record.errorType.name
                            }`}
                            values={record.descriptionSources}
                        />
                    )}
                </div>
            </div>
        );
    }

    generateStatusBadge(execution) {
        let statusClass;
        let status;
        if (!execution.finishedAt) {
            status = <FormattedMessage id="history.all.statusPending" />;
            statusClass = 'badge-default';
        } else {
            switch (execution.status) {
                case 'Error':
                    statusClass = 'badge-danger';
                    status = <FormattedMessage id="history.all.statusError" />;
                    break;
                case 'Warning':
                    statusClass = 'badge-warning';
                    status = (
                        <FormattedMessage
                            id="history.all.statusAmountOfWarnings"
                            values={{
                                amount: execution.warningsNumber,
                            }}
                        />
                    );
                    break;
                case 'OK':
                    statusClass = 'badge-success';
                    status = <FormattedMessage id="history.all.statusOk" />;
                    break;
                default:
            }
        }
        return (
            <span>
                <span className={`badge ${statusClass}`}>{status}</span>
            </span>
        );
    }

    generateTimingDetails(execution) {
        return (
            <span>
                <FormattedDate value={execution.initiatedAt} />{' '}
                <FormattedTime
                    value={execution.initiatedAt}
                    hour="numeric"
                    minute="numeric"
                    second="numeric"
                />
            </span>
        );
    }

    determineTopLevelMessage(requestLogs) {
        if (
            requestLogs &&
            requestLogs.length == 1 &&
            !!requestLogs[0].errorType
        ) {
            return (
                <FormattedMessage
                    id={`history.error.message.${
                        requestLogs[0].errorType.name
                    }`}
                    values={requestLogs[0].descriptionSources}
                />
            );
        }
        if (requestLogs && requestLogs.length > 1) {
            return (
                <span>
                    <FormattedMessage id={`history.all.viewLogs`} />
                    <FontAwesomeIcon
                        style={{ paddingLeft: '10px', fontSize: '24px' }}
                        icon="chevron-down"
                    />
                    <FontAwesomeIcon
                        style={{ paddingLeft: '10px', fontSize: '24px' }}
                        icon="chevron-up"
                    />
                </span>
            );
        }
        return null;
    }

    generateExecutionSummary(execution) {
        const isTestLabel = execution.isTest ? (
            <FormattedMessage id="history.all.testGeneration" />
        ) : (
            <FormattedMessage id="history.all.nonTestGeneration" />
        );
        const message = this.determineTopLevelMessage(execution.requestLogs);
        return (
            <div className="container-fluid" style={{ cursor: 'pointer' }}>
                <div className="row d-flex align-items-center execution-row">
                    <div className={`${gridClasses.column1}`}>
                        {this.generateTimingDetails(execution)}
                    </div>
                    <div className={`${gridClasses.column2}`}>
                        {isTestLabel}
                    </div>

                    <div className={`${gridClasses.column3}`}>
                        {this.generateStatusBadge(execution)}
                    </div>

                    <div className={`${gridClasses.column4}`}>{message}</div>
                </div>
            </div>
        );
    }

    generateExecutionRow(execution, index) {
        const logLines =
            execution.requestLogs &&
            execution.requestLogs.map((log, index) =>
                this.generateLogRecord(log, index)
            );

        const content =
            execution.requestLogs && execution.requestLogs.length > 1 ? (
                <Collapsible trigger={this.generateExecutionSummary(execution)}>
                    <div className="container-fluid brivo-list execution-logs">
                        {logLines}
                    </div>
                </Collapsible>
            ) : (
                this.generateExecutionSummary(execution)
            );

        return (
            <div className="row data1 brivo-list-row history-summary">
                <div
                    className="col-12"
                    style={{ paddingRight: '0px', paddingLeft: '0px' }}
                >
                    {content}
                </div>
            </div>
        );
    }

    generateRows(executions) {
        return executions.map((execution, index) =>
            this.generateExecutionRow(execution, index)
        );
    }

    generateEmptyRow() {
        return (
            <div className="row data1 brivo-list-row execution-list-row">
                <div
                    className="col empty-list-column d-flex align-items-center"
                    id="noHistoryMessage"
                >
                    <FormattedMessage id="history.all.empty" />
                </div>
            </div>
        );
    }

    render() {
        const { history } = this.props;
        let content = null;
        if (!!history) {
            if (history.length > 0) {
                content = this.generateRows(history);
            } else {
                content = this.generateEmptyRow();
            }
        }
        return (
            <div>
                <div className="container-fluid brivo-list">
                    <div className="row brivo-list-header">
                        <div
                            className={`${
                                gridClasses.column1
                            } d-none d-sm-none d-md-block`}
                        >
                            <FormattedMessage id="history.all.timeColumn" />
                        </div>

                        <div
                            className={`${
                                gridClasses.column2
                            } d-none d-sm-none d-md-block`}
                        >
                            <FormattedMessage id="history.all.isTestColumn" />
                        </div>

                        <div
                            className={`${
                                gridClasses.column3
                            } d-none d-sm-none d-md-block`}
                        >
                            <FormattedMessage id="history.all.statusColumn" />
                        </div>
                        <div
                            className={`${
                                gridClasses.column4
                            } d-none d-sm-none d-md-block`}
                        >
                            <FormattedMessage id="history.all.detailsColumn" />
                        </div>
                    </div>
                    {content}
                </div>
            </div>
        );
    }
}

IntercomHistoryList.propTypes = {
    history: PropTypes.array.isRequired,
    intercom: PropTypes.object.isRequired,
};

export default IntercomHistoryList;
