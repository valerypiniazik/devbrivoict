import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Selectors from '../../selectors/';
import { NavigationService } from '../../services';
import { IntercomActions, ErrorActions } from '../../actions';
import { SimpleButton } from '../../widgets/Buttons/';

import { PageTitle, TopButtonBar, ErrorPanel } from '../../widgets/Layout/';
import IntercomHistoryList from './components/IntercomHistoryList';

import {
    INTERCOM_LOADING,
    INTERCOM_HISTORY_LOADING,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

const errorsOfInterest = [
    INTERCOM_LOADING,
    INTERCOM_HISTORY_LOADING,
    INTERCOM_CONFIG_GENERATION,
];

class IntercomHistory extends Component {
    constructor(props) {
        super(props);
        this.loadHistory = this.loadHistory.bind(this);
    }

    componentDidMount() {
        this.loadHistory(false);
    }

    componentWillUnmount() {
        this.props.cancelIntercomHistory(this.props.history);
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    loadHistory(isReload) {
        this.props.initIntercomHistory(
            this.props.match.params.intercomId,
            this.props.history,
            !isReload
        );
    }

    render() {
        return (
            <div>
                <PageTitle
                    title={<FormattedMessage id="history.all.title" />}
                />
                <div className="container-fluid">
                    <ErrorPanel operationTypes={errorsOfInterest} />
                    <div className="row">
                        <TopButtonBar>
                            <SimpleButton
                                id="linkToAllIntercomsPage"
                                label={
                                    <FormattedMessage id="history.all.cancel" />
                                }
                                onClick={this.props.history.goBack}
                            />
                            <SimpleButton
                                id="refreshHistory"
                                label={
                                    <FormattedMessage id="history.all.refresh" />
                                }
                                onClick={this.loadHistory}
                            />
                        </TopButtonBar>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h5 className="entity-name wrapped-text-break-word">
                                {!!this.props.intercom ? (
                                    <FormattedMessage
                                        id="history.all.intercomTitle"
                                        values={{
                                            name: this.props.intercom.name,
                                        }}
                                    />
                                ) : null}
                            </h5>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <IntercomHistoryList
                                history={this.props.records}
                                intercom={this.props.intercom}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    initIntercomHistory: bindActionCreators(
        IntercomActions.initIntercomHistory,
        dispatch
    ),
    cancelIntercomHistory: bindActionCreators(
        IntercomActions.cancelCurrentIntercomHistory,
        dispatch
    ),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    records: Selectors.getIntercomHistory(state),
    intercom: Selectors.getCurrentIntercom(state),
});

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(IntercomHistory)
);
