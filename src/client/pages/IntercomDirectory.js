import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { GroupActions } from '../actions';
import { UserService } from '../services/';
import Selectors from '../selectors/';
import { ROLE_CSR } from '../constants/roles';

import DirectoryRules from './directory/DirectoryRules';
import NewDirectoryRule from './directory/NewDirectoryRule';
import DirectoryRuleDetails from './directory/DirectoryRuleDetails';
import Error404 from './Error404';

const mapDispatchToProps = dispatch => ({
    loadGroups: bindActionCreators(GroupActions.getAllGroups, dispatch),
});

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

class IntercomDirectory extends Component {
    componentDidMount() {
        this.props.loadGroups();
    }

    render() {
        const couldCreateDirectoryRules =
            !!this.props.currentUser &&
            !UserService.hasPermissions(this.props.currentUser, [ROLE_CSR]);
        const newDirectoryRuleRoute = couldCreateDirectoryRules ? (
            <Route
                exact
                path="/intercom/:intercomId/directory/new"
                name="Create New Intercom Directory Rule"
                component={NewDirectoryRule}
            />
        ) : null;

        return (
            <Switch>
                {newDirectoryRuleRoute}
                <Route
                    path="/intercom/:intercomId/directory/:ruleId"
                    name="Edit Intercom Directory Rule"
                    component={DirectoryRuleDetails}
                />
                <Route
                    exact
                    path="/intercom/:intercomId/directory"
                    name="All Intercom Directory Rules"
                    component={DirectoryRules}
                />
                <Route name="Default Error Page" component={Error404} />
            </Switch>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(IntercomDirectory)
);
