import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import AllIntercoms from './intercom/AllIntercoms';
import IntercomDetails from './intercom/IntercomDetails';
import IntercomDirectory from './IntercomDirectory';
import IntercomHistory from './history/IntercomHistory';
import { IntercomActions, OperationActions } from '../actions';
import { INTERCOM } from '../constants/entity';
import Error404 from './Error404';

const mapDispatchToProps = dispatch => ({
    loadIntercom: bindActionCreators(IntercomActions.loadIntercom, dispatch),
    cancelCurrentIntercom: bindActionCreators(
        IntercomActions.cancelCurrentIntercom,
        dispatch
    ),
    cancelOperation: bindActionCreators(
        OperationActions.clearOperation,
        dispatch
    ),
});

class IntercomViews extends Component {
    componentDidMount() {
        this.props.loadIntercom(
            this.props.match.params.intercomId,
            this.props.history
        );
    }

    componentWillUnmount() {
        this.props.cancelCurrentIntercom();
        this.props.cancelOperation(INTERCOM);
    }

    componentWillUpdate() {
        this.props.loadIntercom(
            this.props.match.params.intercomId,
            this.props.history
        );
    }

    render() {
        return (
            <Switch>
                <Route
                    path="/intercom/:intercomId/directory"
                    name="Intercom Directory Page"
                    component={IntercomDirectory}
                />
                <Route
                    path="/intercom/:intercomId/history"
                    name="Intercom History Page"
                    component={IntercomHistory}
                />
                <Route
                    exact
                    path="/intercom/:intercomId"
                    name="Edit Intercom configuration"
                    component={IntercomDetails}
                />
                <Route name="Default Error Page" component={Error404} />
            </Switch>
        );
    }
}

export default withRouter(
    connect(
        null,
        mapDispatchToProps
    )(IntercomViews)
);
