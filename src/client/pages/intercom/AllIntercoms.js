import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import { IntercomActions, UserActions, ErrorActions } from '../../actions';
import Selectors from '../../selectors/';

import { LinkButton, ReconnectButton } from '../../widgets/Buttons/';
import LimitedAccess from '../../widgets/LimitedAccess';
import { PageTitle, TopButtonBar, ErrorPanel } from '../../widgets/Layout/';
import IntercomList from './components/IntercomList';
import TokenStatusComponent from './components/TokenStatusComponent';
import {
    INTERCOM_LOADING,
    ALL_INTERCOMS_LOADING,
    ALL_INTERCOM_TYPES_LOADING,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';
import {
    ROLE_MASTER_ADMIN,
    ROLE_SENIOR_ADMIN,
    ROLE_CSR,
} from '../../constants/roles';

const errorsOfInterest = [
    ALL_INTERCOMS_LOADING,
    INTERCOM_LOADING,
    ALL_INTERCOM_TYPES_LOADING,
    INTERCOM_CONFIG_GENERATION,
];

import { NavigationService } from '../../services';

class AllIntercoms extends Component {
    constructor(props) {
        super(props);
        this.reconnectIntercom = this.reconnectIntercom.bind(this);
    }

    componentDidMount() {
        this.props.initAllIntercoms();
    }

    componentWillUnmount() {
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    reconnectIntercom() {
        this.props.onReconnect();
    }

    render() {
        const { intercoms } = this.props;
        return (
            <div>
                <PageTitle
                    title={<FormattedMessage id="intercoms.all.title" />}
                >
                    <LimitedAccess
                        requiredRoles={[
                            ROLE_MASTER_ADMIN,
                            ROLE_SENIOR_ADMIN,
                            ROLE_CSR,
                        ]}
                    >
                        <div
                            style={{ paddingRight: '40px', height: '100%' }}
                            className="d-flex flex-row justify-content-end"
                        >
                            <div className="d-none d-sm-none d-md-block secondary-header-info">
                                <TokenStatusComponent className="text-right" />
                            </div>
                            <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                                <div>
                                    <div style={{ paddingLeft: '20px' }}>
                                        <ReconnectButton
                                            id="reconnectIntercom"
                                            onClick={this.reconnectIntercom}
                                        />
                                    </div>
                                </div>
                            </LimitedAccess>
                        </div>
                    </LimitedAccess>
                </PageTitle>
                <div className="container-fluid">
                    <ErrorPanel operationTypes={errorsOfInterest} />
                    <div className="row">
                        <TopButtonBar>
                            <LimitedAccess
                                requiredRoles={[
                                    ROLE_MASTER_ADMIN,
                                    ROLE_SENIOR_ADMIN,
                                    ROLE_CSR,
                                ]}
                            >
                                <div
                                    className="d-block d-sm-block d-md-none"
                                    style={{ paddingBottom: '30px' }}
                                >
                                    <TokenStatusComponent className="text-left" />
                                </div>
                            </LimitedAccess>

                            <div className="d-flex justify-content-between ">
                                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                                    <LinkButton
                                        id="newIntercom"
                                        href={NavigationService.buildLinkToNewIntercomsPage()}
                                        label={
                                            <FormattedMessage id="intercoms.all.create" />
                                        }
                                    />
                                </LimitedAccess>
                            </div>
                        </TopButtonBar>
                    </div>
                    <div className="row">
                        <div className="col">
                            <IntercomList intercoms={intercoms} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    initAllIntercoms: bindActionCreators(
        IntercomActions.getAllIntercoms,
        dispatch
    ),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
    onReconnect: bindActionCreators(UserActions.reconnectUser, dispatch),
});

const mapStateToProps = state => ({
    intercoms: Selectors.getAllIntercoms(state),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AllIntercoms);
