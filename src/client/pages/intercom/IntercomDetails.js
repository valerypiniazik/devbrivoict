import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import Selectors from '../../selectors/';
import { IntercomActions, OperationActions, ErrorActions } from '../../actions';

import IntercomWrapper from './components/IntecomWrapper';
import LimitedAccess from '../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../constants/roles';

import { INTERCOM } from '../../constants/entity';
import {
    INTERCOM_UPDATE,
    INTERCOM_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

const errorsOfInterest = [
    INTERCOM_UPDATE,
    INTERCOM_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
];

class IntercomDetails extends Component {
    componentWillUnmount() {
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    render() {
        const title = (
            <Fragment>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    <FormattedMessage id="intercoms.edit.title" />
                </LimitedAccess>
                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    <FormattedMessage id="intercoms.edit.csrTitle" />
                </LimitedAccess>
            </Fragment>
        );
        return !!this.props.current ? (
            <IntercomWrapper
                title={title}
                errorTypes={errorsOfInterest}
                intercom={this.props.current}
                types={this.props.types}
                onChange={this.props.onChange}
                onSave={this.props.onSave}
                onCancel={this.onCancel}
                onDownload={this.props.onDownload}
            />
        ) : null;
    }
}

const mapDispatchToProps = dispatch => ({
    onChange: bindActionCreators(OperationActions.updateOperation, dispatch),
    onSave: bindActionCreators(IntercomActions.updateIntercom, dispatch),
    onDownload: bindActionCreators(
        IntercomActions.downloadIntercomConfig,
        dispatch
    ),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    types: Selectors.getAllIntercomTypes(state),
    current: Selectors.getCurrentOperationObject(state, INTERCOM),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IntercomDetails);
