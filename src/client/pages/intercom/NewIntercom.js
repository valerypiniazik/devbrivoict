import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import Selectors from '../../selectors/';
import { IntercomActions, OperationActions, ErrorActions } from '../../actions';
import { PasswordService } from '../../services/';

import IntercomWrapper from './components/IntecomWrapper';

import { INTERCOM } from '../../constants/entity';
import {
    INTERCOM_SAVING,
    INTERCOM_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

const errorsOfInterest = [
    INTERCOM_SAVING,
    INTERCOM_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
];

class NewIntercom extends Component {
    constructor(props) {
        super(props);
        this.setDefaultModelType = this.setDefaultModelType.bind(this);
    }

    componentDidMount() {
        this.props.onInit(INTERCOM, {
            password: PasswordService.generatePassword(),
            macAddress: '',
        });
    }

    componentWillUnmount() {
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    setDefaultModelType() {
        if (
            !!this.props.types &&
            this.props.types.length == 1 &&
            !!this.props.current
        ) {
            this.props.current.typeId = this.props.types[0].id;
        }
    }

    render() {
        this.setDefaultModelType();

        return !!this.props.current ? (
            <IntercomWrapper
                title={<FormattedMessage id="intercoms.new.title" />}
                errorTypes={errorsOfInterest}
                intercom={this.props.current}
                types={this.props.types}
                onChange={this.props.onChange}
                onSave={this.props.onSave}
                onCancel={this.onCancel}
                onDownload={this.props.onDownload}
            />
        ) : null;
    }
}

const mapDispatchToProps = dispatch => ({
    onInit: bindActionCreators(OperationActions.initOperation, dispatch),
    onSave: bindActionCreators(IntercomActions.saveIntercom, dispatch),
    onChange: bindActionCreators(OperationActions.updateOperation, dispatch),
    onDownload: bindActionCreators(
        IntercomActions.downloadIntercomConfig,
        dispatch
    ),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    types: Selectors.getAllIntercomTypes(state),
    current: Selectors.getCurrentOperationObject(state, INTERCOM),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewIntercom);
