import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import {
    TextInput,
    SmartSelect,
    MacAddressInput,
} from '../../../widgets/Input/';
import { SimpleButton } from '../../../widgets/Buttons/';
import PasswordService from '../../../services/PasswordService';
import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';
import { UserService } from '../../../services/';
import Selectors from '../../../selectors/';

class IntercomForm extends Component {
    constructor(props) {
        super(props);
        this.onPasswordGenerate = this.onPasswordGenerate.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onIntercomTypeChange = this.onIntercomTypeChange.bind(this);
        this.onMacChange = this.onMacChange.bind(this);
        this.onFirstCodeChange = this.onFirstCodeChange.bind(this);
        this.onSecondCodeChange = this.onSecondCodeChange.bind(this);
        this.onThirdCodeChange = this.onThirdCodeChange.bind(this);
        this.onFourthCodeChange = this.onFourthCodeChange.bind(this);
        this.getDefinedSwitchCode = this.getDefinedSwitchCode.bind(this);
        this.onFirstDescriptionChange = this.onFirstDescriptionChange.bind(
            this
        );
        this.onSecondDescriptionChange = this.onSecondDescriptionChange.bind(
            this
        );
        this.onThirdDescriptionChange = this.onThirdDescriptionChange.bind(
            this
        );
        this.onFourthDescriptionChange = this.onFourthDescriptionChange.bind(
            this
        );
    }

    onPasswordGenerate() {
        this.props.onChange('password', PasswordService.generatePassword());
    }

    onNameChange(value) {
        this.props.onChange('name', value);
    }

    onIntercomTypeChange(value) {
        this.props.onChange('typeId', value);
    }

    onMacChange(value) {
        this.props.onChange('macAddress', value);
    }

    onFirstDescriptionChange(value) {
        this.props.onSwitchCodeChange({ name: value }, 0);
    }

    onSecondDescriptionChange(value) {
        this.props.onSwitchCodeChange({ name: value }, 1);
    }

    onThirdDescriptionChange(value) {
        this.props.onSwitchCodeChange({ name: value }, 2);
    }

    onFourthDescriptionChange(value) {
        this.props.onSwitchCodeChange({ name: value }, 3);
    }

    onFirstCodeChange(value) {
        this.props.onSwitchCodeChange({ code: value }, 0);
    }

    onSecondCodeChange(value) {
        this.props.onSwitchCodeChange({ code: value }, 1);
    }

    onThirdCodeChange(value) {
        this.props.onSwitchCodeChange({ code: value }, 2);
    }

    onFourthCodeChange(value) {
        this.props.onSwitchCodeChange({ code: value }, 3);
    }

    getDefinedSwitchCode(switchCodes, index) {
        return !!switchCodes.filter(code => code.index == index)[0]
            ? switchCodes.filter(code => code.index == index)[0]
            : {};
    }

    render() {
        const intercomTypes = this.props.types.map(row => {
            return { value: row.id, label: row.make + ' ' + row.model };
        });
        const switchCodes = this.props.intercom.switchCodes || [];
        const firstSwitchCode = this.getDefinedSwitchCode(switchCodes, 0);
        const secondSwitchCode = this.getDefinedSwitchCode(switchCodes, 1);
        const thirdSwitchCode = this.getDefinedSwitchCode(switchCodes, 2);
        const fourthSwitchCode = this.getDefinedSwitchCode(switchCodes, 3);
        return (
            <div>
                <form>
                    <div className="form-row">
                        <TextInput
                            id="intercomName"
                            value={this.props.intercom.name}
                            containerClassName="col-12"
                            label={
                                <FormattedMessage id="intercoms.form.name" />
                            }
                            max={255}
                            onChange={this.onNameChange}
                        />
                        <SmartSelect
                            id="intercomType"
                            value={this.props.intercom.typeId}
                            options={intercomTypes}
                            readOnlyClassName="col-md-12 d-flex justify-content-start"
                            selectClassName="col-12"
                            label={
                                <FormattedMessage id="intercoms.form.model" />
                            }
                            onChange={this.onIntercomTypeChange}
                            readOnly={UserService.hasPermissions(
                                this.props.currentUser,
                                [ROLE_CSR]
                            )}
                        />
                        <div className="form-group col-5 col-md-3">
                            <label>
                                <FormattedMessage id="intercoms.form.password" />
                            </label>
                            <div
                                className="form-control form-control-plain-text"
                                id="currentIntercomPassword"
                            >
                                {!!this.props.intercom.password
                                    ? this.props.intercom.password
                                    : '########'}
                            </div>
                        </div>
                        <div className="form-group col-7 col-md-9 d-flex align-items-end justify-content-start">
                            <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                                <SimpleButton
                                    id="regeneratePassword"
                                    label={
                                        <FormattedMessage id="intercoms.form.regenerate" />
                                    }
                                    onClick={this.onPasswordGenerate}
                                />
                            </LimitedAccess>
                        </div>
                    </div>
                    <MacAddressInput
                        id="macAddress"
                        containerClassName="col-12"
                        label={<FormattedMessage id="intercoms.form.mac" />}
                        value={this.props.intercom.macAddress}
                        onChange={this.onMacChange}
                    />
                    <div className="form-row">
                        <TextInput
                            id="switchCode0"
                            value={firstSwitchCode.code}
                            label={
                                <FormattedMessage id="intercoms.form.switch1Value" />
                            }
                            containerClassName="col-12 col-md-4"
                            max={15}
                            min={4}
                            onChange={this.onFirstCodeChange}
                        />
                        <TextInput
                            id="switchCode0Description"
                            value={firstSwitchCode.name}
                            label={
                                <FormattedMessage id="intercoms.form.switch1Description" />
                            }
                            max={255}
                            containerClassName="col-12 col-md-8 switch-code-desc"
                            onChange={this.onFirstDescriptionChange}
                        />
                        <TextInput
                            id="switchCode1"
                            value={secondSwitchCode.code}
                            label={
                                <FormattedMessage id="intercoms.form.switch2Value" />
                            }
                            containerClassName="col-12 col-md-4"
                            max={15}
                            min={4}
                            onChange={this.onSecondCodeChange}
                        />
                        <TextInput
                            id="switchCode1Description"
                            value={secondSwitchCode.name}
                            label={
                                <FormattedMessage id="intercoms.form.switch2Description" />
                            }
                            max={255}
                            containerClassName="col-12 col-md-8 switch-code-desc"
                            onChange={this.onSecondDescriptionChange}
                        />
                        <TextInput
                            id="switchCode2"
                            value={thirdSwitchCode.code}
                            label={
                                <FormattedMessage id="intercoms.form.switch3Value" />
                            }
                            containerClassName="col-12 col-md-4"
                            max={15}
                            min={4}
                            onChange={this.onThirdCodeChange}
                        />
                        <TextInput
                            id="switchCode2Description"
                            value={thirdSwitchCode.name}
                            label={
                                <FormattedMessage id="intercoms.form.switch3Description" />
                            }
                            max={255}
                            containerClassName="col-12 col-md-8 switch-code-desc"
                            onChange={this.onThirdDescriptionChange}
                        />
                        <TextInput
                            id="switchCode3"
                            value={fourthSwitchCode.code}
                            label={
                                <FormattedMessage id="intercoms.form.switch4Value" />
                            }
                            containerClassName="col-12 col-md-4"
                            max={15}
                            min={4}
                            onChange={this.onFourthCodeChange}
                        />
                        <TextInput
                            id="switchCode3Description"
                            value={fourthSwitchCode.name}
                            label={
                                <FormattedMessage id="intercoms.form.switch4Description" />
                            }
                            max={255}
                            containerClassName="col-12 col-md-8"
                            onChange={this.onFourthDescriptionChange}
                        />
                    </div>
                </form>
            </div>
        );
    }
}

IntercomForm.propTypes = {
    intercom: PropTypes.object.isRequired,
    intercomTypes: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onSwitchCodeChange: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

export default connect(
    mapStateToProps,
    null
)(IntercomForm);
