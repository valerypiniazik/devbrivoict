import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import { ConfirmationActions } from '../../../actions';
import Selectors from '../../../selectors/';
import { NavigationService } from '../../../services';
import { DeleteButton, LinkButton } from '../../../widgets/Buttons/';
import IntercomRemovalConfirmation from './IntercomRemovalConfirmation';

import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';

const gridClasses = {
    column1: 'col-xl-4 col-md-3 col-5',
    column2: 'col-xl-2 col-md-1 col-3 text-center',
    column3: 'col-xl-2 col-md-2 col-4 text-center',
    column4: 'col-xl-2 col-md-3 col-5 text-md-center',
    column5: 'col-xl-1 col-md-2 col-3 text-center',
    column6: 'col-xl-1 col-md-1 col-4 text-center',
    column5_csr: 'col-xl-2 col-md-3 col-7 text-md-center text-right',
};

class IntercomList extends Component {
    constructor(props) {
        super(props);
        this.generateIntercomRows = this.generateIntercomRows.bind(this);
        this.renderTypeLabel = this.renderTypeLabel.bind(this);
    }

    renderTypeLabel(typeId) {
        const type = this.props.types.filter(row => row.id === typeId)[0];
        return type ? type.make + ' ' + type.model : '';
    }

    generateIntercomRows(intercoms) {
        return intercoms.map(intercom => (
            <div className="row data1 brivo-list-row" key={intercom.id}>
                <div
                    className={`${
                        gridClasses.column1
                    } align-self-center wrapped-text table-cell`}
                >
                    <div className="brivo-list-cell">{intercom.name}</div>
                </div>
                <div
                    className={`${
                        gridClasses.column2
                    } align-self-center table-cell`}
                >
                    {this.renderTypeLabel(intercom.typeId)}
                </div>
                <div
                    className={`${
                        gridClasses.column3
                    } align-self-center table-cell`}
                >
                    <Link
                        className="link"
                        id={'viewIntercomHistory_' + intercom.id}
                        to={NavigationService.buildLinkToIntercomHistoryPage(
                            intercom.id
                        )}
                    >
                        <span className="text">
                            <FormattedMessage id="intercoms.all.history" />
                        </span>
                    </Link>
                </div>
                <div
                    className={`${
                        gridClasses.column4
                    } align-self-center table-cell`}
                >
                    <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                        <LinkButton
                            id={'editIntercom_' + intercom.id}
                            label={<FormattedMessage id="intercoms.all.edit" />}
                            href={NavigationService.buildLinkToIntercomsPage(
                                intercom.id
                            )}
                        />
                    </LimitedAccess>
                    <LimitedAccess requiredRoles={[ROLE_CSR]}>
                        <LinkButton
                            id={'editIntercom_' + intercom.id}
                            label={<FormattedMessage id="intercoms.all.view" />}
                            href={NavigationService.buildLinkToIntercomsPage(
                                intercom.id
                            )}
                        />
                    </LimitedAccess>
                </div>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    <div
                        className={`${
                            gridClasses.column5
                        } align-self-center table-cell`}
                    >
                        <LinkButton
                            id={'editDirectoryRules_' + intercom.id}
                            label={
                                <FormattedMessage id="intercoms.all.define" />
                            }
                            href={NavigationService.buildLinkToIntercomDirectoryPage(
                                intercom.id
                            )}
                        />
                    </div>
                    <div
                        className={`${
                            gridClasses.column6
                        } align-self-center table-cell`}
                    >
                        <DeleteButton
                            label={
                                <FormattedMessage id="intercoms.all.delete" />
                            }
                            classNames="justify-content-center align-items-center"
                            onClick={() => {
                                this.props.showDeleteConfirmation(intercom);
                            }}
                        />
                    </div>
                </LimitedAccess>

                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    <div
                        className={`${
                            gridClasses.column5_csr
                        } align-self-center table-cell justify-content-end justify-content-md-start`}
                    >
                        <LinkButton
                            id={'editDirectoryRules_' + intercom.id}
                            label={
                                <FormattedMessage id="intercoms.all.viewDefinition" />
                            }
                            href={NavigationService.buildLinkToIntercomDirectoryPage(
                                intercom.id
                            )}
                        />
                    </div>
                </LimitedAccess>
            </div>
        ));
    }

    generateEmptyRow() {
        return (
            <div className="row data1 brivo-list-row">
                <div
                    className="col empty-list-column d-flex align-items-center"
                    id="noDevicesMessage"
                >
                    <FormattedMessage id="intercoms.all.empty" />
                </div>
            </div>
        );
    }

    render() {
        const { intercoms } = this.props;
        let content = null;
        if (!!intercoms) {
            if (intercoms.length > 0) {
                content = this.generateIntercomRows(intercoms);
            } else {
                content = this.generateEmptyRow();
            }
        }
        return (
            <Fragment>
                <IntercomRemovalConfirmation />
                <div className="container-fluid brivo-list">
                    <div className="row brivo-list-header">
                        <div className={`${gridClasses.column1}`}>
                            <FormattedMessage id="intercoms.all.nameColumn" />
                        </div>
                        <div className={`${gridClasses.column2} table-cell`}>
                            <FormattedMessage id="intercoms.all.modelColumn" />
                        </div>
                        <div className={`${gridClasses.column3} table-cell`}>
                            <FormattedMessage id="intercoms.all.historyColumn" />
                        </div>
                        <div
                            className={`${
                                gridClasses.column4
                            } table-cell  d-none d-sm-none d-md-block`}
                        >
                            <FormattedMessage id="intercoms.all.deviceColumn" />
                        </div>
                        <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                            <div
                                className={`${
                                    gridClasses.column5
                                } table-cell  d-none d-sm-none d-md-block`}
                            >
                                <FormattedMessage id="intercoms.all.directoryColumn" />
                            </div>
                            <div
                                className={`${
                                    gridClasses.column6
                                } table-cell  d-none d-sm-none d-md-block`}
                            >
                                <FormattedMessage id="intercoms.all.deleteColumn" />
                            </div>
                        </LimitedAccess>
                        <LimitedAccess requiredRoles={[ROLE_CSR]}>
                            <div
                                className={`${
                                    gridClasses.column5_csr
                                } table-cell d-none d-sm-none d-md-block`}
                            >
                                <FormattedMessage id="intercoms.all.directoryColumn" />
                            </div>
                        </LimitedAccess>
                    </div>
                    {content}
                </div>
            </Fragment>
        );
    }
}

IntercomList.propTypes = {
    intercoms: PropTypes.array.isRequired,
};

const mapDispatchToProps = dispatch => ({
    showDeleteConfirmation: bindActionCreators(
        ConfirmationActions.showDeleteConfirmation,
        dispatch
    ),
});

const mapStateToProps = state => ({
    types: Selectors.getAllIntercomTypes(state),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IntercomList);
