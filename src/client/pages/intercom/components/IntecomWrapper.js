import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import { NavigationService } from '../../../services/';

import IntercomForm from './IntercomForm';
import { SimpleButton, LinkButton } from '../../../widgets/Buttons/';
import { PageTitle, ErrorPanel } from '../../../widgets/Layout/';

import { INTERCOM } from '../../../constants/entity';
import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';

class IntercomWrapper extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onSwitchCodeChange = this.onSwitchCodeChange.bind(this);
    }

    onChange(fieldName, value) {
        const updateIntercom = Object.assign({}, this.props.intercom, {
            [fieldName]: value,
        });
        this.props.onChange(INTERCOM, updateIntercom);
    }

    onSwitchCodeChange(updatedValue, index) {
        const switchCodes = this.props.intercom.switchCodes || [];

        if (!!switchCodes.filter(code => code.index == index)[0]) {
            switchCodes.map(code => {
                if (code.index == index) {
                    const updatedSwitchCode = Object.assign(code, updatedValue);
                    code.code = updatedSwitchCode.code;
                    code.name = updatedSwitchCode.name;
                }
            });
        } else {
            const updatedSwitchCode = Object.assign(
                { index: index, intercom_id: this.props.intercom.id },
                updatedValue
            );
            switchCodes.push(updatedSwitchCode);
        }
        this.onChange('switchCodes', switchCodes);
    }

    onSave() {
        const switchCodes = this.props.intercom.switchCodes || [];
        this.props.intercom.switchCodes = switchCodes.filter(
            row => !!row.code || !!row.name
        );
        this.props.onSave(this.props.intercom, this.props.history);
    }

    render() {
        const cancelButton = (
            <LinkButton
                id="cancelIntercom"
                href={NavigationService.buildLinkToIntercomsPage()}
                label={<FormattedMessage id="intercoms.form.cancel" />}
                onClick={this.props.onCancel}
            />
        );
        const generateButton = (
            <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                <SimpleButton
                    id="testIntercom"
                    label={<FormattedMessage id="intercoms.form.test" />}
                    disabled={!this.props.intercom.id}
                    onClick={() =>
                        this.props.onDownload(
                            this.props.intercom,
                            this.props.history
                        )
                    }
                />
            </LimitedAccess>
        );

        const historyButton = (
            <LinkButton
                id="goToHistory"
                disabled={!this.props.intercom.id}
                href={NavigationService.buildLinkToIntercomHistoryPage(
                    this.props.intercom.id
                )}
                label={<FormattedMessage id="intercoms.form.viewHistory" />}
            />
        );
        const saveButton = (
            <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                <SimpleButton
                    id="saveIntercom"
                    label={<FormattedMessage id="intercoms.form.save" />}
                    onClick={this.onSave}
                />
            </LimitedAccess>
        );

        return (
            <div>
                <PageTitle title={this.props.title} />
                <div className="container-fluid">
                    <ErrorPanel operationTypes={this.props.errorTypes} />
                    <div className="row">
                        <div className="col-12 col-md-9 col-lg-7 col-xl-5">
                            <IntercomForm
                                intercom={this.props.intercom}
                                types={this.props.types}
                                onChange={this.onChange}
                                onSwitchCodeChange={this.onSwitchCodeChange}
                            />
                        </div>
                    </div>
                    <div
                        className="row"
                        style={{ paddingTop: '30px', paddingBottom: '20px' }}
                    >
                        <div className="col-12 d-none d-sm-block justify-content-between justify-content-sm-start">
                            <div
                                role="group"
                                aria-label="CRUD actions"
                                style={{ width: '100%' }}
                            >
                                {cancelButton}
                                {generateButton}
                                {historyButton}
                                {saveButton}
                            </div>
                        </div>
                        <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                            <div
                                className="col-12 d-block d-sm-none d-flex justify-content-between justify-content-sm-start"
                                style={{ paddingBottom: '10px' }}
                            >
                                <div
                                    role="group"
                                    aria-label="CRUD actions"
                                    style={{ width: '100%' }}
                                >
                                    {generateButton}
                                    {historyButton}
                                </div>
                            </div>
                            <div className="col-12 d-block d-sm-none d-flex justify-content-between justify-content-sm-start">
                                <div
                                    role="group"
                                    aria-label="CRUD actions"
                                    style={{ width: '100%' }}
                                >
                                    {cancelButton}
                                    {saveButton}
                                </div>
                            </div>
                        </LimitedAccess>

                        <LimitedAccess requiredRoles={[ROLE_CSR]}>
                            <div className="col-12 d-block d-sm-none d-flex justify-content-between justify-content-sm-start">
                                <div
                                    role="group"
                                    aria-label="CRUD actions"
                                    style={{ width: '100%' }}
                                >
                                    {cancelButton}
                                    {historyButton}
                                </div>
                            </div>
                        </LimitedAccess>
                    </div>
                </div>
            </div>
        );
    }
}

IntercomWrapper.propTypes = {
    title: PropTypes.node.isRequired,
    types: PropTypes.array.isRequired,
    intercom: PropTypes.object.isRequired,
    errorTypes: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    onDownload: PropTypes.func.isRequired,
};

export default withRouter(IntercomWrapper);
