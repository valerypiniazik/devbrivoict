import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage, FormattedDate, FormattedTime } from 'react-intl';

import Selectors from '../../../selectors/';
import { IntercomActions } from '../../../actions';
import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';

class TokenStatusComponent extends Component {
    componentDidMount() {
        this.props.getCurrentTokenStatus();
    }

    render() {
        const { tokenStatus } = this.props;
        if (!!tokenStatus) {
            let regularMessageId = '';
            if (tokenStatus.valid) {
                regularMessageId = 'intercoms.all.header_message_valid';
            } else {
                regularMessageId = 'intercoms.all.header_message_invalid';
            }
            let csrMessageId = '';
            if (tokenStatus.valid) {
                csrMessageId = 'intercoms.all.header_message_valid';
            } else {
                csrMessageId = 'intercoms.all.header_message_invalid_csr';
            }
            const updateTime = (
                <Fragment>
                    <FormattedDate value={tokenStatus.updatedAt} />{' '}
                    <FormattedTime value={tokenStatus.updatedAt} />
                </Fragment>
            );
            return (
                <div
                    className={`d-flex align-items-center justify-content-md-end justify-content-start ${
                        this.props.className
                    } refresh-token-status`}
                >
                    <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                        <FormattedMessage
                            id={regularMessageId}
                            values={{
                                username: tokenStatus.username,
                                date: updateTime,
                            }}
                        />
                    </LimitedAccess>
                    <LimitedAccess requiredRoles={[ROLE_CSR]}>
                        <FormattedMessage
                            id={csrMessageId}
                            values={{
                                username: tokenStatus.username,
                                date: updateTime,
                            }}
                        />
                    </LimitedAccess>
                </div>
            );
        } else {
            return null;
        }
    }
}

TokenStatusComponent.propTypes = {
    className: PropTypes.string,
};

TokenStatusComponent.dedaultProps = {
    className: '',
};

const mapDispatchToProps = dispatch => ({
    getCurrentTokenStatus: bindActionCreators(
        IntercomActions.getCurrentTokenStatus,
        dispatch
    ),
});

const mapStateToProps = state => ({
    tokenStatus: Selectors.getTokenStatus(state),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TokenStatusComponent);
