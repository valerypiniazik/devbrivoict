import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import Selectors from '../../../selectors/';
import { IntercomActions, ConfirmationActions } from '../../../actions';
import { DeleteConfirmationModal } from '../../../widgets/Layout/';

class IntercomRemovalConfirmation extends Component {
    constructor(props) {
        super(props);
        this.deleteConfirmedIntercom = this.deleteConfirmedIntercom.bind(this);
        this.cancelConfirmation = this.cancelConfirmation.bind(this);
        this.renderDeleteConfirmationLabel = this.renderDeleteConfirmationLabel.bind(
            this
        );
    }

    deleteConfirmedIntercom() {
        this.props.deleteIntercom(this.props.deletedIntercom.id);
        this.props.hideDeleteConfirmation();
    }

    cancelConfirmation() {
        this.props.hideDeleteConfirmation();
    }

    renderDeleteConfirmationLabel() {
        return (
            <FormattedMessage
                id="intercoms.all.confirmRemoval"
                values={{ name: this.props.deletedIntercom.name }}
            />
        );
    }

    render() {
        return (
            <DeleteConfirmationModal
                id="intercomRemovalConfirmation"
                visible={this.props.isVisible}
                renderLabel={this.renderDeleteConfirmationLabel}
                onConfirm={this.deleteConfirmedIntercom}
                onCancel={() => {
                    this.cancelConfirmation();
                }}
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({
    showDeleteConfirmation: bindActionCreators(
        ConfirmationActions.showDeleteConfirmation,
        dispatch
    ),
    hideDeleteConfirmation: bindActionCreators(
        ConfirmationActions.hideDeleteConfirmation,
        dispatch
    ),
    deleteIntercom: bindActionCreators(
        IntercomActions.deleteIntercom,
        dispatch
    ),
});

const mapStateToProps = state => ({
    isVisible: Selectors.isConfirmationVisible(state),
    deletedIntercom: Selectors.getConfirmationEntity(state),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(IntercomRemovalConfirmation);
