import React, { Component, Fragment } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';

import { NavigationService } from '../../services';
import { DirectoryRulesActions, ErrorActions } from '../../actions';

import Selectors from '../../selectors/';
import LimitedAccess from '../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../constants/roles';
import {
    LinkButton,
    SimpleButton,
    ReconnectButton,
} from '../../widgets/Buttons/';

import DirectoryRuleList from './components/DirectoryRuleList';
import {
    ALL_DIRECTORY_RULES_LOADING,
    DIRECTORY_RULE_REMOVAL,
    DIRECTORY_RULE_LOADING,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

import { PageTitle, TopButtonBar, ErrorPanel } from '../../widgets/Layout/';

const mapDispatchToProps = dispatch => ({
    loadAll: bindActionCreators(
        DirectoryRulesActions.loadAllByIntercom,
        dispatch
    ),
    clearErrors: bindActionCreators(ErrorActions.clearErrorsByTypes, dispatch),
});

const mapStateToProps = state => ({
    rules: Selectors.getAllDirectoryRules(state),
    intercom: Selectors.getCurrentIntercom(state),
});

const errorsOfInterest = [
    ALL_DIRECTORY_RULES_LOADING,
    DIRECTORY_RULE_REMOVAL,
    DIRECTORY_RULE_LOADING,
    INTERCOM_CONFIG_GENERATION,
];

class DirectoryRules extends Component {
    componentDidMount() {
        this.props.loadAll(
            this.props.match.params.intercomId,
            this.props.history
        );
    }

    componentWillUnmount() {
        this.props.clearErrors(errorsOfInterest);
    }

    render() {
        const title = (
            <Fragment>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    <FormattedMessage id="rules.all.title" />
                </LimitedAccess>
                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    <FormattedMessage id="rules.all.csrTitle" />
                </LimitedAccess>
            </Fragment>
        );
        return (
            <div>
                <PageTitle title={title} />
                <div className="container-fluid">
                    <ErrorPanel operationTypes={errorsOfInterest} />
                    <div className="row">
                        <TopButtonBar>
                            <LinkButton
                                id="linkToAllIntercomsPage"
                                href={NavigationService.buildLinkToIntercomsPage()}
                                label={
                                    <FormattedMessage id="rules.all.cancel" />
                                }
                            />
                            <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                                <LinkButton
                                    id="addRule"
                                    href={NavigationService.buildLinkToNewDirectoryRulePage(
                                        this.props.match.params.intercomId
                                    )}
                                    label={
                                        <FormattedMessage id="rules.all.add" />
                                    }
                                />
                            </LimitedAccess>
                        </TopButtonBar>
                    </div>
                    <div className="row">
                        <div className="col">
                            <h5 className="entity-name wrapped-text-break-word">
                                <FormattedMessage
                                    id="rules.all.intercomLabel"
                                    values={{
                                        name:
                                            this.props.intercom &&
                                            this.props.intercom.name,
                                    }}
                                />
                            </h5>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col">
                            <DirectoryRuleList
                                rules={this.props.rules}
                                intercomId={this.props.match.params.intercomId}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DirectoryRules);
