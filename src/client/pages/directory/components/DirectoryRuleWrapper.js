import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

import { SimpleButton, LinkButton } from '../../../widgets/Buttons/';
import { PageTitle, ErrorPanel } from '../../../widgets/Layout/';
import { DIRECTORY_RULE } from '../../../constants/entity';
import DirectoryForm from './DirectoryForm';
import { NavigationService } from '../../../services/';
import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';

class DirectoryRuleWrapper extends Component {
    constructor(props) {
        super(props);
        this.onChange = this.onChange.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onContactTypeChange = this.onContactTypeChange.bind(this);
    }

    onChange(fieldName, value) {
        const updateIntercom = Object.assign({}, this.props.rule, {
            [fieldName]: value,
        });
        this.props.onChange(DIRECTORY_RULE, updateIntercom);
    }

    onContactTypeChange(value, index) {
        const currentContactTypes = this.props.rule.intercomGroupContactConfigs
            ? this.props.rule.intercomGroupContactConfigs.slice()
            : [];
        const matchingType = currentContactTypes.filter(
            row => row.index == index
        )[0];
        if (!!matchingType) {
            matchingType.brivoContactType = value;
        } else {
            currentContactTypes.push({ brivoContactType: value, index: index });
        }
        this.onChange(
            'intercomGroupContactConfigs',
            currentContactTypes.filter(row => !!row.brivoContactType)
        );
    }

    onSave() {
        this.props.onSave(
            this.props.intercom.id,
            this.props.rule,
            this.props.history
        );
    }

    render() {
        const groupsInUse = this.props.allRules
            .filter(row => row.id != this.props.rule.id)
            .map(row => row.brivoGroupId);
        const groupsToUse = this.props.groups.filter(
            group =>
                this.props.rule.brivoGroupId == group.id ||
                !groupsInUse.includes(group.id)
        );
        return (
            <div>
                <PageTitle title={this.props.title} />
                <div className="container-fluid">
                    <ErrorPanel operationTypes={this.props.errorTypes} />

                    <div className="row">
                        <div className="col">
                            <h5 className="entity-name directory-rules">
                                <FormattedMessage
                                    id="rules.form.intercomTitle"
                                    values={{ name: this.props.intercom.name }}
                                />
                            </h5>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-12 col-md-9 col-lg-7 col-xl-6">
                            <DirectoryForm
                                rule={this.props.rule}
                                groups={groupsToUse}
                                onChange={this.onChange}
                                onContactTypeChange={this.onContactTypeChange}
                            />
                        </div>
                    </div>
                    <div className="row" style={{ paddingTop: '30px' }}>
                        <div className="col-12 col-md-9 col-lg-7 col-xl-6 d-flex justify-content-between justify-content-sm-start">
                            <div
                                role="group"
                                aria-label="CRUD actions"
                                style={{ width: '100%' }}
                            >
                                <LinkButton
                                    id="cancelNewDirectoryRule"
                                    href={NavigationService.buildLinkToIntercomDirectoryPage(
                                        this.props.intercom.id
                                    )}
                                    label={
                                        <FormattedMessage id="rules.form.cancel" />
                                    }
                                    onClick={this.props.onCancel}
                                />
                                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                                    <SimpleButton
                                        id="saveNewDirectoryRule"
                                        label={
                                            <FormattedMessage id="rules.form.save" />
                                        }
                                        onClick={this.onSave}
                                    />
                                </LimitedAccess>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

DirectoryRuleWrapper.propTypes = {
    title: PropTypes.string.isRequired,
    rule: PropTypes.object.isRequired,
    allRules: PropTypes.array.isRequired,
    intercom: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    onSave: PropTypes.func.isRequired,
    errorTypes: PropTypes.array.isRequired,
};

export default withRouter(DirectoryRuleWrapper);
