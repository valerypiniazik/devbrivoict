import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Selectors from '../../../selectors/';
import { ConfirmationActions } from '../../../actions';
import { NavigationService } from '../../../services';
import LimitedAccess from '../../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../../constants/roles';
import {
    DeleteButton,
    LinkButton,
    EditButton,
    SimpleButton,
} from '../../../widgets/Buttons/';
import DirectoryRuleRemovalConfirmation from './DirectoryRuleRemovalConfirmation';

class DirectoryRulesList extends Component {
    constructor(props) {
        super(props);
        this.generateRows = this.generateRows.bind(this);
        this.generateValidRow = this.generateValidRow.bind(this);
        this.generateInvalidRow = this.generateInvalidRow.bind(this);
        this.getSpecificContactType = this.getSpecificContactType.bind(this);
        this.renderEditButtons = this.renderEditButtons.bind(this);
        this.renderCSREditButtons = this.renderCSREditButtons.bind(this);
        this.renderDeleteButtons = this.renderDeleteButtons.bind(this);
    }

    generateInvalidRow(rule) {
        return (
            <div
                className="row data1 brivo-list-row directory-rules-summary"
                key={rule.id}
            >
                <div className="col-12 col-md-10 d-flex align-items-center error directory-rules-warning table-cell">
                    <span className="text-md-left text-center">
                        <FormattedMessage id="rules.all.wrongGroup" />
                    </span>
                </div>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    {this.renderEditButtons(
                        rule,
                        'rules.all.editColumn',
                        'col-6 col-md-1'
                    )}
                    {this.renderDeleteButtons(rule)}
                </LimitedAccess>
                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    {this.renderCSREditButtons(
                        rule,
                        'rules.all.viewColumn',
                        'col-12 col-md-2'
                    )}
                </LimitedAccess>
            </div>
        );
    }

    getSpecificContactType(allTypes, index) {
        if (!!allTypes && !!allTypes.length) {
            const matchingTypes = allTypes.filter(row => row.index == index)[0];
            return !!matchingTypes ? matchingTypes.brivoContactType : '';
        } else {
            return '';
        }
    }

    renderEditButtons(rule, labelId, gridClasses) {
        return (
            <Fragment>
                <div
                    className={`${gridClasses} align-self-center text-md-center text-link table-cell d-block d-sm-block d-md-none`}
                >
                    <LinkButton
                        id={'editDirectoryRule' + rule.id}
                        label={<FormattedMessage id={labelId} />}
                        classNames="justify-content-start justify-content-md-center align-items-center"
                        href={NavigationService.buildLinkToIntercomDirectoryRulePage(
                            this.props.intercomId,
                            rule.id
                        )}
                    />
                </div>
                <div
                    className={`${gridClasses} align-self-center text-md-center text-link table-cell  d-none d-sm-none d-md-block`}
                >
                    <EditButton
                        id={'editDirectoryRule' + rule.id}
                        label={<FormattedMessage id={labelId} />}
                        classNames="justify-content-start justify-content-md-center align-items-center"
                        href={NavigationService.buildLinkToIntercomDirectoryRulePage(
                            this.props.intercomId,
                            rule.id
                        )}
                    />
                </div>
            </Fragment>
        );
    }

    renderCSREditButtons(rule, labelId, gridClasses) {
        return (
            <div
                className={`${gridClasses} align-self-center text-md-center table-cell`}
            >
                <LinkButton
                    id={'editDirectoryRule' + rule.id}
                    label={<FormattedMessage id={labelId} />}
                    classNames="justify-content-start justify-content-md-center align-items-center"
                    href={NavigationService.buildLinkToIntercomDirectoryRulePage(
                        this.props.intercomId,
                        rule.id
                    )}
                />
            </div>
        );
    }

    renderDeleteButtons(rule) {
        return (
            <Fragment>
                <div className="col-6 col-md-1 align-self-center text-md-center d-block d-sm-block d-md-none table-cell">
                    <SimpleButton
                        classNames="justify-content-start justify-content-md-center align-items-center"
                        label={<FormattedMessage id="rules.all.delete" />}
                        onClick={() => {
                            this.props.showDeleteConfirmation(rule);
                        }}
                    />
                </div>
                <div className="col-6 col-md-1 align-self-center text-md-center d-none d-sm-none d-md-block table-cell">
                    <DeleteButton
                        classNames="justify-content-start justify-content-md-center align-items-center"
                        label={<FormattedMessage id="rules.all.delete" />}
                        onClick={() => {
                            this.props.showDeleteConfirmation(rule);
                        }}
                    />
                </div>
            </Fragment>
        );
    }

    generateValidRow(rule) {
        const firstContactType = this.getSpecificContactType(
            rule.intercomGroupContactConfigs,
            1
        );
        const secondContactType = this.getSpecificContactType(
            rule.intercomGroupContactConfigs,
            2
        );
        const thirdContactType = this.getSpecificContactType(
            rule.intercomGroupContactConfigs,
            3
        );
        return (
            <div
                className="row data1 brivo-list-row directory-rules-summary"
                key={rule.id}
            >
                <div className="d-block d-sm-block d-md-none col-6 align-self-center">
                    <FormattedMessage id="rules.all.groupColumn" />
                </div>
                <div className="col-6 col-md-2 align-self-center wrapped-text">
                    <div className="brivo-list-cell">{rule.brivoGroupName}</div>
                </div>

                <div className="d-block d-sm-block d-md-none col-6 align-self-center">
                    <FormattedMessage id="rules.all.listingPatternColumn" />
                </div>
                <div className="col-6 col-md-2 align-self-center text-truncate wrapped-text table-cell">
                    <div className="brivo-list-cell">{rule.formatString}</div>
                </div>

                <div className="d-block d-sm-block d-md-none col-6 align-self-center text-md-center">
                    <FormattedMessage id="rules.all.firstPhoneTypeColumn" />
                </div>
                <div className="col-6 col-md-2 align-self-center text-md-center wrapped-text table-cell">
                    <div className="brivo-list-cell">{firstContactType}</div>
                </div>

                <div className="d-block d-sm-block d-md-none col-6 align-self-center text-md-center">
                    <FormattedMessage id="rules.all.secondPhoneTypeColumn" />
                </div>
                <div className="col-6 col-md-2 align-self-center text-md-center wrapped-text table-cell">
                    <div className="brivo-list-cell">{secondContactType}</div>
                </div>

                <div className="d-block d-sm-block d-md-none col-6 align-self-center text-md-center">
                    <FormattedMessage id="rules.all.thirdPhoneTypeColumn" />
                </div>
                <div className="col-6 col-md-2 align-self-center text-md-center wrapped-text table-cell">
                    <div className="brivo-list-cell">{thirdContactType}</div>
                </div>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    {this.renderEditButtons(
                        rule,
                        'rules.all.editColumn',
                        'col-6 col-md-1'
                    )}
                    {this.renderDeleteButtons(rule)}
                </LimitedAccess>
                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    {this.renderCSREditButtons(
                        rule,
                        'rules.all.viewColumn',
                        'col-4 col-md-2'
                    )}
                </LimitedAccess>
            </div>
        );
    }

    generateRows() {
        const { rules } = this.props;
        const result =
            !!rules && rules.length > 0 ? (
                rules.map(rule => {
                    if (!!rule.brivoGroupName) {
                        return this.generateValidRow(rule);
                    } else {
                        return this.generateInvalidRow(rule);
                    }
                })
            ) : (
                <div className="row data1 brivo-list-row">
                    <div
                        className="col empty-list-column d-flex align-items-center"
                        id="noRulesMessage"
                    >
                        <FormattedMessage id="rules.all.empty" />
                    </div>
                </div>
            );
        return result;
    }

    render() {
        const { rules } = this.props;
        const rows = !!rules ? this.generateRows(rules) : null;
        return (
            <Fragment>
                <DirectoryRuleRemovalConfirmation
                    intercomId={this.props.intercomId}
                />
                <div className="container-fluid brivo-list">
                    <div className="row brivo-list-header">
                        <div className="col-2 col-md-2 d-none d-sm-none d-md-block">
                            <FormattedMessage id="rules.all.groupColumn" />
                        </div>
                        <div className="col-2 col-md-2 d-none d-sm-none d-md-block table-cell">
                            <FormattedMessage id="rules.all.listingPatternColumn" />
                        </div>
                        <div className="col-2 col-md-2 d-none d-sm-none d-md-block text-center table-cell">
                            <FormattedMessage id="rules.all.firstPhoneTypeColumn" />
                        </div>
                        <div className="col-2 col-md-2 d-none d-sm-none d-md-block text-center table-cell">
                            <FormattedMessage id="rules.all.secondPhoneTypeColumn" />
                        </div>
                        <div className="col-2 col-md-2 d-none d-sm-none d-md-block text-center table-cell">
                            <FormattedMessage id="rules.all.thirdPhoneTypeColumn" />
                        </div>
                        <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                            <div className="col-2 col-md-1 d-none d-sm-none d-md-block text-center table-cell">
                                <FormattedMessage id="rules.all.editColumn" />
                            </div>
                            <div className="col-2 col-md-1 d-none d-sm-none d-md-block text-center table-cell">
                                <FormattedMessage id="rules.all.deleteColumn" />
                            </div>
                        </LimitedAccess>
                        <LimitedAccess requiredRoles={[ROLE_CSR]}>
                            <div className="col-4 col-md-2 d-none d-sm-none d-md-block text-center table-cell">
                                <FormattedMessage id="rules.all.viewColumn" />
                            </div>
                        </LimitedAccess>
                    </div>
                    {rows}
                </div>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    showDeleteConfirmation: bindActionCreators(
        ConfirmationActions.showDeleteConfirmation,
        dispatch
    ),
});

DirectoryRulesList.propTypes = {
    rules: PropTypes.array.isRequired,
    intercomId: PropTypes.number.isRequired,
};

export default connect(
    null,
    mapDispatchToProps
)(DirectoryRulesList);
