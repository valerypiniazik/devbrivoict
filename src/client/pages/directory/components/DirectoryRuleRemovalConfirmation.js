import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import Selectors from '../../../selectors/';

import { DirectoryRulesActions, ConfirmationActions } from '../../../actions';
import { DeleteConfirmationModal } from '../../../widgets/Layout/';

class DirectoryRuleRemovalConfirmation extends Component {
    constructor(props) {
        super(props);
        this.performConfirmedRemoval = this.performConfirmedRemoval.bind(this);
        this.renderConfirmationLabel = this.renderConfirmationLabel.bind(this);
        this.cancelConfirmation = this.cancelConfirmation.bind(this);
    }

    performConfirmedRemoval() {
        this.props.deleteDirectoryRule(
            this.props.deletedRule.id,
            this.props.intercomId
        );
        this.props.hideDeleteConfirmation();
    }

    cancelConfirmation() {
        this.props.hideDeleteConfirmation();
    }

    renderConfirmationLabel() {
        const groupName = !!this.props.deletedRule.brivoGroupName
            ? this.props.deletedRule.brivoGroupName
            : '';
        return (
            <FormattedMessage
                id="rules.all.confirmRemoval"
                values={{ groupName: groupName }}
            />
        );
    }

    render() {
        return (
            <DeleteConfirmationModal
                id="directoryRulesRemovalConfirmation"
                visible={this.props.isVisible}
                renderLabel={this.renderConfirmationLabel}
                onConfirm={this.performConfirmedRemoval}
                onCancel={this.cancelConfirmation}
            />
        );
    }
}

DirectoryRuleRemovalConfirmation.propTypes = {
    intercomId: PropTypes.number.isRequired,
};

const mapStateToProps = state => ({
    isVisible: Selectors.isConfirmationVisible(state),
    deletedRule: Selectors.getConfirmationEntity(state),
});

const mapDispatchToProps = dispatch => ({
    showDeleteConfirmation: bindActionCreators(
        ConfirmationActions.showDeleteConfirmation,
        dispatch
    ),
    hideDeleteConfirmation: bindActionCreators(
        ConfirmationActions.hideDeleteConfirmation,
        dispatch
    ),
    deleteDirectoryRule: bindActionCreators(
        DirectoryRulesActions.remove,
        dispatch
    ),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DirectoryRuleRemovalConfirmation);
