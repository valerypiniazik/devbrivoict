import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { TextInput, SmartSelect } from '../../../widgets/Input/';
import { SimpleButton } from '../../../widgets/Buttons/';

import { DirectoryRulesActions, OperationActions } from '../../../actions';
import { DIRECTORY_RULE } from '../../../constants/entity';

import { ROLE_CSR } from '../../../constants/roles';
import { UserService } from '../../../services/';
import Selectors from '../../../selectors/';

class DirectoryForm extends Component {
    constructor(props) {
        super(props);
        this.onGroupChange = this.onGroupChange.bind(this);
        this.onFormatStringChange = this.onFormatStringChange.bind(this);
        this.onThirdPhoneTypeChange = this.onThirdPhoneTypeChange.bind(this);
        this.onFirstPhoneTypeChange = this.onFirstPhoneTypeChange.bind(this);
        this.onSecondPhoneTypeChange = this.onSecondPhoneTypeChange.bind(this);
        this.getSpecificContactType = this.getSpecificContactType.bind(this);
    }

    onGroupChange(value) {
        this.props.onChange('brivoGroupId', value);
    }

    onFormatStringChange(value) {
        this.props.onChange('formatString', value);
    }

    onFirstPhoneTypeChange(value) {
        this.props.onContactTypeChange(value, 1);
    }

    onSecondPhoneTypeChange(value) {
        this.props.onContactTypeChange(value, 2);
    }

    onThirdPhoneTypeChange(value) {
        this.props.onContactTypeChange(value, 3);
    }

    getSpecificContactType(allTypes, index) {
        if (!!allTypes && !!allTypes.length) {
            const matchingTypes = allTypes.filter(row => row.index == index)[0];
            return !!matchingTypes ? matchingTypes.brivoContactType : '';
        } else {
            return '';
        }
    }

    render() {
        const buildGroupDto = row => {
            return { value: row.id, label: row.name };
        };
        const groups = this.props.groups.map(buildGroupDto);
        const allContactTypes = this.props.rule.intercomGroupContactConfigs;
        const firstContactType = this.getSpecificContactType(
            allContactTypes,
            1
        );
        const secondContactType = this.getSpecificContactType(
            allContactTypes,
            2
        );
        const thirdContactType = this.getSpecificContactType(
            allContactTypes,
            3
        );

        return (
            <div>
                <form>
                    <div className="form-row">
                        <SmartSelect
                            id="group"
                            value={this.props.rule.brivoGroupId}
                            options={groups}
                            containerClassName="col-12 col-md-4"
                            label={<FormattedMessage id="rules.form.group" />}
                            onChange={this.onGroupChange}
                            readOnly={UserService.hasPermissions(
                                this.props.currentUser,
                                [ROLE_CSR]
                            )}
                        />
                    </div>
                    <div className="form-row">
                        <TextInput
                            id="listingPattern"
                            value={this.props.rule.formatString}
                            containerClassName="col-12 col-md-4"
                            label={<FormattedMessage id="rules.form.pattern" />}
                            tooltip={
                                <FormattedMessage id="rules.form.patternTooltip" />
                            }
                            max={255}
                            onChange={this.onFormatStringChange}
                        />
                    </div>
                    <div className="form-row">
                        <TextInput
                            id="firstPhoneType"
                            value={firstContactType}
                            containerClassName="col-12 col-md-4"
                            label={
                                <FormattedMessage id="rules.form.firstPhoneType" />
                            }
                            max={255}
                            onChange={this.onFirstPhoneTypeChange}
                        />
                        <TextInput
                            id="secondPhoneType"
                            value={secondContactType}
                            containerClassName="col-12 col-md-4"
                            label={
                                <FormattedMessage id="rules.form.secondPhoneType" />
                            }
                            max={255}
                            onChange={this.onSecondPhoneTypeChange}
                        />
                        <TextInput
                            id="thirdPhoneType"
                            value={thirdContactType}
                            containerClassName="col-12 col-md-4"
                            label={
                                <FormattedMessage id="rules.form.thirdPhoneType" />
                            }
                            max={255}
                            onChange={this.onThirdPhoneTypeChange}
                        />
                    </div>
                </form>
            </div>
        );
    }
}

DirectoryForm.propTypes = {
    rule: PropTypes.object.isRequired,
    groups: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired,
    onContactTypeChange: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

export default connect(
    mapStateToProps,
    null
)(DirectoryForm);
