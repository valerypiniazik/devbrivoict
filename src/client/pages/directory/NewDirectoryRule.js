import React, { Component } from 'react';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import Selectors from '../../selectors/';
import {
    DirectoryRulesActions,
    OperationActions,
    ErrorActions,
} from '../../actions';

import { DIRECTORY_RULE } from '../../constants/entity';
import {
    DIRECTORY_RULE_SAVING,
    DIRECTORY_RULE_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

import DirectoryRuleWrapper from './components/DirectoryRuleWrapper';

const mapDispatchToProps = dispatch => ({
    onInit: bindActionCreators(OperationActions.initOperation, dispatch),
    onSave: bindActionCreators(DirectoryRulesActions.save, dispatch),
    onChange: bindActionCreators(OperationActions.updateOperation, dispatch),
    onCancel: bindActionCreators(OperationActions.clearOperation, dispatch),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    intercom: Selectors.getCurrentIntercom(state),
    rules: Selectors.getAllDirectoryRules(state),
    groups: Selectors.getAllGroups(state),
    current: Selectors.getCurrentOperationObject(state, DIRECTORY_RULE),
});

const errorsOfInterest = [
    DIRECTORY_RULE_SAVING,
    DIRECTORY_RULE_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
];

class NewDirectoryRule extends Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
    }

    componentWillUnmount() {
        this.onCancel();
    }

    componentDidMount() {
        this.props.onInit(DIRECTORY_RULE, {});
    }

    onCancel() {
        this.props.onCancel(DIRECTORY_RULE);
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    render() {
        return !!this.props.current && !!this.props.intercom ? (
            <DirectoryRuleWrapper
                title={<FormattedMessage id="rules.new.title" />}
                errorTypes={errorsOfInterest}
                intercom={this.props.intercom}
                rule={this.props.current}
                allRules={this.props.rules}
                groups={this.props.groups}
                onSave={this.props.onSave}
                onChange={this.props.onChange}
                onCancel={this.onCancel}
            />
        ) : null;
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NewDirectoryRule);
