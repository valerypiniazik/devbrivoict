import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import LimitedAccess from '../../widgets/LimitedAccess';
import { ROLE_CSR } from '../../constants/roles';
import DirectoryRuleWrapper from './components/DirectoryRuleWrapper';
import Selectors from '../../selectors/';
import {
    DirectoryRulesActions,
    OperationActions,
    ErrorActions,
} from '../../actions';
import { DIRECTORY_RULE } from '../../constants/entity';
import {
    DIRECTORY_RULE_UPDATE,
    DIRECTORY_RULE_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
} from '../../constants/error';

const mapDispatchToProps = dispatch => ({
    onInit: bindActionCreators(DirectoryRulesActions.loadById, dispatch),
    onSave: bindActionCreators(DirectoryRulesActions.update, dispatch),
    onChange: bindActionCreators(OperationActions.updateOperation, dispatch),
    onCancel: bindActionCreators(OperationActions.clearOperation, dispatch),
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    intercom: Selectors.getCurrentIntercom(state),
    rules: Selectors.getAllDirectoryRules(state),
    groups: Selectors.getAllGroups(state),
    current: Selectors.getCurrentOperationObject(state, DIRECTORY_RULE),
});

const errorsOfInterest = [
    DIRECTORY_RULE_UPDATE,
    DIRECTORY_RULE_VALIDATION,
    INTERCOM_CONFIG_GENERATION,
];

class DirectoryRuleDetails extends Component {
    constructor(props) {
        super(props);
        this.onCancel = this.onCancel.bind(this);
    }

    componentWillUnmount() {
        this.onCancel();
    }

    componentDidMount() {
        this.props.onInit(
            this.props.match.params.intercomId,
            this.props.match.params.ruleId,
            this.props.history
        );
    }

    onCancel() {
        this.props.onCancel(DIRECTORY_RULE);
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    render() {
        const title = (
            <Fragment>
                <LimitedAccess nonAllowedRoles={[ROLE_CSR]}>
                    <FormattedMessage id="rules.edit.title" />
                </LimitedAccess>
                <LimitedAccess requiredRoles={[ROLE_CSR]}>
                    <FormattedMessage id="rules.edit.csrTitle" />
                </LimitedAccess>
            </Fragment>
        );

        return !!this.props.current && !!this.props.intercom ? (
            <DirectoryRuleWrapper
                title={title}
                errorTypes={errorsOfInterest}
                intercom={this.props.intercom}
                rule={this.props.current}
                allRules={this.props.rules}
                groups={this.props.groups}
                onSave={this.props.onSave}
                onChange={this.props.onChange}
                onCancel={this.onCancel}
            />
        ) : null;
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DirectoryRuleDetails);
