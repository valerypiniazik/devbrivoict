import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import AccountSelection from './account/AccountSelection';

class Account extends Component {
    render() {
        return (
            <Switch>
                <Route
                    path="*"
                    name="Selection an Account"
                    component={AccountSelection}
                />
            </Switch>
        );
    }
}

export default withRouter(Account);
