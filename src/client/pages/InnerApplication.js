import React from 'react';
import { CookiesProvider, withCookies } from 'react-cookie';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BRIVO_ACCOUNT_ID_COOKIE } from '../constants/account';
import Selectors from '../selectors/';

import { AccountActions, UserActions } from '../actions';
import { Application } from '../widgets/Layout';
import Intercom from './Intercom';
import Error404 from './Error404';
import Error403 from './Error403';
import ErrorNoAccessForCsr from './ErrorNoAccessForCsr';
import Account from './Account';

import { UserService } from '../services/';
import { ROLE_CSR } from '../constants/roles';

const mapDispatchToProps = dispatch => ({
    getAccountStatus: bindActionCreators(
        AccountActions.getAccountStatus,
        dispatch
    ),
});

const mapStateToProps = state => ({
    accountStatus: Selectors.getCurrentAccountStatus(state),
    currentUser: Selectors.getCurrentUser(state),
});

class InnerApplication extends React.Component {
    constructor(props) {
        super(props);
        this.props.getAccountStatus();
    }

    render() {
        const isCsr =
            !!this.props.currentUser &&
            UserService.hasPermissions(this.props.currentUser, [ROLE_CSR]);

        if (isCsr && !this.props.accountStatus) {
            return null;
        }
        if (
            isCsr &&
            !!this.props.accountStatus &&
            !this.props.accountStatus.active
        ) {
            return (
                <Switch>
                    <Route
                        path="*"
                        name="No access for CSR"
                        component={ErrorNoAccessForCsr}
                    />
                </Switch>
            );
        } else {
            return (
                <Switch>
                    <Route
                        path="/intercom"
                        name="Intercom"
                        component={Intercom}
                    />
                    <Route exact path="/" name="Landing" component={Intercom} />
                    <Route
                        exact
                        path="/error403"
                        name="Forbidden page"
                        component={Error403}
                    />
                    <Route name="Default Error Page" component={Error404} />
                </Switch>
            );
        }
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withCookies(InnerApplication))
);
