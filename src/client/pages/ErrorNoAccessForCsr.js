import React, { Component } from 'react';
import { PageTitle } from '../widgets/Layout/';
import { FormattedMessage } from 'react-intl';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Selectors from '../selectors/';

const mapStateToProps = state => ({
    accountStatus: Selectors.getCurrentAccountStatus(state),
});

class ErrorNoAccessForCsr extends Component {
    render() {
        if (this.props.accountStatus) {
            const status = this.props.accountStatus.admin;
            return (
                <div>
                    <PageTitle title={<FormattedMessage id="error.title" />} />
                    <div className="container-fluid">
                        <div id="error-page-msg">
                            <i className="warning-icon" />
                            <span className="warning">
                                <FormattedMessage id="error.csrNoAccess.rootCause" />
                            </span>
                            <br />
                            <br />
                            <span className="warning">
                                <FormattedMessage
                                    id="error.csrNoAccess.masterId"
                                    values={{ master_admin_id: status.id }}
                                />
                            </span>
                            <br />
                            <span className="warning">
                                <FormattedMessage
                                    id="error.csrNoAccess.accountId"
                                    values={{ account_id: status.accountId }}
                                />
                            </span>
                            <br />
                            <span className="warning">
                                <FormattedMessage
                                    id="error.csrNoAccess.username"
                                    values={{ username: status.username }}
                                />
                            </span>
                            <br />
                            <span className="warning">
                                <FormattedMessage
                                    id="error.csrNoAccess.name"
                                    values={{
                                        name:
                                            status.firstName +
                                            ' ' +
                                            status.lastName,
                                    }}
                                />
                            </span>
                            <br />

                            <span className="warning">
                                <FormattedMessage
                                    id="error.csrNoAccess.email"
                                    values={{ email: status.email }}
                                />
                            </span>
                            <br />
                            <br />
                            <br />
                            <span className="warning">
                                <FormattedMessage id="error.csrNoAccess.checklistMessage" />
                            </span>
                            <ul>
                                <li>
                                    <span className="warning">
                                        <FormattedMessage id="error.csrNoAccess.checklistItem1" />
                                    </span>
                                </li>
                                <li>
                                    <span className="warning">
                                        <FormattedMessage id="error.csrNoAccess.checklistItem2" />
                                    </span>
                                </li>
                                <li>
                                    <span className="warning">
                                        <FormattedMessage id="error.csrNoAccess.checklistItem3" />
                                    </span>
                                </li>
                                <li>
                                    <span className="warning">
                                        <FormattedMessage id="error.csrNoAccess.checklistItem4" />
                                    </span>
                                </li>
                                <li>
                                    <span className="warning">
                                        <FormattedMessage id="error.csrNoAccess.checklistItem5" />
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            );
        } else {
            return null;
        }
    }
}

export default connect(
    mapStateToProps,
    null
)(ErrorNoAccessForCsr);
