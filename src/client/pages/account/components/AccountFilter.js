import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import Selectors from '../../../selectors/';

import { AccountActions } from '../../../actions';
import { TextInput } from '../../../widgets/Input/';
import { SimpleButton } from '../../../widgets/Buttons/';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const mapStateToProps = state => ({
    filter: Selectors.getAccountFilterValue(state),
});

const mapDispatchToProps = dispatch => ({
    onFilter: bindActionCreators(AccountActions.filterAccounts, dispatch),
    onChange: bindActionCreators(AccountActions.changeAccountFilter, dispatch),
});

class AccountFilter extends Component {
    constructor(props) {
        super(props);
        this.onNumberChange = this.onNumberChange.bind(this);
        this.onNameChange = this.onNameChange.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    onNumberChange(newValue) {
        const filter = Object.assign({}, this.props.filter);
        filter.accountNumber = newValue;
        this.props.onChange(filter);
    }

    onNameChange(newValue) {
        const filter = Object.assign({}, this.props.filter);
        filter.accountName = newValue;
        this.props.onChange(filter);
    }

    onSearch(e) {
        if (e) {
            e.preventDefault();
        }
        const { filter } = this.props;
        this.props.onFilter(filter.accountNumber, filter.accountName);
    }

    render() {
        const { filter } = this.props;
        const accountNumberFilter =
            !!filter && !!filter.accountNumber ? filter.accountNumber : '';
        const accountNameFilter =
            !!filter && !!filter.accountName ? filter.accountName : '';
        return (
            <div className="brivo-account-filter">
                <form>
                    <div className="form-row col col-sm-10 col-md-8">
                        <TextInput
                            id="accountId"
                            value={accountNumberFilter}
                            containerClassName="col-12 col-md-5 account-selection-input"
                            label={<FormattedMessage id="accounts.filter.id" />}
                            max={255}
                            onChange={this.onNumberChange}
                        />

                        <TextInput
                            id="accountName"
                            value={accountNameFilter}
                            containerClassName="col-12 col-md-5 account-selection-input"
                            label={
                                <FormattedMessage id="accounts.filter.name" />
                            }
                            max={255}
                            onChange={this.onNameChange}
                        />

                        <div className="form-group col-12 col-md-2 d-flex align-items-end">
                            <SimpleButton
                                id="accountSearch"
                                className="account-search-button"
                                label={
                                    <span>
                                        <FontAwesomeIcon icon="search" />{' '}
                                        <FormattedMessage id="accounts.filter.search" />{' '}
                                    </span>
                                }
                                onClick={this.onSearch}
                            />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(AccountFilter)
);
