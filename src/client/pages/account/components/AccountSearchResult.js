import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';

class AccountSearchResult extends Component {
    render() {
        let query = '';
        return (
            <div className="form-group col-12 brivo-search-result">
                <label>
                    <FormattedMessage id="accounts.filter.searchResult" />
                </label>
                <br />
                <small style={{ color: '#c4c4c4' }}>
                    <FormattedMessage
                        id="accounts.all.resultsTotal"
                        values={{
                            amount: this.props.displayedAmountOfAccounts,
                            total_amount: this.props.totalAmountOfAccounts,
                            query_string: query,
                        }}
                    />
                </small>
            </div>
        );
    }
}

AccountSearchResult.propTypes = {
    totalAmountOfAccounts: PropTypes.number.isRequired,
    displayedAmountOfAccounts: PropTypes.number.isRequired,
    filter: PropTypes.object.isRequired,
};

export default AccountSearchResult;
