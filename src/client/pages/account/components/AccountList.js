import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { AccountActions } from '../../../actions';
import { SimpleButton } from '../../../widgets/Buttons/';

const mapDispatchToProps = dispatch => ({
    selectAccount: bindActionCreators(AccountActions.selectAccount, dispatch),
});

class AccountList extends Component {
    constructor(props) {
        super(props);
        this.generateRows = this.generateRows.bind(this);
    }

    generateRows() {
        const { accounts } = this.props;
        return accounts.map(account => (
            <div className="row data1 brivo-list-row" key={account.id}>
                <div className="d-block d-sm-block d-md-none col-6 align-self-center">
                    <FormattedMessage id="accounts.all.idColumn" />
                </div>
                <div className="col-6 col-md-5 align-self-center">
                    {account.accountNumber}
                </div>
                <div className="d-block d-sm-block d-md-none col-6 align-self-center">
                    <FormattedMessage id="accounts.all.nameColumn" />
                </div>
                <div className="col-6 col-md-5 align-self-center wrapped-text">
                    {account.name}
                </div>
                <div className="col-md-2 col-12 align-self-center">
                    <SimpleButton
                        id={`viewAccount_${account.id}`}
                        label={<FormattedMessage id="accounts.all.view" />}
                        onClick={() => {
                            this.props.selectAccount(account.id);
                        }}
                    />
                </div>
            </div>
        ));
    }

    render() {
        const { accounts } = this.props;
        const rows = !!accounts ? this.generateRows() : null;
        return (
            <div className="brivo-account-list">
                <div className="container-fluid brivo-list">
                    <div className="row brivo-list-header">
                        <div className="col-md-5 d-none d-sm-none d-md-block">
                            <FormattedMessage id="accounts.all.idColumn" />
                        </div>
                        <div className="col-md-5 d-none d-sm-none d-md-block">
                            <FormattedMessage id="accounts.all.nameColumn" />
                        </div>
                        <div className="col-md-2 d-none d-sm-none d-md-block" />
                    </div>
                    {rows}
                </div>
            </div>
        );
    }
}

AccountList.propTypes = {
    accounts: PropTypes.array.isRequired,
};

export default connect(
    null,
    mapDispatchToProps
)(AccountList);
