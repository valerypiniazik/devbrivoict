import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { FormattedMessage } from 'react-intl';
import FadeLoader from 'react-spinners/FadeLoader';

import Selectors from '../../selectors/';

import { AccountActions, ErrorActions } from '../../actions';
import { PageTitle, ErrorPanel } from '../../widgets/Layout/';
import AccountList from './components/AccountList';
import AccountFilter from './components/AccountFilter';
import AccountSearchResult from './components/AccountSearchResult';

import { ALL_ACCOUNTS_LOADING } from '../../constants/error';

const mapDispatchToProps = dispatch => ({
    clearErrorsByTypes: bindActionCreators(
        ErrorActions.clearErrorsByTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    accounts: Selectors.getAllAccounts(state),
    totalAccounts: Selectors.getTotalCountOfAccountsMatching(state),
    isSearchInitiated: Selectors.isAccountSearchStarted(state),
    isSearchInProgress: Selectors.isSearchInProgress(state),
    filterValue: Selectors.getAccountFilterValue(state),
});

const errorsOfInterest = [ALL_ACCOUNTS_LOADING];

class AccountSelection extends Component {
    componentWillUnmount() {
        this.props.clearErrorsByTypes(errorsOfInterest);
    }

    render() {
        const {
            accounts,
            isSearchInitiated,
            isSearchInProgress,
            totalAccounts,
        } = this.props;
        const accountToUse = !!accounts ? accounts.slice(0, 50) : [];
        const accountCount = totalAccounts || 0;

        let content;
        if (isSearchInProgress) {
            content = (
                <div className="col" style={{ height: '100%' }}>
                    <div className="sweet-loading d-flex justify-content-center align-items-center account-search-filter-wrapper">
                        <FadeLoader
                            className="account-search-filter"
                            sizeUnit={'px'}
                            size={150}
                            color={'#fff'}
                            loading={isSearchInProgress}
                        />
                    </div>
                </div>
            );
        } else {
            if (!!isSearchInitiated) {
                content = (
                    <AccountSearchResult
                        totalAmountOfAccounts={accountCount}
                        displayedAmountOfAccounts={accountToUse.length}
                        filter={this.props.filterValue}
                    />
                );
            }
        }

        return (
            <div>
                <PageTitle
                    title={<FormattedMessage id="accounts.all.title" />}
                />
                <div className="container-fluid">
                    <ErrorPanel operationTypes={errorsOfInterest} />
                    <div className="row">
                        <div className="col">
                            <AccountFilter />
                            {content}
                            {!isSearchInProgress &&
                            !!accounts &&
                            accounts.length > 0 ? (
                                <AccountList accounts={accountToUse} />
                            ) : null}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(AccountSelection)
);
