import React, { Component } from 'react';
import { PageTitle } from '../widgets/Layout/';
import { FormattedMessage } from 'react-intl';

class Error403 extends Component {
    render() {
        return (
            <div>
                <PageTitle title={<FormattedMessage id="error.title" />} />
                <div className="container-fluid">
                    <div id="error-page-msg">
                        <i className="warning-icon" />
                        <span className="warning">
                            <FormattedMessage id="error.error403" />
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Error403;
