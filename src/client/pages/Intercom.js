import React, { Component } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { IntercomActions } from '../actions';
import { UserService } from '../services/';
import Selectors from '../selectors/';
import { ROLE_CSR } from '../constants/roles';

import AllIntercoms from './intercom/AllIntercoms';
import NewIntercom from './intercom/NewIntercom';
import IntercomViews from './IntercomViews';
import Error404 from './Error404';

const mapDispatchToProps = dispatch => ({
    initAllIntercomTypes: bindActionCreators(
        IntercomActions.getIntercomTypes,
        dispatch
    ),
});

const mapStateToProps = state => ({
    currentUser: Selectors.getCurrentUser(state),
});

class Intercoms extends Component {
    componentDidMount() {
        this.props.initAllIntercomTypes();
    }

    render() {
        const couldCreateIntercom =
            !!this.props.currentUser &&
            !UserService.hasPermissions(this.props.currentUser, [ROLE_CSR]);
        const newIntercomRoute = couldCreateIntercom ? (
            <Route
                path="/intercom/new"
                name="Configure Intercom"
                component={NewIntercom}
            />
        ) : null;
        return (
            <Switch>
                {newIntercomRoute}
                <Route
                    exact
                    path="/"
                    name="Intercom listing"
                    component={AllIntercoms}
                />
                <Route
                    exact
                    path="/intercom"
                    name="Intercom listing"
                    component={AllIntercoms}
                />
                <Route
                    path="/intercom/:intercomId"
                    name="Data about specific intercom"
                    component={IntercomViews}
                />
                <Route name="Default Error Page" component={Error404} />
            </Switch>
        );
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(Intercoms)
);
