export default {
    getCurrentUser(state) {
        return state.user.current;
    },

    getCurrentOperationObject(state, operation) {
        return state.operation.operations[operation]
            ? state.operation.operations[operation].current
            : undefined;
    },

    getAllIntercomTypes(state) {
        return state.intercom.types;
    },

    getAllGroups(state) {
        return state.group.all;
    },

    getAllDirectoryRules(state) {
        return state.directoryRules.all;
    },

    getCurrentIntercom(state) {
        return state.intercom.current;
    },

    getTokenStatus(state) {
        return state.intercom.tokenStatus;
    },

    getAllIntercoms(state) {
        return state.intercom.intercoms;
    },

    getIntercomHistory(state) {
        return state.intercom.currentIntercomHistory;
    },

    isConfirmationVisible(state) {
        return state.confirmation.isVisible;
    },

    getConfirmationEntity(state) {
        return state.confirmation.confirmationEntity;
    },

    getAllAccounts(state) {
        return state.account.accounts;
    },

    getRawAccounts(state) {
        return state.account.rawAccounts;
    },

    getCurrentAccount(state) {
        return state.account.current;
    },

    getCurrentAccountStatus(state) {
        return state.account.status;
    },

    isAccountSearchStarted(state) {
        return state.account.isSearchInitiated;
    },

    isSearchInProgress(state) {
        return state.account.isSearchInProgress;
    },

    getAccountFilterValue(state) {
        return state.account.filterValue;
    },

    getTotalCountOfAccountsMatching(state) {
        return state.account.totalCount;
    },

    getAllErrors(state) {
        return state.error.errors;
    },
};
