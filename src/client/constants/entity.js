export const INTERCOM = 'INTERCOM';
export const INTERCOM_TYPE = 'INTERCOM_TYPE';
export const ACCOUNT = 'ACCOUNT';
export const USER = 'USER';
export const DIRECTORY_RULE = 'DIRECTORY_RULE';
