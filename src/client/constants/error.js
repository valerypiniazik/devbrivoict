export const RAISE_ERROR = 'RAISE_ERROR';
export const SUPPRESS_ERROR = 'SUPPRESS_ERROR';
export const AUTH_ERROR = 'AUTH_ERROR';

export const ALL_INTERCOM_TYPES_LOADING = 'models.error.all_loading';
export const ALL_ACCOUNTS_LOADING = 'accounts.error.all_loading';
export const INTERCOM_LOADING = 'intercoms.error.single_loading';
export const INTERCOM_HISTORY_LOADING = 'history.error.all_loading';
export const ALL_INTERCOMS_LOADING = 'intercoms.error.all_loading';
export const INTERCOM_SAVING = 'intercoms.error.saving';
export const INTERCOM_REMOVAL = 'intercoms.error.removal';
export const INTERCOM_UPDATE = 'intercoms.error.update';
export const INTERCOM_VALIDATION = 'INTERCOM_VALIDATION';
export const INTERCOM_CONFIG_GENERATION = 'intercoms.error.config_generation';

export const ALL_DIRECTORY_RULES_LOADING = 'rules.error.all_loading';
export const DIRECTORY_RULE_LOADING = 'rules.error.single_loading';
export const DIRECTORY_RULE_REMOVAL = 'rules.error.removal';

export const DIRECTORY_RULE_SAVING = 'rules.error.saving';

export const DIRECTORY_RULE_UPDATE = 'rules.error.update';
export const DIRECTORY_RULE_VALIDATION = 'DIRECTORY_RULE_VALIDATION';
