export const ALL_INTERCOMS = '/intercom';
export const NEW_INTERCOM = '/intercom/new';
export const FILE_GENERATION_HISTORY = '/history';
export const INTERCOM_DIRECTORY = '/directory';
export const NEW_DIRECTORY_RULE = '/directory/new';
export const ACCOUNT_SELECTION = '/account';
export const ACS_BRIVO_COM = 'https://acs.brivo.com';
