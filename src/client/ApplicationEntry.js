import React from 'react';
import { CookiesProvider, withCookies } from 'react-cookie';
import { Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { BRIVO_ACCOUNT_ID_COOKIE } from './constants/account';
import Selectors from './selectors/';

import { AccountActions, UserActions } from './actions';
import { Application } from './widgets/Layout';
import InnerApplication from './pages/InnerApplication';
import Account from './pages/Account';

import { UserService } from './services/';
import { ROLE_CSR } from './constants/roles';

const mapDispatchToProps = dispatch => ({
    fetchAccounts: bindActionCreators(AccountActions.getAllAccounts, dispatch),
    selectAccount: bindActionCreators(AccountActions.selectAccount, dispatch),
    fetchCurrentUser: bindActionCreators(UserActions.getCurrentUser, dispatch),
});

const mapStateToProps = state => ({
    accounts: Selectors.getRawAccounts(state),
    selectedAccount: Selectors.getCurrentAccount(state),
    currentUser: Selectors.getCurrentUser(state),
});

class ApplicationEntry extends React.Component {
    constructor(props) {
        super(props);
        this.determineContent = this.determineContent.bind(this);
        // this.props.cookies.remove(BRIVO_ACCOUNT_ID_COOKIE); //TODO do user with more then one account always has to choose one? Or it needs to happen only on logout?
        this.props.fetchAccounts();
        this.props.fetchCurrentUser();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.selectedAccount != this.props.selectedAccount) {
            if (nextProps.selectedAccount) {
                this.props.cookies.set(
                    BRIVO_ACCOUNT_ID_COOKIE,
                    nextProps.selectedAccount
                );
            } else {
                this.props.cookies.remove(BRIVO_ACCOUNT_ID_COOKIE);
            }
        }
    }

    determineContent() {
        const { accounts } = this.props;
        let content;
        if (!accounts) {
            content = null; //TODO clarify what to display in this case
        }
        if (!!accounts && accounts.length == 0) {
            content = null; //TODO clarify what to display in this case
        }
        if (!!accounts && accounts.length > 1 && !this.props.selectedAccount) {
            content = <Account />;
        }
        if (!!this.props.selectedAccount && !!this.props.currentUser) {
            content = <InnerApplication />;
        }
        return content;
    }

    render() {
        return <Application>{this.determineContent()}</Application>;
    }
}

export default withRouter(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )(withCookies(ApplicationEntry))
);
