const passwordCharactes = '1234567890abcdefghijklmnopqrstuwxyz';

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default {
    generatePassword() {
        let password = '';
        for (let iterator = 0; iterator < 8; iterator++) {
            password =
                password +
                passwordCharactes.charAt(
                    getRandomInt(0, passwordCharactes.length - 1)
                );
        }
        return password;
    },
};
