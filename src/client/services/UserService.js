export default {
    hasPermissions(user, requiredRoles) {
        if (!!user && !!user.roles && !!user.roles.length) {
            const result = requiredRoles
                .map(role => user.roles.includes(role))
                .reduce((a, b) => a || b, false);
            return result;
        } else {
            return false;
        }
    },
};
