import {
    ALL_INTERCOMS,
    NEW_INTERCOM,
    FILE_GENERATION_HISTORY,
    INTERCOM_DIRECTORY,
    ACCOUNT_SELECTION,
    NEW_DIRECTORY_RULE,
    ACS_BRIVO_COM,
} from '../constants/pages';

export default {
    buildLinkToIntercomsPage(intercomId) {
        if (intercomId) {
            return `${ALL_INTERCOMS}/${intercomId}`;
        } else {
            return ALL_INTERCOMS;
        }
    },

    buildLinkToIntercomHistoryPage(intercomId) {
        return `${ALL_INTERCOMS}/${intercomId}${FILE_GENERATION_HISTORY}`;
    },

    buildLinkToIntercomDirectoryPage(intercomId) {
        return `${ALL_INTERCOMS}/${intercomId}${INTERCOM_DIRECTORY}`;
    },

    buildLinkToIntercomDirectoryRulePage(intercomId, directoryRuleId) {
        return `${ALL_INTERCOMS}/${intercomId}${INTERCOM_DIRECTORY}/${directoryRuleId}`;
    },

    buildLinkToNewDirectoryRulePage(intercomId) {
        return `${ALL_INTERCOMS}/${intercomId}${NEW_DIRECTORY_RULE}`;
    },

    buildLinkToNewIntercomsPage() {
        return NEW_INTERCOM;
    },

    buildLinkToLandingPage() {
        return ACS_BRIVO_COM;
    },
};
