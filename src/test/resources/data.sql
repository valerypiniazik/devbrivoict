INSERT INTO type(make, model, pattern,config) VALUES ('2N', 'Verso', 'hipve-$'||'{mac_address}.xml','{"users_limit":"1999","display_name_limit":"63","switch_codes_number":"4","warnings_limit":"25"}');

INSERT INTO error_type(
	name, failure_fatal)
	VALUES ('RETRIEVE_USER_ERROR', true),
  ('RETRIEVE_GROUP_USERS_ERROR', true),
	('INVALID_VARIABLE_NAMES_IN_FORMAT_STRING', true),
	('UNSUPPORTED_CHARACTERS_IN_FORMAT_STRING', true),
	('APPLICATION_ERROR', true),
	('TOO_MANY_WARNINGS', true),
	('MISSING_VALUE_FOR_FORMAT_STRING', false),
	('NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND', false),
	('USER_LIMIT_EXCEEDED', false),
	('DISPLAY_NAME_EXCEEDED_MAX_LENGTH', false);

INSERT INTO configuration_file_version(
    version, content_type, template, intercom_type_id, is_default)
    SELECT '17', 'application/xml', '<?xml version="1.0" encoding="ISO-8859-1"?><DeviceDatabase Version="17"><Display><PhoneBook><Enabled>1</Enabled><Tree/><ManModeEnabled>1</ManModeEnabled></PhoneBook></Display><Switches><#list sources.switches as switch><Switch At="$'||'{switch.switchNumber}"><Enabled>$'||'{switch.enabled}</Enabled><Code At="0"><#list switch.codes as code><#if code.code??><Code>$'||'{code.code?c}</Code><#else><Code/></#if><AccessMode>$'||'{code.accessMode}</AccessMode></#list></Code></Switch></#list></Switches><Directory><#list sources.users as user><User At="$'||'{user.userNumber}"><Uuid/><Name>$'||'{user.shortName}</Name><TreePath>$'||'{user.treePath}</TreePath><Calling><#list user.numbers as number><Position At="$'||'{number.numberNumber}"><Peer>$'||'{number.phoneNumber}</Peer></Position></#list></Calling></User></#list></Directory></DeviceDatabase>', id, true FROM type WHERE make='2N' AND model='Verso';