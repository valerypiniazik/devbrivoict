/*Custom class
https://github.com/TNG/junit-dataprovider/tree/junit4-spring-acceptance-test/junit4/src/integTest/java/com/tngtech/test/java/junit/dataprovider/spring*/
package com.brivo.intercom.integration.data_provider;

import static java.lang.Character.toUpperCase;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.manipulation.Filter;
import org.junit.runner.manipulation.NoTestsRemainException;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.TestClass;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderFilter;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import com.tngtech.java.junit.dataprovider.internal.DataConverter;
import com.tngtech.java.junit.dataprovider.internal.TestGenerator;
import com.tngtech.java.junit.dataprovider.internal.TestValidator;

public class SpringDataProviderRunner extends SpringJUnit4ClassRunner {
    protected DataConverter dataConverter;
    protected TestGenerator testGenerator;
    protected TestValidator testValidator;
    List<FrameworkMethod> computedTestMethods;

    public SpringDataProviderRunner(Class<?> clazz) throws InitializationError {
        super(clazz);
    }

    @Override
    protected void collectInitializationErrors(List<Throwable> errors) {
        initializeHelpers();

        super.collectInitializationErrors(errors);
    }

    protected void initializeHelpers() {
        dataConverter = new DataConverter();
        testGenerator = new TestGenerator(dataConverter);
        testValidator = new TestValidator(dataConverter);
    }

    @Override
    @Deprecated
    protected void validateInstanceMethods(List<Throwable> errors) {
        validatePublicVoidNoArgMethods(After.class, false, errors);
        validatePublicVoidNoArgMethods(Before.class, false, errors);
        validateTestMethods(errors);

        if (errors.isEmpty() && computeTestMethods().size() == 0) {
            errors.add(new Exception("No runnable methods"));
        }
    }

    @Override
    protected void validateTestMethods(List<Throwable> errors) {
        if (errors == null) {
            throw new NullPointerException("errors must not be null");
        }

        for (FrameworkMethod testMethod : getTestClassInt().getAnnotatedMethods(Test.class)) {
            testValidator.validateTestMethod(testMethod, errors);
        }
        for (FrameworkMethod testMethod : getTestClassInt().getAnnotatedMethods(UseDataProvider.class)) {
            FrameworkMethod dataProviderMethod = getDataProviderMethod(testMethod);
            if (dataProviderMethod == null) {
                errors.add(new Exception(String.format(
                        "No valid dataprovider found for test %s. By convention the dataprovider method name must either be equal to the test methods name, have a prefix of 'dataProvider' instead of 'test' or is overridden by using @UseDataProvider#value().",
                        testMethod.getName())));

            } else {
                DataProvider dataProvider = dataProviderMethod.getAnnotation(DataProvider.class);
                if (dataProvider == null) {
                    throw new IllegalStateException(String.format("@%s annotaion not found on dataprovider method %s",
                            DataProvider.class.getSimpleName(), dataProviderMethod.getName()));
                }
                testValidator.validateDataProviderMethod(dataProviderMethod, dataProvider, errors);
            }
        }
    }

    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        if (computedTestMethods == null) {
            computedTestMethods = generateExplodedTestMethodsFor(super.computeTestMethods());
        }
        return computedTestMethods;
    }

    @Override
    public void filter(Filter filter) throws NoTestsRemainException {
        if (filter == null) {
            throw new NullPointerException("filter must not be null");
        }
        super.filter(new DataProviderFilter(filter));
    }

    TestClass getTestClassInt() {
        return getTestClass();
    }

    List<FrameworkMethod> generateExplodedTestMethodsFor(List<FrameworkMethod> testMethods) {
        List<FrameworkMethod> result = new ArrayList<FrameworkMethod>();
        if (testMethods == null) {
            return result;
        }
        for (FrameworkMethod testMethod : testMethods) {
            FrameworkMethod dataProviderMethod = getDataProviderMethod(testMethod);
            result.addAll(testGenerator.generateExplodedTestMethodsFor(testMethod, dataProviderMethod));
        }
        return result;
    }

    FrameworkMethod getDataProviderMethod(FrameworkMethod testMethod) {
        if (testMethod == null) {
            throw new IllegalArgumentException("testMethod must not be null");
        }
        UseDataProvider useDataProvider = testMethod.getAnnotation(UseDataProvider.class);
        if (useDataProvider == null) {
            return null;
        }

        TestClass dataProviderLocation = findDataProviderLocation(useDataProvider);
        List<FrameworkMethod> dataProviderMethods = dataProviderLocation.getAnnotatedMethods(DataProvider.class);
        return findDataProviderMethod(dataProviderMethods, useDataProvider.value(), testMethod.getName());
    }

    TestClass findDataProviderLocation(UseDataProvider useDataProvider) {
        if (useDataProvider.location().length == 0) {
            return getTestClassInt();
        }
        return new TestClass(useDataProvider.location()[0]);
    }

    private FrameworkMethod findDataProviderMethod(List<FrameworkMethod> dataProviderMethods,
                                                   String useDataProviderValue, String testMethodName) {
        if (!UseDataProvider.DEFAULT_VALUE.equals(useDataProviderValue)) {
            return findMethod(dataProviderMethods, useDataProviderValue);
        }

        FrameworkMethod result = findMethod(dataProviderMethods, testMethodName);
        if (result == null) {
            String dataProviderMethodName = testMethodName.replaceAll("^test", "dataProvider");
            result = findMethod(dataProviderMethods, dataProviderMethodName);
        }
        if (result == null) {
            String dataProviderMethodName = testMethodName.replaceAll("^test", "data");
            result = findMethod(dataProviderMethods, dataProviderMethodName);
        }
        if (result == null) {
            String dataProviderMethodName = "dataProvider" + toUpperCase(testMethodName.charAt(0)) + testMethodName.substring(1);
            result = findMethod(dataProviderMethods, dataProviderMethodName);
        }
        if (result == null) {
            String dataProviderMethodName = "data" + toUpperCase(testMethodName.charAt(0)) + testMethodName.substring(1);
            result = findMethod(dataProviderMethods, dataProviderMethodName);
        }
        return result;
    }

    private FrameworkMethod findMethod(List<FrameworkMethod> methods, String methodName) {
        for (FrameworkMethod method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        return null;
    }
}
