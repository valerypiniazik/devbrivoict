package com.brivo.intercom.integration.security;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.integration.endpoints.UserSetUp;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;

import java.util.Collections;

import static com.jayway.restassured.RestAssured.given;

public class CsrUserSecurityIntegrationTest extends UserSetUp
{
    private static final Long GROUP_ID = 1L;
    private static final Long INTERCOM_ID = 1L;

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatGenerateConfigFileEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .when().get(GET_INTERCOMS_PATH + INTERCOM_ID + "/config")
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatDeleteGroupConfigEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .when().delete(GET_GROUP_CONFIG_PATH + GROUP_ID)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatPostGroupConfigEndpointReturns403ForCsrUser(String username, String password) {
        IntercomGroupConfigDTO groupConfig = new IntercomGroupConfigDTO();
        groupConfig.setBrivoGroupId(1L);
        groupConfig.setIntercomId(1L);
        groupConfig.setFormatString("");

        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .body(groupConfig)
            .when().post(GET_GROUP_CONFIG_PATH)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatPutIntercomGroupConfigEndpointReturns403ForCsrUser(String username, String password) {
        IntercomGroupConfigDTO groupConfig = new IntercomGroupConfigDTO();
        groupConfig.setBrivoGroupId(1L);
        groupConfig.setIntercomId(1L);
        groupConfig.setFormatString("");

        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .body(groupConfig)
            .when().put(GET_GROUP_CONFIG_PATH + GROUP_ID)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatDeleteIntercomEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .when().delete(GET_INTERCOMS_PATH + INTERCOM_ID)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatPostIntercomEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .body(new CreateIntercomRequest()
                .setTypeId(1L)
                .setPassword(TEST_PASSWORD_INTERCOM)
                .setName(TEST_INTERCOM)
                .setMacAddress(getRandomMacAddress(6))
                .setSwitchCodes(Collections.emptyList()))
            .when().post(GET_INTERCOMS_PATH)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatPutIntercomEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .body(new UpdateIntercomRequest()
                .setTypeId(1L)
                .setPassword(TEST_PASSWORD_INTERCOM)
                .setName(TEST_INTERCOM)
                .setMacAddress(getRandomMacAddress(6))
                .setSwitchCodes(Collections.emptyList()))
            .when().put(GET_INTERCOMS_PATH + INTERCOM_ID)
            .then()
            .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsCsr")
    public void verifyThatReconnectEndpointReturns403ForCsrUser(String username, String password) {
        given()
            .spec(getRequestSpec(username, password, brivoAccountId))
            .body(new UpdateIntercomRequest())
            .when().post(GET_RECONNECT_PATH)
            .then()
            .assertThat().statusCode(403);
    }
}

