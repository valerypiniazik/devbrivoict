package com.brivo.intercom.integration.security;

import com.brivo.intercom.integration.AbstractIntegrationTest;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;

public abstract class AbstractSecurityIntegrationTest extends AbstractIntegrationTest {

    static final RequestSpecification requestSpecSecurity =  new RequestSpecBuilder()
            .addHeader("Authorization", FAKE_BEARER)
            .addHeader("Accept", "application/json")
            .setContentType("application/json")
            .build();

    static final RequestSpecification requestBasicSpecSecurity =  new RequestSpecBuilder()
            .addHeader("Authorization", FAKE_BASIC_AUTH)
            .addHeader("Accept", "application/json")
            .setContentType("application/json")
            .build();
}
