package com.brivo.intercom.integration.security;

import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class DeviceApiSecurityIntegrationTest extends AbstractSecurityIntegrationTest {
    @Test
    public void verifyThatGetDeviceApiConfigIsSecured() {
        given()
                .spec(requestBasicSpecSecurity)
                .when().get(testSubjectUrl + port + "/deviceapi/config")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatGetDeviceApiConfigIsSecuredNoHeader() {
        given()
                .when().get(testSubjectUrl + port + "/deviceapi/config")
                .then().assertThat().statusCode(401);
    }
}
