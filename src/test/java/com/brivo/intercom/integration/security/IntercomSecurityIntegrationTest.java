package com.brivo.intercom.integration.security;

import com.brivo.intercom.domain.entity.Intercom;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class IntercomSecurityIntegrationTest extends AbstractSecurityIntegrationTest {

    @Test
    public void verifyThatGetAllIntercomsEndpointIsSecured() {
        given()
                .spec(requestSpecSecurity)
                .when().get(testSubjectUrl + port + "/intercoms")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatGetIntercomEndpointIsSecured() {
        given()
                .spec(requestSpecSecurity)
                .when().get(testSubjectUrl + port + "/intercoms/1")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatCreateIntercomEndpointIsSecured() {
        given()
                .body(new Intercom())
                .spec(requestSpecSecurity)
                .when().post(testSubjectUrl + port + "/intercoms")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatPutIntercomEndpointIsSecured() {
        given()
                .body(new Intercom())
                .spec(requestSpecSecurity)
                .when().put(testSubjectUrl + port + "/intercoms/1")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatDeleteIntercomEndpointIsSecured() {
        given()
                .spec(requestSpecSecurity)
                .when().delete(testSubjectUrl + port + "/intercoms/1")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatPatchIntercomEndpointIsSecured() {
        given()
                .body(new Intercom())
                .spec(requestSpecSecurity)
                .when().patch(testSubjectUrl + port + "/intercoms/1")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatGetAllIntercomsEndpointIsSecuredWhenSendingRequestWithoutAnyToken() {
        given()
                .when().get(testSubjectUrl + port + "/intercoms")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatPostReconnectEndpointIsSecuredWhenSendingRequestWithFakeToken() {
        given()
                .spec(requestSpecSecurity)
                .when().post(testSubjectUrl + port + "/api/users/intercoms")
                .then().assertThat().statusCode(401);
    }

    @Test
    public void verifyThatPostReconnectEndpointIsSecuredWhenSendingRequestWithoutAnyToken() {
        given()
                .when().post(testSubjectUrl + port + "/api/users/intercoms")
                .then().assertThat().statusCode(401);
    }
}
