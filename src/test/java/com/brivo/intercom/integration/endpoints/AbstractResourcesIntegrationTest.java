package com.brivo.intercom.integration.endpoints;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.entity.ConfigurationFileVersion;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.integration.AbstractIntegrationTest;
import com.brivo.intercom.repository.BrivoAccountInfoRepository;
import com.brivo.intercom.repository.IntercomGroupConfigRepository;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.repository.RequestLogRepository;
import com.brivo.intercom.repository.SwitchCodeRepository;
import com.brivo.intercom.repository.rest.TypeRepository;
import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;
import java.util.Collections;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public abstract class AbstractResourcesIntegrationTest extends AbstractIntegrationTest {
    protected static String GET_INTERCOMS_PATH;
    protected static String GET_DEVICE_API_CONFIG;
    protected static String GET_TYPE_INTERCOMS_PATH;
    protected static String GET_GROUP_CONFIG_PATH;
    protected static String GET_RECONNECT_PATH;
    protected static final String TEST_INTERCOM = "testIntercom";
    protected static final String TEST_PASSWORD_INTERCOM = "passwordIntercom";
    protected static final Long brivoAccountId = 778813L;
    protected static final Long wrongBrivoAccountId = Long.MAX_VALUE;
    protected static final Long BRIVO_TEST_GROUP_ID = 800881L;
    protected static final Long BRIVO_TEST_GROUP2_ID = 800882L;
    protected static final String BRIVO_TEST_GROUP_NAME = "TestGroupICT";
    protected static final String BRIVO_TEST_GROUP2_NAME = "TestGroupICT2";
    protected static final String CONTACT_TYPE = "home";
    protected static Long validValueOfSwitchCode = 2321L;

    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    protected IntercomRepository intercomRepository;

    @Autowired
    protected TypeRepository typeRepository;

    @Autowired
    protected BrivoAccountInfoRepository brivoAccountInfoRepository;

    @Autowired
    protected IntercomGroupConfigRepository intercomGroupConfigRepository;

    @Autowired
    protected RequestLogRepository requestLogRepository;

    @Autowired
    protected SwitchCodeRepository switchCodeRepository;

    @Before
    public void setUp() {
        GET_INTERCOMS_PATH = testSubjectUrl + port + "/api/intercoms/";
        GET_TYPE_INTERCOMS_PATH = testSubjectUrl + port + "/api/types";
        GET_DEVICE_API_CONFIG = testSubjectUrl + port + "/deviceapi/config/2n/hipve-00-00-34-a1-2b-dd.xml";
        GET_GROUP_CONFIG_PATH = testSubjectUrl + port + "/api/group-configs/";
        GET_RECONNECT_PATH = testSubjectUrl + port + "/api/account-info";
    }

    protected String getIntercomGroupConfigsPath(Long intercomId) {
        return testSubjectUrl + port + "/api/intercoms/" + intercomId + "/group-configs/";
    }

    private ResponseEntity<Map> getResponseAuthorization(String userName, String passwordUser) {
        String credentials = clientId + ":" + clientSecret;
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes()));
        headers.add("api-key", apiKey);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(brivoOauthUrl);
        uriBuilder.queryParam("grant_type", "password");
        uriBuilder.queryParam("username", userName);
        uriBuilder.queryParam("password", passwordUser);

        ResponseEntity<Map> result = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, new HttpEntity<>(null, headers), Map.class);
        return result;
    }

    public String authorizeInBrivo(String userName, String passwordUser, String typeToken) {
        ResponseEntity<Map> result = getResponseAuthorization(userName, passwordUser);
        return result.getBody().get(typeToken).toString();
    }

    public String basicAuthorizeInBrivo(String username, String password) {
        String credentials = username + ":" + password;
        return Base64.getEncoder().encodeToString(credentials.getBytes());
    }

    public Intercom createIntercom(String userName, String password) {
        Type type = createIntercomType();
        BrivoAccountInfo brivoAccountInfo = createBrivoAccountInfo(userName, password);

        Intercom intercom = new Intercom();
        intercom.setHashedPassword(TEST_PASSWORD_INTERCOM);
        intercom.setName(TEST_INTERCOM);
        intercom.setMacAddress(getRandomMacAddress(6));
        intercom.setType(type);
        intercom.setBrivoAccountInfo(brivoAccountInfo);
        intercom.setSwitchCodes(getSwitchCodes(intercom, 1, validValueOfSwitchCode));
        return intercomRepository.save(intercom);
    }

    public SwitchCode createSwitchCode(Intercom intercom, Long switchCodeValue) {
        SwitchCode switchCode = new SwitchCode();
        switchCode.setIndex(1);
        switchCode.setCode(switchCodeValue);
        switchCode.setName("switch_code1");
        switchCode.setIntercom(intercom);
        return switchCode;
    }

    public List<SwitchCode> getSwitchCodes(Intercom intercom, int numberOfSwitchCodes, Long valueSwitchCode) {
        List<SwitchCode> switchCodes = new ArrayList<>();

        for (int i = 0; i < numberOfSwitchCodes; i++) {
            SwitchCode switchCode = createSwitchCode(intercom, valueSwitchCode);
            switchCodes.add(switchCode);
        }
        return switchCodes;
    }

    public Type createIntercomType() {
        Type type = new Type();
        type.setMake("2N");
        type.setModel("Verso");
        type.setDefaultConfigurationVersion(createConfigurationFileVersions(type));
        return typeRepository.save(type);
    }

    public List<ConfigurationFileVersion> createConfigurationFileVersions(Type type) {
        ConfigurationFileVersion configurationFileVersion = new ConfigurationFileVersion();
        configurationFileVersion.setContentType("application/xml");
        configurationFileVersion.setDefault(true);
        configurationFileVersion.setVersion("17");
        configurationFileVersion.setType(type);
        configurationFileVersion.setTemplate("<DeviceDatabase Version=\"17\"><Switches><#list sources.switches as switch>"
            + "<Switch At=\"${switch.switchNumber}\"><Enabled>${switch.enabled}</Enabled><Code At=\"0\"><#list switch.codes as code>"
            + "<#if code.code??><Code>${code.code?c}</Code><#else><Code/></#if><AccessMode>${code.accessMode}</AccessMode></#list></Code></Switch>"
            + "</#list></Switches><#list sources.users as user><User At=\"${user.userNumber}\"><Uuid/><Name>${user.shortName}</Name><TreePath>${user.treePath}"
            + "</TreePath><Calling><#list user.numbers as number><Position At=\"${number.numberNumber}\"><Peer>${number.phoneNumber}</Peer></Position></#list></Calling>"
            + "</User></#list></DeviceDatabase>");
        return Collections.singletonList(configurationFileVersion);
    }

    private BrivoAccountInfo createBrivoAccountInfo(String userName, String password) {
        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo();
        brivoAccountInfo.setId(brivoAccountId);
        brivoAccountInfo.setUsername(userName);
        brivoAccountInfo.setRefreshToken(authorizeInBrivo(userName, password, "refresh_token"));

        return brivoAccountInfoRepository.save(brivoAccountInfo);
    }

    public void deleteIntercom(Long intercomId) {
        intercomRepository.delete(intercomId);
    }

    private String randomInt() {
        Random random = new Random();
        return String.valueOf(random.nextInt(10)) + String.valueOf(random.nextInt(10));
    }

    public String getRandomMacAddress(int length) {
        String s = "";
        for (int i = 0; i < length - 1; i++) {
            s += randomInt() + "-";
        }
        return s + randomInt();
    }

    public IntercomGroupConfig createIntercomGroupConfig(Intercom intercom, Long groupId) {
        IntercomGroupConfig.IntercomGroupContactConfig intercomGroupContactConfig = new IntercomGroupConfig.IntercomGroupContactConfig();
        intercomGroupContactConfig.setBrivoContactType(CONTACT_TYPE);
        intercomGroupContactConfig.setIndex(1);

        List<IntercomGroupConfig.IntercomGroupContactConfig> listIntercomGroupContactConfig = new ArrayList<>();
        listIntercomGroupContactConfig.add(intercomGroupContactConfig);

        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setIntercom(intercom);
        intercomGroupConfig.setBrivoGroupId(groupId);
        intercomGroupConfig.setFormatString("${lastName}");
        intercomGroupConfig.setIntercomGroupContactConfigs(listIntercomGroupContactConfig);
        return intercomGroupConfigRepository.save(intercomGroupConfig);
    }

    public void deleteIntercomGroupConfig(Long intercomGroupConfigId) {
        intercomGroupConfigRepository.delete(intercomGroupConfigId);
    }

    public String getPathForXmlFileConfig(String macAddress) {
        return testSubjectUrl + port + "/deviceapi/config/2n/hipve-" + macAddress + ".xml";
    }

    public RequestSpecification basicRequestSpec(String basicToken) {
        return new RequestSpecBuilder()
                .addHeader("Authorization", "Basic " + basicToken)
                .addHeader("Accept-Encoding", "application/octet-stream")
                .addCookie("brivoAccountId", brivoAccountId)
                .setContentType("application/octet-stream")
                .build();
    }

    public void deleteIntercomAndAccountRelatedInfo() {
        switchCodeRepository.deleteAll();
        intercomRepository.deleteAll();
        brivoAccountInfoRepository.deleteAll();
    }

    public String getFile(String fileName) {
        StringBuilder result = new StringBuilder("");

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(fileName).getFile());

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                result.append(line).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @After
    public void tearDown() {
        deleteIntercomAndAccountRelatedInfo();
    }
}
