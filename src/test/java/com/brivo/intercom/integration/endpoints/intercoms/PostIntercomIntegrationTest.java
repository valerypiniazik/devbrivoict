package com.brivo.intercom.integration.endpoints.intercoms;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.jayway.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class PostIntercomIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestCreatesIntercom(String userName, String password) {
        Type type = createIntercomType();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);
        String pathToJson = getFile("json/post_intercom.json");

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH);

        SoftAssertions softAssert = new SoftAssertions();
        response.then().assertThat().body(matchesJsonSchema(pathToJson));
        softAssert.assertThat(response.getStatusCode()).isEqualTo(201);
        softAssert.assertThat(response.getBody().jsonPath().getString("macAddress")).isEqualTo(intercom.getMacAddress());
        softAssert.assertThat(response.getBody().jsonPath().getString("name")).isEqualTo(intercom.getName());
        softAssert.assertThat(response.getBody().jsonPath().getLong("typeId")).isEqualTo(intercom.getTypeId());
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestCreatesIntercomWithoutSwitchCodes(String userName, String password) {
        Type type = createIntercomType();
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), new ArrayList<>());

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(201);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithSwitchCodeValueLessThanFourDigits(String userName, String password) {
        Type type = createIntercomType();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, 123L);
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithSwitchCodeValueMoreThan15Digits(String userName, String password) {
        Type type = createIntercomType();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, Long.MAX_VALUE);
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithoutRelatedAccount(String userName, String password) {
        Type type = createIntercomType();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);

        given()
                .spec(getRequestSpecWithoutCookie(userName, password))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithoutIntercomType(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        CreateIntercomRequest intercom = getCreateIntercomRequest(null, getRandomMacAddress(6), switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithNotExistingIntercomType(String userName, String password) {
        Long typeIntercomId = Long.MAX_VALUE;
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        CreateIntercomRequest intercom = getCreateIntercomRequest(typeIntercomId, getRandomMacAddress(6), switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithTheSameMacAddress(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        Type type = createIntercomType();
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);
        String macAddress = intercom.getMacAddress();

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH);

        CreateIntercomRequest intercomWithTheSameMacAddress = getCreateIntercomRequest(type.getId(), macAddress, switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercomWithTheSameMacAddress)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(409);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithNameContainingMoreThan255Characters(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        Type type = createIntercomType();
        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), getRandomMacAddress(6), switchCodes);
        intercom.setName(getRandomString(256));

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithMacAddressContainingIncorrectNumberOfSymbols(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        Type type = createIntercomType();
        String macAddress = getRandomMacAddress(4);

        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), macAddress, switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomWithMacAddressContainingIncorrectSymbols(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        Type type = createIntercomType();
        String macAddress = "12-23-12-ML-23-ZX";

        CreateIntercomRequest intercom = getCreateIntercomRequest(type.getId(), macAddress, switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH)
                .then().assertThat().statusCode(400);
    }
}
