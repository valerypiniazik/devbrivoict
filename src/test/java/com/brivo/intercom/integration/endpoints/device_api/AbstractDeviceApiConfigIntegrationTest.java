package com.brivo.intercom.integration.endpoints.device_api;

import com.brivo.intercom.integration.endpoints.intercoms.AbstractIntercomIntegrationTest;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;

public abstract class AbstractDeviceApiConfigIntegrationTest extends AbstractIntercomIntegrationTest {
    protected static final String LAST_NAME = "Chagall";
    protected static final String NUMERIC_PHONE_NAME_HOME = "5417543010";

    NodeList getNodeListByTagNameFromXmlDoc(String xmlString, String tagName) {
        NodeList result = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new InputSource(new StringReader(xmlString)));

            result = document.getElementsByTagName(tagName);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }
}
