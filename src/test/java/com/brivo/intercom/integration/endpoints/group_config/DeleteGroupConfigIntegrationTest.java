package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class DeleteGroupConfigIntegrationTest extends AbstractGroupConfigIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatDeletedIntercomGroupConfigIsNotPresentInGroupConfigList(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().delete(GET_GROUP_CONFIG_PATH + groupConfigId);

        response.then()
                .assertThat().statusCode(204);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatDeleteRequestReturns404TryingToDeleteNotExistingIntercomGroupConfig(String userName, String password) {
        Long intercomGroupConfigId = Long.MAX_VALUE;

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().delete(GET_GROUP_CONFIG_PATH + intercomGroupConfigId)
                .then()
                .assertThat().statusCode(404);
    }
}
