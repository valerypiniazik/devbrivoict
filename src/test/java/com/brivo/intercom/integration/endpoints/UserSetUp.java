package com.brivo.intercom.integration.endpoints;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;
import com.tngtech.java.junit.dataprovider.DataProvider;

public abstract class UserSetUp extends AbstractResourcesIntegrationTest {

    @DataProvider
    public static Object[][] dataProviderSetCredentialsMasterSenior() {
        return new Object[][]{
                {"valery.piniazik", "6OwMustG00n!"},
                {"seniorAdminIct", "12345cV!67se"}
        };
    }

    @DataProvider
    public static Object[][] dataProviderSetCredentialsInstaller() {
        return new Object[][]{
                {"valery.piniazik_inst", "6OwMustG00n!"}
        };
    }

    @DataProvider
    public static Object[][] dataProviderSetCredentialsInstallerMasterSenior() {
        return new Object[][]{
                {"valery.piniazik", "6OwMustG00n!"},
                {"seniorAdminIct", "12345cV!67se"},
                {"valery.piniazik_inst", "6OwMustG00n!"}
        };
    }

    @DataProvider
    public static Object[][] dataProviderSetCredentialsCsr() {
        return new Object[][]{
            {"alex_belyaev_csr", "6OwMustG00n!"}
        };
    }

    @DataProvider
    public static Object[][] dataProviderSetCredentialsInstallerMasterSeniorCsr() {
        return new Object[][]{
            {"valery.piniazik", "6OwMustG00n!"},
            {"seniorAdminIct", "12345cV!67se"},
            {"valery.piniazik_inst", "6OwMustG00n!"},
            {"alex_belyaev_csr", "6OwMustG00n!"}
        };
    }

    protected RequestSpecification getRequestSpec(String userName, String password, Long cookieValue) {
        String realToken = authorizeInBrivo(userName, password, "access_token");
        return new RequestSpecBuilder()
                .addHeader("Authorization", "Bearer " + realToken)
                .addHeader("Accept", "application/json")
                .addCookie("brivoAccountId", cookieValue)
                .setContentType("application/json")
                .build();
    }

    protected RequestSpecification getRequestSpecWithoutCookie(String userName, String password) {
        String realToken = authorizeInBrivo(userName, password, "access_token");
        return new RequestSpecBuilder()
                .addHeader("Authorization", "Bearer " + realToken)
                .addHeader("Accept", "application/json")
                .setContentType("application/json")
                .build();
    }
}
