package com.brivo.intercom.integration.endpoints.history;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.integration.endpoints.UserSetUp;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class GetHistoryIntegrationTest extends UserSetUp {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSeniorCsr")
    public void verifyThatGetHistoryReturns404TryingToRetrieveNotExistedIntercomHistory(String userName, String password) {
        Long notExistedIntercom = Long.MAX_VALUE;

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + notExistedIntercom + "/history")
                .then().assertThat().statusCode(404);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSeniorCsr")
    public void verifyThatGetHistoryRetrievesIntercomHistory(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercomId + "/history")
                .then().assertThat().statusCode(200);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSeniorCsr")
    public void verifyThatGetHistoryRetrievesIntercomHistoryOfConfigFileGeneration(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        String macAddress = intercom.getMacAddress();
        String basicToken = basicAuthorizeInBrivo(macAddress, TEST_PASSWORD_INTERCOM);
        createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);

        given()
                .spec(basicRequestSpec(basicToken))
                .when().get(getPathForXmlFileConfig(macAddress));

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercomId + "/history")
                .then().assertThat().statusCode(200);
    }
}
