package com.brivo.intercom.integration.endpoints.intercoms;

import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class PutIntercomIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestUpdatesIntercom(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();
        Long intercomTypeId = intercom.getType().getId();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);

        UpdateIntercomRequest request = getUpdateIntercomRequest(intercomTypeId, intercom.getMacAddress(), switchCodes);
        request.setName("nameIntercomUpdated");

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(request)
                .when().put(GET_INTERCOMS_PATH + intercomId);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getString("name")).isEqualTo(request.getName());
        softAssert.assertThat(response.getBody().jsonPath().getString("macAddress")).isEqualTo(request.getMacAddress());
        softAssert.assertThat(response.getBody().jsonPath().getLong("typeId")).isEqualTo(intercom.getType().getId());
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestDoesNotUpdatesIntercomWithoutRelatedAccount(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();
        Long intercomTypeId = intercom.getType().getId();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);

        UpdateIntercomRequest request = getUpdateIntercomRequest(intercomTypeId, intercom.getMacAddress(), switchCodes);
        request.setName("nameIntercomUpdated");

        given()
                .spec(getRequestSpecWithoutCookie(userName, password))
                .body(request)
                .when().put(GET_INTERCOMS_PATH + intercomId)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestDoesNotUpdateIntercomWithoutIntercomType(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();
        Long intercomTypeId = intercom.getType().getId();
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);

        Long notExistingIntercomTypeId = Long.MAX_VALUE;
        UpdateIntercomRequest request = getUpdateIntercomRequest(intercomTypeId, intercom.getMacAddress(), switchCodes);
        request.setTypeId(notExistingIntercomTypeId);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(request)
                .when().put(GET_INTERCOMS_PATH + intercomId)
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestReturns404StatusCodeTryingToUpdateNotExistedIntercom(String userName, String password) {
        Long notExistingIntercomId = Long.MAX_VALUE;
        Long notExistingIntercomTypeId = Long.MAX_VALUE;
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);

        UpdateIntercomRequest request = getUpdateIntercomRequest(notExistingIntercomTypeId, getRandomMacAddress(6),
                switchCodes);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(request)
                .when().put(GET_INTERCOMS_PATH + notExistingIntercomId)
                .then().assertThat().statusCode(404);
    }
}
