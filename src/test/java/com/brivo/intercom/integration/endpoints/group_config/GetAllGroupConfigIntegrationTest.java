package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class GetAllGroupConfigIntegrationTest extends AbstractGroupConfigIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetRequestReturnAllCreatedIntercomGroupConfigs(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP2_ID);

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(getIntercomGroupConfigsPath(intercomId));

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getList("id").size()).isEqualTo(2);
        softAssert.assertAll();
    }
}
