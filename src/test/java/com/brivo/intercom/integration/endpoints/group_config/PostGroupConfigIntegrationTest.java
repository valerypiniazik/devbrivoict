package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class PostGroupConfigIntegrationTest extends AbstractGroupConfigIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestCreatesIntercomConfigGroup(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        IntercomGroupConfigDTO groupConfig = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP_ID);
        String pathToJson = getFile("json/post_group_config.json");

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfig)
                .when().post(GET_GROUP_CONFIG_PATH);

        SoftAssertions softAssert = new SoftAssertions();
        response.then().assertThat().body(matchesJsonSchema(pathToJson));
        softAssert.assertThat(response.getStatusCode()).isEqualTo(201);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomConfigGroupWithoutExistedIntercom(String userName, String password) {
        Long intercomId = Long.MAX_VALUE;

        IntercomGroupConfigDTO groupConfig = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP_ID);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfig)
                .when().post(GET_GROUP_CONFIG_PATH)
                .then()
                .assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostRequestDoesNotCreateIntercomConfigGroupWithoutFormatString(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        IntercomGroupConfigDTO groupConfig = createIntercomGroupConfigDto(intercomId, null, BRIVO_TEST_GROUP_ID);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfig)
                .when().post(GET_GROUP_CONFIG_PATH)
                .then()
                .assertThat().statusCode(400);
    }
}
