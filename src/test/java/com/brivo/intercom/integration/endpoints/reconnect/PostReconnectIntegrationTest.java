package com.brivo.intercom.integration.endpoints.reconnect;

import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class PostReconnectIntegrationTest extends AbstractReconnectIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsMasterSenior")
    public void verifyThatPostRequestGeneratesNewRefreshTokenForMasterOrSeniorAdmin(String userName, String password) {
        Response responseReconnect = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().post(GET_RECONNECT_PATH);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(responseReconnect.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(getTokenUpdateTime(brivoAccountId)).isNotNull();
        softAssert.assertThat(getUserNameForRefreshToken(brivoAccountId)).isEqualTo(userName);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstaller")
    public void verifyThatPostRequestDoesNotGenerateNewRefreshTokenForInstaller(String userName, String password) {
        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().post(GET_RECONNECT_PATH)
                .then().assertThat().statusCode(403);
    }
}
