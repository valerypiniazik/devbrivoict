package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.integration.endpoints.UserSetUp;

public abstract class AbstractGroupConfigIntegrationTest extends UserSetUp {
    protected static final String FORMAT_STRING_LAST_NAME = "${lastName}";

    protected IntercomGroupConfigDTO createIntercomGroupConfigDto(Long intercomId, String formatString, Long groupId) {
        IntercomGroupConfigDTO groupConfig = new IntercomGroupConfigDTO();
        groupConfig.setBrivoGroupId(groupId);
        groupConfig.setIntercomId(intercomId);
        groupConfig.setFormatString(formatString);
        return groupConfig;
    }
}
