package com.brivo.intercom.integration.endpoints.reconnect;

import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class GetReconnectIntegrationTest extends AbstractReconnectIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsMasterSenior")
    public void verifyThatGetRequestRetrieveInfoAboutRefreshToken(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);

        Response responsePostReconnect = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().post(GET_RECONNECT_PATH);

        Response responseGetReconnect = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_RECONNECT_PATH);

        deleteIntercom(intercom.getId());

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(responsePostReconnect.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(responseGetReconnect.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getString("username")).isEqualTo(userName);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getBoolean("valid")).isEqualTo(false);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getBoolean("exist")).isEqualTo(true);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsMasterSenior")
    public void verifyThatGetRequestRetrieveStatusRefreshTokenWithoutItGeneration(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);

        Response responseGetReconnect = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_RECONNECT_PATH);

        deleteIntercom(intercom.getId());
        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(responseGetReconnect.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getString("username")).isEqualTo(userName);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getBoolean("valid")).isEqualTo(true);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getBoolean("exist")).isEqualTo(true);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsMasterSenior")
    public void verifyThatGetRequestDoesNotProvideAnyInfoAboutRefreshTokenWithoutIntercom(String userName, String password) {
        Response responseGetReconnect = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_RECONNECT_PATH);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(responseGetReconnect.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(responseGetReconnect.getBody().jsonPath().getBoolean("exist")).isEqualTo(false);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstaller")
    public void verifyThatGetRequestIsNotAvailableForInstaller(String userName, String password) {
        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_RECONNECT_PATH)
                .then().assertThat().statusCode(403);
    }
}
