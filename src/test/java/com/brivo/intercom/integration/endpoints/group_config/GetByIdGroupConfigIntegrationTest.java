package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class GetByIdGroupConfigIntegrationTest extends AbstractGroupConfigIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetByIdReturnsExistedIntercomGroupConfig(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_GROUP_CONFIG_PATH + groupConfigId);

        String pathToJson = getFile("json/get_group_config.json");

        SoftAssertions softAssert = new SoftAssertions();
        response.then().assertThat().body(matchesJsonSchema(pathToJson));
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getLong("id")).isEqualTo(groupConfigId);
        softAssert.assertThat(response.getBody().jsonPath().getLong("brivoGroupId"))
                .isEqualTo(groupConfig.getBrivoGroupId());
        softAssert.assertThat(response.getBody().jsonPath().getString("brivoGroupName")).isEqualTo(BRIVO_TEST_GROUP_NAME);
        softAssert.assertThat(response.getBody().jsonPath().getString("intercomGroupContactConfigs.brivoContactType[0]"))
                .isEqualTo(CONTACT_TYPE);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetByIdReturns404TryingToRetrieveNotExistingIntercomGroupConfig(String userName, String password) {
        Long intercomGroupConfigId = Long.MAX_VALUE;

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_GROUP_CONFIG_PATH + intercomGroupConfigId)
                .then().assertThat().statusCode(404);
    }
}
