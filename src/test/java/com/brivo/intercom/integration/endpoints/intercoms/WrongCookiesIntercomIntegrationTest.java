package com.brivo.intercom.integration.endpoints.intercoms;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class WrongCookiesIntercomIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetIntercomsWithWrongCookiesReturnForbidden(String userName, String password) {
        given()
                .spec(getRequestSpec(userName, password, wrongBrivoAccountId))
                .when().get(GET_INTERCOMS_PATH)
                .then()
                .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetIntercomByIdWithWrongCookiesReturnForbidden(String userName, String password) {
        given()
                .spec(getRequestSpec(userName, password, wrongBrivoAccountId))
                .when().get(GET_INTERCOMS_PATH + createIntercom(userName, password).getId())
                .then()
                .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPostIntercomWithWrongCookiesReturnForbidden(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        given()
                .spec(getRequestSpec(userName, password, wrongBrivoAccountId))
                .body(getCreateIntercomRequest(createIntercomType().getId(), getRandomMacAddress(6), switchCodes))
                .when().post(GET_INTERCOMS_PATH)
                .then()
                .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutIntercomWithWrongCookiesReturnForbidden(String userName, String password) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom(), 1, validValueOfSwitchCode);
        given()
                .spec(getRequestSpec(userName, password, wrongBrivoAccountId))
                .body(getCreateIntercomRequest(createIntercomType().getId(), getRandomMacAddress(6), switchCodes))
                .when().put(GET_INTERCOMS_PATH + createIntercom(userName, password).getId())
                .then()
                .assertThat().statusCode(403);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatDeleteIntercomWithWrongCookiesReturnForbidden(String userName, String password) {
        Long intercomId = createIntercom(userName, password).getId();
        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(given()
                .spec(getRequestSpec(userName, password, wrongBrivoAccountId))
                .when().delete(GET_INTERCOMS_PATH + intercomId)
                .getStatusCode() == 403);
        deleteIntercom(intercomId);
        softAssert.assertAll();
    }
}
