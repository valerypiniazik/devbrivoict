package com.brivo.intercom.integration.endpoints.reconnect;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.integration.endpoints.UserSetUp;

import java.time.LocalDateTime;

public abstract class AbstractReconnectIntegrationTest extends UserSetUp {

    public LocalDateTime getTokenUpdateTime(Long id) {
        BrivoAccountInfo brivoAccountInfo = brivoAccountInfoRepository.findOne(id);
        return brivoAccountInfo.getRefreshTokenUpdateTime();
    }

    public String getUserNameForRefreshToken(Long id) {
        BrivoAccountInfo brivoAccountInfo = brivoAccountInfoRepository.findOne(id);
        return brivoAccountInfo.getUsername();
    }
}
