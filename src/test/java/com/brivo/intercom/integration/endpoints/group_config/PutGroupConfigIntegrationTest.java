package com.brivo.intercom.integration.endpoints.group_config;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class PutGroupConfigIntegrationTest extends AbstractGroupConfigIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestUpdatesIntercomGroupConfigWithNewFormatString(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();
        String formatStringUpdated = "$(lastName)_updated";

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        IntercomGroupConfigDTO groupConfigDto = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP_ID);
        groupConfigDto.setFormatString(formatStringUpdated);

        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfigDto)
                .when().put(GET_GROUP_CONFIG_PATH + groupConfigId);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getLong("brivoGroupId")).isEqualTo(BRIVO_TEST_GROUP_ID);
        softAssert.assertThat(response.getBody().jsonPath().getString("brivoGroupName")).isEqualTo(BRIVO_TEST_GROUP_NAME);
        softAssert.assertThat(response.getBody().jsonPath().getString("formatString")).isEqualTo(formatStringUpdated);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestUpdatesIntercomGroupConfigWithNewGroup(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        IntercomGroupConfigDTO groupConfigDto = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP2_ID);

        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfigDto)
                .when().put(GET_GROUP_CONFIG_PATH + groupConfigId);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getLong("brivoGroupId")).isEqualTo(BRIVO_TEST_GROUP2_ID);
        softAssert.assertThat(response.getBody().jsonPath().getString("brivoGroupName")).isEqualTo(BRIVO_TEST_GROUP2_NAME);
        softAssert.assertThat(response.getBody().jsonPath().getString("formatString")).isEqualTo(FORMAT_STRING_LAST_NAME);
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestDoesNotUpdateIntercomGroupConfigWithoutFormatString(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        IntercomGroupConfigDTO groupConfigDto = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP_ID);
        groupConfigDto.setFormatString(null);

        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfigDto)
                .when().put(GET_GROUP_CONFIG_PATH + groupConfigId);

        response
                .then().assertThat().statusCode(400);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatPutRequestDoesNotUpdateIntercomGroupConfigWithoutBrivoGroup(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        IntercomGroupConfig groupConfig = createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);
        IntercomGroupConfigDTO groupConfigDto = createIntercomGroupConfigDto(intercomId, FORMAT_STRING_LAST_NAME, BRIVO_TEST_GROUP_ID);
        groupConfigDto.setBrivoGroupId(null);

        Long groupConfigId = groupConfig.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .body(groupConfigDto)
                .when().put(GET_GROUP_CONFIG_PATH + groupConfigId);

        response
                .then().assertThat().statusCode(400);
    }
}
