package com.brivo.intercom.integration.endpoints.intercoms;

import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import com.jayway.restassured.response.Response;

import java.util.List;

import static com.jayway.restassured.RestAssured.given;

public class GetIntercomsIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSeniorCsr")
    public void verifyThatGetIntercomsIsReturningIntercomList(String userName, String password) {
        List<Long> intercomsForTest = createIntercoms(5, userName, password);
        Response intercomsResponse = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH);
        List<Long> givenIntercomsId = intercomsResponse.jsonPath().getList("id", Long.class);

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(givenIntercomsId).isEqualTo(intercomsForTest);
        softAssert.assertThat(intercomsResponse.getStatusCode()).isEqualTo(200);
        softAssert.assertAll();
    }
}
