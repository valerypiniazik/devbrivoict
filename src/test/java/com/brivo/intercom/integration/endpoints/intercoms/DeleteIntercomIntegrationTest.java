package com.brivo.intercom.integration.endpoints.intercoms;

import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;

public class DeleteIntercomIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatDeletedIntercomIsNotPresentInIntercomList(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);

        Response detetedIntercomResponse = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().delete(GET_INTERCOMS_PATH + intercom.getId());

        Response getIntercomsResponse = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercom.getId());

        Intercom deletedIntercom = intercomRepository.findOne(intercom.getId());

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(detetedIntercomResponse.getStatusCode()).isEqualTo(204);
        softAssert.assertThat(getIntercomsResponse.getStatusCode()).isEqualTo(404);
        softAssert.assertThat(deletedIntercom).isNull();
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatDeleteIntercomRequestReturns404TryingToDeleteNotExistingIntercom(String userName, String password) {
        Long intercomId = Long.MAX_VALUE;

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().delete(GET_INTERCOMS_PATH + intercomId)
                .then()
                .assertThat().statusCode(404);
    }
}
