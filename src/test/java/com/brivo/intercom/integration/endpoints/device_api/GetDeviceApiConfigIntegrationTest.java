package com.brivo.intercom.integration.endpoints.device_api;

import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;
import org.w3c.dom.NodeList;

import static com.jayway.restassured.RestAssured.given;

public class GetDeviceApiConfigIntegrationTest extends AbstractDeviceApiConfigIntegrationTest {
    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatBasicAuthIsConfiguredForDeviceApiConfig(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        String macAddress = intercom.getMacAddress();
        String basicToken = basicAuthorizeInBrivo(macAddress, TEST_PASSWORD_INTERCOM);

        Response response = given()
                .spec(basicRequestSpec(basicToken))
                .when().get(getPathForXmlFileConfig(macAddress));

        response.then().assertThat().statusCode(200);
    }

    @Test
    public void verifyThatBasicAuthIsConfiguredAndSecuredForDeviceApiConfig() {
        given()
                .auth()
                .basic(TEST_INTERCOM, TEST_PASSWORD_INTERCOM)
                .when().get(GET_DEVICE_API_CONFIG)
                .then()
                .assertThat().statusCode(401);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetRequestGeneratesXmlConfigFileWithUserFromAssignedGroups(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        String macAddress = intercom.getMacAddress();
        String basicToken = basicAuthorizeInBrivo(macAddress, TEST_PASSWORD_INTERCOM);
        createIntercomGroupConfig(intercom, BRIVO_TEST_GROUP_ID);

        Response response = given()
                .spec(basicRequestSpec(basicToken))
                .when().get(getPathForXmlFileConfig(macAddress));

        String xmlString = response.getBody().asString();

        NodeList userNameXml = getNodeListByTagNameFromXmlDoc(xmlString, "Name");
        NodeList phoneNumber = getNodeListByTagNameFromXmlDoc(xmlString, "Peer");

        SoftAssertions softAssert = new SoftAssertions();
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(userNameXml.item(0).getTextContent()).isEqualTo(LAST_NAME);
        softAssert.assertThat(phoneNumber.item(0).getTextContent()).isEqualTo(NUMERIC_PHONE_NAME_HOME);
        softAssert.assertAll();
    }
}
