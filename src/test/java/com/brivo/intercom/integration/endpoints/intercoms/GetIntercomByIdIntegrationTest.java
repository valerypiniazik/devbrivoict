package com.brivo.intercom.integration.endpoints.intercoms;

import com.brivo.intercom.domain.entity.Intercom;
import com.jayway.restassured.response.Response;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import static com.jayway.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class GetIntercomByIdIntegrationTest extends AbstractIntercomIntegrationTest {

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSeniorCsr")
    public void verifyThatGetByIdReturnsExistedIntercom(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        Response response = given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercomId);

        String pathToJson = getFile("json/get_intercom.json");

        SoftAssertions softAssert = new SoftAssertions();
        response.then().assertThat().body(matchesJsonSchema(pathToJson));
        softAssert.assertThat(response.getStatusCode()).isEqualTo(200);
        softAssert.assertThat(response.getBody().jsonPath().getLong("id")).isEqualTo(intercom.getId());
        softAssert.assertThat(response.getBody().jsonPath().getString("macAddress")).isEqualTo(intercom.getMacAddress());
        softAssert.assertThat(response.getBody().jsonPath().getString("name")).isEqualTo(intercom.getName());
        softAssert.assertThat(response.getBody().jsonPath().getLong("typeId")).isEqualTo(intercom.getType().getId());
        softAssert.assertAll();
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetIntercomByIdRequestReturns404TryingToRetrieveNotExistingIntercom(String userName, String password) {
        Long intercomId = Long.MAX_VALUE;

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercomId)
                .then().assertThat().statusCode(404);
    }

    @Test
    @UseDataProvider("dataProviderSetCredentialsInstallerMasterSenior")
    public void verifyThatGetIntercomByIdRequestReturns404TryingToRetrieveDeletedIntercom(String userName, String password) {
        Intercom intercom = createIntercom(userName, password);
        Long intercomId = intercom.getId();

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().delete(GET_INTERCOMS_PATH + intercomId)
                .then().assertThat().statusCode(204);

        given()
                .spec(getRequestSpec(userName, password, brivoAccountId))
                .when().get(GET_INTERCOMS_PATH + intercomId)
                .then().assertThat().statusCode(404);
    }
}
