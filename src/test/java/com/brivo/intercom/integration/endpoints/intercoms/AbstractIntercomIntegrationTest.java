package com.brivo.intercom.integration.endpoints.intercoms;

import java.util.ArrayList;
import java.util.List;

import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.integration.endpoints.UserSetUp;

public abstract class AbstractIntercomIntegrationTest extends UserSetUp {

    CreateIntercomRequest getCreateIntercomRequest(Long typeIntercomId, String macAddress, List<SwitchCode> switchCodes) {
        return new CreateIntercomRequest()
                .setTypeId(typeIntercomId)
                .setPassword(TEST_PASSWORD_INTERCOM)
                .setName(TEST_INTERCOM)
                .setMacAddress(macAddress)
                .setSwitchCodes(switchCodes);
    }

    UpdateIntercomRequest getUpdateIntercomRequest(Long typeIntercomId, String macAddress, List<SwitchCode> switchCodes) {
        return new UpdateIntercomRequest()
                .setTypeId(typeIntercomId)
                .setPassword(TEST_PASSWORD_INTERCOM)
                .setName(TEST_INTERCOM)
                .setMacAddress(macAddress)
                .setSwitchCodes(switchCodes);
    }

    protected List<Long> createIntercoms(int count, String userName, String password) {
        List<Long> intercomsIds = new ArrayList<>();
        for (int x = 1; x <= count; x++) {
            intercomsIds.add(createIntercom(userName, password).getId());
        }
        return intercomsIds;
    }


    protected String getRandomString(int length) {
        StringBuffer outputBuffer = new StringBuffer(length);
        for (int i = 0; i < length; i++) {
            outputBuffer.append("s");
        }
        return outputBuffer.toString();
    }
}
