package com.brivo.intercom.integration;

import com.brivo.intercom.integration.data_provider.SpringDataProviderRunner;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringDataProviderRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {
    protected static final String FAKE_BEARER = "fakeBearer";
    protected static final String FAKE_BASIC_AUTH = "fakeBasicAuth";

    @LocalServerPort
    protected int port;

    @Value("${brivo.oauth.username}")
    protected String userNameOfMasterAdmin;

    @Value("${brivo.oauth.password}")
    protected String passwordOfMasterAdmin;

    @Value("${brivo.oauth.url}")
    protected String brivoOauthUrl;

    @Value("${security.oauth2.client.clientId}")
    protected String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    protected String clientSecret;

    @Value("${brivo.api.key}")
    protected String apiKey;

    @Value("${ictApp.baseUrl}")
    protected String testSubjectUrl;

    @Value("${brivo.oauth.installer.username}")
    protected String userNameOfInstaller;

    @Value("${brivo.oauth.password}")
    protected String passwordOfInstaller;

    @Value("${brivo.senior.admin.username}")
    protected String userNameOfSeniorAdmin;

    @Value("${brivo.senior.admin.password}")
    protected String passwordOfSeniorAdmin;
}
