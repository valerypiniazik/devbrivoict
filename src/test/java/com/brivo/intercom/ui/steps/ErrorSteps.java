package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.ErrorPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class ErrorSteps extends ScenarioSteps {
    ErrorPage errorPage;

    @Step
    public void should403ErrorBeDisplayed() {
        errorPage.should403ErrorBeDisplayed();
    }
}
