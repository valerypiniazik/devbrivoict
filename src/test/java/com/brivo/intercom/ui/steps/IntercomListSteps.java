package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.IntercomListPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.junit.Assert;

import java.time.LocalDateTime;

public class IntercomListSteps extends ScenarioSteps {
    IntercomListPage intercomListPage;

    @Step
    public void shouldBeVisibleNewIntercomButton() {
        intercomListPage.shouldBeVisibleNewIntercomButton();
    }

    @Step
    public void deleteIntercom(String intercomName) {
        if (intercomListPage.getPlatformName().contains("Win32") && intercomListPage.getBrowserEngine().contains("Gecko")) {
            intercomListPage.clickOnDeleteIcon();
            return;
        }
        intercomListPage.clickDeleteIntercom(intercomName);
    }

    @Step
    public void clickOkOnModalWindow() {
        intercomListPage.clickOkButton();
    }

    @Step
    public void intercomShouldDisappear() {
        Assert.assertTrue(intercomListPage.emptyListOfIntercomsIsDisplayed());
    }

    @Step
    public void clickEditIntercom(String intercomName) {
        if (intercomListPage.getPlatformName().contains("Win32") && intercomListPage.getBrowserEngine().contains("Gecko")) {
            intercomListPage.clickOnEditIntercomButton();
            return;
        }
        intercomListPage.clickEditButton(intercomName);
    }

    @Step
    public void clickOnNewIntercomButton() {
        intercomListPage.clickOnNewIntercomButton();
    }

    @Step
    public void shouldNotBeVisibleEmptyListOfIntercoms() {
        intercomListPage.shouldNotBeVisibleEmptyListOfIntercoms();
    }

    @Step
    public void shouldBeVisibleEditDeviceButton(String intercomName) {
        intercomListPage.shouldBeVisibleEditDeviceButton(intercomName);
    }

    @Step
    public void shouldBeVisibleViewIntercomHistoryLink() {
        intercomListPage.shouldBeVisibleViewIntercomHistoryLink();
    }

    @Step
    public void shouldBeVisibleEditDirectoryRuledButton(String intercomName) {
        intercomListPage.shouldBeVisibleEditDirectoryRuledButton(intercomName);
    }

    @Step
    public void shouldBeVisibleDeleteButton(String intercomName) {
        intercomListPage.shouldBeVisibleDeleteIcon(intercomName);
    }

    @Step
    public void shouldBeIntercomListSortedByName() {
        intercomListPage.shouldBeIntercomListSortedByName();
    }

    @Step
    public void shouldBeVisibleConfirmModal() {
        intercomListPage.shouldBeVisibleConfirmModal();
    }

    @Step
    public void clickEditDirectoryRules(String intercomName) {
        intercomListPage.clickEditDirectoryRulesButton(intercomName);
    }

    @Step
    public void clickOnEditDirectoryRulesButton() {
        intercomListPage.clickOnEditDirectoryRulesButton();
    }

    @Step
    public void clickIntercomHistoryLink() {
        intercomListPage.clickIntercomHistoryLink();
    }

    @Step
    public void shouldNotBePresentReconnectIntercomButton() {
        intercomListPage.shouldNotBePresentReconnectIntercomButton();
    }

    public void verifyNoAudioDevicesConfiguredIsDisplaying() {
        intercomListPage.verifyNoAudioDevicesConfiguredIsDisplaying();
    }

    public void shouldSeeNameAtInfoLabel(String name) {
        intercomListPage.shouldSeeNameAtInfoLabel(name);
    }

    public void shouldSeeCorrectDateAtInfoLabel(LocalDateTime refreshTokenUpdateTime) {
        intercomListPage.shouldSeeCorrectDateAtInfoLabel(refreshTokenUpdateTime);
    }
}
