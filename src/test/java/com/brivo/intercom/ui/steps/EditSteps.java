package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.EditPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.util.ArrayList;
import java.util.List;

public class EditSteps extends ScenarioSteps {
    EditPage editPage;

    @Step
    public void enterIntercomName(String name) {
        editPage.clearIntercomName();
        editPage.enterIntercomName(name);
    }

    @Step
    public void enterMacAddress(String macAdderss) {
        editPage.enterMacAddress(macAdderss);
    }

    @Step
    public void saveDevice() {
        editPage.clickSaveDevice();
    }

    @Step
    public void clickCancel() {
        editPage.clickCancel();
    }

    @Step
    public void shouldBeVisibleSaveIntercomButton() {
        editPage.shouldBeVisibleSaveIntercomButton();
    }

    @Step
    public void shouldBeVisibleTypeOfModel() {
        editPage.shouldBeVisibleTypeOfModel();
    }

    @Step
    public void clickRegeneratePasswordButton() {
        editPage.clickRegeneratePasswordButton();
    }

    @Step
    public void verifyPasswordNotEmpty() {
        editPage.verifyPasswordNotEmpty();
    }

    @Step
    public void verifyPasswordIsLowercase() {
        editPage.verifyPasswordIsLowercase();
    }

    @Step
    public void verifyPasswordsLenght() {
        editPage.verifyPasswordsLength();
    }

    @Step
    public String getCurrentIntercomPassword() {
        return editPage.getCurrentIntercomPassword();
    }

    @Step
    public void verifyPasswordsUniqueness(Integer amountOfIterations) {
        List<String> regeneratedPasswords = new ArrayList<>();
        for (int i = 0; i < amountOfIterations; i++) {
            clickRegeneratePasswordButton();
            regeneratedPasswords.add(getCurrentIntercomPassword());
        }
        editPage.verifyUniquePasswords(regeneratedPasswords);
    }

    @Step
    public void shouldNotBePasswordDisplayed() {
        editPage.shouldNotBePasswordDisplayed();
    }

    @Step
    public void clickTestDevice() {
        editPage.clickTestDeviceButton();
    }

    @Step
    public void shouldSeeErrorMessage() {
        editPage.shouldSeeErrorMessage();
    }

    @Step
    public void shouldNotSeeErrorMessage() {
        editPage.shouldNotSeeErrorMessage();
    }

    public void verifyMacHasChanged(String macAddress) {
        editPage.verifyMacHasChanged(macAddress);
    }

    public void verifyNameHasChanged(String newName) {
        editPage.verifyNameHasChanged(newName);
    }

    public void shouldNotBeVisibleSaveIntercomButton() {
        editPage.shouldNotBeVisibleSaveIntercomButton();
    }
}
