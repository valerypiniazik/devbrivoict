package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.NewIntercomPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NewIntercomSteps extends ScenarioSteps {
    NewIntercomPage newIntercomPage;

    @Step
    public void enterIntercomName(String intercomName) {
        newIntercomPage.enterIntercomName(intercomName);
    }

    @Step
    public void enterMacAddress() {
        newIntercomPage.enterMacAddress(6);
    }

    @Step
    public void clickOnSaveDeviceButton() {
        newIntercomPage.clickOnSaveDeviceButton();
    }

    @Step
    public void shouldBeVisibleSaveNewIntercomButton() {
        newIntercomPage.shouldBeVisibleSaveNewIntercomButton();
    }

    @Step
    public void clickCancel() {
        newIntercomPage.clickCancel();
    }

    @Step
    public void shouldBePasswordDisplayed() {
        newIntercomPage.shouldBePasswordDisplayed();
    }

    @Step
    public void shouldPasswordContainsLowercaseAlfanumericSymbols() {
        newIntercomPage.shouldPasswordContainsLowercaseAlfanumericSymbols();
    }

    @Step
    public void clickOnRegenerateButton() {
        newIntercomPage.clickOnRegenerateButton();
    }

    public String rememberPassword() {
        return newIntercomPage.getPassword();
    }

    @Step
    public void shouldPasswordBeDifferentFromGeneratedOne(String oldPassword, String newPassword) {
        newIntercomPage.shouldPasswordBeDifferentFromGeneratedOne(oldPassword, newPassword);
    }

    @Step
    public void enterInSwitchCode0(String code) {
        newIntercomPage.enterInSitchCode0(code);
    }

    @Step
    public void enterInSwitchCode1(String code) {
        newIntercomPage.enterInSwitchCode1(code);
    }

    @Step
    public void enterInSwitchCode2(String code) {
        newIntercomPage.enterInSwitchCode2(code);
    }

    @Step
    public void enterInSwitchCode3(String code) {
        newIntercomPage.enterInSwitchCode3(code);
    }

    public void checkIntercomNameOverflow(int count) {
        newIntercomPage.checkIntercomNameOverflow(count);
    }

    public void checkMacAddressOverflow(int count) {
        newIntercomPage.checkMacAddressOverflow(count);
    }

    public void checkSwitchCodesOverflow(int count) {
        newIntercomPage.checkSwitchCodesOverflow(count);
    }

    public void checkDescriptionsOverflow(int count) {
        newIntercomPage.checkDescriptionsOverflow(count);
    }

    public void verifyErrorMessageContains(String errorMessage) {
        newIntercomPage.verifyErrorMessageContains(errorMessage);
    }
}
