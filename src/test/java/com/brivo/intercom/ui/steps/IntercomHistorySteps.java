package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.IntercomHistoryPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class IntercomHistorySteps extends ScenarioSteps {
    IntercomHistoryPage intercomHistoryPage;

    @Step
    public void shouldSeeAllLogs() {
        intercomHistoryPage.logsShouldBeVisible();
    }

    @Step
    public void firstLogShouldBeWithError() {
        intercomHistoryPage.firstLogShouldBeWithError();
    }

    @Step
    public void firstLogShouldBeOK() {
        intercomHistoryPage.firstLogShouldBeOK();
    }

    public void clickBackButton() {
        intercomHistoryPage.clickBackButton();
    }

    @Step
    public void verifyConfigurationFileIsGenerated() {
        if (intercomHistoryPage.getPlatformName().contains("Win32") && intercomHistoryPage.getBrowserEngine().contains("Gecko")) {
            intercomHistoryPage.verifyConfigurationFileIsGeneratedSuccessfully();
            return;
        }
        intercomHistoryPage.verifyConfigurationFileIsGenerated();
    }
}
