package com.brivo.intercom.ui.steps;

import net.thucydides.core.annotations.Steps;
import net.thucydides.core.steps.ScenarioSteps;

public class User extends ScenarioSteps {

    @Steps
    public AccountSteps atAccountPage;

    @Steps
    public IntercomListSteps atIntercomListPage;

    @Steps
    public EditSteps atEditIntercomPage;

    @Steps
    public NewIntercomSteps atNewIntercomPage;

    @Steps
    public DirectoryRulesStpes atDirectoryRulesPage;

    @Steps
    public NewDirectoryRuleSteps atNewDirectoryRulePage;

    @Steps
    public EditDirectoryRuleSteps atEditDirectoryRulePage;

    @Steps
    public ErrorSteps atErrorPage;

    @Steps
    public IntercomHistorySteps atIntercomHistoryPage;

    @Steps
    public AnyPageSteps atAnyPage;

    @Steps
    public BrivoLoginSteps atBrivoLoginPage;
}
