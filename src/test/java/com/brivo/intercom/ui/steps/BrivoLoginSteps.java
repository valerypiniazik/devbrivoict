package com.brivo.intercom.ui.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.junit.Assert.*;

public class BrivoLoginSteps extends ScenarioSteps {
    @Step
    public void verifyCorrectUrl() {
        assertEquals(getDriver().getCurrentUrl(), "https://acs.brivo.com/login/Login.do");
    }
}
