package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.EditDirectoryRulePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class EditDirectoryRuleSteps extends ScenarioSteps {
    EditDirectoryRulePage editDirectoryRulePage;

    @Step
    public void enterInListingPattern(String text) {
        editDirectoryRulePage.enterInListingPattern(text);
    }

    @Step
    public void enterInFirstPhoneTypeInput(String text) {
        editDirectoryRulePage.enterInFirstPhoneTypeInput(text);
    }

    @Step
    public void enterInSecondPhoneTypeInput(String text) {
        editDirectoryRulePage.enterInSecondPhoneTypeInput(text);
    }

    @Step
    public void enterInThirdPhoneTypeInput(String text) {
        editDirectoryRulePage.enterInThirdPhoneTypeInput(text);
    }

    @Step
    public void clickSaveEditedDirectoryRule() {
        editDirectoryRulePage.clickSaveEditedDirectoryRuleButton();
    }

    public void verifyIntercomGroupConfigChanged(String listingPattern, String firstPhoneType, String secondPhoneType, String thirdPhoneType) {
        editDirectoryRulePage.verifyIntercomGroupConfigChanged(listingPattern, firstPhoneType, secondPhoneType, thirdPhoneType);
    }
}
