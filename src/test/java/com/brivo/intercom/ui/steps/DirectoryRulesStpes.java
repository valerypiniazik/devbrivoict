package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.DirectoryRulesPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.junit.Assert.*;

public class DirectoryRulesStpes extends ScenarioSteps {
    DirectoryRulesPage directoryRulesPage;

    @Step
    public void clickAddRule() {
        directoryRulesPage.clickAddRuleButton();
    }

    @Step
    public void shouldNotBeVisibleEmptyListOfRules() {
        directoryRulesPage.shouldNotBeVisibleEmptyListOfIntercoms();
    }

    @Step
    public void clickEditRule(String listingPattern) {
        directoryRulesPage.clickEditRule(listingPattern);
    }

    @Step
    public void clickDeleteRule(String listingPattern) {
        directoryRulesPage.clickDeleteRule(listingPattern);
    }

    @Step
    public void rulesShouldDisappear() {
        assertTrue(directoryRulesPage.emptyListOfRulesIsDisplayed());
    }

    @Step
    public void clickOkOnModalWindow() {
        directoryRulesPage.clickOkButton();
    }

    public void clickGoToDeviceListButton() {
        directoryRulesPage.clickGoToDeviceListButton();
    }

    public void verifyNoDirectoryRulesDefinedIsDisplaying() {
        directoryRulesPage.verifyNoDirectoryRulesDefinedIsDisplaying();
    }
}
