package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.AccountPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class AccountSteps extends ScenarioSteps {
    AccountPage accountPage;

    @Step
    public void enterUserName(String userName) {
        accountPage.enterUserName(userName);
    }

    @Step
    public void enterPassword(String password) {
        accountPage.enterPassword(password);
    }

    @Step
    public void clickOnLoginButton() {
        accountPage.clickOnButtonlogIn();
    }

    @Step
    public void enterAccountPage(String url) {
        accountPage.maximizeWindow();
        accountPage.enterAccountPage(url);
    }

    @Step
    public void clickOnSubmitButton() {
        accountPage.clickOnSubmitButton();
    }

    @Step
    public void enterAccountId(String accountId) {
        accountPage.enterAccountId(accountId);
    }

    @Step
    public void clickOnSearchButton() {
        accountPage.clickOnSearchButton();
    }

    @Step
    public void clickOnViewButton() {
        accountPage.clickOnViewButton();
    }

    @Step
    public void login(String username, String password, String accountId) {
        enterUserName(username);
        enterPassword(password);
        clickOnLoginButton();
        clickOnSubmitButton();
//        enterAccountId(accountId);
//        clickOnSearchButton();
//        clickOnViewButton();
    }

    public void waitViewButton() {
        accountPage.waitViewButton();
    }
}
