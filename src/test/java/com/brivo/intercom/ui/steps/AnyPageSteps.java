package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.LayOut;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import java.io.IOException;
import java.net.URL;

public class AnyPageSteps extends ScenarioSteps {
    LayOut layOut;

    @Step
    public void clickBrivoOnairLogo() {
        layOut.clickBriviOnairLogo();
    }

    public void goToBaseUrl() {
        try {
            URL url = new URL(getDriver().getCurrentUrl());
            String baseUrl = url.getProtocol() + "://" + url.getHost() + ":" + url.getPort();
            if (!url.toString().replace(baseUrl, "").equals("/#/")) {
                getDriver().navigate().to(baseUrl);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
