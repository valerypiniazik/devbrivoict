package com.brivo.intercom.ui.steps;

import com.brivo.intercom.ui.pages.NewDirectoryRulePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class NewDirectoryRuleSteps extends ScenarioSteps {
    NewDirectoryRulePage newDirectoryRulePage;

    @Step
    public void enterInListingPattern(String text) {
        newDirectoryRulePage.enterInListingPattern(text);
    }

    @Step
    public void enterInFirstPhoneTypeInput(String text) {
        newDirectoryRulePage.enterInFirstPhoneTypeInput(text);
    }

    @Step
    public void enterInSecondPhoneTypeInput(String text) {
        newDirectoryRulePage.enterInSecondPhoneTypeInput(text);
    }

    @Step
    public void enterInThirdPhoneTypeInput(String text) {
        newDirectoryRulePage.enterInThirdPhoneTypeInput(text);
    }

    @Step
    public void clickSaveNewDirectoryRule() {
        newDirectoryRulePage.clickSaveNewDirectoryRuleButton();
    }

    @Step
    public void enterGroup(String group) {
        newDirectoryRulePage.enterGroup(group);
    }

    public void checkListingPatternOverflow(int count) {
        newDirectoryRulePage.checkListingPatternOverflow(count);
    }

    public void checkFirstPhoneTypeOverflow(int count) {
        newDirectoryRulePage.checkFirstPhoneTypeOverflow(count);
    }

    public void checkSecondPhoneTypeOverflow(int count) {
        newDirectoryRulePage.checkSecondPhoneTypeOverflow(count);
    }

    public void checkThirdPhoneTypeOverflow(int count) {
        newDirectoryRulePage.checkThirdPhoneTypeOverflow(count);
    }

    public void verifyErrorMessageContains(String errorMessage) {
        newDirectoryRulePage.verifyErrorMessageContains(errorMessage);
    }
}
