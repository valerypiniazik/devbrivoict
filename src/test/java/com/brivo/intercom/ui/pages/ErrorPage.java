package com.brivo.intercom.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ErrorPage extends BasePage {

    @FindBy(xpath = "//div[contains(text(), '403')]")
    private WebElement errorMessage;

    public ErrorPage(WebDriver driver) {
        super(driver);
    }

    public void should403ErrorBeDisplayed() {
        element(errorMessage).shouldBeVisible();
    }
}
