package com.brivo.intercom.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LayOut extends BasePage {
    public LayOut(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".logo")
    private WebElement brivoOnairLogo;

    public void clickBriviOnairLogo() {
        brivoOnairLogo.click();
    }
}
