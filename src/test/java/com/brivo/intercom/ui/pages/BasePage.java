package com.brivo.intercom.ui.pages;

import com.brivo.intercom.ui.data.TestData;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;

public abstract class BasePage extends PageObject {
    BasePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    protected Wait<WebDriver> wait(WebDriver driver, Duration timeOut, Duration pollingInterval) {
        return new FluentWait<>(driver).withTimeout(timeOut)
                .pollingEvery(pollingInterval).ignoring(NoSuchElementException.class);
    }

    public String getPlatformName() {
        return (String) ((JavascriptExecutor) getDriver()).executeScript("return navigator.platform;");
    }

    public String getBrowserEngine() {
        return (String) ((JavascriptExecutor) getDriver()).executeScript("return navigator.product;");
    }

    protected boolean isMobileVersion() {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        return ((Number) js.executeScript("return window.innerWidth")).intValue() < TestData.S_SIZE_OF_SCREEN;
    }

    protected void scrollDownThePage() {
        ((JavascriptExecutor) getDriver()).executeScript("window.scrollTo(0,document.body.scrollHeight)");
    }

    protected boolean testIsOnFirefoxWin32() {
        return "Win32".contains(getPlatformName()) && "Gecko".contains(getBrowserEngine());
    }
}
