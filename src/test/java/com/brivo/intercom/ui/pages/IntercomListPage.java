package com.brivo.intercom.ui.pages;

import static org.junit.Assert.*;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.time.format.DateTimeFormatter;

import java.time.LocalDateTime;
import java.util.List;

public class IntercomListPage extends BasePage {

    @FindBy(id = "newIntercom")
    private WebElement newIntercomButton;

    @FindBy(id = "confirmModal_intercomRemovalConfirmation")
    private WebElement modalWindowOkButton;

    @FindBy(id = "noDevicesMessage")
    private WebElement emptyListOfIntercoms;

    @FindBy(css = ".brivo-list-row .col-md-3 div")
    private List<WebElement> intercomNames;

    @FindBy(css = "[id^=viewIntercomHistory]")
    private WebElement intercomHistoryLink;

    @FindBy(id = "reconnectIntercom")
    private WebElement reconnectIntercomButton;

    @FindBy(css = ".empty-list-column")
    private WebElement emptyListMessage;

    @FindBy(css = ".secondary-header-info")
    private WebElement infoLabel;

    @FindBy(css = ".brivo-list-row")
    private List<WebElement> intercomList;

    @FindBy(css = ".deleteIcon")
    private WebElement deleteIcon;

    @FindBy(css = "a[id*='editIntercom']")
    private WebElement editIntercomButton;

    @FindBy(css = "[id*=editDirectoryRules]")
    private WebElement editDirectoryRulesButton;

    private By editDirectoryRules = By.cssSelector("[id^=editDirectoryRules]");

    private By editIntercom = By.cssSelector("[id^=editIntercom]");

    private By deleteButton = By.cssSelector(".deleteIcon");

    public IntercomListPage(WebDriver driver) {
        super(driver);
    }

    public void shouldBeVisibleNewIntercomButton() {
        element(newIntercomButton).shouldBeVisible();
    }

    public void clickOnDeleteIcon() {
        if (getPlatformName().contains("Win32") && getBrowserEngine().contains("Gecko")) {
            waitFor(deleteIcon);
        }
        deleteIcon.click();
    }

    public void clickDeleteIntercom(String intercomName) {
        if (getPlatformName().contains("Win32") && getBrowserEngine().contains("Gecko")) {
            waitFor(deleteIcon);
            deleteIcon.click();
            return;
        }
        getElementWithinIntercom(intercomName, deleteButton).click();
    }

    public void clickOkButton() {
        modalWindowOkButton.click();
    }

    public void clickEditButton(String intercomName) {
        getElementWithinIntercom(intercomName, editIntercom).click();
    }

    public void clickOnEditIntercomButton() {
        if (getPlatformName().contains("Win32") && getBrowserEngine().contains("Gecko")) {
            waitFor(editIntercomButton);
        }
        editIntercomButton.click();
    }

    public void clickEditDirectoryRulesButton(String intercomName) {
        getElementWithinIntercom(intercomName, editDirectoryRules).click();
    }

    public void clickOnEditDirectoryRulesButton() {
        editDirectoryRulesButton.click();
    }

    public Boolean emptyListOfIntercomsIsDisplayed() {
        waitFor(emptyListOfIntercoms);
        return emptyListOfIntercoms.isDisplayed();
    }

    public void clickOnNewIntercomButton() {
        if (getPlatformName().contains("Win32") && getBrowserEngine().contains("Gecko")) {
            waitFor(newIntercomButton);
        }
        element(newIntercomButton).click();
    }

    public void shouldNotBeVisibleEmptyListOfIntercoms() {
        element(emptyListOfIntercoms).shouldNotBeVisible();
    }

    public void shouldBeVisibleEditDeviceButton(String intercomName) {
        element(getElementWithinIntercom(intercomName, By.cssSelector("[id^=editIntercom]"))).shouldBeVisible();
    }

    public void shouldBeVisibleViewIntercomHistoryLink() {
        element(intercomHistoryLink).shouldBeVisible();
    }

    public void shouldBeVisibleEditDirectoryRuledButton(String intercomName) {
        element(getElementWithinIntercom(intercomName, editDirectoryRules)).shouldBeVisible();
    }

    public void shouldBeVisibleDeleteIcon(String intercomName) {
        element(getElementWithinIntercom(intercomName, deleteButton)).shouldBeVisible();
    }

    private boolean areIntercomNamesSortedByName() {
        int k = 0;
        String currentName = "";
        for (WebElement intercom : intercomNames) {
            String intercomName = intercom.getText();
            while (intercomName.compareToIgnoreCase(currentName) > 0) {
                currentName = intercomName;
                k++;
            }
        }

        if (k != intercomNames.size()) {
            return false;
        }
        return true;
    }

    public void shouldBeIntercomListSortedByName() {
        assertTrue(areIntercomNamesSortedByName());
    }

    public void shouldBeVisibleConfirmModal() {
        element(modalWindowOkButton).shouldBeVisible();
    }

    public void clickIntercomHistoryLink() {
        intercomHistoryLink.click();
    }

    public void shouldNotBePresentReconnectIntercomButton() {
        element(reconnectIntercomButton).shouldNotBePresent();
    }

    public void verifyNoAudioDevicesConfiguredIsDisplaying() {
        element(emptyListMessage).isVisible();
    }

    public void shouldSeeNameAtInfoLabel(String name) {
        waitFor(ExpectedConditions.visibilityOf(infoLabel));
        assertTrue(infoLabel.getText().contains(name));
    }

    public void shouldSeeCorrectDateAtInfoLabel(LocalDateTime refreshTokenUpdateTime) {
        waitFor(ExpectedConditions.visibilityOf(infoLabel));
        assertTrue(infoLabel.getText().contains(refreshTokenUpdateTime.format(DateTimeFormatter.ofPattern("M/d/y"))));
        assertTrue(infoLabel.getText().contains(refreshTokenUpdateTime.format(DateTimeFormatter.ofPattern("h:mm a"))));
    }

    private WebElement getElementWithinIntercom(String intercomName, By internalLocator) {
        WebElement foundElement = null;
        wait(getDriver(), Duration.ofSeconds(10), Duration.ofSeconds(1)).until(driver -> {
            return isElementVisible(By.cssSelector(".brivo-list-row"));
        });
        for (WebElement intercom : intercomList) {
            if (intercom.findElement(By.xpath("//div[contains(@class,'brivo-list-row')]//div[@class='brivo-list-cell' and text()='" + intercomName + "']")).isDisplayed()) {
                foundElement = intercom.findElement(internalLocator);
                break;
            }

        }
        if (foundElement == null) {
            throw new ElementNotFoundException("Element within intercom" + intercomName, "internal locator", internalLocator.toString());
        } else {
            return foundElement;
        }
    }
}

