package com.brivo.intercom.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import static org.junit.Assert.*;

public class EditPage extends BasePage {
    public EditPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "intercomName")
    private WebElement editIntercomNameInput;

    @FindBy(css = "input[mask]")
    private WebElement macAddressInput;

    @FindBy(css = ".d-sm-block #saveIntercom")
    private WebElement saveDeviceButton;

    @FindBy(css = ".mac-input-part")
    private List<WebElement> macInputs;

    @FindBy(id = "cancelIntercom")
    private WebElement cancelIntercomButton;

    @FindBy(id = "intercomType")
    private WebElement typeOfModel;

    @FindBy(id = "regeneratePassword")
    private WebElement regeneratePasswordButton;

    @FindBy(id = "currentIntercomPassword")
    private WebElement currentIntercomPassword;

    @FindBy(id = "testIntercom")
    private WebElement testDeviceButton;

    @FindBy(css = ".d-sm-none #testIntercom")
    private WebElement testDeviceButtonMobile;

    @FindBy(css = ".error")
    private WebElement errorMessage;

    public void enterIntercomName(String name) {
        editIntercomNameInput.sendKeys(name);
    }

    public void clearIntercomName() {
        editIntercomNameInput.clear();
    }

    public void enterMacAddress(String macAddress) {
        macAddressInput.clear();
        macAddressInput.sendKeys(macAddress);
    }

    public void clickSaveDevice() {

        if (isMobileVersion()) {
            scrollDownThePage();
            getDriver().findElement(By.cssSelector(".d-sm-none #saveIntercom")).click();
        } else {
            if (testIsOnFirefoxWin32()) {
                waitFor(saveDeviceButton);
            }
            saveDeviceButton.click();
        }
    }

    public void clickCancel() {
        cancelIntercomButton.click();
    }

    public void shouldBeVisibleSaveIntercomButton() {
        element(saveDeviceButton).shouldBeVisible();
    }

    public void shouldBeVisibleTypeOfModel() {
        element(typeOfModel).shouldBeVisible();
    }

    public void clickRegeneratePasswordButton() {
        regeneratePasswordButton.click();
    }

    public String getCurrentIntercomPassword() {
        return currentIntercomPassword.getText();
    }

    public void verifyPasswordNotEmpty() {
        assertFalse(currentIntercomPassword.getText().isEmpty());
    }

    public void verifyPasswordIsLowercase() {
        final Pattern hasUppercase = Pattern.compile("[A-Z]");
        assertTrue(!hasUppercase.matcher(currentIntercomPassword.getText()).find());
    }

    public void verifyPasswordsLength() {
        assertEquals(currentIntercomPassword.getText().length(), 8);
    }

    public void verifyUniquePasswords(List<String> passwords) {
        Boolean hasDuplicate = false;
        Set<String> set = new HashSet<>();
        for (String each : passwords) if (!set.add(each)) hasDuplicate = true;
        assertFalse(hasDuplicate);
    }

    public void shouldNotBePasswordDisplayed() {
        assertEquals(currentIntercomPassword.getText(), "########");
    }

    public void clickTestDeviceButton() {
        if (isMobileVersion()) {
            testDeviceButtonMobile.click();
        } else {
            if (testIsOnFirefoxWin32()) {
                waitFor(testDeviceButton);
            }
            testDeviceButton.click();
        }
    }

    public void shouldSeeErrorMessage() {
        waitFor(ExpectedConditions.visibilityOf(errorMessage));
    }

    public void shouldNotSeeErrorMessage() {
        element(errorMessage).shouldNotBeVisible();
    }

    public void verifyMacHasChanged(String macAddress) {
        assertEquals(macAddressInput.getAttribute("value").replace("-", ""), macAddress);
    }

    public void verifyNameHasChanged(String newName) {
        assertEquals(editIntercomNameInput.getAttribute("value"), newName);
    }

    public void shouldNotBeVisibleSaveIntercomButton() {
        element(saveDeviceButton).shouldNotBeVisible();
    }
}
