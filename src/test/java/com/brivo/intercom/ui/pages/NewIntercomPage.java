package com.brivo.intercom.ui.pages;

import net.serenitybdd.core.annotations.findby.FindBy;

import static org.junit.Assert.*;

import org.apache.commons.lang.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.Random;

import static org.junit.Assert.assertTrue;

public class NewIntercomPage extends BasePage {
    private String macAddress = "";

    @FindBy(id = "intercomName")
    private WebElement intercomNameField;

    @FindBy(xpath = "//*[@mask]")
    private WebElement macAddressField;

    @FindBy(id = "saveIntercom")
    private WebElement saveNewIntercomButton;

    @FindBy(id = "cancelIntercom")
    private WebElement cancelNewIntercom;

    @FindBy(id = "currentIntercomPassword")
    private WebElement password;

    @FindBy(id = "regeneratePassword")
    private WebElement regeneratePasswordButton;

    @FindBy(id = "switchCode0")
    private WebElement switchCode0;

    @FindBy(id = "switchCode1")
    private WebElement switchCode1;

    @FindBy(id = "switchCode2")
    private WebElement switchCode2;

    @FindBy(id = "switchCode3")
    private WebElement switchCode3;

    @FindBy(id = "switchCode0Description")
    private WebElement switchCode0Description;

    @FindBy(id = "switchCode1Description")
    private WebElement switchCode1Description;

    @FindBy(id = "switchCode2Description")
    private WebElement switchCode2Description;

    @FindBy(id = "switchCode3Description")
    private WebElement switchCode3Description;

    @FindBy(css = ".error span")
    private WebElement error;

    public NewIntercomPage(WebDriver driver) {
        super(driver);
    }

    public void enterIntercomName(String intercomName) {
        intercomNameField.sendKeys(intercomName);
    }

    public String randomInt() {
        Random random = new Random();
        return String.valueOf(random.nextInt(10)) + String.valueOf(random.nextInt(10));
    }

    public void enterMacAddress(int lengthMacAddress) {
        element(macAddressField).click();
        for (int i = 0; i < lengthMacAddress; i++) {
            String randomValue = randomInt();
            getDriver().switchTo().activeElement().sendKeys(randomValue);
            macAddress += randomValue + "-";
        }
    }

    public void clickOnSaveDeviceButton() {
        if (isMobileVersion()) {
            scrollDownThePage();
            getDriver().findElement(By.cssSelector(".d-sm-none #saveIntercom")).click();
        } else {
            if (testIsOnFirefoxWin32()) {
                waitFor(saveNewIntercomButton);
                saveNewIntercomButton.click();
                return;
            }
            element(saveNewIntercomButton).click();
        }
    }

    public void shouldBeVisibleSaveNewIntercomButton() {
        element(saveNewIntercomButton).shouldBeVisible();
    }

    public void clickCancel() {
        cancelNewIntercom.click();
    }

    public void shouldBePasswordDisplayed() {
        assertTrue(!element(password).getText().isEmpty());
    }

    public boolean isPasswordContainsLowercaseAlfanumericSymbols() {
        String passwordString = element(password).getText();
        String pattern = "[\\da-z]{8}$";
        return passwordString.matches(pattern);
    }

    public String getPassword() {
        return element(password).getText();
    }

    public void shouldPasswordBeDifferentFromGeneratedOne(String oldPassword, String newPassword) {
        assertTrue(!oldPassword.equals(newPassword));
    }

    public void shouldPasswordContainsLowercaseAlfanumericSymbols() {
        assertTrue(isPasswordContainsLowercaseAlfanumericSymbols());
    }

    public void clickOnRegenerateButton() {
        element(regeneratePasswordButton).click();
    }

    public void enterInSitchCode0(String code) {
        switchCode0.sendKeys(code);
    }

    public void enterInSwitchCode1(String code) {
        switchCode1.sendKeys(code);
    }

    public void enterInSwitchCode2(String code) {
        switchCode2.sendKeys(code);
    }

    public void enterInSwitchCode3(String code) {
        switchCode3.sendKeys(code);
    }

    public void checkIntercomNameOverflow(int count) {
        String generatedString = RandomStringUtils.random(count, true, true);
        intercomNameField.sendKeys(generatedString);
        assertEquals(intercomNameField.getAttribute("value").length(), 255);
    }

    public void checkMacAddressOverflow(int count) {
        String generatedMac = RandomStringUtils.random(count, false, true);
        macAddressField.sendKeys(generatedMac);
        assertEquals(macAddressField.getAttribute("value").length(), 17);
    }

    public void checkSwitchCodesOverflow(int count) {
        String generatedSwitchCode = RandomStringUtils.random(count, true, true);
        switchCode0.sendKeys(generatedSwitchCode);
        switchCode1.sendKeys(generatedSwitchCode);
        switchCode2.sendKeys(generatedSwitchCode);
        switchCode3.sendKeys(generatedSwitchCode);
        assertEquals(switchCode0.getAttribute("value").length(), 15);
        assertEquals(switchCode1.getAttribute("value").length(), 15);
        assertEquals(switchCode2.getAttribute("value").length(), 15);
        assertEquals(switchCode3.getAttribute("value").length(), 15);
    }

    public void checkDescriptionsOverflow(int count) {
        String generatedDescription = RandomStringUtils.random(count, true, true);
        switchCode0Description.sendKeys(generatedDescription);
        switchCode1Description.sendKeys(generatedDescription);
        switchCode2Description.sendKeys(generatedDescription);
        switchCode3Description.sendKeys(generatedDescription);
        assertEquals(switchCode0Description.getAttribute("value").length(), 255);
        assertEquals(switchCode1Description.getAttribute("value").length(), 255);
        assertEquals(switchCode2Description.getAttribute("value").length(), 255);
        assertEquals(switchCode3Description.getAttribute("value").length(), 255);
    }

    public void verifyErrorMessageContains(String errorMessage) {
        element(error).shouldBeVisible();
        assertTrue(error.getText().contains(errorMessage));
    }
}
