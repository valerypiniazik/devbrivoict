package com.brivo.intercom.ui.pages;

import org.apache.commons.lang.RandomStringUtils;

import static org.junit.Assert.*;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NewDirectoryRulePage extends BasePage {
    public NewDirectoryRulePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "listingPattern")
    private WebElement listingPatternInput;

    @FindBy(id = "firstPhoneType")
    private WebElement firstPhoneTypeInput;

    @FindBy(id = "secondPhoneType")
    private WebElement secondPhoneTypeInput;

    @FindBy(id = "thirdPhoneType")
    private WebElement thirdPhoneTypeInput;

    @FindBy(id = "saveNewDirectoryRule")
    private WebElement saveNewDirectoryRuleButton;

    @FindBy(css = ".Select-arrow")
    private WebElement selectArrow;

    @FindBy(css = ".Select-control input")
    private WebElement groupInput;

    @FindBy(css = ".Select-menu-outer")
    private WebElement selectMenu;

    @FindBy(css = ".error span")
    private WebElement error;

    public void enterInListingPattern(String text) {
        listingPatternInput.sendKeys(text);
    }

    public void enterInFirstPhoneTypeInput(String text) {
        firstPhoneTypeInput.sendKeys(text);
    }

    public void enterInSecondPhoneTypeInput(String text) {
        secondPhoneTypeInput.sendKeys(text);
    }

    public void enterInThirdPhoneTypeInput(String text) {
        thirdPhoneTypeInput.sendKeys(text);
    }

    public void clickSaveNewDirectoryRuleButton() {
        saveNewDirectoryRuleButton.click();
    }

    public void enterGroup(String group) {
        selectArrow.click();
        selectMenu.findElement(By.cssSelector("[aria-label=" + group + "]")).click();
    }

    public void checkListingPatternOverflow(int count) {
        String generatedListingPattern = RandomStringUtils.random(count, true, true);
        listingPatternInput.sendKeys(generatedListingPattern);
        assertEquals(listingPatternInput.getAttribute("value").length(), 255);
    }

    public void checkFirstPhoneTypeOverflow(int count) {
        String generatedFirstPhoneType = RandomStringUtils.random(count, true, true);
        firstPhoneTypeInput.sendKeys(generatedFirstPhoneType);
        assertEquals(firstPhoneTypeInput.getAttribute("value").length(), 255);
    }

    public void checkSecondPhoneTypeOverflow(int count) {
        String generatedSecondPhoneType = RandomStringUtils.random(count, true, true);
        secondPhoneTypeInput.sendKeys(generatedSecondPhoneType);
        assertEquals(secondPhoneTypeInput.getAttribute("value").length(), 255);
    }

    public void checkThirdPhoneTypeOverflow(int count) {
        String generatedThirdPhoneType = RandomStringUtils.random(count, true, true);
        thirdPhoneTypeInput.sendKeys(generatedThirdPhoneType);
        assertEquals(thirdPhoneTypeInput.getAttribute("value").length(), 255);
    }

    public void verifyErrorMessageContains(String errorMessage) {
        element(error).shouldBeVisible();
        assertTrue(error.getText().contains(errorMessage));
    }
}
