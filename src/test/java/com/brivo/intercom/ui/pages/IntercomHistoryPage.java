package com.brivo.intercom.ui.pages;

import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.Duration;
import java.util.List;

public class IntercomHistoryPage extends BasePage {
    public IntercomHistoryPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".row.data1.brivo-list-row")
    private List<WebElement> listOfLogs;

    @FindBy(id = "linkToAllIntercomsPage")
    private WebElement backButton;

    @FindBy(css = ".badge span")
    private List<WebElement> LogsStatuses;

    @FindBy(id = "noHistoryMessage")
    private WebElement noHistoryMessage;

    public void logsShouldBeVisible() {
        for (WebElement log : listOfLogs) {
            element(log).shouldBeVisible();
        }
    }

    public void firstLogShouldBeWithError() {
        logsShouldBeVisible();
        wait(getDriver(), Duration.ofSeconds(30), Duration.ofSeconds(5)).until(driver -> {
            if (LogsStatuses.get(0).getText().equals("ERROR")) {
                return true;
            } else {
                driver.navigate().refresh();
                return false;
            }
        });
    }

    public void firstLogShouldBeOK() {
        logsShouldBeVisible();
        wait(getDriver(), Duration.ofSeconds(30), Duration.ofSeconds(5)).until(driver -> {
            if (LogsStatuses.get(0).getText().equals("OK")) {
                return true;
            } else {
                driver.navigate().refresh();
                return false;
            }
        });
    }

    public void clickBackButton() {
        backButton.click();
    }

    public void verifyConfigurationFileIsGenerated() {
        assertTrue(listOfLogs.size() > 0);
        wait(getDriver(), Duration.ofSeconds(30), Duration.ofSeconds(5)).until(driver -> {
            if (!LogsStatuses.get(0).getText().equals("IN PROGRESS") && !LogsStatuses.get(0).getText().equals("ERROR")) {
                return true;
            } else {
                driver.navigate().refresh();
                return false;
            }
        });
    }

    public void verifyConfigurationFileIsGeneratedSuccessfully() {
        element(noHistoryMessage).shouldNotBePresent();
    }
}
