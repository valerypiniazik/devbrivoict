package com.brivo.intercom.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.time.Duration;

public class AccountPage extends BasePage {

    @FindBy(id = "username")
    private WebElement inputUserName;

    @FindBy(id = "password")
    private WebElement inputPassword;

    @FindBy(css = "input[type='submit']")
    private WebElement logInButton;

    @FindBy(css = "button")
    private WebElement submitButton;

    @FindBy(id = "accountId")
    private WebElement accountIdField;

    @FindBy(id = "accountName")
    private WebElement accountNameField;

    @FindBy(id = "accountSearch")
    private WebElement searchButton;

    @FindBy(id = "viewAccount_682627")
    private WebElement viewButton;

    public AccountPage(WebDriver driver) {
        super(driver);
    }

    public void enterAccountPage(String url) {
        getDriver().get(url);
    }

    public void enterUserName(String userName) {
        inputUserName.sendKeys(userName);
    }

    public void enterPassword(String password) {
        inputPassword.sendKeys(password);
    }

    public void clickOnButtonlogIn() {
        logInButton.click();
    }

    public void clickOnSubmitButton() {
        if (getPlatformName().contains("Win32") && getBrowserEngine().contains("Gecko")) {
            waitFor(submitButton);
            submitButton.click();
            return;
        }
        element(submitButton).click();
    }

    public void enterAccountId(String accountId) {
        element(accountIdField).type(accountId);
    }

    public void clickOnSearchButton() {
        element(searchButton).click();
    }

    public void clickOnViewButton() {
        element(viewButton).click();
    }

    public void maximizeWindow() {
        getDriver().manage().window().maximize();
    }

    public void waitViewButton() {
        wait(getDriver(), Duration.ofMinutes(2), Duration.ofSeconds(2)).until(driver -> element(viewButton).isVisible());
    }
}
