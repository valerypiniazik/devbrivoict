package com.brivo.intercom.ui.pages;

import static org.junit.Assert.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EditDirectoryRulePage extends BasePage {
    public EditDirectoryRulePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "listingPattern")
    private WebElement listingPatternInput;

    @FindBy(id = "firstPhoneType")
    private WebElement firstPhoneTypeInput;

    @FindBy(id = "secondPhoneType")
    private WebElement secondPhoneTypeInput;

    @FindBy(id = "thirdPhoneType")
    private WebElement thirdPhoneTypeInput;

    @FindBy(id = "saveNewDirectoryRule")
    private WebElement saveNewDirectoryRuleButton;

    @FindBy(id = "group")
    private WebElement groupDropDown;

    public void enterInListingPattern(String text) {
        listingPatternInput.clear();
        listingPatternInput.sendKeys(text);
    }

    public void enterInFirstPhoneTypeInput(String text) {
        firstPhoneTypeInput.clear();
        firstPhoneTypeInput.sendKeys(text);
    }

    public void enterInSecondPhoneTypeInput(String text) {
        secondPhoneTypeInput.clear();
        secondPhoneTypeInput.sendKeys(text);
    }

    public void enterInThirdPhoneTypeInput(String text) {
        thirdPhoneTypeInput.clear();
        thirdPhoneTypeInput.sendKeys(text);
    }

    public void clickSaveEditedDirectoryRuleButton() {
        saveNewDirectoryRuleButton.click();
    }

    public void verifyIntercomGroupConfigChanged(String listingPattern, String firstPhoneType, String secondPhoneType, String thirdPhoneType) {
        assertEquals(listingPatternInput.getAttribute("value"), listingPattern);
        assertEquals(firstPhoneTypeInput.getAttribute("value"), firstPhoneType);
        assertEquals(secondPhoneTypeInput.getAttribute("value"), secondPhoneType);
        assertEquals(thirdPhoneTypeInput.getAttribute("value"), thirdPhoneType);
    }
}
