package com.brivo.intercom.ui.pages;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;
import java.util.List;

public class DirectoryRulesPage extends BasePage {
    public DirectoryRulesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "addRule")
    private WebElement addRuleButton;

    @FindBy(css = ".empty-list-column")
    private WebElement emptyListOfRuless;

    @FindBy(id = "confirmModal_directoryRulesRemovalConfirmation")
    private WebElement modalWindowOkButton;

    @FindBy(id = "linkToAllIntercomsPage")
    private WebElement goToDeviceListButton;

    @FindBy(css = ".empty-list-column")
    private WebElement emptyListMessage;

    @FindBy(css = ".brivo-list-row.directory-rules-summary")
    private List<WebElement> listOfRules;

    private By editRuleButton = By.cssSelector("[id^=editDirectoryRule] div");

    private By editRuleButtonMobile = By.cssSelector(".d-block [id^=editDirectoryRule]");

    private By deleteRuleButton = By.cssSelector(".deleteIcon");

    private By deleteRuleButtonMobile = By.xpath("//button//span[text()='Delete']");

    public void clickAddRuleButton() {
        if (testIsOnFirefoxWin32()) {
            waitFor(addRuleButton);
        }
        addRuleButton.click();
    }

    public void shouldNotBeVisibleEmptyListOfIntercoms() {
        element(emptyListOfRuless).shouldNotBeVisible();
    }

    public void clickEditRule(String listingPattern) {
        if (isMobileVersion()) {
            getElementWithinRule(listingPattern, editRuleButtonMobile).click();
        } else {
            getElementWithinRule(listingPattern, editRuleButton).click();
        }
    }

    public void clickDeleteRule(String listingPattern) {
        if (isMobileVersion()) {
            getElementWithinRule(listingPattern, deleteRuleButtonMobile).click();
        } else {
            getElementWithinRule(listingPattern, deleteRuleButton).click();
        }
    }

    public Boolean emptyListOfRulesIsDisplayed() {
        waitFor(ExpectedConditions.visibilityOf(emptyListOfRuless));
        return emptyListOfRuless.isDisplayed();
    }

    public void clickOkButton() {
        modalWindowOkButton.click();
    }

    public void clickGoToDeviceListButton() {
        goToDeviceListButton.click();
    }

    public void verifyNoDirectoryRulesDefinedIsDisplaying() {
        element(emptyListMessage).isVisible();
    }

    private WebElement getElementWithinRule(String ruleListingPattern, By internalLocator) {
        WebElement foundElement = null;
        wait(getDriver(), Duration.ofSeconds(10), Duration.ofSeconds(1)).until(driver -> {
            return isElementVisible(By.cssSelector(".brivo-list-row.directory-rules-summary"));
        });
        for (WebElement rule : listOfRules) {
            if (rule.findElement(By.xpath("//div[contains(@class,'brivo-list-row directory-rules-summary')]//div[contains(@class,'brivo-list-cell') and text()='" + ruleListingPattern + "']")).isDisplayed()) {
                foundElement = rule.findElement(internalLocator);
            }
        }
        if (foundElement == null) {
            throw new ElementNotFoundException("Element within rule with listing pattern" + ruleListingPattern, "internal locator", internalLocator.toString());
        } else {
            return foundElement;
        }
    }
}
