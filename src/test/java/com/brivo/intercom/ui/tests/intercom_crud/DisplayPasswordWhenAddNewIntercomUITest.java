package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class DisplayPasswordWhenAddNewIntercomUITest extends SimpleTestTemplate {
    @Test
    public void seePasswordWhenAddingNewIntercom() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.shouldBePasswordDisplayed();
        String oldPassword = user.atNewIntercomPage.rememberPassword();
        user.atNewIntercomPage.shouldPasswordContainsLowercaseAlfanumericSymbols();
        user.atNewIntercomPage.clickOnRegenerateButton();
        String newPassword = user.atNewIntercomPage.rememberPassword();
        user.atNewIntercomPage.shouldPasswordContainsLowercaseAlfanumericSymbols();
        user.atNewIntercomPage.shouldPasswordBeDifferentFromGeneratedOne(oldPassword, newPassword);
    }
}
