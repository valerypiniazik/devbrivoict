package com.brivo.intercom.ui.tests.ict_access;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class IctAccessAssistantAdminUITest extends SimpleTestTemplate {
    @Test
    public void logInIctAppAsAssistantAdmin() {
        user.atAccountPage.enterUserName(userNameOfAssistantAdmin);
        user.atAccountPage.enterPassword(passwordOfAssistantAdmin);
        user.atAccountPage.clickOnLoginButton();
        user.atAccountPage.clickOnSubmitButton();
        user.atErrorPage.should403ErrorBeDisplayed();
    }
}
