package com.brivo.intercom.ui.tests.intercom_list;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class IntercomListUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
        createIntercom();
    }

    @Test
    public void seeIntercomList() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.shouldBeVisibleEditDeviceButton(getIntercomName());
        user.atIntercomListPage.shouldBeVisibleViewIntercomHistoryLink();
        user.atIntercomListPage.shouldBeVisibleEditDirectoryRuledButton(getIntercomName());
        user.atIntercomListPage.shouldBeVisibleDeleteButton(getIntercomName());
        user.atIntercomListPage.shouldBeIntercomListSortedByName();
    }
}
