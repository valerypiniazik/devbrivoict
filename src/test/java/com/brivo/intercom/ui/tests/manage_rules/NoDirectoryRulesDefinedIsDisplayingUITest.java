package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class NoDirectoryRulesDefinedIsDisplayingUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verifyNoDirectoryRulesDefinedIsDisplaying() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.verifyNoDirectoryRulesDefinedIsDisplaying();
    }
}
