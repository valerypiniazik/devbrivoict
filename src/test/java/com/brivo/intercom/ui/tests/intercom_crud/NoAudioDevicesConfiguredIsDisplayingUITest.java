package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class NoAudioDevicesConfiguredIsDisplayingUITest extends SimpleTestTemplate {
    @Test
    public void verifyNoAudioDevicesConfiguredIsDisplaying() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.verifyNoAudioDevicesConfiguredIsDisplaying();
    }
}
