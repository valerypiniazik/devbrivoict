package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class DeleteRuleUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
        createGroupConfig();
    }

    @Test
    public void deleteRuleByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickDeleteRule(getListingPattern());
        user.atDirectoryRulesPage.clickOkOnModalWindow();
        user.atDirectoryRulesPage.rulesShouldDisappear();
    }
}
