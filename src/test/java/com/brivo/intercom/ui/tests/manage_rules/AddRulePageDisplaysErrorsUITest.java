package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class AddRulePageDisplaysErrorsUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void addRuleByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickAddRule();
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atNewDirectoryRulePage.verifyErrorMessageContains("Unknown phone type");
        user.atNewDirectoryRulePage.enterInFirstPhoneTypeInput(TestData.FIRST_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atNewDirectoryRulePage.verifyErrorMessageContains("Select group");
        user.atNewDirectoryRulePage.enterGroup(TestData.GROUP_FOR_RULE);
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atNewDirectoryRulePage.verifyErrorMessageContains("Define the directory listing pattern");
        user.atNewDirectoryRulePage.enterInListingPattern(TestData.VALID_LISTING_PATTERN);
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atDirectoryRulesPage.shouldNotBeVisibleEmptyListOfRules();
    }
}
