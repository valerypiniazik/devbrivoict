package com.brivo.intercom.ui.tests.installer;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Test;

public class ReconnectButtonShouldNotBeDisplayedForInstallerUITest extends TestWithDataCreation {
    @Test
    public void reconnectButtonShouldNotBeDisplayedForInstaller() {
        user.atAccountPage.login(userNameOfInstaller, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.shouldNotBePresentReconnectIntercomButton();
    }
}
