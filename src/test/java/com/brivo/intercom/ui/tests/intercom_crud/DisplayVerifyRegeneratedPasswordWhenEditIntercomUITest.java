package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class DisplayVerifyRegeneratedPasswordWhenEditIntercomUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void displayAndVerifyRegeneratedPasswordWhenEditIntercom() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.shouldNotBePasswordDisplayed();
        user.atEditIntercomPage.clickRegeneratePasswordButton();
        user.atEditIntercomPage.verifyPasswordNotEmpty();
        user.atEditIntercomPage.verifyPasswordIsLowercase();
        user.atEditIntercomPage.verifyPasswordsLenght();
        user.atEditIntercomPage.verifyPasswordsUniqueness(4);
    }
}
