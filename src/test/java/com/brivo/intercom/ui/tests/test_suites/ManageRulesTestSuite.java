package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.manage_rules.AddRuleUITest;
import com.brivo.intercom.ui.tests.manage_rules.DeleteRuleUITest;
import com.brivo.intercom.ui.tests.manage_rules.EditRuleUITest;
import com.brivo.intercom.ui.tests.manage_rules.OverflowInputsWhenAddRuleUITest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        AddRuleUITest.class,
        DeleteRuleUITest.class,
        EditRuleUITest.class,
        OverflowInputsWhenAddRuleUITest.class
})

public class ManageRulesTestSuite {
}
