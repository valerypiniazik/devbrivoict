package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.generate_config_file.GenerateConfigFileUITest;
import com.brivo.intercom.ui.tests.generate_config_file.UserCanAbleToSeeGenerationErrorsUITest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        GenerateConfigFileUITest.class,
        UserCanAbleToSeeGenerationErrorsUITest.class
})

public class GenerateConfigFileTestSuite {

}
