package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Test;

public class AddIntercomUITest extends TestWithDataCreation {
    @Test
    public void addIntercomByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.enterIntercomName(getManualIntercomName());
        user.atNewIntercomPage.enterMacAddress();
        user.atNewIntercomPage.enterInSwitchCode0(TestData.INTERCOM_SWITCH_CODE0);
        user.atNewIntercomPage.enterInSwitchCode1(TestData.INTERCOM_SWITCH_CODE1);
        user.atNewIntercomPage.enterInSwitchCode2(TestData.INTERCOM_SWITCH_CODE2);
        user.atNewIntercomPage.enterInSwitchCode3(TestData.INTERCOM_SWITCH_CODE3);
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atIntercomListPage.shouldNotBeVisibleEmptyListOfIntercoms();
    }
}
