package com.brivo.intercom.ui.tests.csr;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Before;
import org.junit.Test;

public class VerifyCSRPermissionsUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verifyCSRPermissions(){
        user.atAccountPage.enterUserName(userNameOfCsr);
        user.atAccountPage.enterPassword(passwordOfMasterAdmin);
        user.atAccountPage.clickOnLoginButton();
        user.atAccountPage.clickOnSubmitButton();
        user.atAccountPage.enterAccountId(accountId);
        user.atAccountPage.clickOnSearchButton();
        user.atAccountPage.waitViewButton();
        user.atAccountPage.clickOnViewButton();
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.shouldNotBeVisibleSaveIntercomButton();
    }
}
