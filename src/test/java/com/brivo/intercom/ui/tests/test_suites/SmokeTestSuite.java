package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.generate_config_file.GenerateConfigFileUITest;
import com.brivo.intercom.ui.tests.intercom_crud.AddIntercomUITest;
import com.brivo.intercom.ui.tests.intercom_crud.DeleteIntercomUITest;
import com.brivo.intercom.ui.tests.intercom_crud.EditIntercomWithMacAddressUITest;
import com.brivo.intercom.ui.tests.intercom_crud.EditIntercomWithNameUITest;
import com.brivo.intercom.ui.tests.manage_rules.AddRuleUITest;
import com.brivo.intercom.ui.tests.manage_rules.DeleteRuleUITest;
import com.brivo.intercom.ui.tests.manage_rules.EditRuleUITest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        AddIntercomUITest.class,
        DeleteIntercomUITest.class,
        EditIntercomWithMacAddressUITest.class,
        EditIntercomWithNameUITest.class,
        AddRuleUITest.class,
        DeleteRuleUITest.class,
        EditRuleUITest.class,
        GenerateConfigFileUITest.class
})

public class SmokeTestSuite {
}
