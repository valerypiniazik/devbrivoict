package com.brivo.intercom.ui.tests;

import com.brivo.intercom.ui.steps.User;
import com.brivo.intercom.ui.data.Api.BrivoApi;
import net.serenitybdd.junit.spring.integration.SpringIntegrationSerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(SpringIntegrationSerenityRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class SimpleTestTemplate {
    @Value("${brivo.oauth.username}")
    protected String userNameOfMasterAdmin;

    @Value("${brivo.oauth.installer.username}")
    protected String userNameOfInstaller;

    @Value("${brivo.oauth.csr.username}")
    protected String userNameOfCsr;

    @Value("${brivo.oauth.password}")
    protected String passwordOfMasterAdmin;

    @Value("${brivo.assistant.admin.username}")
    protected String userNameOfAssistantAdmin;

    @Value("${brivo.assistant.admin.password}")
    protected String passwordOfAssistantAdmin;

    @Value("${ictApp.baseUrl}")
    protected String baseUrl;

    @Value("${brivo.account.master.admin.id}")
    protected String accountId;

    @Value("${security.oauth2.client.clientId}")
    protected String clientId;

    @Value("${security.oauth2.client.clientSecret}")
    protected String clientSecret;

    @Autowired
    protected BrivoApi brivoApi;

    @LocalServerPort
    protected int port;

    @Managed
    WebDriver driver;

    @Steps
    public User user;

    @Before
    public void setUp() {
        brivoApi.setPort(port);
        user.atAccountPage.enterAccountPage(baseUrl + port);
    }
}
