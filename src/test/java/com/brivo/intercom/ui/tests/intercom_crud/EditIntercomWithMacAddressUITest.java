package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.DataHelpers.IntercomDataHelper;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class EditIntercomWithMacAddressUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void editIntercomWithMacAddress() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        String macAddress = IntercomDataHelper.getRandomMacAddressDigits(12);
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.enterMacAddress(macAddress);
        user.atEditIntercomPage.saveDevice();
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.verifyMacHasChanged(macAddress);
    }
}
