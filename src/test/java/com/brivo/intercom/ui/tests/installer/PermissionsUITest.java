package com.brivo.intercom.ui.tests.installer;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class PermissionsUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercomForInstaller();
    }

    @Test
    public void installerPermissionsTest() {
        user.atAccountPage.login(userNameOfInstaller, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.shouldBeVisibleSaveNewIntercomButton();
        user.atNewIntercomPage.clickCancel();
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.shouldBeVisibleSaveIntercomButton();
        user.atEditIntercomPage.clickCancel();
        user.atIntercomListPage.deleteIntercom(getIntercomName());
        user.atIntercomListPage.shouldBeVisibleConfirmModal();
    }
}
