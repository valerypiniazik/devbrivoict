package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class EditIntercomWithNameUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void editIntercomWithName() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.enterIntercomName(TestData.NEW_NAME);
        user.atEditIntercomPage.saveDevice();
        user.atIntercomListPage.clickEditIntercom(TestData.NEW_NAME);
        user.atEditIntercomPage.verifyNameHasChanged(TestData.NEW_NAME);
    }
}
