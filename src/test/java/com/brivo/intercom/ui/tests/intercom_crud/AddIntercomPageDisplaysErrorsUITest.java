package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Test;

public class AddIntercomPageDisplaysErrorsUITest extends TestWithDataCreation {
    @Test
    public void editIntercomWithMacAddress() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atNewIntercomPage.verifyErrorMessageContains("Invalid device name");
        user.atNewIntercomPage.enterIntercomName(getManualIntercomName());
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atNewIntercomPage.verifyErrorMessageContains("Invalid MAC address");
        user.atNewIntercomPage.enterMacAddress();
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atNewIntercomPage.verifyErrorMessageContains("Device must have at least 1 switch code");
        user.atNewIntercomPage.enterInSwitchCode0("123456789");
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atIntercomListPage.shouldNotBeVisibleEmptyListOfIntercoms();
    }
}
