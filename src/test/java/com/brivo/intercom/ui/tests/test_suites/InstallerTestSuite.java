package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.installer.PermissionsUITest;
import com.brivo.intercom.ui.tests.installer.ReconnectButtonShouldNotBeDisplayedForInstallerUITest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        PermissionsUITest.class,
        ReconnectButtonShouldNotBeDisplayedForInstallerUITest.class
})

public class InstallerTestSuite {
}
