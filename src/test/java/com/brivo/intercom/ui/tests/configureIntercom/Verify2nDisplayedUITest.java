package com.brivo.intercom.ui.tests.configureIntercom;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class Verify2nDisplayedUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verify2nVersoDisplayedOnTheEditPage() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.shouldBeVisibleTypeOfModel();
    }
}
