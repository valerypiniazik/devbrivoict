package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class DeleteIntercomUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void deleteIntercom() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.deleteIntercom(getIntercomName());
        user.atIntercomListPage.clickOkOnModalWindow();
        user.atIntercomListPage.intercomShouldDisappear();
    }
}
