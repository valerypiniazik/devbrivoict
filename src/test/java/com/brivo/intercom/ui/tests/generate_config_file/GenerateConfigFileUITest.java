package com.brivo.intercom.ui.tests.generate_config_file;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class GenerateConfigFileUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void generateConfigFileByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditIntercom(getIntercomName());
        user.atEditIntercomPage.clickTestDevice();
        user.atIntercomHistoryPage.verifyConfigurationFileIsGenerated();
    }
}
