package com.brivo.intercom.ui.tests.end2end;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Test;

public class HappyPathUITest extends TestWithDataCreation {
    @Test
    public void happyPathTest() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.enterIntercomName(getManualIntercomName());
        user.atNewIntercomPage.enterMacAddress();
        user.atNewIntercomPage.enterInSwitchCode0(TestData.INTERCOM_SWITCH_CODE0);
        user.atNewIntercomPage.enterInSwitchCode1(TestData.INTERCOM_SWITCH_CODE1);
        user.atNewIntercomPage.enterInSwitchCode2(TestData.INTERCOM_SWITCH_CODE2);
        user.atNewIntercomPage.enterInSwitchCode3(TestData.INTERCOM_SWITCH_CODE3);
        user.atNewIntercomPage.clickOnSaveDeviceButton();
        user.atIntercomListPage.shouldNotBeVisibleEmptyListOfIntercoms();
        user.atIntercomListPage.clickEditIntercom(getManualIntercomName());
        user.atEditIntercomPage.clickTestDevice();
        user.atIntercomHistoryPage.shouldSeeAllLogs();
        user.atIntercomHistoryPage.clickBackButton();
        user.atEditIntercomPage.clickCancel();
        user.atIntercomListPage.clickEditDirectoryRules(getManualIntercomName());
        user.atDirectoryRulesPage.clickAddRule();
        user.atNewDirectoryRulePage.enterGroup(TestData.GROUP_FOR_RULE);
        user.atNewDirectoryRulePage.enterInListingPattern(TestData.INVALID_LISTING_PATTERN);
        user.atNewDirectoryRulePage.enterInFirstPhoneTypeInput(TestData.FIRST_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.enterInSecondPhoneTypeInput(TestData.SECOND_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.enterInThirdPhoneTypeInput(TestData.THIRD_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atDirectoryRulesPage.shouldNotBeVisibleEmptyListOfRules();
        user.atDirectoryRulesPage.clickGoToDeviceListButton();
        user.atIntercomListPage.clickEditIntercom(getManualIntercomName());
        user.atEditIntercomPage.clickTestDevice();
        user.atEditIntercomPage.shouldSeeErrorMessage();
        user.atIntercomHistoryPage.clickBackButton();
        user.atEditIntercomPage.clickCancel();
        user.atIntercomListPage.clickIntercomHistoryLink();
        user.atIntercomHistoryPage.firstLogShouldBeWithError();
        user.atIntercomHistoryPage.clickBackButton();
        user.atIntercomListPage.clickEditDirectoryRules(getManualIntercomName());
        user.atDirectoryRulesPage.clickEditRule(TestData.INVALID_LISTING_PATTERN);
        user.atEditDirectoryRulePage.enterInListingPattern(TestData.VALID_LISTING_PATTERN);
        user.atEditDirectoryRulePage.clickSaveEditedDirectoryRule();
        user.atDirectoryRulesPage.shouldNotBeVisibleEmptyListOfRules();
        user.atDirectoryRulesPage.clickGoToDeviceListButton();
        user.atIntercomListPage.clickEditIntercom(getManualIntercomName());
        user.atEditIntercomPage.clickTestDevice();
        user.atEditIntercomPage.shouldNotSeeErrorMessage();
        user.atIntercomHistoryPage.firstLogShouldBeOK();
    }
}
