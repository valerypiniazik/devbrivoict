package com.brivo.intercom.ui.tests.ict_access;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class IctAccessMasterAdminUITest extends SimpleTestTemplate {
    @Test
    public void logInIctAppAsMaterAdmin() {
        user.atAccountPage.enterUserName(userNameOfMasterAdmin);
        user.atAccountPage.enterPassword(passwordOfMasterAdmin);
        user.atAccountPage.clickOnLoginButton();
        user.atAccountPage.clickOnSubmitButton();
        //temporary solution
//        user.atAccountPage.enterAccountId(accountId);
//        user.atAccountPage.clickOnSearchButton();
//        user.atAccountPage.clickOnViewButton();
        user.atIntercomListPage.shouldBeVisibleNewIntercomButton();
    }
}
