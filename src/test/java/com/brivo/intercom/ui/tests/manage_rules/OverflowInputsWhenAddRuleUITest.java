package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class OverflowInputsWhenAddRuleUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verifyOverflowInputsWhenAddRule() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickAddRule();
        user.atNewDirectoryRulePage.checkListingPatternOverflow(256);
        user.atNewDirectoryRulePage.checkFirstPhoneTypeOverflow(256);
        user.atNewDirectoryRulePage.checkSecondPhoneTypeOverflow(256);
        user.atNewDirectoryRulePage.checkThirdPhoneTypeOverflow(256);
    }
}
