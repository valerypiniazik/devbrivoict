package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class AddRuleUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void addRuleByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickAddRule();
        user.atNewDirectoryRulePage.enterGroup(TestData.GROUP_FOR_RULE);
        user.atNewDirectoryRulePage.enterInListingPattern(TestData.VALID_LISTING_PATTERN);
        user.atNewDirectoryRulePage.enterInFirstPhoneTypeInput(TestData.FIRST_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.enterInSecondPhoneTypeInput(TestData.SECOND_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.enterInThirdPhoneTypeInput(TestData.THIRD_PHONE_TYPE_INPUT);
        user.atNewDirectoryRulePage.clickSaveNewDirectoryRule();
        user.atDirectoryRulesPage.shouldNotBeVisibleEmptyListOfRules();
    }
}
