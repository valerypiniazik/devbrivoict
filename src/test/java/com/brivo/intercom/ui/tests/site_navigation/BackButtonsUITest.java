package com.brivo.intercom.ui.tests.site_navigation;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class BackButtonsUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verifyUserCanGoBackToListIntercomsPageByBackButton() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickIntercomHistoryLink();
        user.atIntercomHistoryPage.clickBackButton();
        user.atIntercomListPage.shouldNotBeVisibleEmptyListOfIntercoms();
    }

    @Test
    public void verifyUserCanGoBackToListIntercomsPageByGoToDeviceListButton() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickGoToDeviceListButton();
        user.atDirectoryRulesPage.shouldNotBeVisibleEmptyListOfRules();
    }
}
