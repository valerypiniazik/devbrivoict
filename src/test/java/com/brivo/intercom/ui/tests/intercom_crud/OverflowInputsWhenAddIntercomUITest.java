package com.brivo.intercom.ui.tests.intercom_crud;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class OverflowInputsWhenAddIntercomUITest extends SimpleTestTemplate {
    @Test
    public void verifyOverflowInputsWhenAddIntercom() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickOnNewIntercomButton();
        user.atNewIntercomPage.checkIntercomNameOverflow(256);
        user.atNewIntercomPage.checkMacAddressOverflow(13);
        user.atNewIntercomPage.checkSwitchCodesOverflow(16);
        user.atNewIntercomPage.checkDescriptionsOverflow(256);
    }
}
