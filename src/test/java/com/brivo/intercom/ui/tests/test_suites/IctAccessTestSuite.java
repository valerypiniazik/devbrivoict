package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.ict_access.IctAccessAssistantAdminUITest;
import com.brivo.intercom.ui.tests.ict_access.IctAccessMasterAdminUITest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        IctAccessAssistantAdminUITest.class,
        IctAccessMasterAdminUITest.class
})

public class IctAccessTestSuite {
}
