package com.brivo.intercom.ui.tests.site_navigation;

import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.Test;

public class BrivoOnairLogoButtonHasCorrectBehaviorUITest extends SimpleTestTemplate {
    @Test
    public void verifyBrivoOnairLogoButtonHasCorrectBehavior(){
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atAnyPage.clickBrivoOnairLogo();
        user.atBrivoLoginPage.verifyCorrectUrl();
    }
}
