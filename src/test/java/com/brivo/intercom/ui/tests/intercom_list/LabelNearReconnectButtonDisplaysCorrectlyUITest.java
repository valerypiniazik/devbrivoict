package com.brivo.intercom.ui.tests.intercom_list;

import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

public class LabelNearReconnectButtonDisplaysCorrectlyUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
    }

    @Test
    public void verifyLabelNearReconnectButtonDisplaysCorrectly() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.shouldSeeNameAtInfoLabel(userNameOfMasterAdmin);
        user.atIntercomListPage.shouldSeeCorrectDateAtInfoLabel(LocalDateTime.now());
    }
}
