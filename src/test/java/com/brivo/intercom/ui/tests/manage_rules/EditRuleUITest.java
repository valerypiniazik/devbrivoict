package com.brivo.intercom.ui.tests.manage_rules;

import com.brivo.intercom.ui.data.TestData;
import com.brivo.intercom.ui.data.TestWithDataCreation;
import org.junit.Before;
import org.junit.Test;

public class EditRuleUITest extends TestWithDataCreation {
    @Before
    public void beforeTest() {
        createIntercom();
        createGroupConfig();
    }

    @Test
    public void editRuleByMasterAdmin() {
        user.atAccountPage.login(userNameOfMasterAdmin, passwordOfMasterAdmin, accountId);
        user.atIntercomListPage.clickEditDirectoryRules(getIntercomName());
        user.atDirectoryRulesPage.clickEditRule(getListingPattern());
        user.atEditDirectoryRulePage.enterInListingPattern(TestData.LISTING_PATTERN);
        user.atEditDirectoryRulePage.enterInFirstPhoneTypeInput(TestData.FIRST_PHONE_TYPE_INPUT);
        user.atEditDirectoryRulePage.enterInSecondPhoneTypeInput(TestData.SECOND_PHONE_TYPE_INPUT);
        user.atEditDirectoryRulePage.enterInThirdPhoneTypeInput(TestData.THIRD_PHONE_TYPE_INPUT);
        user.atEditDirectoryRulePage.clickSaveEditedDirectoryRule();
        user.atDirectoryRulesPage.clickEditRule(TestData.LISTING_PATTERN);
        user.atEditDirectoryRulePage.verifyIntercomGroupConfigChanged(TestData.LISTING_PATTERN, TestData.FIRST_PHONE_TYPE_INPUT, TestData.SECOND_PHONE_TYPE_INPUT, TestData.THIRD_PHONE_TYPE_INPUT);
    }
}
