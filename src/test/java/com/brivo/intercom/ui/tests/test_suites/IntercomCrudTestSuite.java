package com.brivo.intercom.ui.tests.test_suites;

import com.brivo.intercom.ui.tests.intercom_crud.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        AddIntercomUITest.class,
        DeleteIntercomUITest.class,
        DisplayPasswordWhenAddNewIntercomUITest.class,
        DisplayVerifyRegeneratedPasswordWhenEditIntercomUITest.class,
        OverflowInputsWhenAddIntercomUITest.class,
        NoAudioDevicesConfiguredIsDisplayingUITest.class
})

public class IntercomCrudTestSuite {
}
