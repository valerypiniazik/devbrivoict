package com.brivo.intercom.ui.data.Api;

import com.jayway.restassured.builder.RequestSpecBuilder;
import com.jayway.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Base64;
import java.util.Map;

@Component
public class ApiCore {
    @Value("${brivo.oauth.username}")
    protected String userNameOfMasterAdmin;

    @Value("${brivo.oauth.installer.username}")
    protected String userNameOfInstaller;

    @Value("${brivo.oauth.password}")
    protected String passwordOfMasterAdmin;

    @Value("${security.oauth2.client.clientId.installer}")
    protected String installerClientId;

    @Value("${security.oauth2.client.clientId}")
    protected String clientId;

    @Value("${security.oauth2.client.clientSecret.installer}")
    protected String installerClientSecret;

    @Value("${security.oauth2.client.clientSecret}")
    protected String clientSecret;

    @Value("${brivo.oauth.account_id}")
    protected String accountId;

    @Value("${brivo.oauth.installer.account_id}")
    protected String installerAccountId;

    @Value("${brivo.api.key}")
    protected String apiKey;

    @Value("${brivo.oauth.url}")
    protected String brivoOauthUrl;

    protected final String LOCAL_HOST="http://localhost:";

    protected String GET_INTERCOMS_PATH;

    protected String GET_GROUP_CONFIG_PATH;

    private int port;

    public void setPort(int port) {
        this.port = port;
        initRequests();
    }

    private void initRequests() {
        GET_INTERCOMS_PATH = LOCAL_HOST + port + "/api/intercoms/";
        GET_GROUP_CONFIG_PATH = LOCAL_HOST + port + "/api/group-configs/";
    }

    private RestTemplate restTemplate = new RestTemplate();

    protected RequestSpecification getRequestSpec(String userName, String password, String clientId,
                                                  String clientSecret, Long cookieValue) {
        String realToken = authorizeInBrivo(userName, password, clientId, clientSecret,
                "access_token");
        return new RequestSpecBuilder()
                .addHeader("Authorization", "Bearer " + realToken)
                .addHeader("Accept", "application/json")
                .addCookie("brivoAccountId", cookieValue)
                .setContentType("application/json")
                .build();
    }

    public String authorizeInBrivo(String userName, String passwordUser, String clientId, String clientSecret, String typeToken) {
        ResponseEntity<Map> result = getResponseAuthorization(userName, passwordUser, clientId, clientSecret);
        return result.getBody().get(typeToken).toString();
    }

    private ResponseEntity<Map> getResponseAuthorization(String userName, String passwordUser, String clientId, String clientSecret) {
        String credentials = clientId + ":" + clientSecret;
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Basic " + Base64.getEncoder().encodeToString(credentials.getBytes()));
        headers.add("api-key", apiKey);

        UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromUriString(brivoOauthUrl);
        uriBuilder.queryParam("grant_type", "password");
        uriBuilder.queryParam("username", userName);
        uriBuilder.queryParam("password", passwordUser);

        ResponseEntity<Map> result = restTemplate.exchange(uriBuilder.build().toUriString(), HttpMethod.POST, new HttpEntity<>(null, headers), Map.class);
        return result;
    }
}
