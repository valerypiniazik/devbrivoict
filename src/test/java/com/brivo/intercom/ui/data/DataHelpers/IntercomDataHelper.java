package com.brivo.intercom.ui.data.DataHelpers;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.domain.model.request.CreateIntercomRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class IntercomDataHelper {

    private static Random random = new Random();

    public static CreateIntercomRequest createIntercomRequest(String intercomPassword, String intercomName) {
        List<SwitchCode> switchCodes = getSwitchCodes(new Intercom());
        return new CreateIntercomRequest()
                .setTypeId(1L)
                .setPassword(intercomPassword)
                .setName(intercomName)
                .setMacAddress(getRandomMacAddress(6))
                .setSwitchCodes(switchCodes);
    }

    private static List<SwitchCode> getSwitchCodes(Intercom intercom) {
        List<SwitchCode> switchCodes = new ArrayList<>();

        SwitchCode switchCode = new SwitchCode();
        switchCode.setIndex(1);
        switchCode.setCode(12345L);
        switchCode.setName("switch_code1");
        switchCode.setIntercom(intercom);
        switchCodes.add(switchCode);
        return switchCodes;
    }

    public static String getRandomMacAddress(int length) {
        String s = "";
        for (int i = 0; i < length - 1; i++) {
            s += randomInt() + "-";
        }
        return s + randomInt();
    }

    public static String getRandomMacAddressDigits(int length) {
        String s = "";
        for (int i = 1; i <= length; i++) {
            s += random.nextInt(10);
        }
        return s;
    }

    private static String randomInt() {
        return String.valueOf(random.nextInt(10)) + String.valueOf(random.nextInt(10));
    }
}
