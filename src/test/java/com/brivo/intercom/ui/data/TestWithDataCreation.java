package com.brivo.intercom.ui.data;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.ui.tests.SimpleTestTemplate;
import org.junit.After;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class TestWithDataCreation extends SimpleTestTemplate {
    private String manualIntercomName;

    private Intercom currentIntercom;

    private IntercomGroupConfig currentIntercomGroupConfig;

    private List<Intercom> createdMasterAdminIntercoms = new ArrayList<>();

    private List<Intercom> createdInstallerIntercoms = new ArrayList<>();

    public String getIntercomName() {
        return currentIntercom.getName();
    }

    public String getManualIntercomName() {
        if (manualIntercomName == null) {
            manualIntercomName = UUID.randomUUID().toString();
            return manualIntercomName;
        } else {
            return manualIntercomName;
        }
    }

    public String getListingPattern() {
        return currentIntercomGroupConfig.getFormatString();
    }

    protected void createIntercom() {
        currentIntercom = brivoApi.createMasterAdminIntercom();
        createdMasterAdminIntercoms.add(currentIntercom);
    }

    protected void createIntercomForInstaller() {
        currentIntercom = brivoApi.createInstallerIntercom();
        createdInstallerIntercoms.add(currentIntercom);
    }

    protected void createGroupConfig() {
        currentIntercomGroupConfig = brivoApi.createMasterAdminGroupConfig(currentIntercom.getId());

    }

    protected void createGroupConfigForInstaller() {
        currentIntercomGroupConfig = brivoApi.createInstallerGroupConfig(currentIntercom.getId());
    }

    protected void clearData() {
        for (Intercom intercom : createdMasterAdminIntercoms) {
            brivoApi.deleteMasterAdminIntercom(intercom.getId());
        }
        for (Intercom intercom : createdInstallerIntercoms) {
            brivoApi.deleteInstallerIntercom(intercom.getId());
        }
        manuallyDeleteIntercom();
    }

    private void manuallyDeleteIntercom() {
        if (manualIntercomName != null) {
            brivoApi.deleteMasterAdminIntercomByName(manualIntercomName);
        }
    }

    @After
    public void afterTest() {
        clearData();
    }
}
