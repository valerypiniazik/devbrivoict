package com.brivo.intercom.ui.data.Api;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.ui.data.DataHelpers.GroupConfigDataHelper;
import com.brivo.intercom.ui.data.DataHelpers.IntercomDataHelper;
import com.jayway.restassured.path.json.JsonPath;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;

@Component
public class ApiController extends ApiCore {
    protected Intercom createIntercom(String userName, String password, String clientId, String clientSecret, String accountId) {
        CreateIntercomRequest intercom = IntercomDataHelper.createIntercomRequest(UUID.randomUUID().toString(), UUID.randomUUID().toString());
        JsonPath resJson = given()
                .spec(getRequestSpec(userName, password, clientId, clientSecret, Long.parseLong(accountId)))
                .body(intercom)
                .when().post(GET_INTERCOMS_PATH).jsonPath();
        Intercom resultIntercom = new Intercom();
        Integer intercomId = resJson.get("id");
        resultIntercom.setId(intercomId.longValue());
        resultIntercom.setMacAddress(resJson.get("macAddress"));
        resultIntercom.setName(resJson.get("name"));
        return resultIntercom;
    }

    protected void deleteIntercom(String userName, String password, String clientId, String clientSecret, String accountId, Long intercomId) {
        given()
                .spec(getRequestSpec(userName, password, clientId, clientSecret, Long.parseLong(accountId)))
                .when().delete(GET_INTERCOMS_PATH + intercomId);
    }

    protected void deleteIntercomByName(String userName, String password, String clientId, String clientSecret, String accountId, String intercomName) {
        JsonPath response = given()
                .spec(getRequestSpec(userName, password, clientId, clientSecret, Long.parseLong(accountId)))
                .when().get(GET_INTERCOMS_PATH).jsonPath();
        HashMap<String, Integer> foundIntercom = (response.param("name", intercomName).get("findAll { a -> a.name == name  }[0]"));
        deleteIntercom(userName, password, clientId, clientSecret, accountId, foundIntercom.get("id").longValue());
    }

    protected IntercomGroupConfig createGroupConfig(String userName, String password, String clientId, String clientSecret, String accountId, Long intercomId) {
        JsonPath resJson = given()
                .spec(getRequestSpec(userName, password, clientId, clientSecret, Long.parseLong(accountId)))
                .body(GroupConfigDataHelper.createIntercomGroupConfigDto(intercomId, UUID.randomUUID().toString()))
                .when().post(GET_GROUP_CONFIG_PATH).jsonPath();
        IntercomGroupConfig resultGroupConfig = new IntercomGroupConfig();
        Integer coupConfigId = resJson.get("id");
        Integer brivoGroupId = resJson.get("brivoGroupId");
        resultGroupConfig.setId(coupConfigId.longValue());
        resultGroupConfig.setBrivoGroupId(brivoGroupId.longValue());
        resultGroupConfig.setFormatString(resJson.get("formatString"));
        return resultGroupConfig;
    }
}
