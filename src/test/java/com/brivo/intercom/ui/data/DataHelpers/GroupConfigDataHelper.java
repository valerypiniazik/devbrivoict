package com.brivo.intercom.ui.data.DataHelpers;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;

import java.util.ArrayList;
import java.util.List;

public class GroupConfigDataHelper {
    public static IntercomGroupConfigDTO createIntercomGroupConfigDto(Long intercomId, String formatString) {
        IntercomGroupConfigDTO groupConfig = new IntercomGroupConfigDTO();
        List<IntercomGroupConfigDTO.IntercomGroupContactConfigDTO> listOfContacts = new ArrayList<>();
        listOfContacts.add(createContactType("firstContactType", 1));
        listOfContacts.add(createContactType("secondContactType", 2));
        listOfContacts.add(createContactType("thirdContactType", 3));
        groupConfig.setIntercomGroupContactConfigs(listOfContacts);
        groupConfig.setBrivoGroupId(751787L);
        groupConfig.setIntercomId(intercomId);
        groupConfig.setFormatString(formatString);
        return groupConfig;
    }

    private static IntercomGroupConfigDTO.IntercomGroupContactConfigDTO createContactType(String value, int id) {
        IntercomGroupConfigDTO.IntercomGroupContactConfigDTO contactType = new IntercomGroupConfigDTO.IntercomGroupContactConfigDTO(value);
        contactType.setIndex(id);
        return contactType;
    }
}


