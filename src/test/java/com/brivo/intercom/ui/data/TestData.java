package com.brivo.intercom.ui.data;

public class TestData {
    public static final String NEW_NAME = "editedName";
    public static final String INTERCOM_SWITCH_CODE0 = "123456";
    public static final String INTERCOM_SWITCH_CODE1 = "456789";
    public static final String INTERCOM_SWITCH_CODE2 = "789098";
    public static final String INTERCOM_SWITCH_CODE3 = "987890";
    public static final String LISTING_PATTERN = "testListingPattern";
    public static final String INVALID_LISTING_PATTERN = "${ololoTestTest}";
    public static final String VALID_LISTING_PATTERN = "TestPattern";
    public static final String FIRST_PHONE_TYPE_INPUT = "work";
    public static final String SECOND_PHONE_TYPE_INPUT = "home";
    public static final String THIRD_PHONE_TYPE_INPUT = "other";
    public static final String GROUP_FOR_RULE = "TestGroupICT";
    public static final int S_SIZE_OF_SCREEN = 576;
    public static final String TYPE_MAKE = "2N";
    public static final String TYPE_MODEL = "Verso";
    public static final String TYPE_PATTERN = "TestPattern";
}
