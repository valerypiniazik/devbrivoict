package com.brivo.intercom.ui.data.Api;

import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import org.springframework.stereotype.Component;

@Component
public class BrivoApi extends ApiController {
    public Intercom createMasterAdminIntercom() {
        return createIntercom(userNameOfMasterAdmin, passwordOfMasterAdmin, clientId, clientSecret, accountId);
    }

    public Intercom createInstallerIntercom() {
        return createIntercom(userNameOfInstaller, passwordOfMasterAdmin, installerClientId, installerClientSecret, installerAccountId);
    }

    public void deleteMasterAdminIntercom(Long intercomId) {
        deleteIntercom(userNameOfMasterAdmin, passwordOfMasterAdmin, clientId, clientSecret, accountId, intercomId);
    }

    public void deleteMasterAdminIntercomByName(String intercomName) {
        deleteIntercomByName(userNameOfMasterAdmin, passwordOfMasterAdmin, clientId, clientSecret, accountId, intercomName);
    }

    public void deleteInstallerIntercom(Long intercomId) {
        deleteIntercom(userNameOfInstaller, passwordOfMasterAdmin, installerClientId, installerClientSecret, installerAccountId, intercomId);
    }

    public IntercomGroupConfig createMasterAdminGroupConfig(Long intercomId) {
        return createGroupConfig(userNameOfMasterAdmin, passwordOfMasterAdmin, clientId, clientSecret, accountId, intercomId);
    }

    public IntercomGroupConfig createInstallerGroupConfig(Long intercomId) {
        return createGroupConfig(userNameOfInstaller, passwordOfMasterAdmin, installerClientId, installerClientSecret, installerAccountId, intercomId);
    }
}
