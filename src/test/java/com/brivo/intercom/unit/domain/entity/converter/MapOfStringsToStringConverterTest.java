package com.brivo.intercom.unit.domain.entity.converter;

import com.brivo.intercom.domain.entity.converter.MapOfStringsToStringConverter;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class MapOfStringsToStringConverterTest
{
    private final MapOfStringsToStringConverter testSubject = new MapOfStringsToStringConverter();

    @Test
    public void convertToDatabaseColumnWorksInExpectedWay() {
        //given
        Map<String, String> data = new HashMap<>();
        data.put("firstName", "just a first name");
        data.put("lastName", "just a last name");

        String expected = "{\"firstName\":\"just a first name\",\"lastName\":\"just a last name\"}";

        //when
        String result = testSubject.convertToDatabaseColumn(data);

        //then
        assertEquals(expected, result);
    }

    @Test
    public void convertToDatabaseColumnShouldTolerateNullValues() {
        //given
        String expected = "{}";

        //when
        String result = testSubject.convertToDatabaseColumn(null);

        //then
        assertEquals(expected, result);
    }

    @Test
    public void convertToDatabaseColumnShouldTolerateEmptyValues() {
        //given
        String expected = "{}";

        //when
        String result = testSubject.convertToDatabaseColumn(Collections.emptyMap());

        //then
        assertEquals(expected, result);
    }


    @Test
    public void convertToEntityAttributeWorksInExpectedWay() {
        //given
        String data = "{\"firstName\":\"just a first name\",\"lastName\":\"just a last name\"}";

        Map<String, String> expected = new HashMap<>();
        expected.put("firstName", "just a first name");
        expected.put("lastName", "just a last name");

        //when
        Map<String, String> result = testSubject.convertToEntityAttribute(data);

        //then
        assertEquals(expected, result);
    }

    @Test
    public void convertToEntityAttributeShouldTolerateNullValues() {
        //given
        Map<String, String> expected = Collections.emptyMap();

        //when
        Map<String, String> result = testSubject.convertToEntityAttribute(null);

        //then
        assertEquals(expected, result);
    }

    @Test
    public void convertToEntityAttributeShouldTolerateEmptyValues() {
        //given
        Map<String, String> expected = Collections.emptyMap();

        //when
        Map<String, String> result = testSubject.convertToEntityAttribute("");

        //then
        assertEquals(expected, result);
    }

}
