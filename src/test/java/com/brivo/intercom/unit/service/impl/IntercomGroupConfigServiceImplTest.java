package com.brivo.intercom.unit.service.impl;

import com.brivo.intercom.domain.dto.IntercomGroupConfigDTO;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.repository.IntercomGroupConfigRepository;
import com.brivo.intercom.service.impl.IntercomGroupConfigServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class IntercomGroupConfigServiceImplTest
{
    @Mock
    private IntercomGroupConfigRepository intercomGroupConfigRepository;

    @Mock
    private IntercomGroupConfigMapper mapper;

    @Captor
    private ArgumentCaptor<IntercomGroupConfig> intercomGroupConfigArgumentCaptor;

    private IntercomGroupConfigServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new IntercomGroupConfigServiceImpl(intercomGroupConfigRepository, mapper);
    }

    @Test
    public void verifyThatSaveSetIntercomBeforeSaveAndUseCorrectRepositoryAndMapperMethods() {
        //given
        Intercom intercom = new Intercom();
        IntercomGroupConfigDTO intercomGroupConfigDto = new IntercomGroupConfigDTO();

        given(mapper.intercomGroupConfigDtoToIntercomGroupConfig(any())).willReturn(new IntercomGroupConfig());

        //when
        testSubject.save(intercom, intercomGroupConfigDto);

        //then
        verify(mapper).intercomGroupConfigDtoToIntercomGroupConfig(any());
        verify(intercomGroupConfigRepository).save(intercomGroupConfigArgumentCaptor.capture());

        IntercomGroupConfig intercomGroupConfig = intercomGroupConfigArgumentCaptor.getValue();

        assertEquals(intercom, intercomGroupConfig.getIntercom());
    }


    @Test
    public void verifyThatFindAllUseCorrectRepositoryMethod() {
        //given

        //when
        testSubject.findAll(1L, 2L);

        //then
        verify(intercomGroupConfigRepository).findAllByIntercomIdAndIntercomBrivoAccountInfoId(1L, 2L);
    }

    @Test
    public void verifyThatFindByIdUseCorrectRepositoryMethod() {
        //given

        //when
        testSubject.findById(1L, 2L);

        //then
        verify(intercomGroupConfigRepository).findAllByIdAndIntercomBrivoAccountInfoId(1L, 2L);
    }

    @Test
    public void verifyThatUpdateUseCorrectRepositoryAndMapperMethods() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        IntercomGroupConfigDTO intercomGroupConfigDto = new IntercomGroupConfigDTO();

        //when
        testSubject.update(intercomGroupConfig, intercomGroupConfigDto);

        //then
        verify(mapper).updateIntercomGroupConfig(intercomGroupConfig, intercomGroupConfigDto);
        verify(intercomGroupConfigRepository).save(any(IntercomGroupConfig.class));
    }

    @Test
    public void verifyThatDeleteUseCorrectRepositoryMethod() {
        //given

        //when
        testSubject.delete(1L);

        //then
        verify(intercomGroupConfigRepository).delete(1L);
    }
}
