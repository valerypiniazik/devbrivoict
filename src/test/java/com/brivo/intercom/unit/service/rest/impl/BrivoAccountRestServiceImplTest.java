package com.brivo.intercom.unit.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.account.Account;
import com.brivo.intercom.service.rest.impl.BrivoAccountRestServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class BrivoAccountRestServiceImplTest
{
    @Mock
    private OAuth2RestTemplate oAuth2RestTemplate;

    private BrivoAccountRestServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new BrivoAccountRestServiceImpl(oAuth2RestTemplate);
    }

    @Test
    public void verifyThatGetAccountsReturnsOnlyFirstPageWhenUserIsNotCsr() {
        //given
        ApiPageResult<Account> firstResponse = new ApiPageResult<>(1000, Collections.nCopies(50, new Account()));

        setAuthentication(Collections.singletonList(new SimpleGrantedAuthority("ROLE_NOT_CSR")));

        given(oAuth2RestTemplate.exchange(eq("null/accounts?pageSize=50"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Account>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        ApiPageResult<Account> result = testSubject.getAccounts(null, null);

        //then
        assertEquals(50, result.getData().size());
        assertEquals(new Integer(1000), result.getCount());
    }

    @Test
    public void verifyThatGetAccountsReturnsOnlyFirstPageWhenUserIsCsrButSearchParametersAreNotSpecified() {
        //given
        ApiPageResult<Account> firstResponse = new ApiPageResult<>(1000, Collections.nCopies(50, new Account()));

        setAuthentication(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CSR")));

        given(oAuth2RestTemplate.exchange(eq("null/accounts?pageSize=50"),
            eq(HttpMethod.GET),
            any(),
            eq(new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        ApiPageResult<Account> result = testSubject.getAccounts(null, null);

        //then
        assertEquals(50, result.getData().size());
        assertEquals(new Integer(1000), result.getCount());
    }

    @Test
    public void verifyThatGetAccountsReturnsAllPagesAndCacheThemWhenUserIsCsrAnyParameterIsSpecified() {
        //given
        ApiPageResult<Account> firstResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(100, getAccount("1", null))));
        ApiPageResult<Account> secondResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(1, getAccount("1", null))));

        setAuthentication(Collections.singletonList(new SimpleGrantedAuthority("ROLE_CSR")));

        given(oAuth2RestTemplate.exchange(eq("null/accounts?offset=0&pageSize=100"),
            eq(HttpMethod.GET),
            any(),
            eq(new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }))).willReturn(ResponseEntity.ok(firstResponse));

        given(oAuth2RestTemplate.exchange(eq("null/accounts?offset=100&pageSize=100"),
            eq(HttpMethod.GET),
            any(),
            eq(new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }))).willReturn(ResponseEntity.ok(secondResponse));

        //when
        testSubject.getAccounts("1", null);
        testSubject.getAccounts("1", null);

        //then
        verify(oAuth2RestTemplate, times(2)).exchange(anyString(),
            eq(HttpMethod.GET),
            eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
            eq(new ParameterizedTypeReference<ApiPageResult<Account>>() {
            }));
    }

    @Test
    public void verifyThatGetAccountByIdWorksInExpectedWay() {
        //given
        Account account = new Account(1L);

        given(oAuth2RestTemplate.exchange(
            "null/accounts/" + account.getId(),
            HttpMethod.GET,
            new HttpEntity<>(testSubject.brivoHttpHeaders), Account.class)
        ).willReturn(ResponseEntity.ok(account));

        //when
        Account result = testSubject.getAccountById(account.getId());

        //then
        assertEquals(account, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void verifyThatGetAccountByIdReturnsNullWhenThereWasError() {
        //given
        Account account = new Account(1L);

        given(oAuth2RestTemplate.exchange(
            "null/accounts/" + account.getId(),
            HttpMethod.GET,
            new HttpEntity<>(testSubject.brivoHttpHeaders), Account.class)
        ).willThrow(Exception.class);

        //when
        Account result = testSubject.getAccountById(account.getId());

        //then
        assertNull(result);
    }

    private Account getAccount(String accountNumber, String name) {
        Account account = new Account();
        account.setAccountNumber(accountNumber);
        account.setName(name);

        return account;
    }

    private void setAuthentication(List<GrantedAuthority> grantedAuthority) {
        SecurityContextHolder.getContext().setAuthentication(new AbstractAuthenticationToken(grantedAuthority) {
            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return null;
            }
        });
    }
}
