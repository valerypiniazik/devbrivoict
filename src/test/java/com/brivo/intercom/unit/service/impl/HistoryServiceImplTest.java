package com.brivo.intercom.unit.service.impl;

import com.brivo.intercom.repository.ExecutionRepository;
import com.brivo.intercom.service.HistoryService;
import com.brivo.intercom.service.impl.HistoryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class HistoryServiceImplTest
{
    @Mock
    private ExecutionRepository executionRepository;

    private HistoryService testSubject;

    @Before
    public void setUp() {
        testSubject = new HistoryServiceImpl(executionRepository);
    }

    @Test
    public void verifyThatGetHistoryUseCorrectRepositoryMethod() {
        //given
        Pageable pageable = new PageRequest(10, 10);
        Long intercomId = 1L;
        Long brivoAccountId = 2L;

        //when
        testSubject.getHistory(pageable, 1L, 2L);

        //then
        verify(executionRepository).findAllByIntercomIdAndIntercomBrivoAccountInfoId(intercomId, brivoAccountId, pageable);
    }
}
