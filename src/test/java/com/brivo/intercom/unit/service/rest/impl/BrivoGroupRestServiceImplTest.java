package com.brivo.intercom.unit.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.group.Group;
import com.brivo.intercom.service.rest.BrivoAccountRestService;
import com.brivo.intercom.service.rest.BrivoAdministratorRestService;
import com.brivo.intercom.service.rest.impl.BrivoGroupRestServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class BrivoGroupRestServiceImplTest
{
    @Mock
    private OAuth2RestTemplate oAuth2RestTemplate;

    @Mock
    private BrivoAccountRestService brivoAccountRestService;

    @Mock
    private BrivoAdministratorRestService brivoAdministratorRestService;


    private BrivoGroupRestServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new BrivoGroupRestServiceImpl(oAuth2RestTemplate, brivoAccountRestService, brivoAdministratorRestService);

        SecurityContextHolder.getContext().setAuthentication(
            new AnonymousAuthenticationToken("username", "password", Collections.singleton(new SimpleGrantedAuthority("ROLE"))));
    }

    @Test
    public void verifyThatGetGroupsFetchDataInExpectedWayWhenThereIsOneEmptyPage() {
        //given
        ApiPageResult<Group> firstResponse = new ApiPageResult<>(Collections.emptyList());

        given(oAuth2RestTemplate.exchange(eq("null/groups?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<Group> result = testSubject.getGroups(1L);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }));

        assertEquals(0, result.size());
    }

    @Test
    public void verifyThatGetGroupsFetchDataInExpectedWayWhenThereIsOnePage() {
        //given
        ApiPageResult<Group> firstResponse = new ApiPageResult<>(Collections.nCopies(5, new Group()));

        given(oAuth2RestTemplate.exchange(eq("null/groups?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<Group> result = testSubject.getGroups(1L);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }));

        assertEquals(5, result.size());
    }

    @Test
    public void verifyThatGetGroupsFetchDataInExpectedWayWhenThereIsOneFullPage() {
        //given
        ApiPageResult<Group> firstResponse = new ApiPageResult<>(100, new ArrayList<>(Collections.nCopies(100, new Group())));

        given(oAuth2RestTemplate.exchange(eq("null/groups?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<Group> result = testSubject.getGroups(1L);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }));

        assertEquals(100, result.size());
    }

    @Test
    public void verifyThatGetGroupsFetchDataInExpectedWayWhenThereIsMoreThanOnePage() {
        //given
        ApiPageResult<Group> firstResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(100, new Group())));
        ApiPageResult<Group> secondResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(1, new Group())));

        given(oAuth2RestTemplate.exchange(eq("null/groups?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        given(oAuth2RestTemplate.exchange(eq("null/groups?offset=100&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }))).willReturn(ResponseEntity.ok(secondResponse));

        //when
        List<Group> result = testSubject.getGroups(1L);

        //then
        verify(oAuth2RestTemplate, times(2)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<Group>>() {
                }));

        assertEquals(101, result.size());
    }

    @Test
    public void verifyThatGetGroupByIdWorksInExpectedWay() {
        //given
        Group group = new Group();
        group.setId(1L);

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + group.getId()),
                eq(HttpMethod.GET),
                any(),
                eq(Group.class))).willReturn(ResponseEntity.ok(group));

        //when
        Group result = testSubject.getGroupById(group.getId(), 1L);

        //then
        verify(oAuth2RestTemplate).exchange("null/groups/" + group.getId(),
                HttpMethod.GET,
                new HttpEntity<>(testSubject.brivoHttpHeaders),
                Group.class);

        assertEquals(group, result);
    }

    @Test
    public void verifyThatGetGroupByIdReturnNullIfThereWasException() {
        //given
        Group group = new Group();
        group.setId(1L);

        //when
        Group result = testSubject.getGroupById(group.getId(), 1L);

        //then
        verify(oAuth2RestTemplate).exchange("null/groups/" + group.getId(),
                HttpMethod.GET,
                new HttpEntity<>(testSubject.brivoHttpHeaders),
                Group.class);

        assertNull(result);
    }
}
