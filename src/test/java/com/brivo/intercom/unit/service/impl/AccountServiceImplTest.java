package com.brivo.intercom.unit.service.impl;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.model.RefreshTokenStatus;
import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.repository.BrivoAccountInfoRepository;
import com.brivo.intercom.service.impl.AccountServiceImpl;
import com.brivo.intercom.service.security.SecurityContextService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Base64;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class AccountServiceImplTest
{
    private static final String PAYLOAD_PLACEHOLDER = "PAYLOAD_PLACEHOLDER";
    private static final String REFRESH_TOKEN_PATTERN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.PAYLOAD_PLACEHOLDER.mU9YACZymrwyK1R5otDwrgbrA74yFZt7dA0lXiTRp4g";
    private static final String EXP_PLACEHOLDER = "EXP_PLACEHOLDER";
    private static final String PAYLOAD_PATTERN = "{ \"exp\" : EXP_PLACEHOLDER }";

    @Mock
    private SecurityContextService securityContextService;

    @Mock
    private BrivoAccountInfoRepository brivoAccountInfoRepository;

    @Captor
    private ArgumentCaptor<BrivoAccountInfo> brivoAccountInfoArgumentCaptor;

    private AccountServiceImpl testSubject;

    private UserInfo testUserInfo;

    @Before
    public void setUp() {
        testSubject = new AccountServiceImpl(securityContextService, brivoAccountInfoRepository, new ObjectMapper());

        testUserInfo = new UserInfo()
                .setUsername("testUser")
                .setRefreshToken("testToken");

        given(securityContextService.getCurrentlyLoggedUserInfo()).willReturn(testUserInfo);
    }

    @Test
    public void reconnectCreateBrivoAccountInfoIfInfoWasNotFound() {
        //given
        Long brivoAccountId = 100500L;

        //when
        testSubject.reconnect(brivoAccountId);

        //then
        verify(brivoAccountInfoRepository).save(brivoAccountInfoArgumentCaptor.capture());

        BrivoAccountInfo savedInfo = brivoAccountInfoArgumentCaptor.getValue();

        assertEquals(brivoAccountId, savedInfo.getId());
        assertEquals(testUserInfo.getUsername(), savedInfo.getUsername());
        assertEquals(testUserInfo.getRefreshToken(), savedInfo.getRefreshToken());
    }

    @Test
    public void reconnectUpdateBrivoAccountInfoIfInfoWasFound() {
        //given
        Long brivoAccountId = 100500L;

        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo();
        brivoAccountInfo.setId(brivoAccountId);
        brivoAccountInfo.setUsername("justUser");
        brivoAccountInfo.setRefreshToken("justToken");

        given(brivoAccountInfoRepository.findOne(any())).willReturn(brivoAccountInfo);

        //when
        testSubject.reconnect(brivoAccountId);

        //then
        verify(brivoAccountInfoRepository).save(brivoAccountInfoArgumentCaptor.capture());

        BrivoAccountInfo savedInfo = brivoAccountInfoArgumentCaptor.getValue();

        assertEquals(brivoAccountInfo.getId(), savedInfo.getId());
        assertEquals(testUserInfo.getUsername(), savedInfo.getUsername());
        assertEquals(testUserInfo.getRefreshToken(), savedInfo.getRefreshToken());
    }

    @Test
    public void getRefreshTokenStatusReturnInvalidStatusWhenBrivoAccountInfoIsNotFoundInDB() {
        //given
        Long brivoAccountInfoId = 1L;

        given(brivoAccountInfoRepository.findOne(brivoAccountInfoId)).willReturn(null);

        //when
        RefreshTokenStatus result = testSubject.getRefreshTokenStatus(brivoAccountInfoId);

        //then
        assertFalse(result.getValid());
    }

    @Test
    public void getRefreshTokenStatusReturnInvalidStatusWhenRefreshTokenInBrivoAccountInfoIsExpired() {
        //given
        Long expiredTimeSeconds = System.currentTimeMillis() / 1000 - 10;

        String encodedPayload = encodePayload(expiredTimeSeconds);

        String expiredRefreshToken = REFRESH_TOKEN_PATTERN.replace(PAYLOAD_PLACEHOLDER, encodedPayload);

        Long brivoAccountInfoId = 1L;

        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo(brivoAccountInfoId);
        brivoAccountInfo.setRefreshToken(expiredRefreshToken);

        given(brivoAccountInfoRepository.findOne(brivoAccountInfoId)).willReturn(brivoAccountInfo);

        //when
        RefreshTokenStatus result = testSubject.getRefreshTokenStatus(brivoAccountInfoId);

        //then
        assertFalse(result.getValid());
    }

    @Test
    public void getRefreshTokenStatusReturnValidStatusWhenRefreshTokenInBrivoAccountInfoIsNotExpired() {
        //given
        Long timeSeconds = System.currentTimeMillis() / 1000 + 10;

        String encodedPayload = encodePayload(timeSeconds);

        String refreshToken = REFRESH_TOKEN_PATTERN.replace(PAYLOAD_PLACEHOLDER, encodedPayload);

        Long brivoAccountInfoId = 1L;

        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo(brivoAccountInfoId);
        brivoAccountInfo.setRefreshToken(refreshToken);


        given(brivoAccountInfoRepository.findOne(brivoAccountInfoId)).willReturn(brivoAccountInfo);

        //when
        RefreshTokenStatus result = testSubject.getRefreshTokenStatus(brivoAccountInfoId);

        //then
        assertTrue(result.getValid());
    }

    private String encodePayload(Long exp) {
        return Base64.getEncoder().encodeToString(PAYLOAD_PATTERN.replace(EXP_PLACEHOLDER, exp.toString()).getBytes());
    }
}
