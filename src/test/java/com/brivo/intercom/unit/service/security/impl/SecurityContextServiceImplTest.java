package com.brivo.intercom.unit.service.security.impl;

import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.service.security.impl.SecurityContextServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.DefaultOAuth2RefreshToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.Principal;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class SecurityContextServiceImplTest {
    @Mock
    private OAuth2RestTemplate oAuth2RestTemplate;

    private SecurityContextServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new SecurityContextServiceImpl(oAuth2RestTemplate);
    }

    @Test
    public void verifyThatGetCurrentlyLoggedUserWorksInExpectedWay() {
        //given
        List<GrantedAuthority> roles = Collections.singletonList(new SimpleGrantedAuthority("SIMPLE_ROLE"));
        String username = "simple user";

        setAuthentication(roles, () -> username);

        String accessToken = "access token";
        String refreshToken = "refresh token";


        DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken);
        token.setRefreshToken(new DefaultOAuth2RefreshToken(refreshToken));
        token.setExpiration(new Date());

        given(oAuth2RestTemplate.getAccessToken()).willReturn(token);

        //when
        UserInfo result = testSubject.getCurrentlyLoggedUserInfo();

        //then
        assertEquals(roles.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()), result.getRoles());
        assertEquals(username, result.getUsername());
        assertEquals(refreshToken, result.getRefreshToken());
    }

    private void setAuthentication(List<GrantedAuthority> roles, Principal principal) {
        SecurityContextHolder.getContext().setAuthentication(new OAuth2Authentication(null, new AbstractAuthenticationToken(roles) {
            @Override
            public Object getCredentials() {
                return null;
            }

            @Override
            public Object getPrincipal() {
                return principal;
            }
        }));
    }
}
