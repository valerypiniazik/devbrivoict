package com.brivo.intercom.unit.service.security.impl;

import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.service.security.impl.IntercomUserDetailsServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class IntercomUserDetailsServiceImplTest
{
    @Mock
    private IntercomRepository intercomRepository;

    @Mock
    private OAuth2RestTemplate oAuth2RestTemplate;

    private IntercomUserDetailsServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new IntercomUserDetailsServiceImpl(intercomRepository, oAuth2RestTemplate);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void verifyThatLoadUserByUsernameWillThrowExceptionWhenIntercomWasNotFoundInDB() {
        //given
        String macAddress = "11-11-11-11-11-11";

        given(intercomRepository.findOneByMacAddress(macAddress)).willReturn(Optional.empty());

        //when
        testSubject.loadUserByUsername(macAddress);

        //then
    }

    @Test
    public void verifyThatLoadUserByUsernameWorksInExpectedWay() {
        //given
        String macAddress = "11-11-11-11-11-11";
        String hashedPassword = "hashedPassword";
        String refreshToken = "valid refresh token";

        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo(refreshToken);

        Intercom intercom = new Intercom(macAddress, hashedPassword, "intercom", new Type());
        intercom.setBrivoAccountInfo(brivoAccountInfo);

        OAuth2ClientContext context = new DefaultOAuth2ClientContext();

        given(intercomRepository.findOneByMacAddress(macAddress)).willReturn(Optional.of(intercom));
        given(oAuth2RestTemplate.getOAuth2ClientContext()).willReturn(context);

        //when
        UserDetails result = testSubject.loadUserByUsername(macAddress);

        //then
        assertEquals(Collections.singleton(new SimpleGrantedAuthority("INTERCOM")), result.getAuthorities());
        assertEquals(macAddress, result.getUsername());
        assertEquals(hashedPassword, result.getPassword());

        assertEquals("invalid access token", context.getAccessToken().getValue());
        assertEquals(intercom.getBrivoAccountInfo().getRefreshToken(), context.getAccessToken().getRefreshToken().getValue());
        assertEquals(intercom.getBrivoAccountInfo().getRefreshToken(), context.getAccessToken().getRefreshToken().getValue());
    }
}
