package com.brivo.intercom.unit.service.impl;

import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.BrivoAccountInfo;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.model.UserInfo;
import com.brivo.intercom.repository.BrivoAccountInfoRepository;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.repository.rest.TypeRepository;
import com.brivo.intercom.service.impl.IntercomServiceImpl;
import com.brivo.intercom.service.security.SecurityContextService;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class IntercomServiceImplTest
{
    private static final String TEST_USERNAME = "testUsername";
    private static final String TEST_REFRESH_TOKEN = "testRefreshToken";

    @Mock
    private IntercomRepository intercomRepository;

    @Mock
    private TypeRepository typeRepository;

    @Mock
    private BrivoAccountInfoRepository brivoAccountInfoRepository;

    @Mock
    private SecurityContextService securityContextService;

    @Captor
    private ArgumentCaptor<Intercom> intercomArgumentCaptor;

    @Captor
    private ArgumentCaptor<BrivoAccountInfo> brivoAccountInfoArgumentCaptor;

    private IntercomServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new IntercomServiceImpl(intercomRepository, typeRepository, brivoAccountInfoRepository, securityContextService);

        given(securityContextService.getCurrentlyLoggedUserInfo())
                .willReturn(new UserInfo().setUsername(TEST_USERNAME).setRefreshToken(TEST_REFRESH_TOKEN));
    }

    @Test
    public void verifyThatFindAllUseCorrectRepositoryMethod() {
        //given

        //when
        testSubject.findAll(1L);

        //then
        verify(intercomRepository).findAllByBrivoAccountInfoIdOrderByName(anyLong());
    }

    @Test
    public void verifyThatFindByIdUseCorrectRepositoryMethod() {
        //given

        //when
        testSubject.findById(1L, 1L);

        //then
        verify(intercomRepository).findByIdAndBrivoAccountInfoId(anyLong(), anyLong());
    }

    @Test
    public void verifyThatSaveCreateNewIntercomWhenIntercomWithSuchMacAddressIsNotFound() {
        //given
        CreateIntercomRequest request = new CreateIntercomRequest();
        request.setPassword("newPassword");
        request.setMacAddress("te-st-ma-ca-dd-rr");
        request.setSwitchCodes(Collections.emptyList());

        //when
        testSubject.save(request, 1L);

        //then
        verify(intercomRepository).save(intercomArgumentCaptor.capture());

        Intercom savedIntercom = intercomArgumentCaptor.getValue();

        assertNull(savedIntercom.getId());
    }

    @Test
    public void verifyThatSaveCreateBrivoAccountInfoWhenSuchBrivoAccountInfoIsNotFound() {
        //given
        Long brivoAccountId = 1L;

        CreateIntercomRequest request = new CreateIntercomRequest();
        request.setPassword("newPassword");
        request.setSwitchCodes(Collections.emptyList());

        given(brivoAccountInfoRepository.findOne(any())).willReturn(null);

        //when
        testSubject.save(request, brivoAccountId);

        //then
        verify(brivoAccountInfoRepository).save(brivoAccountInfoArgumentCaptor.capture());

        BrivoAccountInfo savedBrivoAccountInfo = brivoAccountInfoArgumentCaptor.getValue();

        assertEquals(brivoAccountId, savedBrivoAccountInfo.getId());
        assertEquals(TEST_USERNAME, savedBrivoAccountInfo.getUsername());
        assertEquals(TEST_REFRESH_TOKEN, savedBrivoAccountInfo.getRefreshToken());
    }

    @Test
    public void verifyThatUpdateUpdateIntercomAndBrivoAccountInfo() {
        //given
        BrivoAccountInfo brivoAccountInfo = new BrivoAccountInfo();
        brivoAccountInfo.setId(1L);
        brivoAccountInfo.setUsername("oldUsername");
        brivoAccountInfo.setRefreshToken("oldRefreshToken");

        Intercom intercom = new Intercom();
        intercom.setId(1L);
        intercom.setName("oldName");
        intercom.setBrivoAccountInfo(brivoAccountInfo);

        UpdateIntercomRequest request = new UpdateIntercomRequest();
        request.setName("newName");
        request.setPassword("newPassword");
        request.setSwitchCodes(Collections.emptyList());

        //when
        testSubject.update(request, intercom);

        //then
        verify(intercomRepository).save(intercomArgumentCaptor.capture());

        Intercom savedIntercom = intercomArgumentCaptor.getValue();

        assertEquals(savedIntercom.getBrivoAccountInfo().getId(), savedIntercom.getBrivoAccountInfo().getId());
        assertEquals(TEST_USERNAME, savedIntercom.getBrivoAccountInfo().getUsername());
        assertEquals(TEST_REFRESH_TOKEN, savedIntercom.getBrivoAccountInfo().getRefreshToken());

        assertEquals(request.getName(), savedIntercom.getName());
    }

    @Test
    public void verifyThatDeleteWillInvokeDeleteOnRepository() {
        //given
        Intercom intercom = new Intercom();

        //when
        testSubject.delete(intercom);

        //then
        verify(intercomRepository).delete(intercom);
    }
}
