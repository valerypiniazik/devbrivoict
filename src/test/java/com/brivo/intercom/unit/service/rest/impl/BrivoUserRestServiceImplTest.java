package com.brivo.intercom.unit.service.rest.impl;

import com.brivo.api.rest.model.ApiPageResult;
import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.service.rest.impl.BrivoUserRestServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class BrivoUserRestServiceImplTest
{
    @Mock
    private OAuth2RestTemplate oAuth2RestTemplate;

    private BrivoUserRestServiceImpl testSubject;

    @Before
    public void setUp() {
        testSubject = new BrivoUserRestServiceImpl(oAuth2RestTemplate);
    }

    @Test
    public void verifyThatGetGroupUsersFetchDataInExpectedWayWhenThereIsOneEmptyPage() {
        //given
        ApiPageResult<User> firstResponse = new ApiPageResult<>(Collections.emptyList());

        Long brivoGroupId = 1L;

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + brivoGroupId + "/users?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<User> result = testSubject.getGroupUsers(brivoGroupId);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }));

        assertEquals(0, result.size());
    }

    @Test
    public void verifyThatGetGroupUsersFetchDataInExpectedWayWhenThereIsOnePage() {
        //given
        ApiPageResult<User> firstResponse = new ApiPageResult<>(Collections.nCopies(5, new User()));

        Long brivoGroupId = 1L;

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + brivoGroupId + "/users?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<User> result = testSubject.getGroupUsers(brivoGroupId);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }));

        assertEquals(5, result.size());
    }

    @Test
    public void verifyThatGetGroupUsersFetchDataInExpectedWayWhenThereIsOneFullPage() {
        //given
        ApiPageResult<User> firstResponse = new ApiPageResult<>(100, new ArrayList<>(Collections.nCopies(100, new User())));

        Long brivoGroupId = 1L;

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + brivoGroupId + "/users?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        //when
        List<User> result = testSubject.getGroupUsers(brivoGroupId);

        //then
        verify(oAuth2RestTemplate, times(1)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }));

        assertEquals(100, result.size());
    }

    @Test
    public void verifyThatGetGroupUsersFetchDataInExpectedWayWhenThereIsMoreThanOnePage() {
        //given
        ApiPageResult<User> firstResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(100, new User())));
        ApiPageResult<User> secondResponse = new ApiPageResult<>(101, new ArrayList<>(Collections.nCopies(1, new User())));

        Long brivoGroupId = 1L;

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + brivoGroupId + "/users?offset=0&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }))).willReturn(ResponseEntity.ok(firstResponse));

        given(oAuth2RestTemplate.exchange(eq("null/groups/" + brivoGroupId + "/users?offset=100&pageSize=100"),
                eq(HttpMethod.GET),
                any(),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }))).willReturn(ResponseEntity.ok(secondResponse));

        //when
        List<User> result = testSubject.getGroupUsers(brivoGroupId);

        //then
        verify(oAuth2RestTemplate, times(2)).exchange(anyString(),
                eq(HttpMethod.GET),
                eq(new HttpEntity<>(testSubject.brivoHttpHeaders)),
                eq(new ParameterizedTypeReference<ApiPageResult<User>>() {
                }));

        assertEquals(101, result.size());
    }

    @Test
    public void verifyThatGetUserByIdWorksInExpectedWay() {
        //given
        User user = new User(1L);

        given(oAuth2RestTemplate.exchange(eq("null/users/" + user.getId()),
                eq(HttpMethod.GET),
                any(),
                eq(User.class))).willReturn(ResponseEntity.ok(user));

        //when
        User result = testSubject.getUserById(user.getId());

        //then
        verify(oAuth2RestTemplate).exchange("null/users/" + user.getId(),
                HttpMethod.GET,
                new HttpEntity<>(testSubject.brivoHttpHeaders),
                User.class);

        assertEquals(user, result);
    }
}
