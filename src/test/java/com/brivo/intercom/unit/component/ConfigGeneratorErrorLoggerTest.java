package com.brivo.intercom.unit.component;

import com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger;
import com.brivo.intercom.domain.entity.ErrorType;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.RequestLog;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.mapper.RequestLogMapper;
import com.brivo.intercom.domain.model.generator.ErrorSources;
import com.brivo.intercom.exception.generator.ConfigFileFatalFailureException;
import com.brivo.intercom.repository.ExecutionRepository;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.APPLICATION_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.INVALID_VARIABLE_NAMES_IN_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.RETRIEVE_USER_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.TOO_MANY_WARNINGS;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ConfigGeneratorErrorLoggerTest
{
    @Mock
    private RequestLogMapper requestLogMapper;

    @Mock
    private ExecutionRepository executionRepository;

    @Mock
    private BrivoGroupRestService brivoGroupRestService;

    @Mock
    private IntercomGroupConfigMapper intercomGroupConfigMapper;

    @Captor
    private ArgumentCaptor<Execution> executionArgumentCaptor;

    private Map<String, ErrorType> errorTypes;

    private Execution execution;

    private ConfigGeneratorErrorLogger testSubject;

    @Before
    public void setUp() {
        testSubject = new ConfigGeneratorErrorLogger(requestLogMapper, executionRepository, brivoGroupRestService, intercomGroupConfigMapper);
        initErrorTypes();
        execution = new Execution();
        execution.setIntercom(new Intercom().setType(new Type(StringUtils.EMPTY, Collections.emptyMap())));
    }

    @Test(expected = ConfigFileFatalFailureException.class)
    public void trackErrorThrowExceptionWhenErrorIsFatal() {
        testSubject.trackError(errorTypes, APPLICATION_ERROR, new ErrorSources(), execution);
    }

    @Test
    public void trackErrorAddWarningWhenErrorIsNotFatal() {
        //given

        //when
        testSubject.trackError(errorTypes, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, new ErrorSources(), execution);

        //then
        assertEquals(1L, execution.getRequestLogs().size());
    }

    @Test(expected = ConfigFileFatalFailureException.class)
    public void trackErrorThrowExceptionWhenWarningLimitExceedAndLimitErrorIsFatal() {
        //given
        ErrorSources errorDescriptionSources = new ErrorSources();

        //when
        IntStream.range(0, 25).forEach(i -> testSubject.trackError(errorTypes, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, errorDescriptionSources, execution));

        //then
    }

    @Test
    public void trackErrorAddWarningWhenWarningLimitExceedAndLimitErrorIsNotFatal() {
        //given
        ErrorSources errorDescriptionSources = new ErrorSources();
        errorTypes.get(TOO_MANY_WARNINGS).setFailureFatal(false);

        //when
        IntStream.range(0, 25).forEach(i -> testSubject.trackError(errorTypes, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, errorDescriptionSources, execution));

        //then
        assertEquals(26L, execution.getRequestLogs().size());
        assertEquals(TOO_MANY_WARNINGS, execution.getRequestLogs().get(25).getErrorType().getName());
    }

    @Test
    public void verifyThatLogSuccessAddLogAndSaveExecutionWithWarningStatusWhenLogsSizeIsMoreThanOne() {
        //given
        execution.setRequestLogs(new ArrayList<>(Arrays.asList(new RequestLog(), new RequestLog())));

        //when
        testSubject.logSuccess(execution);

        //then
        verify(executionRepository).save(executionArgumentCaptor.capture());

        Execution savedExecution = executionArgumentCaptor.getValue();

        assertEquals("Warning", savedExecution.getStatus());
        assertEquals(3, savedExecution.getRequestLogs().size());
    }

    @Test
    public void verifyThatLogSuccessAddLogAndSaveExecutionWithWarningStatusWhenLogsSizeIsLessThanOne() {
        //given
        Execution execution = new Execution();
        execution.setRequestLogs(new ArrayList<>());

        //when
        testSubject.logSuccess(execution);

        //then
        verify(executionRepository).save(executionArgumentCaptor.capture());

        Execution savedExecution = executionArgumentCaptor.getValue();

        assertEquals("OK", savedExecution.getStatus());
        assertEquals(1, savedExecution.getRequestLogs().size());
    }

    private void initErrorTypes() {
        errorTypes = Stream.of(RETRIEVE_USER_ERROR, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING)
            .collect(Collectors.toMap(key -> key, this::notFatalError));

        errorTypes.putAll(Stream.of(TOO_MANY_WARNINGS, APPLICATION_ERROR)
            .collect(Collectors.toMap(key -> key, this::fatalError)));
    }

    private ErrorType fatalError(String name) {
        ErrorType fatalError = new ErrorType(name);
        fatalError.setFailureFatal(true);
        return fatalError;
    }

    private ErrorType notFatalError(String name) {
        ErrorType fatalError = new ErrorType(name);
        fatalError.setFailureFatal(false);
        return fatalError;
    }
}
