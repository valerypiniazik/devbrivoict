package com.brivo.intercom.unit.component;

import com.brivo.api.rest.model.identity.CustomFieldValue;
import com.brivo.api.rest.model.identity.Telephone;
import com.brivo.api.rest.model.identity.User;
import com.brivo.intercom.component.generator.ConfigGenerator;
import com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger;
import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.entity.ErrorType;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.IntercomGroupConfig;
import com.brivo.intercom.domain.entity.IntercomGroupConfig.IntercomGroupContactConfig;
import com.brivo.intercom.domain.entity.SwitchCode;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.domain.model.generator.ConfigFile;
import com.brivo.intercom.domain.model.generator.ErrorSources;
import com.brivo.intercom.domain.model.generator.Switch;
import com.brivo.intercom.domain.model.generator.UserModel;
import com.brivo.intercom.service.rest.BrivoUserRestService;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.APPLICATION_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.DISPLAY_NAME_EXCEEDED_LENGTH_LIMIT;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.INVALID_VARIABLE_NAMES_IN_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.MISSING_VALUE_FOR_FORMAT_STRING;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.RETRIEVE_GROUP_USERS_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.RETRIEVE_USER_ERROR;
import static com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger.USER_LIMIT_EXCEEDED;
import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ConfigGeneratorTest {
    private static final String FILE_NAME = "fileName";
    private static final String PHONE_NUMBER_WITH_NON_NUMERIC_SYMBOLS = "!@#$%^&*()-+_=qwerty + (123)-45- 67 - 89";
    private static final String PHONE_NUMBER_ONLY_NUMERIC_SYMBOLS = "123456789";

    @Mock
    private TemplateEngine templateEngine;

    @Mock
    private BrivoUserRestService brivoRestService;

    @Mock
    private ConfigGeneratorErrorLogger configGeneratorErrorLogger;

    private ConfigGenerator testSubject;
    private Map<String, ErrorType> errorTypes = Stream.of(RETRIEVE_USER_ERROR, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, APPLICATION_ERROR)
        .collect(Collectors.toMap(key -> key, key -> new ErrorType(key, true)));

    @Before
    public void setUp() {
        testSubject = new ConfigGenerator(templateEngine, brivoRestService, configGeneratorErrorLogger);
    }

    @Test
    public void verifyThatGenerateConfigWorksInExpectedWay() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("$firstName $c_customField");
        intercomGroupConfig.setIntercomGroupContactConfigs(Arrays.asList(
                new IntercomGroupContactConfig("work", 1),
                new IntercomGroupContactConfig("home", 2)));

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());
        intercom.setSwitchCodes(Collections.nCopies(2, new SwitchCode(123456789012345L, 0)));

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        CustomFieldValue customField = new CustomFieldValue();
        customField.setFieldName("customField");
        customField.setValue("customValue");

        User user = new User(1L);
        user.setPhoneNumbers(Arrays.asList(new Telephone("3", "work"), new Telephone("2", "work"), new Telephone("1", "home")));
        user.setFirstName("Baboo");
        user.setLastName("last_name");
        user.setMiddleName("middle_name");
        user.setExternalId("external_id");
        user.setCustomFields(Collections.singletonList(customField));

        User user2 = new User(2L);
        user2.setFirstName("Alex");
        user2.setPhoneNumbers(Collections.singletonList(new Telephone("5", "work")));

        given(brivoRestService.getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Arrays.asList(user, user2));

        given(brivoRestService.getUserById(user.getId())).willReturn(user);

        given(brivoRestService.getUserById(user2.getId())).willReturn(user2);

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("baboo short name").willReturn("alex short name");

        //when
        ConfigFile result = testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        List<Switch> switches = result.getSwitches();
        List<UserModel> users = result.getUsers();

        assertEquals(FILE_NAME, result.getFileName());
        assertEquals(2, switches.size());
        assertEquals(2, users.size());
        assertEquals("alex short name", users.get(0).getShortName());
        assertEquals(1, users.get(0).getNumbers().size());
        assertEquals("baboo short name", users.get(1).getShortName());
        assertEquals(2, users.get(1).getNumbers().size());
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenThereWasErrorOnGroupUsersFetching() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);
        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willThrow(new RuntimeException());

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, RETRIEVE_GROUP_USERS_ERROR, errorDescriptionSources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenUsersLimitExceeded() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        given(brivoRestService.getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(LongStream.range(0, 2000).mapToObj(User::new).collect(Collectors.toList()));

        given(brivoRestService
            .getUserById(any()))
            .willThrow(new RuntimeException());

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("");

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, USER_LIMIT_EXCEEDED, errorDescriptionSources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenThereWasErrorOnUserFetching() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        User user = new User(1L);

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(user));

        given(brivoRestService
            .getUserById(user.getId()))
            .willThrow(new RuntimeException());

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("");

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, RETRIEVE_USER_ERROR, errorDescriptionSources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenThereWereNotAnyPhoneNumbersFound() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        User user = new User(1L);

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);
        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(user));

        given(brivoRestService
            .getUserById(user.getId()))
            .willReturn(user);

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("");

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, NO_PHONE_NUMBERS_OF_SPECIFIED_TYPE_FOUND, errorDescriptionSources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenUserDoesNotHaveValuesSpecifiedInTemplate() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("$firstName");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());
        User user = new User(1L);

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(user));

        given(brivoRestService
            .getUserById(user.getId()))
            .willReturn(user);

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("");

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, MISSING_VALUE_FOR_FORMAT_STRING, errorDescriptionSources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenUserDoesNotHaveCustomFieldSpecifiedInTemplate() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("$c_notExistingField");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        User user = new User(1L);

        ErrorSources sources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(user));

        given(brivoRestService
            .getUserById(user.getId()))
            .willReturn(user);

        given(templateEngine.compileTemplate(any(), any(), any())).willThrow(new RuntimeException());

        //when
        testSubject.generateConfig(intercom, errorTypes, sources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, INVALID_VARIABLE_NAMES_IN_FORMAT_STRING, sources, execution);
    }

    @Test
    public void verifyThatGenerateConfigTrackErrorWhenShortNameExceedSizeLimit() {
        //given
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("");

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        User user = new User(1L);

        ErrorSources errorDescriptionSources = new ErrorSources();
        Execution execution = new Execution();
        execution.setIntercom(intercom);

        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(user));

        given(brivoRestService
            .getUserById(user.getId()))
            .willReturn(user);

        given(templateEngine.compileTemplate(any(), any(), any())).willReturn(StringUtils.repeat(" ", 64));

        //when
        testSubject.generateConfig(intercom, errorTypes, errorDescriptionSources, FILE_NAME, execution);

        //then
        verify(configGeneratorErrorLogger).trackError(errorTypes, DISPLAY_NAME_EXCEEDED_LENGTH_LIMIT, errorDescriptionSources, execution);
    }

    @Test
    public void checkNonNumericSymbolsRemovedFromPhoneNumber() {
        IntercomGroupConfig intercomGroupConfig = new IntercomGroupConfig();
        intercomGroupConfig.setBrivoGroupId(1L);
        intercomGroupConfig.setFormatString("");
        intercomGroupConfig.setIntercomGroupContactConfigs(Collections.singletonList(new IntercomGroupContactConfig("work", 1)));

        Intercom intercom = new Intercom();
        intercom.setIntercomGroupConfigs(Collections.singletonList(intercomGroupConfig));
        intercom.setType(new Type());

        Execution execution = new Execution();
        execution.setIntercom(intercom);


        User userWithNonNumericPhoneNumber = new User();
        userWithNonNumericPhoneNumber.setPhoneNumbers(Collections.singletonList(new Telephone(PHONE_NUMBER_WITH_NON_NUMERIC_SYMBOLS, "work")));

        given(brivoRestService.getUserById(anyLong())).willReturn(userWithNonNumericPhoneNumber);
        given(brivoRestService
            .getGroupUsers(intercom.getIntercomGroupConfigs().get(0).getBrivoGroupId()))
            .willReturn(Collections.singletonList(userWithNonNumericPhoneNumber));
        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("");

        ConfigFile configFile = testSubject.generateConfig(intercom, errorTypes, new ErrorSources(), FILE_NAME, execution);

        assertEquals(PHONE_NUMBER_ONLY_NUMERIC_SYMBOLS, configFile.getUsers().get(0).getNumbers().get(0).getPhoneNumber());
    }

}
