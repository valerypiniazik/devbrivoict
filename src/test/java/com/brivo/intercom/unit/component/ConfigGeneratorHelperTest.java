package com.brivo.intercom.unit.component;

import com.brivo.intercom.component.generator.ConfigGenerator;
import com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger;
import com.brivo.intercom.component.generator.ConfigGeneratorHelper;
import com.brivo.intercom.component.generator.ConfigGeneratorHelper.UserCountGauge;
import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.entity.ConfigurationFileVersion;
import com.brivo.intercom.domain.entity.Execution;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.entity.Type;
import com.brivo.intercom.domain.model.generator.ConfigFile;
import com.brivo.intercom.repository.ErrorTypeRepository;
import com.brivo.intercom.repository.ExecutionRepository;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class ConfigGeneratorHelperTest
{
    @Mock
    private ConfigGenerator generator;

    @Mock
    private ConfigGeneratorErrorLogger configGeneratorErrorLogger;

    @Mock
    private ErrorTypeRepository errorTypeRepository;

    @Mock
    private ExecutionRepository executionRepository;

    @Mock
    private TemplateEngine templateEngine;

    @Mock
    private MetricRegistry metricRegistry;

    @Mock
    private UserCountGauge userCountGauge;

    @Mock
    private Histogram requestDurationHistogram;

    @Mock
    private Meter requestCountMeter;

    private ConfigGeneratorHelper testSubject;

    @Before
    public void setUp() {
        given(metricRegistry.register(any(), any())).willReturn(userCountGauge);
        given(metricRegistry.histogram(any())).willReturn(requestDurationHistogram);
        given(metricRegistry.meter(any())).willReturn(requestCountMeter);

        testSubject = new ConfigGeneratorHelper(generator, configGeneratorErrorLogger, errorTypeRepository, executionRepository,
            templateEngine, metricRegistry);
    }

    @Test
    public void verifyThatGenerateConfigFileResponseWorksInExpectedWay() {
        //given
        ConfigurationFileVersion version = new ConfigurationFileVersion();
        version.setDefault(true);

        Type type = new Type();
        type.setDefaultConfigurationVersion(Collections.singletonList(version));

        Intercom intercom = new Intercom();
        intercom.setType(type);

        ConfigFile configFile = new ConfigFile(null, null, Collections.emptyList());

        given(executionRepository.save(any(Execution.class))).willReturn(new Execution());
        given(errorTypeRepository.findAll()).willReturn(Collections.emptyList());
        given(generator.generateConfig(any(), any(), any(), any(), any())).willReturn(configFile);
        given(templateEngine.compileTemplate(any(), any(), any())).willReturn("<>");

        //when
        ResponseEntity<String> result = testSubject.generateConfigFileResponse("just a name", intercom, null);

        //then
        verify(requestDurationHistogram).update(anyLong());
        verify(requestCountMeter).mark();
        verify(userCountGauge).setValue(any());

        verify(configGeneratorErrorLogger).logSuccess(any());

        assertEquals(MediaType.APPLICATION_OCTET_STREAM ,result.getHeaders().getContentType());
        assertEquals("<>".getBytes().length ,result.getHeaders().getContentLength());
        assertEquals("attachment; filename=just a name", result.getHeaders().get(HttpHeaders.CONTENT_DISPOSITION).get(0));

        assertEquals("<>", result.getBody());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void verifyThatGenerateConfigFileResponseReturnsEmptyStringAndTrackErrorIfThereWasError() {
        //given
        ConfigFile configFile = new ConfigFile(null, null, Collections.emptyList());

        given(executionRepository.save(any(Execution.class))).willReturn(new Execution());
        given(errorTypeRepository.findAll()).willReturn(Collections.emptyList());
        given(generator.generateConfig(any(), any(), any(), any(), any())).willReturn(configFile);

        //when
        ResponseEntity<String> result = testSubject.generateConfigFileResponse(null, null, null);

        //then
        verify(requestDurationHistogram).update(anyLong());
        verify(requestCountMeter).mark();
        verify(userCountGauge).setValue(any());

        verify(configGeneratorErrorLogger).trackError(any(), any(), any(), any());

        assertEquals("", result.getBody());
    }
}
