package com.brivo.intercom.unit.fake;

import com.brivo.intercom.domain.model.request.CreateIntercomRequest;
import com.brivo.intercom.domain.model.request.UpdateIntercomRequest;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.service.IntercomService;

import java.util.List;
import java.util.Optional;

public class FakeIntercomServiceImpl implements IntercomService
{
    private static Intercom mockedIntercom;

    @Override
    public Intercom save(CreateIntercomRequest request, Long brivoAccountId) {
        return null;
    }

    @Override
    public List<Intercom> findAll(Long brivoAccountId) {
        return null;
    }

    @Override
    public Optional<Intercom> findById(Long id, Long brivoAccountId) {
        return Optional.ofNullable(mockedIntercom);
    }

    @Override
    public Intercom update(UpdateIntercomRequest intercomDto, Intercom intercom) {
        return null;
    }

    @Override
    public void delete(Intercom intercom) {
    }

    public static void mockIntercom(Intercom intercom) {
        mockedIntercom = intercom;
    }
}
