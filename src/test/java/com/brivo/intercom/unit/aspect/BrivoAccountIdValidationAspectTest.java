package com.brivo.intercom.unit.aspect;

import com.brivo.api.rest.model.account.Account;
import com.brivo.intercom.aspect.BrivoAccountIdValidationAspect;
import com.brivo.intercom.component.generator.ConfigGeneratorErrorLogger;
import com.brivo.intercom.component.generator.ConfigGeneratorHelper;
import com.brivo.intercom.component.template.TemplateEngine;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.domain.mapper.ExecutionMapper;
import com.brivo.intercom.domain.mapper.IntercomGroupConfigMapper;
import com.brivo.intercom.domain.mapper.IntercomMapper;
import com.brivo.intercom.domain.mapper.RequestLogMapper;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.service.HistoryService;
import com.brivo.intercom.service.IntercomGroupConfigService;
import com.brivo.intercom.service.IntercomService;
import com.brivo.intercom.service.rest.BrivoAccountRestService;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import com.brivo.intercom.web.controller.IntercomController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.Cookie;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = BrivoAccountIdValidationAspectTest.TestConfig.class)
public class BrivoAccountIdValidationAspectTest {

    @MockBean
    private BrivoAccountRestService brivoRestService;

    @MockBean
    private IntercomService intercomService;

    @MockBean
    private IntercomGroupConfigService intercomGroupConfigService;

    @MockBean
    private HistoryService historyService;

    @MockBean
    private BrivoGroupRestService brivoGroupRestService;

    @MockBean
    private IntercomMapper intercomMapper;

    @MockBean
    private RequestLogMapper requestLogMapper;

    @MockBean
    private IntercomGroupConfigMapper intercomGroupConfigMapper;

    @MockBean
    private ConfigGeneratorHelper generatorHelper;

    @MockBean
    private IntercomRepository intercomRepository;

    @MockBean
    private ConfigGeneratorErrorLogger configGeneratorErrorLogger;

    @MockBean
    private ExecutionMapper executionMapper;

    @Autowired
    private IntercomController intercomController;

    @MockBean
    private TemplateEngine templateEngine;

    @Test
    public void return403IfUserDoesNotHaveSuchBrivoAccountId() {
        //given
        Cookie brivoAccountId = new Cookie("brivoAccountId", "5");
        given(brivoRestService.getAccountById(5L)).willReturn(null);

        //when
        ResponseEntity responseEntity = intercomController.getIntercom(10L, brivoAccountId);

        //then
        assertEquals(HttpStatus.FORBIDDEN, responseEntity.getStatusCode());
    }

    @Test
    public void return200ResponseIfUserHaveSuchBrivoAccountId() {
        //given
        Cookie brivoAccountId = new Cookie("brivoAccountId", "5");
        given(brivoRestService.getAccountById(5L)).willReturn(new Account(5L));
        given(intercomService.findById(any(), any())).willReturn(Optional.of(new Intercom()));

        //when
        ResponseEntity responseEntity = intercomController.getIntercom(10L, brivoAccountId);

        //then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }

    @Configuration
    @EnableAspectJAutoProxy(proxyTargetClass = true)
    @Import({BrivoAccountIdValidationAspect.class, IntercomController.class})
    public static class TestConfig {
    }
}
