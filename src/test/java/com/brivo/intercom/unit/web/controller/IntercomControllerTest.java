package com.brivo.intercom.unit.web.controller;

import com.brivo.intercom.domain.mapper.IntercomMapper;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.service.IntercomService;
import com.brivo.intercom.web.controller.IntercomController;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.Cookie;
import java.util.Optional;

import static org.mockito.BDDMockito.given;


@RunWith(SpringRunner.class)
public class IntercomControllerTest
{
    @Mock
    private IntercomService intercomService;

    @Mock
    private IntercomMapper intercomMapper;

    @Mock
    private IntercomRepository intercomRepository;

    private IntercomController testSubject;

    @Before
    public void setUp() {
        testSubject = new IntercomController(intercomService, null, null, null, intercomMapper,
            null, null, null, intercomRepository, null, null);
    }

    @Test(expected = ResourceNotFoundException.class)
    public void verifyThatGetIntercomThrowResourceNotFoundExceptionWhenIntercomIsNotFound() {
        //given
        Long brivoAccountId = 1L;
        Long intercomId = 2L;

        given(intercomService.findById(intercomId, brivoAccountId)).willReturn(Optional.empty());

        //when
        testSubject.getIntercom(intercomId, new Cookie("brivoAccountId", brivoAccountId.toString()));

        //then
    }

    @Test(expected = ResourceNotFoundException.class)
    public void verifyThatUpdateIntercomThrowResourceNotFoundExceptionWhenIntercomIsNotFound() {
        //given
        Long brivoAccountId = 1L;
        Long intercomId = 2L;

        given(intercomService.findById(intercomId, brivoAccountId)).willReturn(Optional.empty());

        //when
        testSubject.update(intercomId, null, new Cookie("brivoAccountId", brivoAccountId.toString()));

        //then
    }

    @Test(expected = ResourceNotFoundException.class)
    public void verifyThatDeleteIntercomThrowResourceNotFoundExceptionWhenIntercomIsNotFound() {
        //given
        Long brivoAccountId = 1L;
        Long intercomId = 2L;

        given(intercomService.findById(intercomId, brivoAccountId)).willReturn(Optional.empty());

        //when
        testSubject.delete(intercomId, new Cookie("brivoAccountId", brivoAccountId.toString()));

        //then
    }

    @Test(expected = ResourceNotFoundException.class)
    public void verifyThatGetRequestLogsThrowResourceNotFoundExceptionWhenIntercomIsNotFound() {
        //given
        Long brivoAccountId = 1L;
        Long intercomId = 2L;

        given(intercomService.findById(intercomId, brivoAccountId)).willReturn(Optional.empty());

        //when
        testSubject.getRequestLogs(new PageRequest(10, 10), intercomId, new Cookie("brivoAccountId", brivoAccountId.toString()));

        //then
    }


    @Test(expected = ResourceNotFoundException.class)
    public void verifyThatDownloadConfigFileThrowResourceNotFoundExceptionWhenIntercomIsNotFound() throws JsonProcessingException {
        //given
        Long brivoAccountId = 1L;
        Long intercomId = 2L;

        given(intercomRepository.findByIdAndBrivoAccountInfoId(intercomId, brivoAccountId)).willReturn(Optional.empty());

        //when
        testSubject.downloadConfigFile(intercomId, new Cookie("brivoAccountId", brivoAccountId.toString()));

        //then
    }
}
