package com.brivo.intercom.unit.web.controller;

import com.brivo.api.rest.model.group.Group;
import com.brivo.intercom.service.rest.BrivoGroupRestService;
import com.brivo.intercom.web.controller.GroupsController;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class GroupsControllerTest
{
    @Mock
    private BrivoGroupRestService brivoGroupRestService;

    private GroupsController testSubject;

    @Before
    public void setUp() {
        testSubject = new GroupsController(brivoGroupRestService);
    }

    @Test
    public void verifyThatGetGroupsReturnsSortedGroupListByName() {
        //given
        Group groupA = new Group();
        groupA.setName("A");

        Group groupB = new Group();
        groupB.setName("B");

        Group groupC = new Group();
        groupC.setName("C");

        List<Group> expected = Arrays.asList(groupA, groupB, groupC);

        List<Group> notSortedList = Arrays.asList(groupB, groupC, groupA);

        given(brivoGroupRestService.getGroups(1L)).willReturn(notSortedList);

        //when
        List<Group> result = testSubject.getGroups(new Cookie("brivoAccountId", "1"));

        //then
        assertEquals(expected.get(0), result.get(0));
        assertEquals(expected.get(1), result.get(1));
        assertEquals(expected.get(2), result.get(2));
    }
}
