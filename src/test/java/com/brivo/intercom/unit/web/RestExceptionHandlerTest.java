package com.brivo.intercom.unit.web;

import com.brivo.intercom.web.handler.RestExceptionHandler;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Test;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;

import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class RestExceptionHandlerTest {
    private final RestExceptionHandler testSubject = new RestExceptionHandler();

    @Test
    public void shouldReturnBadRequestErrorWhenMessageContainsNullKeyWord() {
        //given
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServletPath("/some-path");

        ResponseEntity<RestExceptionHandler.RestApiError> expected =
            buildResponse(HttpStatus.BAD_REQUEST, "Bad Request", "contains", "/some-path");

        ConstraintViolationException constraintExc = new ConstraintViolationException("", new SQLException("contains; NULL"), "");
        DataIntegrityViolationException exc = new DataIntegrityViolationException("", constraintExc);

        //when
        ResponseEntity<RestExceptionHandler.RestApiError> result = testSubject.handle(exc, request);

        //then
        assertEquals(expected.getStatusCode(), result.getStatusCode());
        assertEquals(expected.getBody().getPath(), result.getBody().getPath());
        assertEquals(expected.getBody().getMessage(), result.getBody().getMessage());
    }

    @Test
    public void shouldReturnConflictErrorWhenMessageContainsDetailsAboutConstraints() {
        //given
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.setServletPath("/some-path");

        ResponseEntity<RestExceptionHandler.RestApiError> expected =
            buildResponse(HttpStatus.CONFLICT, "Conflict", "already exists", "/some-path");

        ConstraintViolationException constraintExc = new ConstraintViolationException("", new SQLException("Detail: already exists"), "");
        DataIntegrityViolationException exc = new DataIntegrityViolationException("", constraintExc);

        //when
        ResponseEntity<RestExceptionHandler.RestApiError> result = testSubject.handle(exc, request);

        //then
        assertEquals(expected.getStatusCode(), result.getStatusCode());
        assertEquals(expected.getBody().getPath(), result.getBody().getPath());
        assertEquals(expected.getBody().getMessage(), result.getBody().getMessage());
    }

    private ResponseEntity<RestExceptionHandler.RestApiError> buildResponse(HttpStatus status, String error, String message, String path) {
        return ResponseEntity
            .status(status)
            .body(new RestExceptionHandler.RestApiError()
                .setStatus(status.value())
                .setError(error)
                .setException(DataIntegrityViolationException.class.getName())
                .setMessage(message)
                .setPath(path));
    }
}
