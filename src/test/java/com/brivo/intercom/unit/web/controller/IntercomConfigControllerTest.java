package com.brivo.intercom.unit.web.controller;

import com.brivo.intercom.component.generator.ConfigGeneratorHelper;
import com.brivo.intercom.domain.entity.Intercom;
import com.brivo.intercom.repository.IntercomRepository;
import com.brivo.intercom.web.controller.IntercomConfigController;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
public class IntercomConfigControllerTest
{
    @Mock
    private ConfigGeneratorHelper generatorHelper;

    @Mock
    private IntercomRepository intercomRepository;

    private IntercomConfigController testSubject;

    @Before
    public void setUp() {
        testSubject = new IntercomConfigController(generatorHelper, intercomRepository);
    }

    @Test
    public void verifyThatDownloadConfigFileWorksInExpectedWay() throws JsonProcessingException {
        //given
        String macAddress = "MA-CA-DD-RE-SS-00";

        given(intercomRepository.findOneByMacAddress(macAddress)).willReturn(Optional.of(new Intercom()));

        //when
        testSubject.downloadConfigFile(null, () -> macAddress);

        //then
        verify(generatorHelper).generateConfigFileResponse(any(), any(), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void verifyThatDownloadConfigFileReturns500WhenFileGenerationFailed() throws JsonProcessingException {
        //given
        String macAddress = "MA-CA-DD-RE-SS-00";

        given(intercomRepository.findOneByMacAddress(macAddress)).willReturn(Optional.of(new Intercom()));
        given(generatorHelper.generateConfigFileResponse(any(), any(), any())).willThrow(Exception.class);

        //when
        ResponseEntity<String> result = testSubject.downloadConfigFile(null, () -> macAddress);

        //then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, result.getStatusCode());
    }

    @Test
    public void verifyThatDownloadConfigFileReturns500WhenIntercomWithSuchMacAddressIsNotFound() {
        //given
        String macAddress = "MA-CA-DD-RE-SS-00";

        given(intercomRepository.findOneByMacAddress(macAddress)).willReturn(Optional.empty());

        //when
        testSubject.downloadConfigFile(null, () -> macAddress);

        //then
    }
}
