# [Brivo Project](https://bitbucket.org/brivoinc/brivo-project/overview) codebase

## Configure and Deploy Intercom Configuration Tool
### Requrements
Build and deploy requires:
```
Java runtime environment (jre) 1.8.*
Gradle 4.*
Node from 4.* to 9.* version
npm 5.*
```
### 1. Define database
Define project database

### 2. Configuring Properties
#### Application properties
Sample application config file can be found [here](https://bitbucket.org/brivoinc/app-configs/src/abelyaev/intercom-service.properties).
Edit application.properties and update the following properties for your developer setup:
```
spring.datasource.url=
spring.datasource.username=
spring.datasource.password=
```
#### Flyway properties
Rename flyway.properties.template file from repository to flyway.properties.
Update following properties according to your database setup:
```
flyway.url=
flyway.user=
flyway.password=
flyway.schemas=
```

### 3. Build
In order to build project, run
```
gradle build -Dflyway.configFiles=etc/brivo/intercom-service/flyway.properties
```
with path to local flyway.properties file as ```-Dflyway.configFiles``` argument value.
Flyway migration scripts are executed at build time

If build failes, it might be necessary to run
```
npm install
```
and try to build project again.
### 4. Start the application
In order to start application, run
```
gradle bootRun 
```
Run build once before first start of application in order to initialize database. 