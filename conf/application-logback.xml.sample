<?xml version="1.0" encoding="UTF-8"?>
<configuration scan="true">
    <!--
        Logback has the following hierarchy:
        TRACE < DEBUG < INFO <  WARN < ERROR
    -->


    <!--
        Setup our STDOUT logging, we only want to log INFO levels here,
        and only log Trace events from Spring as it is starting up prior to log registering
    -->
    <appender name="STDOUT" class="ch.qos.logback.core.ConsoleAppender">
        <filter class="ch.qos.logback.classic.filter.ThresholdFilter">
            <level>INFO</level>
        </filter>
        <!--
            We do want to know if Tomcat was able to start, and bind on a port
        -->
        <filter class="com.brivo.intercom.ThresholdLoggerFilter">
            <logger>org.springframework.boot.context.embedded</logger>
            <level>INFO</level>
        </filter>
        <encoder>
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
    </appender>

    <!--
        Setup our file appender operations.log file, and setup log rotation policy
    -->
    <appender name="FILE"
              class="ch.qos.logback.core.rolling.RollingFileAppender">
        <file>/var/log/brivo/intercom/operations.log</file>
        <encoder class="ch.qos.logback.classic.encoder.PatternLayoutEncoder">
            <pattern>%d{yyyy-MM-dd HH:mm:ss.SSS} [%thread] %-5level %logger{36} - %msg%n</pattern>
        </encoder>
        <rollingPolicy class="ch.qos.logback.core.rolling.FixedWindowRollingPolicy">
            <fileNamePattern>/var/log/brivo/brivo-project/operations.log.%i</fileNamePattern>
        </rollingPolicy>
        <triggeringPolicy
                class="ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy">
            <MaxFileSize>50MB</MaxFileSize>
        </triggeringPolicy>
    </appender>

    <!--
        Good to know Springboot status in our logs
    -->
    <logger name="org.springframework.boot.context.embedded">
        <appender-ref ref="STDOUT" />
    </logger>

    <!--
      We want to log INFO for our com.brivo code
    -->
    <logger name="com.brivo" level="INFO" additivity="false">
        <appender-ref ref="FILE" />
    </logger>

    <!--
        Lets log all to a file
    -->
    <root level="ERROR">
        <appender-ref ref="FILE" />
    </root>
</configuration>